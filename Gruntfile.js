'use strict';

var fs = require('fs'),
    path = require('path'),
    helpers = require('@ambers/sdk').helpers;

function findAmberPath(options) {
    var result;
    options.some(function (x) {
        var candidate = path.join(__dirname, x);
        return (
            fs.existsSync(path.join(candidate, 'support/boot.js')) ||
            fs.existsSync(path.join(candidate, 'base/boot.js'))
        ) && (result = candidate);
    });
    return result;
}

module.exports = function (grunt) {
    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('@ambers/sdk');

    // Default task.
    grunt.registerTask('default', ['amdconfig:app', 'less', 'amberc:all']);
    grunt.registerTask('test', ['amdconfig:app', 'requirejs:test_runner', 'exec:test_runner', 'clean:test_runner']);
    grunt.registerTask('devel', ['amdconfig:app']);

    var polyfillThenPromiseApp = function () {
        define(["require", "amber/es6-promise"], function (require, promiseLib) {
            promiseLib.polyfill();
            return new Promise(function (resolve, reject) {
                require(["__app__"], resolve, reject);
            });
        });
    };

    // Project configuration.
    grunt.initConfig({
        // Metadata.
        // pkg: grunt.file.readJSON(''),
        banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
        '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
        '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
        '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
        ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
        // task configuration
        less: {
            development: {
                files: {
                    'resources/helios.css': 'resources/helios.less'
                }
            }
        },

        amberc: {
            options: {
                amber_dir: findAmberPath(['../amber/lang', 'node_modules/@ambers/lang']),
                configFile: "config.js"
            },
            all: {
                src: [
                    // list all sources in dependency order
                    'src/Helios-Core.st', 'src/Helios-Exceptions.st', 'src/Helios-Announcements.st',
                    'src/Helios-KeyBindings.st', 'src/Helios-Layout.st', 'src/Helios-Helpers.st',
                    'src/Helios-Commands-Core.st',
                    'src/Helios-Commands-Tools.st', 'src/Helios-Commands-Browser.st', 'src/Helios-Commands-SUnit.st',
                    'src/Helios-References.st', 'src/Helios-Inspector.st', 'src/Helios-Browser.st',
                    'src/Helios-Transcript.st', 'src/Helios-Workspace.st', 'src/Helios-Debugger.st',
                    'src/Helios-SUnit.st',
                    // list all tests in dependency order
                    'src/Helios-Browser-Tests.st', 'src/Helios-Workspace-Tests.st', 'src/Helios-SUnit-Tests.st'
                ],
                libraries: ['amber/core/SUnit', 'amber/web/Web'],
                amd_namespace: 'helios',
                jsGlobals: ['navigator']
            }
        },

        amdconfig: {app: {dest: 'config.js'}},

        requirejs: {
            options: {
                useStrict: true
            },
            test_runner: {
                options: {
                    mainConfigFile: "config.js",
                    rawText: {
                        "jquery": "/* do not load in node test runner */",
                        "__app__": "(" + function () {
                            define(["amber/deploy", "helios/all", "amber/core/Platform-Node", "amber_devkit/NodeTestRunner"], function (amber) {
                                amber.initialize().then(function () {
                                    amber.globals.NodeTestRunner._main();
                                });
                            });
                        } + "());",
                        "app": "(" + polyfillThenPromiseApp + "());"
                    },
                    paths: {"amber_devkit": helpers.libPath},
                    pragmas: {
                        excludeIdeData: true
                    },
                    include: ['app'],
                    findNestedDependencies: true,
                    insertRequire: ['app'],
                    optimize: "none",
                    wrap: helpers.nodeWrapperWithShebang,
                    out: "test_runner.js"
                }
            }
        },

        exec: {
            test_runner: 'node test_runner.js'
        },

        clean: {
            test_runner: ['test_runner.js']
        },

        watch: {
            less: {
                files: ['resources/*.less'],
                tasks: ['less'],
                options: {
                    spawn: false
                }
            }
        }
    });

};
