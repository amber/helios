define(["amber/boot", "require", "amber/core/Kernel-Objects"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-Announcements");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLAboutToChange", $globals.Object, "Helios-Announcements");
$core.setSlots($globals.HLAboutToChange, ["actionBlock"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLAboutToChange.comment="I am announced whenever a change of context is about to be made, and unsaved changes could be lost.\x0a\x0aI am used within `HLModel` to handle such user actions. See `HLModel >> withChangesDo:`.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "actionBlock",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "actionBlock\x0a\x09^ actionBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.actionBlock;

}; }),
$globals.HLAboutToChange);

$core.addMethod(
$core.method({
selector: "actionBlock:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBlock"],
source: "actionBlock: aBlock\x0a\x09actionBlock := aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aBlock){
var self=this,$self=this;
$self.actionBlock=aBlock;
return self;

}; }),
$globals.HLAboutToChange);



$core.addClass("HLAnnouncement", $globals.Object, "Helios-Announcements");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLAnnouncement.comment="I am the root of the announcement class hierarchy used in the Helios UI.";
//>>excludeEnd("ide");

$core.addMethod(
$core.method({
selector: "classTag",
protocol: "helios",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "classTag\x0a\x09^ 'announcement'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "announcement";

}; }),
$globals.HLAnnouncement.a$cls);


$core.addClass("HLCodeHandled", $globals.HLAnnouncement, "Helios-Announcements");
$core.setSlots($globals.HLCodeHandled, ["code"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLCodeHandled.comment="I am the root class of announcements emitted by `HLCodeWidget`s";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "code",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "code\x0a\x0a\x09^ code",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.code;

}; }),
$globals.HLCodeHandled);

$core.addMethod(
$core.method({
selector: "code:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aModel"],
source: "code: aModel\x0a\x0a\x09code := aModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aModel){
var self=this,$self=this;
$self.code=aModel;
return self;

}; }),
$globals.HLCodeHandled);


$core.addMethod(
$core.method({
selector: "on:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCodeModel"],
source: "on: aCodeModel\x0a\x0a\x09^ self new \x0a    \x09code: aCodeModel;\x0a        yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["code:", "new", "yourself"]
}, function ($methodClass){ return function (aCodeModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._code_(aCodeModel);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{aCodeModel:aCodeModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeHandled.a$cls);


$core.addClass("HLDoItExecuted", $globals.HLCodeHandled, "Helios-Announcements");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLDoItExecuted.comment="I am emitted by a `HLCodeWidget` after a DoIt has been executed.";
//>>excludeEnd("ide");


$core.addClass("HLDebuggerAnnouncement", $globals.HLAnnouncement, "Helios-Announcements");
$core.setSlots($globals.HLDebuggerAnnouncement, ["context"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLDebuggerAnnouncement.comment="I am the root class of debugger announcements, and hold onto the debugged `context`.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "context",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "context\x0a\x09^ context",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.context;

}; }),
$globals.HLDebuggerAnnouncement);

$core.addMethod(
$core.method({
selector: "context:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aContext"],
source: "context: aContext\x0a\x09context := aContext",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aContext){
var self=this,$self=this;
$self.context=aContext;
return self;

}; }),
$globals.HLDebuggerAnnouncement);



$core.addClass("HLDebuggerContextSelected", $globals.HLDebuggerAnnouncement, "Helios-Announcements");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLDebuggerContextSelected.comment="I am announced when a new context is selected in a debugger, to update the user interface.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "context",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "context\x0a\x09^ context",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.context;

}; }),
$globals.HLDebuggerContextSelected);

$core.addMethod(
$core.method({
selector: "context:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aContext"],
source: "context: aContext\x0a\x09context := aContext",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aContext){
var self=this,$self=this;
$self.context=aContext;
return self;

}; }),
$globals.HLDebuggerContextSelected);



$core.addClass("HLDebuggerProceeded", $globals.HLDebuggerAnnouncement, "Helios-Announcements");


$core.addClass("HLDebuggerStepped", $globals.HLDebuggerAnnouncement, "Helios-Announcements");


$core.addClass("HLDebuggerWhere", $globals.HLDebuggerAnnouncement, "Helios-Announcements");


$core.addClass("HLDiveRequested", $globals.HLAnnouncement, "Helios-Announcements");


$core.addClass("HLEditComment", $globals.HLAnnouncement, "Helios-Announcements");


$core.addClass("HLErrorRaised", $globals.HLAnnouncement, "Helios-Announcements");
$core.setSlots($globals.HLErrorRaised, ["error"]);
$core.addMethod(
$core.method({
selector: "error",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "error\x0a\x09^ error",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.error;

}; }),
$globals.HLErrorRaised);

$core.addMethod(
$core.method({
selector: "error:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anError"],
source: "error: anError\x0a\x09error := anError",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anError){
var self=this,$self=this;
$self.error=anError;
return self;

}; }),
$globals.HLErrorRaised);



$core.addClass("HLCompileErrorRaised", $globals.HLErrorRaised, "Helios-Announcements");


$core.addClass("HLParseErrorRaised", $globals.HLErrorRaised, "Helios-Announcements");
$core.setSlots($globals.HLParseErrorRaised, ["line", "column", "message"]);
$core.addMethod(
$core.method({
selector: "column",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "column\x0a\x09^ column",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.column;

}; }),
$globals.HLParseErrorRaised);

$core.addMethod(
$core.method({
selector: "column:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInteger"],
source: "column: anInteger\x0a\x09column := anInteger",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anInteger){
var self=this,$self=this;
$self.column=anInteger;
return self;

}; }),
$globals.HLParseErrorRaised);

$core.addMethod(
$core.method({
selector: "line",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "line\x0a\x09^ line",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.line;

}; }),
$globals.HLParseErrorRaised);

$core.addMethod(
$core.method({
selector: "line:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInteger"],
source: "line: anInteger\x0a\x09line := anInteger",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anInteger){
var self=this,$self=this;
$self.line=anInteger;
return self;

}; }),
$globals.HLParseErrorRaised);

$core.addMethod(
$core.method({
selector: "message",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "message\x0a\x09^ message",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.message;

}; }),
$globals.HLParseErrorRaised);

$core.addMethod(
$core.method({
selector: "message:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "message: aString\x0a\x09message := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.message=aString;
return self;

}; }),
$globals.HLParseErrorRaised);



$core.addClass("HLUnknownVariableErrorRaised", $globals.HLErrorRaised, "Helios-Announcements");


$core.addClass("HLFocusRequested", $globals.HLAnnouncement, "Helios-Announcements");


$core.addClass("HLClassesFocusRequested", $globals.HLFocusRequested, "Helios-Announcements");


$core.addClass("HLDocumentationFocusRequested", $globals.HLFocusRequested, "Helios-Announcements");


$core.addClass("HLMethodsFocusRequested", $globals.HLFocusRequested, "Helios-Announcements");


$core.addClass("HLPackagesFocusRequested", $globals.HLFocusRequested, "Helios-Announcements");


$core.addClass("HLProtocolsFocusRequested", $globals.HLFocusRequested, "Helios-Announcements");


$core.addClass("HLSourceCodeFocusRequested", $globals.HLFocusRequested, "Helios-Announcements");


$core.addClass("HLInstVarAdded", $globals.HLAnnouncement, "Helios-Announcements");
$core.setSlots($globals.HLInstVarAdded, ["theClass", "variableName"]);
$core.addMethod(
$core.method({
selector: "theClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "theClass\x0a\x09^ theClass",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.theClass;

}; }),
$globals.HLInstVarAdded);

$core.addMethod(
$core.method({
selector: "theClass:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass"],
source: "theClass: aClass\x0a\x09theClass := aClass",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aClass){
var self=this,$self=this;
$self.theClass=aClass;
return self;

}; }),
$globals.HLInstVarAdded);

$core.addMethod(
$core.method({
selector: "variableName",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "variableName\x0a\x09^ variableName",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.variableName;

}; }),
$globals.HLInstVarAdded);

$core.addMethod(
$core.method({
selector: "variableName:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "variableName: aString\x0a\x09variableName := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.variableName=aString;
return self;

}; }),
$globals.HLInstVarAdded);



$core.addClass("HLItemSelected", $globals.HLAnnouncement, "Helios-Announcements");
$core.setSlots($globals.HLItemSelected, ["item", "soft"]);
$core.addMethod(
$core.method({
selector: "beSoft",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "beSoft\x0a\x09soft := true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
$self.soft=true;
return self;

}; }),
$globals.HLItemSelected);

$core.addMethod(
$core.method({
selector: "initialize",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initialize\x0a\x09super initialize.\x0a\x09item := nil.\x0a\x09soft := false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initialize"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._initialize.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$self.item=nil;
$self.soft=false;
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initialize",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLItemSelected);

$core.addMethod(
$core.method({
selector: "isSoft",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isSoft\x0a\x09^ soft",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.soft;

}; }),
$globals.HLItemSelected);

$core.addMethod(
$core.method({
selector: "item",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "item\x0a\x09^ item",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.item;

}; }),
$globals.HLItemSelected);

$core.addMethod(
$core.method({
selector: "item:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "item: anObject\x0a\x09item := anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
$self.item=anObject;
return self;

}; }),
$globals.HLItemSelected);


$core.addMethod(
$core.method({
selector: "on:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anItem"],
source: "on: anItem\x0a\x09^ self new\x0a    \x09item: anItem;\x0a        yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["item:", "new", "yourself"]
}, function ($methodClass){ return function (anItem){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._item_(anItem);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{anItem:anItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLItemSelected.a$cls);

$core.addMethod(
$core.method({
selector: "softOn:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anItem"],
source: "softOn: anItem\x0a\x09^ self new\x0a    \x09item: anItem;\x0a\x09\x09beSoft;\x0a        yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["item:", "new", "beSoft", "yourself"]
}, function ($methodClass){ return function (anItem){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._item_(anItem);
$recv($1)._beSoft();
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"softOn:",{anItem:anItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLItemSelected.a$cls);


$core.addClass("HLClassSelected", $globals.HLItemSelected, "Helios-Announcements");


$core.addClass("HLInstanceVariableSelected", $globals.HLItemSelected, "Helios-Announcements");


$core.addClass("HLMethodSelected", $globals.HLItemSelected, "Helios-Announcements");


$core.addClass("HLPackageSelected", $globals.HLItemSelected, "Helios-Announcements");


$core.addClass("HLProtocolSelected", $globals.HLItemSelected, "Helios-Announcements");


$core.addClass("HLItemUnselected", $globals.HLAnnouncement, "Helios-Announcements");
$core.setSlots($globals.HLItemUnselected, ["item"]);
$core.addMethod(
$core.method({
selector: "item",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "item\x0a\x09^ item",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.item;

}; }),
$globals.HLItemUnselected);

$core.addMethod(
$core.method({
selector: "item:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "item: anObject\x0a\x09item := anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
$self.item=anObject;
return self;

}; }),
$globals.HLItemUnselected);


$core.addMethod(
$core.method({
selector: "on:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anItem"],
source: "on: anItem\x0a\x09^ self new\x0a    \x09item: anItem;\x0a        yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["item:", "new", "yourself"]
}, function ($methodClass){ return function (anItem){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._item_(anItem);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{anItem:anItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLItemUnselected.a$cls);


$core.addClass("HLClassUnselected", $globals.HLItemUnselected, "Helios-Announcements");


$core.addClass("HLPackageUnselected", $globals.HLItemUnselected, "Helios-Announcements");


$core.addClass("HLRunTests", $globals.HLAnnouncement, "Helios-Announcements");
$core.setSlots($globals.HLRunTests, ["testSuiteRunner"]);
$core.addMethod(
$core.method({
selector: "testSuiteRunner",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testSuiteRunner\x0a\x09^ testSuiteRunner",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.testSuiteRunner;

}; }),
$globals.HLRunTests);

$core.addMethod(
$core.method({
selector: "testSuiteRunner:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "testSuiteRunner: anObject\x0a\x09testSuiteRunner := anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
$self.testSuiteRunner=anObject;
return self;

}; }),
$globals.HLRunTests);


$core.addMethod(
$core.method({
selector: "on:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTestSuiteRunner"],
source: "on: aTestSuiteRunner\x0a\x09^self new\x0a\x09\x09testSuiteRunner: aTestSuiteRunner;\x0a\x09\x09yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["testSuiteRunner:", "new", "yourself"]
}, function ($methodClass){ return function (aTestSuiteRunner){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._testSuiteRunner_(aTestSuiteRunner);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{aTestSuiteRunner:aTestSuiteRunner})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRunTests.a$cls);


$core.addClass("HLSaveSourceCode", $globals.HLAnnouncement, "Helios-Announcements");


$core.addClass("HLSearchReferences", $globals.HLAnnouncement, "Helios-Announcements");
$core.setSlots($globals.HLSearchReferences, ["searchString"]);
$core.addMethod(
$core.method({
selector: "searchString",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "searchString\x0a\x09^ searchString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.searchString;

}; }),
$globals.HLSearchReferences);

$core.addMethod(
$core.method({
selector: "searchString:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "searchString: aString\x0a\x09searchString := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.searchString=aString;
return self;

}; }),
$globals.HLSearchReferences);



$core.addClass("HLShowCommentToggled", $globals.HLAnnouncement, "Helios-Announcements");


$core.addClass("HLShowInstanceToggled", $globals.HLAnnouncement, "Helios-Announcements");


$core.addClass("HLShowTemplate", $globals.HLAnnouncement, "Helios-Announcements");
$core.setSlots($globals.HLShowTemplate, ["template"]);
$core.addMethod(
$core.method({
selector: "template",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "template\x0a\x09^ template",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.template;

}; }),
$globals.HLShowTemplate);

$core.addMethod(
$core.method({
selector: "template:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "template: aString\x0a\x09template := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.template=aString;
return self;

}; }),
$globals.HLShowTemplate);



$core.addClass("HLSourceCodeSaved", $globals.HLAnnouncement, "Helios-Announcements");


$core.addClass("HLTabLabelChanged", $globals.HLAnnouncement, "Helios-Announcements");
$core.setSlots($globals.HLTabLabelChanged, ["label", "widget"]);
$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ label",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.label;

}; }),
$globals.HLTabLabelChanged);

$core.addMethod(
$core.method({
selector: "label:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "label: aString\x0a\x09label := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.label=aString;
return self;

}; }),
$globals.HLTabLabelChanged);

$core.addMethod(
$core.method({
selector: "widget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "widget\x0a\x09^ widget",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.widget;

}; }),
$globals.HLTabLabelChanged);

$core.addMethod(
$core.method({
selector: "widget:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aWidget"],
source: "widget: aWidget\x0a\x09widget := aWidget",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aWidget){
var self=this,$self=this;
$self.widget=aWidget;
return self;

}; }),
$globals.HLTabLabelChanged);


});
