define(["amber/boot", "require", "amber/core/SUnit"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-SUnit-Tests");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLSUnitModelTest", $globals.TestCase, "Helios-SUnit-Tests");
$core.setSlots($globals.HLSUnitModelTest, ["model"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnitModelTest.comment="Test cases for the functionality of  `HLSUnitModel`";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "setUp",
protocol: "initializing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setUp\x0a\x09super setUp.\x0a\x09model := HLSUnitModel new",
referencedClasses: ["HLSUnitModel"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["setUp", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._setUp.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$self.model=$recv($globals.HLSUnitModel)._new();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setUp",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModelTest);

$core.addMethod(
$core.method({
selector: "testClassBecomesAvailable",
protocol: "tests",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testClassBecomesAvailable\x0a\x09self assert: model testClasses isEmpty.\x0a\x09model selectPackage: self thisPackage.\x0a\x09self assert: (model testClasses includes: self class).",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["assert:", "isEmpty", "testClasses", "selectPackage:", "thisPackage", "includes:", "class"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$self._assert_($recv([$recv($self.model)._testClasses()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["testClasses"]=1
//>>excludeEnd("ctx");
][0])._isEmpty())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["assert:"]=1
//>>excludeEnd("ctx");
][0];
$recv($self.model)._selectPackage_($self._thisPackage());
$self._assert_($recv($recv($self.model)._testClasses())._includes_($self._class()));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testClassBecomesAvailable",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModelTest);

$core.addMethod(
$core.method({
selector: "testEmptyTestResults",
protocol: "tests",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testEmptyTestResults\x0a\x09self assert: (model testResult isKindOf: TestResult)",
referencedClasses: ["TestResult"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["assert:", "isKindOf:", "testResult"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._assert_($recv($recv($self.model)._testResult())._isKindOf_($globals.TestResult));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testEmptyTestResults",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModelTest);

$core.addMethod(
$core.method({
selector: "testInvertSelectedClasses",
protocol: "tests",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testInvertSelectedClasses\x0a\x09model selectAllPackages.\x0a\x09model selectAllClasses.\x0a\x09model unselectClass: self class.\x0a\x09self assert: model selectedClasses notEmpty.\x0a\x09self assert: model selectedClasses size equals: model testClasses size - 1.\x0a\x09model invertSelectedClasses.\x0a\x09self assert: model selectedClasses size equals: 1.\x0a\x09self assert: model selectedClasses anyOne equals: self class.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectAllPackages", "selectAllClasses", "unselectClass:", "class", "assert:", "notEmpty", "selectedClasses", "assert:equals:", "size", "-", "testClasses", "invertSelectedClasses", "anyOne"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.model)._selectAllPackages();
$recv($self.model)._selectAllClasses();
$recv($self.model)._unselectClass_([$self._class()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class"]=1
//>>excludeEnd("ctx");
][0]);
$self._assert_($recv([$recv($self.model)._selectedClasses()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedClasses"]=1
//>>excludeEnd("ctx");
][0])._notEmpty());
[$self._assert_equals_([$recv([$recv($self.model)._selectedClasses()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedClasses"]=2
//>>excludeEnd("ctx");
][0])._size()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["size"]=1
//>>excludeEnd("ctx");
][0],$recv([$recv($recv($self.model)._testClasses())._size()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["size"]=2
//>>excludeEnd("ctx");
][0]).__minus((1)))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["assert:equals:"]=1
//>>excludeEnd("ctx");
][0];
$recv($self.model)._invertSelectedClasses();
[$self._assert_equals_($recv([$recv($self.model)._selectedClasses()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedClasses"]=3
//>>excludeEnd("ctx");
][0])._size(),(1))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["assert:equals:"]=2
//>>excludeEnd("ctx");
][0];
$self._assert_equals_($recv($recv($self.model)._selectedClasses())._anyOne(),$self._class());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testInvertSelectedClasses",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModelTest);

$core.addMethod(
$core.method({
selector: "testInvertSelectedPackages",
protocol: "tests",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testInvertSelectedPackages\x0a\x09model selectAllPackages.\x0a\x09model unselectPackage: self thisPackage.\x0a\x09self assert: model selectedPackages notEmpty.\x0a\x09self assert: model selectedPackages size equals: model testPackages size - 1.\x0a\x09model invertSelectedPackages.\x0a\x09self assert: model selectedPackages size equals: 1.\x0a\x09self assert: model selectedPackages anyOne equals: self thisPackage.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectAllPackages", "unselectPackage:", "thisPackage", "assert:", "notEmpty", "selectedPackages", "assert:equals:", "size", "-", "testPackages", "invertSelectedPackages", "anyOne"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.model)._selectAllPackages();
$recv($self.model)._unselectPackage_([$self._thisPackage()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["thisPackage"]=1
//>>excludeEnd("ctx");
][0]);
$self._assert_($recv([$recv($self.model)._selectedPackages()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedPackages"]=1
//>>excludeEnd("ctx");
][0])._notEmpty());
[$self._assert_equals_([$recv([$recv($self.model)._selectedPackages()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedPackages"]=2
//>>excludeEnd("ctx");
][0])._size()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["size"]=1
//>>excludeEnd("ctx");
][0],$recv([$recv($recv($self.model)._testPackages())._size()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["size"]=2
//>>excludeEnd("ctx");
][0]).__minus((1)))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["assert:equals:"]=1
//>>excludeEnd("ctx");
][0];
$recv($self.model)._invertSelectedPackages();
[$self._assert_equals_($recv([$recv($self.model)._selectedPackages()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedPackages"]=3
//>>excludeEnd("ctx");
][0])._size(),(1))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["assert:equals:"]=2
//>>excludeEnd("ctx");
][0];
$self._assert_equals_($recv($recv($self.model)._selectedPackages())._anyOne(),$self._thisPackage());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testInvertSelectedPackages",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModelTest);

$core.addMethod(
$core.method({
selector: "testSelectAllClasses",
protocol: "tests",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testSelectAllClasses\x0a\x09model selectAllPackages.\x0a\x09self assert: model testClasses notEmpty.\x0a\x09model selectAllClasses.\x0a\x09self assert: model selectedClasses size equals: model testClasses size",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectAllPackages", "assert:", "notEmpty", "testClasses", "selectAllClasses", "assert:equals:", "size", "selectedClasses"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.model)._selectAllPackages();
$self._assert_($recv([$recv($self.model)._testClasses()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["testClasses"]=1
//>>excludeEnd("ctx");
][0])._notEmpty());
$recv($self.model)._selectAllClasses();
$self._assert_equals_([$recv($recv($self.model)._selectedClasses())._size()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["size"]=1
//>>excludeEnd("ctx");
][0],$recv($recv($self.model)._testClasses())._size());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testSelectAllClasses",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModelTest);

$core.addMethod(
$core.method({
selector: "testSelectAllPackages",
protocol: "tests",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testSelectAllPackages\x0a\x09self assert: model selectedPackages isEmpty.\x0a\x09model selectAllPackages.\x0a\x09self assert: model selectedPackages size equals: model testPackages size",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["assert:", "isEmpty", "selectedPackages", "selectAllPackages", "assert:equals:", "size", "testPackages"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._assert_($recv([$recv($self.model)._selectedPackages()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedPackages"]=1
//>>excludeEnd("ctx");
][0])._isEmpty());
$recv($self.model)._selectAllPackages();
$self._assert_equals_([$recv($recv($self.model)._selectedPackages())._size()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["size"]=1
//>>excludeEnd("ctx");
][0],$recv($recv($self.model)._testPackages())._size());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testSelectAllPackages",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModelTest);

$core.addMethod(
$core.method({
selector: "testSelectClass",
protocol: "tests",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testSelectClass\x0a\x09| announcementFired |\x0a\x09model selectPackage: self thisPackage.\x0a\x09self assert: model selectedClasses isEmpty.\x0a\x09model announcer on: HLClassSelected\x0a\x09\x09do: [ announcementFired := true ]\x0a\x09\x09for: self.\x0a\x09model selectClass: self class.\x0a\x09self assert: model selectedClasses anyOne equals: self class.\x0a\x09self assert: announcementFired.",
referencedClasses: ["HLClassSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectPackage:", "thisPackage", "assert:", "isEmpty", "selectedClasses", "on:do:for:", "announcer", "selectClass:", "class", "assert:equals:", "anyOne"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var announcementFired;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.model)._selectPackage_($self._thisPackage());
[$self._assert_($recv([$recv($self.model)._selectedClasses()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedClasses"]=1
//>>excludeEnd("ctx");
][0])._isEmpty())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["assert:"]=1
//>>excludeEnd("ctx");
][0];
$recv($recv($self.model)._announcer())._on_do_for_($globals.HLClassSelected,(function(){
announcementFired=true;
return announcementFired;

}),self);
$recv($self.model)._selectClass_([$self._class()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class"]=1
//>>excludeEnd("ctx");
][0]);
$self._assert_equals_($recv($recv($self.model)._selectedClasses())._anyOne(),$self._class());
$self._assert_(announcementFired);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testSelectClass",{announcementFired:announcementFired})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModelTest);

$core.addMethod(
$core.method({
selector: "testSelectPackage",
protocol: "tests",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testSelectPackage\x0a\x09| announcementFired |\x0a\x09self assert: model selectedPackages isEmpty.\x0a\x09model announcer on: HLPackageSelected\x0a\x09\x09do: [ announcementFired := true ]\x0a\x09\x09for: self.\x0a\x09model selectPackage: self thisPackage.\x0a\x09self assert: model selectedPackages anyOne equals: self thisPackage.\x0a\x09self assert: announcementFired",
referencedClasses: ["HLPackageSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["assert:", "isEmpty", "selectedPackages", "on:do:for:", "announcer", "selectPackage:", "thisPackage", "assert:equals:", "anyOne"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var announcementFired;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$self._assert_($recv([$recv($self.model)._selectedPackages()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedPackages"]=1
//>>excludeEnd("ctx");
][0])._isEmpty())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["assert:"]=1
//>>excludeEnd("ctx");
][0];
$recv($recv($self.model)._announcer())._on_do_for_($globals.HLPackageSelected,(function(){
announcementFired=true;
return announcementFired;

}),self);
$recv($self.model)._selectPackage_([$self._thisPackage()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["thisPackage"]=1
//>>excludeEnd("ctx");
][0]);
$self._assert_equals_($recv($recv($self.model)._selectedPackages())._anyOne(),$self._thisPackage());
$self._assert_(announcementFired);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testSelectPackage",{announcementFired:announcementFired})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModelTest);

$core.addMethod(
$core.method({
selector: "testSelectedClassNotListedIfPackageUnselected",
protocol: "tests",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testSelectedClassNotListedIfPackageUnselected\x0a\x09model selectPackage: self thisPackage.\x0a\x09model selectClass: self class.\x0a\x09self assert: model selectedClasses anyOne equals: self class.\x0a\x09model unselectPackage: self thisPackage.\x0a\x09self assert: model selectedClasses isEmpty.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectPackage:", "thisPackage", "selectClass:", "class", "assert:equals:", "anyOne", "selectedClasses", "unselectPackage:", "assert:", "isEmpty"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.model)._selectPackage_([$self._thisPackage()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["thisPackage"]=1
//>>excludeEnd("ctx");
][0]);
$recv($self.model)._selectClass_([$self._class()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class"]=1
//>>excludeEnd("ctx");
][0]);
$self._assert_equals_($recv([$recv($self.model)._selectedClasses()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedClasses"]=1
//>>excludeEnd("ctx");
][0])._anyOne(),$self._class());
$recv($self.model)._unselectPackage_($self._thisPackage());
$self._assert_($recv($recv($self.model)._selectedClasses())._isEmpty());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testSelectedClassNotListedIfPackageUnselected",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModelTest);

$core.addMethod(
$core.method({
selector: "testTestClassHasOnlyTestClasses",
protocol: "tests",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testTestClassHasOnlyTestClasses\x0a\x09| notATestClass |\x0a\x09notATestClass := Object subclass: #HLNotATestClass\x0a\x09\x09instanceVariableNames: ''\x0a\x09\x09package: self class category.\x0a\x09model selectPackage: self thisPackage.\x0a\x09self deny: (model testClasses includes: notATestClass).\x0a\x09Smalltalk removeClass: notATestClass.",
referencedClasses: ["Object", "Smalltalk"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["subclass:instanceVariableNames:package:", "category", "class", "selectPackage:", "thisPackage", "deny:", "includes:", "testClasses", "removeClass:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var notATestClass;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
notATestClass=$recv($globals.Object)._subclass_instanceVariableNames_package_("HLNotATestClass","",$recv($self._class())._category());
$recv($self.model)._selectPackage_($self._thisPackage());
$self._deny_($recv($recv($self.model)._testClasses())._includes_(notATestClass));
$recv($globals.Smalltalk)._removeClass_(notATestClass);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testTestClassHasOnlyTestClasses",{notATestClass:notATestClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModelTest);

$core.addMethod(
$core.method({
selector: "testTestPackages",
protocol: "tests",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testTestPackages\x0a\x09self assert: model testPackages notEmpty.\x0a\x09self assert: (model testPackages anySatisfy: [:each | each = self thisPackage]).",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["assert:", "notEmpty", "testPackages", "anySatisfy:", "=", "thisPackage"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$self._assert_($recv([$recv($self.model)._testPackages()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["testPackages"]=1
//>>excludeEnd("ctx");
][0])._notEmpty())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["assert:"]=1
//>>excludeEnd("ctx");
][0];
$self._assert_($recv($recv($self.model)._testPackages())._anySatisfy_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each).__eq($self._thisPackage());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
})));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testTestPackages",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModelTest);

$core.addMethod(
$core.method({
selector: "testUnselectClass",
protocol: "tests",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testUnselectClass\x0a\x09| announcementFired |\x0a\x09model selectPackage: self thisPackage.\x0a\x09model selectClass: self class.\x0a\x09model announcer on: HLClassUnselected\x0a\x09\x09do: [ announcementFired := true ]\x0a\x09\x09for: self.\x0a\x09model unselectClass: self class.\x0a\x09self assert: model selectedClasses isEmpty.\x0a\x09self assert: announcementFired",
referencedClasses: ["HLClassUnselected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectPackage:", "thisPackage", "selectClass:", "class", "on:do:for:", "announcer", "unselectClass:", "assert:", "isEmpty", "selectedClasses"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var announcementFired;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.model)._selectPackage_($self._thisPackage());
$recv($self.model)._selectClass_([$self._class()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class"]=1
//>>excludeEnd("ctx");
][0]);
$recv($recv($self.model)._announcer())._on_do_for_($globals.HLClassUnselected,(function(){
announcementFired=true;
return announcementFired;

}),self);
$recv($self.model)._unselectClass_($self._class());
[$self._assert_($recv($recv($self.model)._selectedClasses())._isEmpty())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["assert:"]=1
//>>excludeEnd("ctx");
][0];
$self._assert_(announcementFired);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testUnselectClass",{announcementFired:announcementFired})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModelTest);

$core.addMethod(
$core.method({
selector: "testUnselectPackage",
protocol: "tests",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testUnselectPackage\x0a\x09| announcementFired |\x0a\x09model selectPackage: self thisPackage.\x0a\x09model announcer on: HLPackageUnselected\x0a\x09\x09do: [ announcementFired := true ]\x0a\x09\x09for: self.\x0a\x09model unselectPackage: self thisPackage.\x0a\x09self assert: model selectedPackages isEmpty.\x0a\x09self assert: announcementFired.",
referencedClasses: ["HLPackageUnselected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectPackage:", "thisPackage", "on:do:for:", "announcer", "unselectPackage:", "assert:", "isEmpty", "selectedPackages"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var announcementFired;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.model)._selectPackage_([$self._thisPackage()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["thisPackage"]=1
//>>excludeEnd("ctx");
][0]);
$recv($recv($self.model)._announcer())._on_do_for_($globals.HLPackageUnselected,(function(){
announcementFired=true;
return announcementFired;

}),self);
$recv($self.model)._unselectPackage_($self._thisPackage());
[$self._assert_($recv($recv($self.model)._selectedPackages())._isEmpty())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["assert:"]=1
//>>excludeEnd("ctx");
][0];
$self._assert_(announcementFired);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testUnselectPackage",{announcementFired:announcementFired})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModelTest);

$core.addMethod(
$core.method({
selector: "thisPackage",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "thisPackage\x0a\x09^self class package",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["package", "class"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._class())._package();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"thisPackage",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModelTest);


});
