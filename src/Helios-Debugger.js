define(["amber/boot", "require", "amber/core/Kernel-Objects", "helios/Helios-Core", "helios/Helios-Workspace"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-Debugger");
$pkg.imports = ["amber/core/Compiler-Interpreter"];
//>>excludeStart("imports", pragmas.excludeImports);
$pkg.isReady = new Promise(function (resolve, reject) { requirejs(["amber/core/Compiler-Interpreter"], function () {resolve();}, reject); });
//>>excludeEnd("imports");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLContextInspectorDecorator", $globals.Object, "Helios-Debugger");
$core.setSlots($globals.HLContextInspectorDecorator, ["context"]);
$core.addMethod(
$core.method({
selector: "context",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "context\x0a\x09^ context",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.context;

}; }),
$globals.HLContextInspectorDecorator);

$core.addMethod(
$core.method({
selector: "evaluate:on:",
protocol: "evaluating",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString", "anEvaluator"],
source: "evaluate: aString on: anEvaluator\x0a\x09^ self context evaluate: aString on: anEvaluator",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["evaluate:on:", "context"]
}, function ($methodClass){ return function (aString,anEvaluator){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._context())._evaluate_on_(aString,anEvaluator);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"evaluate:on:",{aString:aString,anEvaluator:anEvaluator})});
//>>excludeEnd("ctx");
}; }),
$globals.HLContextInspectorDecorator);

$core.addMethod(
$core.method({
selector: "initializeFromContext:",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aContext"],
source: "initializeFromContext: aContext\x0a\x09context := aContext",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aContext){
var self=this,$self=this;
$self.context=aContext;
return self;

}; }),
$globals.HLContextInspectorDecorator);

$core.addMethod(
$core.method({
selector: "inspectOn:",
protocol: "inspecting",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInspector"],
source: "inspectOn: anInspector\x0a\x09| variables inspectedContext |\x0a\x0a\x09inspectedContext := self context. console log: 'paso por aqui'.\x0a\x09variables := Array streamContents: [ :stream |\x0a\x09\x09stream nextPutAll: inspectedContext locals associations.\x0a\x09\x09\x0a\x09\x09[ inspectedContext notNil and: [ inspectedContext isBlockContext ] ] whileTrue: [\x0a\x09\x09\x09inspectedContext := inspectedContext outerContext.\x0a\x09\x09\x09inspectedContext ifNotNil: [\x0a\x09\x09\x09\x09stream nextPutAll: inspectedContext locals associations ] ] ].\x0a\x09\x0a\x09anInspector\x0a\x09\x09setLabel: 'Context';\x0a\x09\x09setVariables: variables",
referencedClasses: ["Array"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["context", "log:", "streamContents:", "nextPutAll:", "associations", "locals", "whileTrue:", "and:", "notNil", "isBlockContext", "outerContext", "ifNotNil:", "setLabel:", "setVariables:"]
}, function ($methodClass){ return function (anInspector){
var self=this,$self=this;
var variables,inspectedContext;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
inspectedContext=$self._context();
$recv(console)._log_("paso por aqui");
variables=$recv($globals.Array)._streamContents_((function(stream){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
[$recv(stream)._nextPutAll_([$recv([$recv(inspectedContext)._locals()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["locals"]=1
//>>excludeEnd("ctx");
][0])._associations()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["associations"]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["nextPutAll:"]=1
//>>excludeEnd("ctx");
][0];
return $recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
if($core.assert($recv(inspectedContext)._notNil())){
return $recv(inspectedContext)._isBlockContext();
} else {
return false;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}))._whileTrue_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
inspectedContext=$recv(inspectedContext)._outerContext();
$1=inspectedContext;
if($1 == null || $1.a$nil){
return $1;
} else {
return $recv(stream)._nextPutAll_($recv($recv(inspectedContext)._locals())._associations());
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,4)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({stream:stream},$ctx1,1)});
//>>excludeEnd("ctx");
}));
$recv(anInspector)._setLabel_("Context");
$recv(anInspector)._setVariables_(variables);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inspectOn:",{anInspector:anInspector,variables:variables,inspectedContext:inspectedContext})});
//>>excludeEnd("ctx");
}; }),
$globals.HLContextInspectorDecorator);


$core.addMethod(
$core.method({
selector: "on:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aContext"],
source: "on: aContext\x0a\x09^ self new\x0a\x09\x09initializeFromContext: aContext;\x0a\x09\x09yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initializeFromContext:", "new", "yourself"]
}, function ($methodClass){ return function (aContext){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._initializeFromContext_(aContext);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{aContext:aContext})});
//>>excludeEnd("ctx");
}; }),
$globals.HLContextInspectorDecorator.a$cls);


$core.addClass("HLDebugger", $globals.HLFocusableWidget, "Helios-Debugger");
$core.setSlots($globals.HLDebugger, ["model", "stackListWidget", "codeWidget", "inspectorWidget"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLDebugger.comment="I am the main widget for the Helios debugger.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "codeWidget",
protocol: "widgets",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "codeWidget\x0a\x09^ codeWidget ifNil: [ codeWidget := HLDebuggerCodeWidget new\x0a\x09\x09model: (HLDebuggerCodeModel new\x0a\x09\x09\x09debuggerModel: self model;\x0a\x09\x09\x09yourself);\x0a\x09\x09browserModel: self model;\x0a\x09\x09yourself ]",
referencedClasses: ["HLDebuggerCodeWidget", "HLDebuggerCodeModel"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "model:", "new", "debuggerModel:", "model", "yourself", "browserModel:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3;
$1=$self.codeWidget;
if($1 == null || $1.a$nil){
$2=[$recv($globals.HLDebuggerCodeWidget)._new()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["new"]=1
//>>excludeEnd("ctx");
][0];
$3=$recv($globals.HLDebuggerCodeModel)._new();
$recv($3)._debuggerModel_([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0]);
$recv($2)._model_([$recv($3)._yourself()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["yourself"]=1
//>>excludeEnd("ctx");
][0]);
$recv($2)._browserModel_($self._model());
$self.codeWidget=$recv($2)._yourself();
return $self.codeWidget;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"codeWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebugger);

$core.addMethod(
$core.method({
selector: "cssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "cssClass\x0a\x09^ super cssClass, ' hl_debugger'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: [",", "cssClass"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv([(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._cssClass.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0]).__comma(" hl_debugger");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"cssClass",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebugger);

$core.addMethod(
$core.method({
selector: "focus",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focus\x0a\x09self stackListWidget focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focus", "stackListWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._stackListWidget())._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focus",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebugger);

$core.addMethod(
$core.method({
selector: "initializeFromError:",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anError"],
source: "initializeFromError: anError\x0a\x09model := HLDebuggerModel on: anError.\x0a\x09self observeModel",
referencedClasses: ["HLDebuggerModel"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:", "observeModel"]
}, function ($methodClass){ return function (anError){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.model=$recv($globals.HLDebuggerModel)._on_(anError);
$self._observeModel();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initializeFromError:",{anError:anError})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebugger);

$core.addMethod(
$core.method({
selector: "inspectorWidget",
protocol: "widgets",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inspectorWidget\x0a\x09^ inspectorWidget ifNil: [ \x0a\x09\x09inspectorWidget := HLInspectorWidget new ]",
referencedClasses: ["HLInspectorWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.inspectorWidget;
if($1 == null || $1.a$nil){
$self.inspectorWidget=$recv($globals.HLInspectorWidget)._new();
return $self.inspectorWidget;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inspectorWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebugger);

$core.addMethod(
$core.method({
selector: "model",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "model\x0a\x09^ model ifNil: [ model := HLDebuggerModel new ]",
referencedClasses: ["HLDebuggerModel"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.model;
if($1 == null || $1.a$nil){
$self.model=$recv($globals.HLDebuggerModel)._new();
return $self.model;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"model",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebugger);

$core.addMethod(
$core.method({
selector: "observeModel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeModel\x0a\x09self model announcer \x0a\x09\x09on: HLDebuggerContextSelected\x0a\x09\x09send: #onContextSelected:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLDebuggerStepped\x0a\x09\x09send: #onDebuggerStepped:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLDebuggerProceeded\x0a\x09\x09send: #onDebuggerProceeded\x0a\x09\x09to: self",
referencedClasses: ["HLDebuggerContextSelected", "HLDebuggerStepped", "HLDebuggerProceeded"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._model())._announcer();
[$recv($1)._on_send_to_($globals.HLDebuggerContextSelected,"onContextSelected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLDebuggerStepped,"onDebuggerStepped:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=2
//>>excludeEnd("ctx");
][0];
$recv($1)._on_send_to_($globals.HLDebuggerProceeded,"onDebuggerProceeded",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebugger);

$core.addMethod(
$core.method({
selector: "onContextSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onContextSelected: anAnnouncement\x0a\x09self inspectorWidget inspect: (HLContextInspectorDecorator on: anAnnouncement context)",
referencedClasses: ["HLContextInspectorDecorator"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["inspect:", "inspectorWidget", "on:", "context"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._inspectorWidget())._inspect_($recv($globals.HLContextInspectorDecorator)._on_($recv(anAnnouncement)._context()));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onContextSelected:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebugger);

$core.addMethod(
$core.method({
selector: "onDebuggerProceeded",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onDebuggerProceeded\x0a\x09self removeTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["removeTab"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._removeTab();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onDebuggerProceeded",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebugger);

$core.addMethod(
$core.method({
selector: "onDebuggerStepped:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onDebuggerStepped: anAnnouncement\x0a\x09self model atEnd ifTrue: [ self removeTab ].\x0a\x09\x0a\x09self inspectorWidget inspect: (HLContextInspectorDecorator on: anAnnouncement context).\x0a\x09self stackListWidget refresh",
referencedClasses: ["HLContextInspectorDecorator"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "atEnd", "model", "removeTab", "inspect:", "inspectorWidget", "on:", "context", "refresh", "stackListWidget"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv($self._model())._atEnd())){
$self._removeTab();
}
$recv($self._inspectorWidget())._inspect_($recv($globals.HLContextInspectorDecorator)._on_($recv(anAnnouncement)._context()));
$recv($self._stackListWidget())._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onDebuggerStepped:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebugger);

$core.addMethod(
$core.method({
selector: "registerBindingsOn:",
protocol: "keybindings",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBindingGroup"],
source: "registerBindingsOn: aBindingGroup\x0a\x09HLToolCommand \x0a\x09\x09registerConcreteClassesOn: aBindingGroup \x0a\x09\x09for: self model",
referencedClasses: ["HLToolCommand"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["registerConcreteClassesOn:for:", "model"]
}, function ($methodClass){ return function (aBindingGroup){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($globals.HLToolCommand)._registerConcreteClassesOn_for_(aBindingGroup,$self._model());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"registerBindingsOn:",{aBindingGroup:aBindingGroup})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebugger);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09self renderHeadOn: html.\x0a\x09html with: (HLContainer with: (HLVerticalSplitter\x0a\x09\x09with: self codeWidget\x0a\x09\x09with: (HLHorizontalSplitter\x0a\x09\x09\x09with: self stackListWidget\x0a\x09\x09\x09with: self inspectorWidget)))",
referencedClasses: ["HLContainer", "HLVerticalSplitter", "HLHorizontalSplitter"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["renderHeadOn:", "with:", "with:with:", "codeWidget", "stackListWidget", "inspectorWidget"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._renderHeadOn_(html);
[$recv(html)._with_($recv($globals.HLContainer)._with_([$recv($globals.HLVerticalSplitter)._with_with_($self._codeWidget(),$recv($globals.HLHorizontalSplitter)._with_with_($self._stackListWidget(),$self._inspectorWidget()))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:with:"]=1
//>>excludeEnd("ctx");
][0]))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebugger);

$core.addMethod(
$core.method({
selector: "renderHeadOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderHeadOn: html\x0a\x09html div \x0a\x09\x09class: 'head'; \x0a\x09\x09with: [ html h2 with: self model error messageText ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "with:", "h2", "messageText", "error", "model"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._div();
$recv($1)._class_("head");
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv(html)._h2())._with_($recv($recv($self._model())._error())._messageText());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderHeadOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebugger);

$core.addMethod(
$core.method({
selector: "stackListWidget",
protocol: "widgets",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "stackListWidget\x0a\x09^ stackListWidget ifNil: [ \x0a\x09\x09stackListWidget := (HLStackListWidget on: self model)\x0a\x09\x09\x09next: self codeWidget;\x0a\x09\x09\x09yourself ]",
referencedClasses: ["HLStackListWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "next:", "on:", "model", "codeWidget", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$self.stackListWidget;
if($1 == null || $1.a$nil){
$2=$recv($globals.HLStackListWidget)._on_($self._model());
$recv($2)._next_($self._codeWidget());
$self.stackListWidget=$recv($2)._yourself();
return $self.stackListWidget;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"stackListWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebugger);

$core.addMethod(
$core.method({
selector: "unregister",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "unregister\x0a\x09super unregister.\x0a\x09self inspectorWidget unregister",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unregister", "inspectorWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._unregister.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["unregister"]=1,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv($self._inspectorWidget())._unregister();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unregister",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebugger);


$core.addMethod(
$core.method({
selector: "on:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anError"],
source: "on: anError\x0a\x09^ self new\x0a\x09\x09initializeFromError: anError;\x0a\x09\x09yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initializeFromError:", "new", "yourself"]
}, function ($methodClass){ return function (anError){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._initializeFromError_(anError);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{anError:anError})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebugger.a$cls);

$core.addMethod(
$core.method({
selector: "tabClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabClass\x0a\x09^ 'debugger'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "debugger";

}; }),
$globals.HLDebugger.a$cls);

$core.addMethod(
$core.method({
selector: "tabLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabLabel\x0a\x09^ 'Debugger'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Debugger";

}; }),
$globals.HLDebugger.a$cls);


$core.addClass("HLDebuggerCodeModel", $globals.HLCodeModel, "Helios-Debugger");
$core.setSlots($globals.HLDebuggerCodeModel, ["debuggerModel"]);
$core.addMethod(
$core.method({
selector: "debuggerModel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "debuggerModel\x0a\x09^ debuggerModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.debuggerModel;

}; }),
$globals.HLDebuggerCodeModel);

$core.addMethod(
$core.method({
selector: "debuggerModel:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "debuggerModel: anObject\x0a\x09debuggerModel := anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
$self.debuggerModel=anObject;
return self;

}; }),
$globals.HLDebuggerCodeModel);

$core.addMethod(
$core.method({
selector: "doIt:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "doIt: aString\x0a\x09^ self debuggerModel evaluate: aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["evaluate:", "debuggerModel"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._debuggerModel())._evaluate_(aString);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"doIt:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerCodeModel);



$core.addClass("HLDebuggerCodeWidget", $globals.HLBrowserCodeWidget, "Helios-Debugger");
$core.addMethod(
$core.method({
selector: "addStopAt:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInteger"],
source: "addStopAt: anInteger\x0a\x09editor\x0a\x09\x09setGutterMarker: anInteger\x0a\x09\x09gutter: 'stops'\x0a\x09\x09value: '<div class=\x22stop\x22></div>' asJQuery toArray first",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["setGutterMarker:gutter:value:", "first", "toArray", "asJQuery"]
}, function ($methodClass){ return function (anInteger){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.editor)._setGutterMarker_gutter_value_(anInteger,"stops",$recv($recv("<div class=\x22stop\x22></div>"._asJQuery())._toArray())._first());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"addStopAt:",{anInteger:anInteger})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerCodeWidget);

$core.addMethod(
$core.method({
selector: "clearHighlight",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "clearHighlight\x0a\x09self editor clearGutter: 'stops'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["clearGutter:", "editor"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._editor())._clearGutter_("stops");
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"clearHighlight",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerCodeWidget);

$core.addMethod(
$core.method({
selector: "contents:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "contents: aString\x0a\x09self clearHighlight.\x0a\x09super contents: aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["clearHighlight", "contents:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._clearHighlight();
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._contents_.call($self,aString))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"contents:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerCodeWidget);

$core.addMethod(
$core.method({
selector: "editorOptions",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "editorOptions\x0a\x09^ super editorOptions\x0a\x09\x09at: 'gutters' put: #('CodeMirror-linenumbers' 'stops');\x0a\x09\x09yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["at:put:", "editorOptions", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._editorOptions.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv($1)._at_put_("gutters",["CodeMirror-linenumbers", "stops"]);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"editorOptions",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerCodeWidget);

$core.addMethod(
$core.method({
selector: "highlight",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "highlight\x0a\x09self browserModel nextNode ifNotNil: [ :node |\x0a\x09\x09self highlightNode: node ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "nextNode", "browserModel", "highlightNode:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._browserModel())._nextNode();
if($1 == null || $1.a$nil){
$1;
} else {
var node;
node=$1;
$self._highlightNode_(node);
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"highlight",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerCodeWidget);

$core.addMethod(
$core.method({
selector: "highlightNode:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aNode"],
source: "highlightNode: aNode\x0a\x09| token |\x0a\x09\x0a\x09aNode ifNotNil: [\x0a\x09\x09self\x0a\x09\x09\x09clearHighlight;\x0a\x09\x09\x09addStopAt: aNode positionStart x - 1.\x0a\x0a\x09\x09self editor \x0a\x09\x09\x09setSelection: #{ 'line' -> (aNode positionStart x - 1). 'ch' -> (aNode positionStart y - 1) }\x0a\x09\x09\x09to: #{ 'line' -> (aNode positionEnd x - 1). 'ch' -> (aNode positionEnd y) } ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "clearHighlight", "addStopAt:", "-", "x", "positionStart", "setSelection:to:", "editor", "y", "positionEnd"]
}, function ($methodClass){ return function (aNode){
var self=this,$self=this;
var token;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if(aNode == null || aNode.a$nil){
aNode;
} else {
$self._clearHighlight();
$self._addStopAt_([$recv([$recv([$recv(aNode)._positionStart()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["positionStart"]=1
//>>excludeEnd("ctx");
][0])._x()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["x"]=1
//>>excludeEnd("ctx");
][0]).__minus((1))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["-"]=1
//>>excludeEnd("ctx");
][0]);
$recv($self._editor())._setSelection_to_($globals.HashedCollection._newFromPairs_(["line",[$recv([$recv([$recv(aNode)._positionStart()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["positionStart"]=2
//>>excludeEnd("ctx");
][0])._x()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["x"]=2
//>>excludeEnd("ctx");
][0]).__minus((1))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["-"]=2
//>>excludeEnd("ctx");
][0],"ch",[$recv([$recv($recv(aNode)._positionStart())._y()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["y"]=1
//>>excludeEnd("ctx");
][0]).__minus((1))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["-"]=3
//>>excludeEnd("ctx");
][0]]),$globals.HashedCollection._newFromPairs_(["line",$recv($recv([$recv(aNode)._positionEnd()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["positionEnd"]=1
//>>excludeEnd("ctx");
][0])._x()).__minus((1)),"ch",$recv($recv(aNode)._positionEnd())._y()]));
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"highlightNode:",{aNode:aNode,token:token})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerCodeWidget);

$core.addMethod(
$core.method({
selector: "observeBrowserModel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeBrowserModel\x0a\x09super observeBrowserModel.\x0a\x09\x0a\x09self browserModel announcer \x0a\x09\x09on: HLDebuggerContextSelected\x0a\x09\x09send: #onContextSelected\x0a\x09\x09to: self.\x0a\x09\x0a\x09self browserModel announcer \x0a\x09\x09on: HLDebuggerStepped\x0a\x09\x09send: #onContextSelected\x0a\x09\x09to: self.\x0a\x09\x0a\x09self browserModel announcer \x0a\x09\x09on: HLDebuggerWhere\x0a\x09\x09send: #onContextSelected\x0a\x09\x09to: self",
referencedClasses: ["HLDebuggerContextSelected", "HLDebuggerStepped", "HLDebuggerWhere"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["observeBrowserModel", "on:send:to:", "announcer", "browserModel"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._observeBrowserModel.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
[$recv([$recv([$self._browserModel()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["browserModel"]=1
//>>excludeEnd("ctx");
][0])._announcer()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["announcer"]=1
//>>excludeEnd("ctx");
][0])._on_send_to_($globals.HLDebuggerContextSelected,"onContextSelected",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
[$recv([$recv([$self._browserModel()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["browserModel"]=2
//>>excludeEnd("ctx");
][0])._announcer()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["announcer"]=2
//>>excludeEnd("ctx");
][0])._on_send_to_($globals.HLDebuggerStepped,"onContextSelected",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=2
//>>excludeEnd("ctx");
][0];
$recv($recv($self._browserModel())._announcer())._on_send_to_($globals.HLDebuggerWhere,"onContextSelected",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeBrowserModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerCodeWidget);

$core.addMethod(
$core.method({
selector: "onContextSelected",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onContextSelected\x0a\x09self highlight",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["highlight"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._highlight();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onContextSelected",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerCodeWidget);

$core.addMethod(
$core.method({
selector: "renderOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderOn: html\x0a\x09super renderOn: html.\x0a\x09self contents: self browserModel selectedMethod source",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["renderOn:", "contents:", "source", "selectedMethod", "browserModel"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._renderOn_.call($self,html))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$self._contents_($recv($recv($self._browserModel())._selectedMethod())._source());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerCodeWidget);



$core.addClass("HLDebuggerModel", $globals.HLToolModel, "Helios-Debugger");
$core.setSlots($globals.HLDebuggerModel, ["rootContext", "debugger", "error"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLDebuggerModel.comment="I am a model for debugging Amber code in Helios.\x0a\x0aMy instances hold a reference to an `ASTDebugger` instance, itself referencing the current `context`. The context should be the root of the context stack.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "atEnd",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "atEnd\x0a\x09^ self debugger atEnd",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["atEnd", "debugger"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._debugger())._atEnd();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"atEnd",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerModel);

$core.addMethod(
$core.method({
selector: "contexts",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "contexts\x0a\x09| contexts context |\x0a\x09\x0a\x09contexts := OrderedCollection new.\x0a\x09context := self rootContext.\x0a\x09\x0a\x09[ context notNil ] whileTrue: [\x0a\x09\x09contexts add: context.\x0a\x09\x09context := context outerContext ].\x0a\x09\x09\x0a\x09^ contexts",
referencedClasses: ["OrderedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["new", "rootContext", "whileTrue:", "notNil", "add:", "outerContext"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var contexts,context;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
contexts=$recv($globals.OrderedCollection)._new();
context=$self._rootContext();
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(context)._notNil();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._whileTrue_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$recv(contexts)._add_(context);
context=$recv(context)._outerContext();
return context;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return contexts;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"contexts",{contexts:contexts,context:context})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerModel);

$core.addMethod(
$core.method({
selector: "currentContext",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "currentContext\x0a\x09^ self debugger context",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["context", "debugger"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._debugger())._context();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"currentContext",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerModel);

$core.addMethod(
$core.method({
selector: "currentContext:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aContext"],
source: "currentContext: aContext\x0a\x09self withChangesDo: [ \x0a\x09\x09self selectedMethod: aContext method.\x0a\x09\x09self debugger context: aContext.\x0a\x09\x09self announcer announce: (HLDebuggerContextSelected new\x0a\x09\x09\x09context: aContext;\x0a\x09\x09\x09yourself) ]",
referencedClasses: ["HLDebuggerContextSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "selectedMethod:", "method", "context:", "debugger", "announce:", "announcer", "new", "yourself"]
}, function ($methodClass){ return function (aContext){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$self._selectedMethod_($recv(aContext)._method());
[$recv($self._debugger())._context_(aContext)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["context:"]=1
//>>excludeEnd("ctx");
][0];
$1=$self._announcer();
$2=$recv($globals.HLDebuggerContextSelected)._new();
$recv($2)._context_(aContext);
return $recv($1)._announce_($recv($2)._yourself());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"currentContext:",{aContext:aContext})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerModel);

$core.addMethod(
$core.method({
selector: "debugger",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "debugger\x0a\x09^ debugger ifNil: [ debugger := ASTDebugger new ]",
referencedClasses: ["ASTDebugger"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.debugger;
if($1 == null || $1.a$nil){
$self.debugger=$recv($globals.ASTDebugger)._new();
return $self.debugger;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"debugger",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerModel);

$core.addMethod(
$core.method({
selector: "error",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "error\x0a\x09^ error",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.error;

}; }),
$globals.HLDebuggerModel);

$core.addMethod(
$core.method({
selector: "evaluate:",
protocol: "evaluating",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "evaluate: aString\x0a\x09^ self environment \x0a\x09\x09evaluate: aString \x0a\x09\x09for: self currentContext",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["evaluate:for:", "environment", "currentContext"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._environment())._evaluate_for_(aString,$self._currentContext());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"evaluate:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerModel);

$core.addMethod(
$core.method({
selector: "flushInnerContexts",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "flushInnerContexts\x0a\x09\x22When stepping, the inner contexts are not relevent anymore,\x0a\x09and can be flushed\x22\x0a\x09\x0a\x09self currentContext innerContext: nil.\x0a\x09rootContext := self currentContext.\x0a\x09self initializeContexts",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["innerContext:", "currentContext", "initializeContexts"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv([$self._currentContext()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["currentContext"]=1
//>>excludeEnd("ctx");
][0])._innerContext_(nil);
$self.rootContext=$self._currentContext();
$self._initializeContexts();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"flushInnerContexts",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerModel);

$core.addMethod(
$core.method({
selector: "initializeFromError:",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anError"],
source: "initializeFromError: anError\x0a\x09| errorContext |\x0a\x09\x0a\x09error := anError.\x0a\x09errorContext := (AIContext fromMethodContext: error context).\x0a\x09rootContext := error signalerContextFrom: errorContext.\x0a\x09self selectedMethod: rootContext method",
referencedClasses: ["AIContext"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["fromMethodContext:", "context", "signalerContextFrom:", "selectedMethod:", "method"]
}, function ($methodClass){ return function (anError){
var self=this,$self=this;
var errorContext;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.error=anError;
errorContext=$recv($globals.AIContext)._fromMethodContext_($recv($self.error)._context());
$self.rootContext=$recv($self.error)._signalerContextFrom_(errorContext);
$self._selectedMethod_($recv($self.rootContext)._method());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initializeFromError:",{anError:anError,errorContext:errorContext})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerModel);

$core.addMethod(
$core.method({
selector: "isReferencesModel",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isReferencesModel\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLDebuggerModel);

$core.addMethod(
$core.method({
selector: "nextNode",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "nextNode\x0a\x09^ self debugger node",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["node", "debugger"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._debugger())._node();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"nextNode",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerModel);

$core.addMethod(
$core.method({
selector: "onStep",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onStep\x0a\x09rootContext := self currentContext.\x0a\x09\x0a\x09\x22Force a refresh of the context list and code widget\x22\x0a\x09self selectedMethod: self currentContext method.\x0a\x09self announcer announce: (HLDebuggerContextSelected new\x0a\x09\x09context: self currentContext;\x0a\x09\x09yourself)",
referencedClasses: ["HLDebuggerContextSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["currentContext", "selectedMethod:", "method", "announce:", "announcer", "context:", "new", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$self.rootContext=[$self._currentContext()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["currentContext"]=1
//>>excludeEnd("ctx");
][0];
$self._selectedMethod_($recv([$self._currentContext()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["currentContext"]=2
//>>excludeEnd("ctx");
][0])._method());
$1=$self._announcer();
$2=$recv($globals.HLDebuggerContextSelected)._new();
$recv($2)._context_($self._currentContext());
$recv($1)._announce_($recv($2)._yourself());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onStep",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerModel);

$core.addMethod(
$core.method({
selector: "openMethod",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "openMethod\x0a\x09| browser |\x0a\x09\x0a\x09self selectedMethod ifNil: [ ^ self ].\x0a\x09\x0a\x09self withChangesDo: [\x0a\x09\x09browser := HLBrowser openAsTab.\x0a\x09\x09browser openMethod: self selectedMethod ]",
referencedClasses: ["HLBrowser"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "selectedMethod", "withChangesDo:", "openAsTab", "openMethod:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var browser;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=[$self._selectedMethod()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedMethod"]=1
//>>excludeEnd("ctx");
][0];
if($1 == null || $1.a$nil){
return self;
} else {
$1;
}
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
browser=$recv($globals.HLBrowser)._openAsTab();
return $recv(browser)._openMethod_($self._selectedMethod());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"openMethod",{browser:browser})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerModel);

$core.addMethod(
$core.method({
selector: "proceed",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "proceed\x0a\x09self debugger proceed.\x0a\x09\x0a\x09self announcer announce: HLDebuggerProceeded new",
referencedClasses: ["HLDebuggerProceeded"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["proceed", "debugger", "announce:", "announcer", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._debugger())._proceed();
$recv($self._announcer())._announce_($recv($globals.HLDebuggerProceeded)._new());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"proceed",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerModel);

$core.addMethod(
$core.method({
selector: "restart",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "restart\x0a\x09self debugger restart.\x0a\x09self onStep.\x0a\x09\x0a\x09self announcer announce: (HLDebuggerStepped new\x0a\x09\x09context: self currentContext;\x0a\x09\x09yourself)",
referencedClasses: ["HLDebuggerStepped"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["restart", "debugger", "onStep", "announce:", "announcer", "context:", "new", "currentContext", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$recv($self._debugger())._restart();
$self._onStep();
$1=$self._announcer();
$2=$recv($globals.HLDebuggerStepped)._new();
$recv($2)._context_($self._currentContext());
$recv($1)._announce_($recv($2)._yourself());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"restart",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerModel);

$core.addMethod(
$core.method({
selector: "rootContext",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "rootContext\x0a\x09^ rootContext",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.rootContext;

}; }),
$globals.HLDebuggerModel);

$core.addMethod(
$core.method({
selector: "stepOver",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "stepOver\x0a\x09self debugger stepOver.\x0a\x09self onStep.\x0a\x09\x0a\x09self announcer announce: (HLDebuggerStepped new\x0a\x09\x09context: self currentContext;\x0a\x09\x09yourself)",
referencedClasses: ["HLDebuggerStepped"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["stepOver", "debugger", "onStep", "announce:", "announcer", "context:", "new", "currentContext", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$recv($self._debugger())._stepOver();
$self._onStep();
$1=$self._announcer();
$2=$recv($globals.HLDebuggerStepped)._new();
$recv($2)._context_($self._currentContext());
$recv($1)._announce_($recv($2)._yourself());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"stepOver",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerModel);

$core.addMethod(
$core.method({
selector: "where",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "where\x0a\x09self announcer announce: HLDebuggerWhere new",
referencedClasses: ["HLDebuggerWhere"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._announcer())._announce_($recv($globals.HLDebuggerWhere)._new());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"where",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerModel);


$core.addMethod(
$core.method({
selector: "on:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anError"],
source: "on: anError\x0a\x09^ self new\x0a\x09\x09initializeFromError: anError;\x0a\x09\x09yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initializeFromError:", "new", "yourself"]
}, function ($methodClass){ return function (anError){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._initializeFromError_(anError);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{anError:anError})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDebuggerModel.a$cls);


$core.addClass("HLErrorHandler", $globals.Object, "Helios-Debugger");
$core.setSlots($globals.HLErrorHandler, ["confirms"]);
$core.addMethod(
$core.method({
selector: "confirmDebugError:",
protocol: "error handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anError"],
source: "confirmDebugError: anError\x0a\x09confirms ifFalse: [\x0a\x09\x09confirms := true.\x0a\x09\x09HLConfirmationWidget new\x0a\x09\x09\x09confirmationString: anError messageText;\x0a\x09\x09\x09cancelBlock: [ confirms := false ];\x0a\x09\x09\x09actionBlock: [ confirms := false. self debugError: anError ];\x0a\x09\x09\x09cancelButtonLabel: 'Abandon';\x0a\x09\x09\x09confirmButtonLabel: 'Debug';\x0a\x09\x09\x09show ]",
referencedClasses: ["HLConfirmationWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifFalse:", "confirmationString:", "new", "messageText", "cancelBlock:", "actionBlock:", "debugError:", "cancelButtonLabel:", "confirmButtonLabel:", "show"]
}, function ($methodClass){ return function (anError){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
if(!$core.assert($self.confirms)){
$self.confirms=true;
$1=$recv($globals.HLConfirmationWidget)._new();
$recv($1)._confirmationString_($recv(anError)._messageText());
$recv($1)._cancelBlock_((function(){
$self.confirms=false;
return $self.confirms;

}));
$recv($1)._actionBlock_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$self.confirms=false;
return $self._debugError_(anError);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,3)});
//>>excludeEnd("ctx");
}));
$recv($1)._cancelButtonLabel_("Abandon");
$recv($1)._confirmButtonLabel_("Debug");
$recv($1)._show();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"confirmDebugError:",{anError:anError})});
//>>excludeEnd("ctx");
}; }),
$globals.HLErrorHandler);

$core.addMethod(
$core.method({
selector: "debugError:",
protocol: "error handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anError"],
source: "debugError: anError\x0a\x0a\x09[\x0a\x09\x09anError context ifNil: [ anError context: thisContext ].\x0a\x09\x09(HLDebugger on: anError) openAsTab ]\x0a\x09on: Error do: [ :error | ConsoleErrorHandler new handleError: error ]",
referencedClasses: ["HLDebugger", "Error", "ConsoleErrorHandler"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:do:", "ifNil:", "context", "context:", "openAsTab", "on:", "handleError:", "new"]
}, function ($methodClass){ return function (anError){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$1=$recv(anError)._context();
if($1 == null || $1.a$nil){
$recv(anError)._context_($core.getThisContext());
} else {
$1;
}
return $recv($recv($globals.HLDebugger)._on_(anError))._openAsTab();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._on_do_($globals.Error,(function(error){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv($globals.ConsoleErrorHandler)._new())._handleError_(error);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({error:error},$ctx1,3)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"debugError:",{anError:anError})});
//>>excludeEnd("ctx");
}; }),
$globals.HLErrorHandler);

$core.addMethod(
$core.method({
selector: "handleError:",
protocol: "error handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anError"],
source: "handleError: anError\x0a\x09self confirmDebugError: anError",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["confirmDebugError:"]
}, function ($methodClass){ return function (anError){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._confirmDebugError_(anError);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"handleError:",{anError:anError})});
//>>excludeEnd("ctx");
}; }),
$globals.HLErrorHandler);

$core.addMethod(
$core.method({
selector: "initialize",
protocol: "error handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initialize\x0a\x09confirms := false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
$self.confirms=false;
return self;

}; }),
$globals.HLErrorHandler);

$core.addMethod(
$core.method({
selector: "onErrorHandled",
protocol: "error handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onErrorHandled\x0a\x09\x22when an error is handled, we need to make sure that\x0a\x09any progress bar widget gets removed. Because HLProgressBarWidget is asynchronous,\x0a\x09it has to be done here.\x22\x0a\x09\x0a\x09HLProgressWidget default \x0a\x09\x09flush; \x0a\x09\x09remove",
referencedClasses: ["HLProgressWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["flush", "default", "remove"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLProgressWidget)._default();
$recv($1)._flush();
$recv($1)._remove();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onErrorHandled",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLErrorHandler);



$core.addClass("HLStackListWidget", $globals.HLToolListWidget, "Helios-Debugger");
$core.addMethod(
$core.method({
selector: "items",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "items\x0a\x09^ self model contexts",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["contexts", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._contexts();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"items",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLStackListWidget);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Call stack'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Call stack";

}; }),
$globals.HLStackListWidget);

$core.addMethod(
$core.method({
selector: "observeModel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeModel\x0a\x09super observeModel.\x0a\x09\x0a\x09self model announcer \x0a\x09\x09on: HLDebuggerStepped\x0a\x09\x09send: #onDebuggerStepped:\x0a\x09\x09to: self",
referencedClasses: ["HLDebuggerStepped"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["observeModel", "on:send:to:", "announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._observeModel.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv($recv($self._model())._announcer())._on_send_to_($globals.HLDebuggerStepped,"onDebuggerStepped:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLStackListWidget);

$core.addMethod(
$core.method({
selector: "onDebuggerStepped:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onDebuggerStepped: anAnnouncement\x0a\x09items := nil.\x0a\x09self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.items=nil;
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onDebuggerStepped:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLStackListWidget);

$core.addMethod(
$core.method({
selector: "proceed",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "proceed\x0a\x09self model proceed",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["proceed", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._proceed();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"proceed",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLStackListWidget);

$core.addMethod(
$core.method({
selector: "renderButtonsOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderButtonsOn: html\x0a\x09html div \x0a\x09\x09class: 'debugger_bar'; \x0a\x09\x09with: [\x0a\x09\x09\x09html button \x0a\x09\x09\x09\x09class: 'btn btn-default restart';\x0a\x09\x09\x09\x09with: 'Restart';\x0a\x09\x09\x09\x09onClick: [ self restart ].\x0a\x09\x09\x09html button \x0a\x09\x09\x09\x09class: 'btn btn-default where';\x0a\x09\x09\x09\x09with: 'Where';\x0a\x09\x09\x09\x09onClick: [ self where ].\x0a\x09\x09\x09html button \x0a\x09\x09\x09\x09class: 'btn btn-default stepOver';\x0a\x09\x09\x09\x09with: 'Step over';\x0a\x09\x09\x09\x09onClick: [ self stepOver ].\x0a\x09\x09\x09html button \x0a\x09\x09\x09\x09class: 'btn btn-default proceed';\x0a\x09\x09\x09\x09with: 'Proceed';\x0a\x09\x09\x09\x09onClick: [ self proceed ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "with:", "button", "onClick:", "restart", "where", "stepOver", "proceed"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3,$4,$5;
$1=$recv(html)._div();
[$recv($1)._class_("debugger_bar")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$2=[$recv(html)._button()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["button"]=1
//>>excludeEnd("ctx");
][0];
[$recv($2)._class_("btn btn-default restart")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["class:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($2)._with_("Restart")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($2)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._restart();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["onClick:"]=1
//>>excludeEnd("ctx");
][0];
$3=[$recv(html)._button()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["button"]=2
//>>excludeEnd("ctx");
][0];
[$recv($3)._class_("btn btn-default where")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["class:"]=3
//>>excludeEnd("ctx");
][0];
[$recv($3)._with_("Where")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=3
//>>excludeEnd("ctx");
][0];
[$recv($3)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._where();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,3)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["onClick:"]=2
//>>excludeEnd("ctx");
][0];
$4=[$recv(html)._button()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["button"]=3
//>>excludeEnd("ctx");
][0];
[$recv($4)._class_("btn btn-default stepOver")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["class:"]=4
//>>excludeEnd("ctx");
][0];
[$recv($4)._with_("Step over")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=4
//>>excludeEnd("ctx");
][0];
[$recv($4)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._stepOver();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,4)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["onClick:"]=3
//>>excludeEnd("ctx");
][0];
$5=$recv(html)._button();
$recv($5)._class_("btn btn-default proceed");
$recv($5)._with_("Proceed");
return $recv($5)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._proceed();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,5)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderButtonsOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLStackListWidget);

$core.addMethod(
$core.method({
selector: "restart",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "restart\x0a\x09self model restart",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["restart", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._restart();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"restart",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLStackListWidget);

$core.addMethod(
$core.method({
selector: "selectItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aContext"],
source: "selectItem: aContext\x0a   \x09self model currentContext: aContext.\x0a\x09super selectItem: aContext",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["currentContext:", "model", "selectItem:"]
}, function ($methodClass){ return function (aContext){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._currentContext_(aContext);
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._selectItem_.call($self,aContext))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectItem:",{aContext:aContext})});
//>>excludeEnd("ctx");
}; }),
$globals.HLStackListWidget);

$core.addMethod(
$core.method({
selector: "selectedItem",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectedItem\x0a   \x09^ self model currentContext",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["currentContext", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._currentContext();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectedItem",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLStackListWidget);

$core.addMethod(
$core.method({
selector: "stepOver",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "stepOver\x0a\x09self model stepOver",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["stepOver", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._stepOver();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"stepOver",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLStackListWidget);

$core.addMethod(
$core.method({
selector: "where",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "where\x0a\x09self model where",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["where", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._where();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"where",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLStackListWidget);


});
