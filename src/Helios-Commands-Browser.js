define(["amber/boot", "require", "helios/Helios-Commands-Tools"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-Commands-Browser");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLBrowserCommand", $globals.HLToolCommand, "Helios-Commands-Browser");

$core.addMethod(
$core.method({
selector: "isValidFor:",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aModel"],
source: "isValidFor: aModel\x0a\x09^ aModel isBrowserModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["isBrowserModel"]
}, function ($methodClass){ return function (aModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv(aModel)._isBrowserModel();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isValidFor:",{aModel:aModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCommand.a$cls);


$core.addClass("HLBrowserGoToCommand", $globals.HLBrowserCommand, "Helios-Commands-Browser");

$core.addMethod(
$core.method({
selector: "isValidFor:",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aModel"],
source: "isValidFor: aModel\x0a\x09^ aModel isBrowserModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["isBrowserModel"]
}, function ($methodClass){ return function (aModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv(aModel)._isBrowserModel();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isValidFor:",{aModel:aModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserGoToCommand.a$cls);

$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'g'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "g";

}; }),
$globals.HLBrowserGoToCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Go to'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Go to";

}; }),
$globals.HLBrowserGoToCommand.a$cls);


$core.addClass("HLGoToClassesCommand", $globals.HLBrowserGoToCommand, "Helios-Commands-Browser");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model focusOnClasses",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focusOnClasses", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._focusOnClasses();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLGoToClassesCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'c'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "c";

}; }),
$globals.HLGoToClassesCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Classes'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Classes";

}; }),
$globals.HLGoToClassesCommand.a$cls);


$core.addClass("HLGoToDocumentationCommand", $globals.HLBrowserGoToCommand, "Helios-Commands-Browser");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model focusOnDocumentation",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focusOnDocumentation", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._focusOnDocumentation();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLGoToDocumentationCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'd'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "d";

}; }),
$globals.HLGoToDocumentationCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Documentation'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Documentation";

}; }),
$globals.HLGoToDocumentationCommand.a$cls);


$core.addClass("HLGoToMethodsCommand", $globals.HLBrowserGoToCommand, "Helios-Commands-Browser");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model focusOnMethods",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focusOnMethods", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._focusOnMethods();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLGoToMethodsCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'm'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "m";

}; }),
$globals.HLGoToMethodsCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Methods'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Methods";

}; }),
$globals.HLGoToMethodsCommand.a$cls);


$core.addClass("HLGoToPackagesCommand", $globals.HLBrowserGoToCommand, "Helios-Commands-Browser");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model focusOnPackages",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focusOnPackages", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._focusOnPackages();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLGoToPackagesCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'p'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "p";

}; }),
$globals.HLGoToPackagesCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Packages'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Packages";

}; }),
$globals.HLGoToPackagesCommand.a$cls);


$core.addClass("HLGoToProtocolsCommand", $globals.HLBrowserGoToCommand, "Helios-Commands-Browser");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model focusOnProtocols",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focusOnProtocols", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._focusOnProtocols();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLGoToProtocolsCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 't'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "t";

}; }),
$globals.HLGoToProtocolsCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Protocols'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Protocols";

}; }),
$globals.HLGoToProtocolsCommand.a$cls);


$core.addClass("HLGoToSourceCodeCommand", $globals.HLBrowserGoToCommand, "Helios-Commands-Browser");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model focusOnSourceCode",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focusOnSourceCode", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._focusOnSourceCode();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLGoToSourceCodeCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 's'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "s";

}; }),
$globals.HLGoToSourceCodeCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Source code'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Source code";

}; }),
$globals.HLGoToSourceCodeCommand.a$cls);


$core.addClass("HLEditCommentCommand", $globals.HLBrowserCommand, "Helios-Commands-Browser");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model editComment",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["editComment", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._editComment();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLEditCommentCommand);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ self model showComment and: [ self model selectedClass notNil ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["and:", "showComment", "model", "notNil", "selectedClass"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._showComment())){
return $recv($recv($self._model())._selectedClass())._notNil();
} else {
return false;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLEditCommentCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'd'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "d";

}; }),
$globals.HLEditCommentCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Edit documentation'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Edit documentation";

}; }),
$globals.HLEditCommentCommand.a$cls);


$core.addClass("HLGenerateCommand", $globals.HLBrowserCommand, "Helios-Commands-Browser");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLGenerateCommand.comment="I am a group command used to gather all the commands generating code (`accessors`, `initialize`, etc)";
//>>excludeEnd("ide");

$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'h'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "h";

}; }),
$globals.HLGenerateCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Generate'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Generate";

}; }),
$globals.HLGenerateCommand.a$cls);


$core.addClass("HLCategorizeUnclassifiedCommand", $globals.HLGenerateCommand, "Helios-Commands-Browser");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLCategorizeUnclassifiedCommand.comment="I am the command used to categorize unclassified methods";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09| targetClass unclassified |\x0a\x09targetClass := self model selectedClass.\x0a\x0a\x09unclassified := targetClass methods select:[ :e | e protocol = 'as yet unclassified' ].\x0a\x09\x09\x0a\x09HLMethodClassifier new\x0a\x09\x09classifyAll: unclassified",
referencedClasses: ["HLMethodClassifier"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectedClass", "model", "select:", "methods", "=", "protocol", "classifyAll:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var targetClass,unclassified;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
targetClass=$recv($self._model())._selectedClass();
unclassified=$recv($recv(targetClass)._methods())._select_((function(e){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv(e)._protocol()).__eq("as yet unclassified");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({e:e},$ctx1,1)});
//>>excludeEnd("ctx");
}));
$recv($recv($globals.HLMethodClassifier)._new())._classifyAll_(unclassified);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{targetClass:targetClass,unclassified:unclassified})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCategorizeUnclassifiedCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'c'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "c";

}; }),
$globals.HLCategorizeUnclassifiedCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Categorize'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Categorize";

}; }),
$globals.HLCategorizeUnclassifiedCommand.a$cls);


$core.addClass("HLGenerateAccessorsCommand", $globals.HLGenerateCommand, "Helios-Commands-Browser");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLGenerateAccessorsCommand.comment="I am the command used to generate the `getter` and the `setter` methods depending of the selected class";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09| targetClass output first |\x0a\x09targetClass := self model selectedClass.\x0a\x0a\x09output := HLInitializeGenerator new\x0a\x09\x09class: targetClass;\x0a\x09\x09generate;\x0a\x09\x09output.\x0a\x09\x09\x0a\x09output compile.\x0a\x09first := output sourceCodes first.\x0a\x09self model\x0a\x09\x09selectedProtocol: output protocol;\x0a\x09\x09selectedMethod:(targetClass>>first selector);\x0a\x09\x09focusOnSourceCode",
referencedClasses: ["HLInitializeGenerator"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectedClass", "model", "class:", "new", "generate", "output", "compile", "first", "sourceCodes", "selectedProtocol:", "protocol", "selectedMethod:", ">>", "selector", "focusOnSourceCode"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var targetClass,output,first;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
targetClass=$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._selectedClass();
$1=$recv($globals.HLInitializeGenerator)._new();
$recv($1)._class_(targetClass);
$recv($1)._generate();
output=$recv($1)._output();
$recv(output)._compile();
first=$recv($recv(output)._sourceCodes())._first();
$2=$self._model();
$recv($2)._selectedProtocol_($recv(output)._protocol());
$recv($2)._selectedMethod_($recv(targetClass).__gt_gt($recv(first)._selector()));
$recv($2)._focusOnSourceCode();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{targetClass:targetClass,output:output,first:first})});
//>>excludeEnd("ctx");
}; }),
$globals.HLGenerateAccessorsCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'i'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "i";

}; }),
$globals.HLGenerateAccessorsCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Initialize'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Initialize";

}; }),
$globals.HLGenerateAccessorsCommand.a$cls);


$core.addClass("HLGenerateInitializeCommand", $globals.HLGenerateCommand, "Helios-Commands-Browser");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLGenerateInitializeCommand.comment="I am the command used to generate the `initialize` method depending of the selected class";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09| targetClass output |\x0a\x09targetClass := self model selectedClass.\x0a\x0a\x09output := HLAccessorsGenerator new\x0a\x09\x09class: targetClass;\x0a\x09\x09generate;\x0a\x09\x09output.\x0a\x09\x09\x0a\x09output compile.\x0a\x09self model selectedProtocol: output protocol",
referencedClasses: ["HLAccessorsGenerator"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectedClass", "model", "class:", "new", "generate", "output", "compile", "selectedProtocol:", "protocol"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var targetClass,output;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
targetClass=$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._selectedClass();
$1=$recv($globals.HLAccessorsGenerator)._new();
$recv($1)._class_(targetClass);
$recv($1)._generate();
output=$recv($1)._output();
$recv(output)._compile();
$recv($self._model())._selectedProtocol_($recv(output)._protocol());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{targetClass:targetClass,output:output})});
//>>excludeEnd("ctx");
}; }),
$globals.HLGenerateInitializeCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'a'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "a";

}; }),
$globals.HLGenerateInitializeCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Accessors'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Accessors";

}; }),
$globals.HLGenerateInitializeCommand.a$cls);


$core.addClass("HLToggleCommand", $globals.HLBrowserCommand, "Helios-Commands-Browser");

$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 't'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "t";

}; }),
$globals.HLToggleCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Toggle'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Toggle";

}; }),
$globals.HLToggleCommand.a$cls);


$core.addClass("HLToggleClassCommentCommand", $globals.HLToggleCommand, "Helios-Commands-Browser");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model showComment: self model showComment not",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["showComment:", "model", "not", "showComment"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._showComment_($recv($recv($self._model())._showComment())._not());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToggleClassCommentCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'd'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "d";

}; }),
$globals.HLToggleClassCommentCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Documentation'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Documentation";

}; }),
$globals.HLToggleClassCommentCommand.a$cls);


$core.addClass("HLToggleClassSideCommand", $globals.HLToggleCommand, "Helios-Commands-Browser");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model showInstance: self model showInstance not",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["showInstance:", "model", "not", "showInstance"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._showInstance_($recv($recv($self._model())._showInstance())._not());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToggleClassSideCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'c'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "c";

}; }),
$globals.HLToggleClassSideCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Class side'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Class side";

}; }),
$globals.HLToggleClassSideCommand.a$cls);

});
