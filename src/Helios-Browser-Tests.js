define(["amber/boot", "require", "amber/core/SUnit"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-Browser-Tests");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLBrowserTest", $globals.TestCase, "Helios-Browser-Tests");
$core.setSlots($globals.HLBrowserTest, ["browser"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLBrowserTest.comment="Test cases for the functionality of  `HLBrowserModel`";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "setUp",
protocol: "tests",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setUp\x0a\x09browser := HLBrowserModel new.",
referencedClasses: ["HLBrowserModel"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.browser=$recv($globals.HLBrowserModel)._new();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setUp",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserTest);

$core.addMethod(
$core.method({
selector: "testAllProtocolsSelectedWhenSelectingAClass",
protocol: "tests",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testAllProtocolsSelectedWhenSelectingAClass\x0a\x09self assert: browser selectedProtocol isNil.\x0a\x09browser selectedClass: Object.\x0a\x09self assert: browser selectedProtocol equals: browser allProtocol.",
referencedClasses: ["Object"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["assert:", "isNil", "selectedProtocol", "selectedClass:", "assert:equals:", "allProtocol"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._assert_($recv([$recv($self.browser)._selectedProtocol()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedProtocol"]=1
//>>excludeEnd("ctx");
][0])._isNil());
$recv($self.browser)._selectedClass_($globals.Object);
$self._assert_equals_($recv($self.browser)._selectedProtocol(),$recv($self.browser)._allProtocol());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testAllProtocolsSelectedWhenSelectingAClass",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserTest);

$core.addMethod(
$core.method({
selector: "testProtocolSelectedWhenChangingClass",
protocol: "tests",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testProtocolSelectedWhenChangingClass\x0a\x09| protocolSelectedCalled |\x0a\x09protocolSelectedCalled := 0.\x0a\x09browser announcer on: HLProtocolSelected do: [protocolSelectedCalled := protocolSelectedCalled + 1] for: self.\x0a\x09browser selectedClass: Object.\x0a\x09self assert: protocolSelectedCalled equals: 1.\x0a\x09browser selectedClass: ProtoObject.\x0a\x09self assert: protocolSelectedCalled equals: 2.",
referencedClasses: ["HLProtocolSelected", "Object", "ProtoObject"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:do:for:", "announcer", "+", "selectedClass:", "assert:equals:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var protocolSelectedCalled;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
protocolSelectedCalled=(0);
$recv($recv($self.browser)._announcer())._on_do_for_($globals.HLProtocolSelected,(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
protocolSelectedCalled=$recv(protocolSelectedCalled).__plus((1));
return protocolSelectedCalled;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}),self);
[$recv($self.browser)._selectedClass_($globals.Object)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedClass:"]=1
//>>excludeEnd("ctx");
][0];
[$self._assert_equals_(protocolSelectedCalled,(1))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["assert:equals:"]=1
//>>excludeEnd("ctx");
][0];
$recv($self.browser)._selectedClass_($globals.ProtoObject);
$self._assert_equals_(protocolSelectedCalled,(2));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testProtocolSelectedWhenChangingClass",{protocolSelectedCalled:protocolSelectedCalled})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserTest);


});
