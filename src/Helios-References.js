define(["amber/boot", "require", "helios/Helios-Core"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-References");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLReferences", $globals.HLWidget, "Helios-References");
$core.setSlots($globals.HLReferences, ["model", "sendersListWidget", "implementorsListWidget", "classReferencesListWidget", "regexpListWidget", "sourceCodeWidget"]);
$core.addMethod(
$core.method({
selector: "classReferencesListWidget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "classReferencesListWidget\x0a\x09^ classReferencesListWidget ifNil: [\x0a      \x09classReferencesListWidget := HLClassReferencesListWidget on: self model.\x0a\x09\x09classReferencesListWidget next: self regexpListWidget ]",
referencedClasses: ["HLClassReferencesListWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "on:", "model", "next:", "regexpListWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.classReferencesListWidget;
if($1 == null || $1.a$nil){
$self.classReferencesListWidget=$recv($globals.HLClassReferencesListWidget)._on_($self._model());
return $recv($self.classReferencesListWidget)._next_($self._regexpListWidget());
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"classReferencesListWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferences);

$core.addMethod(
$core.method({
selector: "implementorsListWidget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "implementorsListWidget\x0a\x09^ implementorsListWidget ifNil: [\x0a      \x09implementorsListWidget := HLImplementorsListWidget on: self model.\x0a\x09\x09implementorsListWidget next: self classReferencesListWidget ]",
referencedClasses: ["HLImplementorsListWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "on:", "model", "next:", "classReferencesListWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.implementorsListWidget;
if($1 == null || $1.a$nil){
$self.implementorsListWidget=$recv($globals.HLImplementorsListWidget)._on_($self._model());
return $recv($self.implementorsListWidget)._next_($self._classReferencesListWidget());
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"implementorsListWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferences);

$core.addMethod(
$core.method({
selector: "model",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "model\x0a\x09^ model ifNil: [\x0a\x09\x09model := (HLReferencesModel new\x0a\x09\x09\x09environment: self manager environment;\x0a\x09\x09\x09yourself) ]",
referencedClasses: ["HLReferencesModel"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "environment:", "new", "environment", "manager", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$self.model;
if($1 == null || $1.a$nil){
$2=$recv($globals.HLReferencesModel)._new();
$recv($2)._environment_($recv($self._manager())._environment());
$self.model=$recv($2)._yourself();
return $self.model;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"model",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferences);

$core.addMethod(
$core.method({
selector: "model:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aModel"],
source: "model: aModel\x0a\x09model := aModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aModel){
var self=this,$self=this;
$self.model=aModel;
return self;

}; }),
$globals.HLReferences);

$core.addMethod(
$core.method({
selector: "regexpListWidget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "regexpListWidget\x0a\x09^ regexpListWidget ifNil: [\x0a      \x09regexpListWidget := HLRegexpListWidget on: self model.\x0a\x09\x09regexpListWidget next: self sourceCodeWidget ]",
referencedClasses: ["HLRegexpListWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "on:", "model", "next:", "sourceCodeWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.regexpListWidget;
if($1 == null || $1.a$nil){
$self.regexpListWidget=$recv($globals.HLRegexpListWidget)._on_($self._model());
return $recv($self.regexpListWidget)._next_($self._sourceCodeWidget());
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"regexpListWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferences);

$core.addMethod(
$core.method({
selector: "registerBindingsOn:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBindingGroup"],
source: "registerBindingsOn: aBindingGroup\x0a\x09HLToolCommand \x0a\x09\x09registerConcreteClassesOn: aBindingGroup \x0a\x09\x09for: self model",
referencedClasses: ["HLToolCommand"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["registerConcreteClassesOn:for:", "model"]
}, function ($methodClass){ return function (aBindingGroup){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($globals.HLToolCommand)._registerConcreteClassesOn_for_(aBindingGroup,$self._model());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"registerBindingsOn:",{aBindingGroup:aBindingGroup})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferences);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09html with: (HLContainer with: (HLHorizontalSplitter \x0a    \x09with: (HLVerticalSplitter\x0a        \x09with: (HLVerticalSplitter\x0a            \x09with: self sendersListWidget\x0a                with: self implementorsListWidget)\x0a            with: (HLVerticalSplitter\x0a            \x09with: self classReferencesListWidget\x0a                with: self regexpListWidget)) \x0a        with: self sourceCodeWidget)).\x0a\x09\x0a\x09self sendersListWidget focus",
referencedClasses: ["HLContainer", "HLHorizontalSplitter", "HLVerticalSplitter"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "with:with:", "sendersListWidget", "implementorsListWidget", "classReferencesListWidget", "regexpListWidget", "sourceCodeWidget", "focus"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$recv(html)._with_($recv($globals.HLContainer)._with_([$recv($globals.HLHorizontalSplitter)._with_with_([$recv($globals.HLVerticalSplitter)._with_with_([$recv($globals.HLVerticalSplitter)._with_with_([$self._sendersListWidget()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["sendersListWidget"]=1
//>>excludeEnd("ctx");
][0],$self._implementorsListWidget())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:with:"]=3
//>>excludeEnd("ctx");
][0],$recv($globals.HLVerticalSplitter)._with_with_($self._classReferencesListWidget(),$self._regexpListWidget()))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:with:"]=2
//>>excludeEnd("ctx");
][0],$self._sourceCodeWidget())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:with:"]=1
//>>excludeEnd("ctx");
][0]))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
$recv($self._sendersListWidget())._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferences);

$core.addMethod(
$core.method({
selector: "search:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "search: aString\x0a\x09self model search: aString.\x0a\x09self setTabLabel: aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["search:", "model", "setTabLabel:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._search_(aString);
$self._setTabLabel_(aString);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"search:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferences);

$core.addMethod(
$core.method({
selector: "sendersListWidget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "sendersListWidget\x0a\x09^ sendersListWidget ifNil: [\x0a      \x09sendersListWidget := HLSendersListWidget on: self model.\x0a\x09\x09sendersListWidget next: self implementorsListWidget ]",
referencedClasses: ["HLSendersListWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "on:", "model", "next:", "implementorsListWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.sendersListWidget;
if($1 == null || $1.a$nil){
$self.sendersListWidget=$recv($globals.HLSendersListWidget)._on_($self._model());
return $recv($self.sendersListWidget)._next_($self._implementorsListWidget());
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"sendersListWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferences);

$core.addMethod(
$core.method({
selector: "sourceCodeWidget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "sourceCodeWidget\x0a\x09^ sourceCodeWidget ifNil: [\x0a      \x09sourceCodeWidget := HLBrowserCodeWidget new\x0a\x09\x09\x09browserModel: self model;\x0a\x09\x09\x09yourself ]",
referencedClasses: ["HLBrowserCodeWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "browserModel:", "new", "model", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$self.sourceCodeWidget;
if($1 == null || $1.a$nil){
$2=$recv($globals.HLBrowserCodeWidget)._new();
$recv($2)._browserModel_($self._model());
$self.sourceCodeWidget=$recv($2)._yourself();
return $self.sourceCodeWidget;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"sourceCodeWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferences);


$core.addMethod(
$core.method({
selector: "canBeOpenAsTab",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "canBeOpenAsTab\x0a\x09^ false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return false;

}; }),
$globals.HLReferences.a$cls);

$core.addMethod(
$core.method({
selector: "tabClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabClass\x0a\x09^ 'references'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "references";

}; }),
$globals.HLReferences.a$cls);

$core.addMethod(
$core.method({
selector: "tabLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabLabel\x0a\x09^ 'References'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "References";

}; }),
$globals.HLReferences.a$cls);

$core.addMethod(
$core.method({
selector: "tabPriority",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabPriority\x0a\x09^ 100",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return (100);

}; }),
$globals.HLReferences.a$cls);


$core.addClass("HLReferencesListWidget", $globals.HLToolListWidget, "Helios-References");
$core.addMethod(
$core.method({
selector: "activateListItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anItem"],
source: "activateListItem: anItem\x0a\x09self model withChangesDo: [ super activateListItem: anItem ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "model", "activateListItem:"]
}, function ($methodClass){ return function (anItem){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return [(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._activateListItem_.call($self,anItem))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.supercall = false
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activateListItem:",{anItem:anItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesListWidget);

$core.addMethod(
$core.method({
selector: "commandCategory",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "commandCategory\x0a\x09^ 'Methods'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Methods";

}; }),
$globals.HLReferencesListWidget);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'List'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "List";

}; }),
$globals.HLReferencesListWidget);

$core.addMethod(
$core.method({
selector: "observeModel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeModel\x0a\x09self model announcer\x0a\x09\x09on: HLSearchReferences\x0a\x09\x09do: [ :ann | self onSearchReferences: ann searchString ];\x0a\x09\x09on: HLMethodSelected\x0a\x09\x09do: [ :ann | self onMethodSelected: ann item ]",
referencedClasses: ["HLSearchReferences", "HLMethodSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:do:", "announcer", "model", "onSearchReferences:", "searchString", "onMethodSelected:", "item"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._model())._announcer();
[$recv($1)._on_do_($globals.HLSearchReferences,(function(ann){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._onSearchReferences_($recv(ann)._searchString());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({ann:ann},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:do:"]=1
//>>excludeEnd("ctx");
][0];
$recv($1)._on_do_($globals.HLMethodSelected,(function(ann){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._onMethodSelected_($recv(ann)._item());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({ann:ann},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesListWidget);

$core.addMethod(
$core.method({
selector: "onMethodSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aMethod"],
source: "onMethodSelected: aMethod\x0a\x09aMethod ifNil: [ ^ self ].\x0a\x09(self items includes: aMethod) ifFalse: [ ^ self ].\x0a\x09\x0a\x09self \x0a\x09\x09selectedItem: aMethod;\x0a\x09\x09activateItem: aMethod",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "ifFalse:", "includes:", "items", "selectedItem:", "activateItem:"]
}, function ($methodClass){ return function (aMethod){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if(aMethod == null || aMethod.a$nil){
return self;
} else {
aMethod;
}
if(!$core.assert($recv($self._items())._includes_(aMethod))){
return self;
}
$self._selectedItem_(aMethod);
$self._activateItem_(aMethod);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onMethodSelected:",{aMethod:aMethod})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesListWidget);

$core.addMethod(
$core.method({
selector: "onSearchReferences:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "onSearchReferences: aString\x0a\x09self subclassResponsibility",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["subclassResponsibility"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._subclassResponsibility();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onSearchReferences:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesListWidget);

$core.addMethod(
$core.method({
selector: "renderItemLabel:on:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aMethod", "html"],
source: "renderItemLabel: aMethod on: html\x0a\x09html with: aMethod asString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "asString"]
}, function ($methodClass){ return function (aMethod,html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(html)._with_($recv(aMethod)._asString());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderItemLabel:on:",{aMethod:aMethod,html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesListWidget);

$core.addMethod(
$core.method({
selector: "reselectItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aMethod"],
source: "reselectItem: aMethod\x0a\x09self selectItem: aMethod",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectItem:"]
}, function ($methodClass){ return function (aMethod){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._selectItem_(aMethod);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"reselectItem:",{aMethod:aMethod})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesListWidget);

$core.addMethod(
$core.method({
selector: "selectItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aMethod"],
source: "selectItem: aMethod\x0a\x09super selectItem: aMethod.\x0a\x09self model selectedClass: nil; selectedMethod: aMethod",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectItem:", "selectedClass:", "model", "selectedMethod:"]
}, function ($methodClass){ return function (aMethod){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._selectItem_.call($self,aMethod))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$1=$self._model();
$recv($1)._selectedClass_(nil);
$recv($1)._selectedMethod_(aMethod);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectItem:",{aMethod:aMethod})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesListWidget);


$core.addMethod(
$core.method({
selector: "on:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aModel"],
source: "on: aModel\x0a\x09^ self new \x0a\x09\x09model: aModel; \x0a\x09\x09yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["model:", "new", "yourself"]
}, function ($methodClass){ return function (aModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._model_(aModel);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{aModel:aModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesListWidget.a$cls);


$core.addClass("HLClassReferencesListWidget", $globals.HLReferencesListWidget, "Helios-References");
$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Class references'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Class references";

}; }),
$globals.HLClassReferencesListWidget);

$core.addMethod(
$core.method({
selector: "onSearchReferences:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "onSearchReferences: aString\x0a\x09self selectItem: nil.\x0a\x09self items: (self model classReferencesOf: aString).\x0a\x09self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectItem:", "items:", "classReferencesOf:", "model", "refresh"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._selectItem_(nil);
$self._items_($recv($self._model())._classReferencesOf_(aString));
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onSearchReferences:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassReferencesListWidget);



$core.addClass("HLImplementorsListWidget", $globals.HLReferencesListWidget, "Helios-References");
$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Implementors'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Implementors";

}; }),
$globals.HLImplementorsListWidget);

$core.addMethod(
$core.method({
selector: "onSearchReferences:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "onSearchReferences: aString\x0a\x09self selectItem: nil.\x0a\x09self items: (self model implementorsOf: aString).\x0a\x09self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectItem:", "items:", "implementorsOf:", "model", "refresh"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._selectItem_(nil);
$self._items_($recv($self._model())._implementorsOf_(aString));
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onSearchReferences:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLImplementorsListWidget);



$core.addClass("HLRegexpListWidget", $globals.HLReferencesListWidget, "Helios-References");
$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Source search'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Source search";

}; }),
$globals.HLRegexpListWidget);

$core.addMethod(
$core.method({
selector: "onSearchReferences:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "onSearchReferences: aString\x0a\x09self selectItem: nil.\x0a\x09self items: (self model regexpReferencesOf: aString).\x0a\x09self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectItem:", "items:", "regexpReferencesOf:", "model", "refresh"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._selectItem_(nil);
$self._items_($recv($self._model())._regexpReferencesOf_(aString));
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onSearchReferences:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRegexpListWidget);



$core.addClass("HLSendersListWidget", $globals.HLReferencesListWidget, "Helios-References");
$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Senders'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Senders";

}; }),
$globals.HLSendersListWidget);

$core.addMethod(
$core.method({
selector: "onSearchReferences:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "onSearchReferences: aString\x0a\x09self selectItem: nil.\x0a\x09self items: (self model sendersOf: aString).\x0a\x09self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectItem:", "items:", "sendersOf:", "model", "refresh"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._selectItem_(nil);
$self._items_($recv($self._model())._sendersOf_(aString));
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onSearchReferences:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSendersListWidget);



$core.addClass("HLReferencesModel", $globals.HLToolModel, "Helios-References");
$core.setSlots($globals.HLReferencesModel, ["methodsCache", "classesAndMetaclassesCache"]);
$core.addMethod(
$core.method({
selector: "allMethods",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "allMethods\x0a\x09^ self methodsCache",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["methodsCache"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $self._methodsCache();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"allMethods",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesModel);

$core.addMethod(
$core.method({
selector: "classReferencesOf:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "classReferencesOf: aString\x0a\x09\x22Answer all methods referencing the class named aString\x22\x0a\x09\x0a\x09^self allMethods select: [ :each |\x0a\x09\x09\x09(each referencedClasses includes: aString) ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["select:", "allMethods", "includes:", "referencedClasses"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._allMethods())._select_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv(each)._referencedClasses())._includes_(aString);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"classReferencesOf:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesModel);

$core.addMethod(
$core.method({
selector: "classesAndMetaclasses",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "classesAndMetaclasses\x0a\x09^ self classesAndMetaclassesCache",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["classesAndMetaclassesCache"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $self._classesAndMetaclassesCache();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"classesAndMetaclasses",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesModel);

$core.addMethod(
$core.method({
selector: "classesAndMetaclassesCache",
protocol: "cache",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "classesAndMetaclassesCache\x0a\x09classesAndMetaclassesCache ifNil: [ self updateClassesAndMetaclassesCache ].\x0a\x09^ classesAndMetaclassesCache",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "updateClassesAndMetaclassesCache"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.classesAndMetaclassesCache;
if($1 == null || $1.a$nil){
$self._updateClassesAndMetaclassesCache();
} else {
$1;
}
return $self.classesAndMetaclassesCache;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"classesAndMetaclassesCache",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesModel);

$core.addMethod(
$core.method({
selector: "implementorsOf:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "implementorsOf: aString\x0a\x09^ self allMethods select: [ :each |\x0a\x09\x09each selector = aString ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["select:", "allMethods", "=", "selector"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._allMethods())._select_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv(each)._selector()).__eq(aString);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"implementorsOf:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesModel);

$core.addMethod(
$core.method({
selector: "isReferencesModel",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isReferencesModel\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLReferencesModel);

$core.addMethod(
$core.method({
selector: "methodsCache",
protocol: "cache",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "methodsCache\x0a\x09methodsCache ifNil: [ self updateMethodsCache ].\x0a\x09^ methodsCache",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "updateMethodsCache"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.methodsCache;
if($1 == null || $1.a$nil){
$self._updateMethodsCache();
} else {
$1;
}
return $self.methodsCache;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"methodsCache",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesModel);

$core.addMethod(
$core.method({
selector: "openClassNamed:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "openClassNamed: aString\x0a\x09| browser |\x0a\x09\x0a\x09self withChangesDo: [\x0a\x09\x09browser := HLBrowser openAsTab.\x0a\x09\x09browser openClassNamed: aString ]",
referencedClasses: ["HLBrowser"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "openAsTab", "openClassNamed:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
var browser;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
browser=$recv($globals.HLBrowser)._openAsTab();
return $recv(browser)._openClassNamed_(aString);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"openClassNamed:",{aString:aString,browser:browser})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesModel);

$core.addMethod(
$core.method({
selector: "openMethod",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "openMethod\x0a\x09| browser |\x0a\x09\x0a\x09self selectedMethod ifNil: [ ^ self ].\x0a\x09\x0a\x09self withChangesDo: [\x0a\x09\x09browser := HLBrowser openAsTab.\x0a\x09\x09browser openMethod: self selectedMethod ]",
referencedClasses: ["HLBrowser"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "selectedMethod", "withChangesDo:", "openAsTab", "openMethod:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var browser;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=[$self._selectedMethod()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedMethod"]=1
//>>excludeEnd("ctx");
][0];
if($1 == null || $1.a$nil){
return self;
} else {
$1;
}
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
browser=$recv($globals.HLBrowser)._openAsTab();
return $recv(browser)._openMethod_($self._selectedMethod());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"openMethod",{browser:browser})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesModel);

$core.addMethod(
$core.method({
selector: "regexpReferencesOf:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "regexpReferencesOf: aString\x0a\x09^ self allMethods select: [ :each |\x0a\x09\x09each source match: aString ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["select:", "allMethods", "match:", "source"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._allMethods())._select_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv(each)._source())._match_(aString);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"regexpReferencesOf:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesModel);

$core.addMethod(
$core.method({
selector: "search:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "search: aString\x0a\x09self updateCaches.\x0a\x09\x0a\x09self announcer announce: (HLSearchReferences new\x0a\x09\x09searchString: aString;\x0a\x09\x09yourself)",
referencedClasses: ["HLSearchReferences"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["updateCaches", "announce:", "announcer", "searchString:", "new", "yourself"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$self._updateCaches();
$1=$self._announcer();
$2=$recv($globals.HLSearchReferences)._new();
$recv($2)._searchString_(aString);
$recv($1)._announce_($recv($2)._yourself());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"search:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesModel);

$core.addMethod(
$core.method({
selector: "sendersOf:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "sendersOf: aString\x0a\x09^ self allMethods select: [ :each |\x0a\x09\x09each messageSends includes: aString ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["select:", "allMethods", "includes:", "messageSends"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._allMethods())._select_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv(each)._messageSends())._includes_(aString);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"sendersOf:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesModel);

$core.addMethod(
$core.method({
selector: "updateCaches",
protocol: "cache",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "updateCaches\x0a\x09self \x0a\x09\x09updateClassesAndMetaclassesCache;\x0a\x09\x09updateMethodsCache",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["updateClassesAndMetaclassesCache", "updateMethodsCache"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._updateClassesAndMetaclassesCache();
$self._updateMethodsCache();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"updateCaches",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesModel);

$core.addMethod(
$core.method({
selector: "updateClassesAndMetaclassesCache",
protocol: "cache",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "updateClassesAndMetaclassesCache\x0a\x09classesAndMetaclassesCache := OrderedCollection new.\x0a\x09\x0a\x09self environment classes do: [ :eachClass | eachClass includingPossibleMetaDo: [ :each |\x0a\x09\x09classesAndMetaclassesCache add: each ] ]",
referencedClasses: ["OrderedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["new", "do:", "classes", "environment", "includingPossibleMetaDo:", "add:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.classesAndMetaclassesCache=$recv($globals.OrderedCollection)._new();
$recv($recv($self._environment())._classes())._do_((function(eachClass){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(eachClass)._includingPossibleMetaDo_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv($self.classesAndMetaclassesCache)._add_(each);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({each:each},$ctx2,2)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({eachClass:eachClass},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"updateClassesAndMetaclassesCache",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesModel);

$core.addMethod(
$core.method({
selector: "updateMethodsCache",
protocol: "cache",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "updateMethodsCache\x0a\x09methodsCache := OrderedCollection new.\x0a\x09\x0a\x09self classesAndMetaclasses\x0a\x09\x09do: [ :each | methodsCache addAll: each methods ]",
referencedClasses: ["OrderedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["new", "do:", "classesAndMetaclasses", "addAll:", "methods"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.methodsCache=$recv($globals.OrderedCollection)._new();
$recv($self._classesAndMetaclasses())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self.methodsCache)._addAll_($recv(each)._methods());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"updateMethodsCache",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLReferencesModel);


});
