define(["amber/boot", "require", "amber/core/Kernel-Objects", "amber/web/Web", "helios/Helios-Core"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-KeyBindings");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLBinding", $globals.Object, "Helios-KeyBindings");
$core.setSlots($globals.HLBinding, ["key", "label"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLBinding.comment="I am the abstract representation of a keybinding in Helios. My instances hold a key (integer value) and a label. \x0a\x0aBindings are built into a tree of keys, so pressing a key may result in more key choices (for example, to open a workspace, 'o' is pressed first then 'w' is pressed).\x0a\x0aBinding action handling and selection is handled by the `current` instance of `HLKeyBinder`.\x0a\x0aSubclasses implement specific behavior like evaluating actions or (sub-)grouping other bindings.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "apply",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "apply",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return self;

}; }),
$globals.HLBinding);

$core.addMethod(
$core.method({
selector: "atKey:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aKey"],
source: "atKey: aKey\x0a\x09\x22Answer the sub-binding at key aKey.\x0a\x09Always answer nil here. See HLBindingGroup for more.\x22\x0a\x09\x0a\x09^ nil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aKey){
var self=this,$self=this;
return nil;

}; }),
$globals.HLBinding);

$core.addMethod(
$core.method({
selector: "displayLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "displayLabel\x0a\x09^ self label",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["label"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $self._label();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"displayLabel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBinding);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ self subclassResponsibility",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["subclassResponsibility"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $self._subclassResponsibility();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBinding);

$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ key",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.key;

}; }),
$globals.HLBinding);

$core.addMethod(
$core.method({
selector: "key:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInteger"],
source: "key: anInteger\x0a\x09key := anInteger",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anInteger){
var self=this,$self=this;
$self.key=anInteger;
return self;

}; }),
$globals.HLBinding);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ label",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.label;

}; }),
$globals.HLBinding);

$core.addMethod(
$core.method({
selector: "label:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "label: aString\x0a\x09label := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.label=aString;
return self;

}; }),
$globals.HLBinding);

$core.addMethod(
$core.method({
selector: "release",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "release",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return self;

}; }),
$globals.HLBinding);

$core.addMethod(
$core.method({
selector: "renderOn:html:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBindingHelper", "html"],
source: "renderOn: aBindingHelper html: html",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aBindingHelper,html){
var self=this,$self=this;
return self;

}; }),
$globals.HLBinding);

$core.addMethod(
$core.method({
selector: "shortcut",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "shortcut\x0a\x09^ String fromCharCode: self key",
referencedClasses: ["String"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["fromCharCode:", "key"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.String)._fromCharCode_($self._key());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"shortcut",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBinding);


$core.addMethod(
$core.method({
selector: "on:labelled:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInteger", "aString"],
source: "on: anInteger labelled: aString\x0a\x09^ self new\x0a    \x09key: anInteger;\x0a        label: aString;\x0a        yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["key:", "new", "label:", "yourself"]
}, function ($methodClass){ return function (anInteger,aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._key_(anInteger);
$recv($1)._label_(aString);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:labelled:",{anInteger:anInteger,aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBinding.a$cls);


$core.addClass("HLBindingAction", $globals.HLBinding, "Helios-KeyBindings");
$core.setSlots($globals.HLBindingAction, ["command"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLBindingAction.comment="My instances are the leafs of the binding tree. They evaluate actions through commands, instances of concrete subclasses of `HLCommand`.\x0a\x0aThe `#apply` methods is used to evaluate the `command`. If the command requires user input, an `inputWidget` will be displayed to the user.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "apply",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "apply\x0a\x09self command isInputRequired\x0a\x09\x09ifTrue: [ HLKeyBinder current helper showWidget: self inputWidget ]\x0a\x09\x09ifFalse: [ self executeCommand ]",
referencedClasses: ["HLKeyBinder"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:ifFalse:", "isInputRequired", "command", "showWidget:", "helper", "current", "inputWidget", "executeCommand"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv($self._command())._isInputRequired())){
$recv($recv($recv($globals.HLKeyBinder)._current())._helper())._showWidget_($self._inputWidget());
} else {
$self._executeCommand();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"apply",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingAction);

$core.addMethod(
$core.method({
selector: "command",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "command\x0a\x09^ command",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.command;

}; }),
$globals.HLBindingAction);

$core.addMethod(
$core.method({
selector: "command:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCommand"],
source: "command: aCommand\x0a\x09command := aCommand",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aCommand){
var self=this,$self=this;
$self.command=aCommand;
return self;

}; }),
$globals.HLBindingAction);

$core.addMethod(
$core.method({
selector: "executeCommand",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "executeCommand\x0a\x09self command execute.\x0a\x09HLKeyBinder current deactivate",
referencedClasses: ["HLKeyBinder"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["execute", "command", "deactivate", "current"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._command())._execute();
$recv($recv($globals.HLKeyBinder)._current())._deactivate();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"executeCommand",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingAction);

$core.addMethod(
$core.method({
selector: "input:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "input: aString\x0a\x09self command input: aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["input:", "command"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._command())._input_(aString);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"input:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingAction);

$core.addMethod(
$core.method({
selector: "inputBinding",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inputBinding\x0a\x09^ HLBindingInput new\x0a\x09\x09label: self command inputLabel;\x0a\x09\x09ghostText: self command displayLabel;\x0a\x09\x09defaultValue: self command defaultInput;\x0a\x09\x09inputCompletion: self command inputCompletion;\x0a\x09\x09callback: [ :val | \x0a\x09\x09\x09self command \x0a\x09\x09\x09\x09input: val;\x0a\x09\x09\x09\x09execute ];\x0a\x09\x09yourself",
referencedClasses: ["HLBindingInput"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["label:", "new", "inputLabel", "command", "ghostText:", "displayLabel", "defaultValue:", "defaultInput", "inputCompletion:", "inputCompletion", "callback:", "input:", "execute", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$recv($globals.HLBindingInput)._new();
$recv($1)._label_($recv([$self._command()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["command"]=1
//>>excludeEnd("ctx");
][0])._inputLabel());
$recv($1)._ghostText_($recv([$self._command()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["command"]=2
//>>excludeEnd("ctx");
][0])._displayLabel());
$recv($1)._defaultValue_($recv([$self._command()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["command"]=3
//>>excludeEnd("ctx");
][0])._defaultInput());
$recv($1)._inputCompletion_($recv([$self._command()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["command"]=4
//>>excludeEnd("ctx");
][0])._inputCompletion());
$recv($1)._callback_((function(val){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$2=$self._command();
$recv($2)._input_(val);
return $recv($2)._execute();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({val:val},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inputBinding",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingAction);

$core.addMethod(
$core.method({
selector: "inputWidget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inputWidget\x0a\x09^ HLBindingActionInputWidget new\x0a\x09\x09ghostText: self command displayLabel;\x0a\x09\x09defaultValue: self command defaultInput;\x0a\x09\x09inputCompletion: self command inputCompletion;\x0a\x09\x09callback: [ :value | \x0a\x09\x09\x09self \x0a\x09\x09\x09\x09input: value;\x0a\x09\x09\x09\x09executeCommand ];\x0a\x09\x09yourself",
referencedClasses: ["HLBindingActionInputWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ghostText:", "new", "displayLabel", "command", "defaultValue:", "defaultInput", "inputCompletion:", "inputCompletion", "callback:", "input:", "executeCommand", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLBindingActionInputWidget)._new();
$recv($1)._ghostText_($recv([$self._command()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["command"]=1
//>>excludeEnd("ctx");
][0])._displayLabel());
$recv($1)._defaultValue_($recv([$self._command()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["command"]=2
//>>excludeEnd("ctx");
][0])._defaultInput());
$recv($1)._inputCompletion_($recv($self._command())._inputCompletion());
$recv($1)._callback_((function(value){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$self._input_(value);
return $self._executeCommand();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({value:value},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inputWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingAction);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ self command isActive",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["isActive", "command"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._command())._isActive();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingAction);



$core.addClass("HLBindingGroup", $globals.HLBinding, "Helios-KeyBindings");
$core.setSlots($globals.HLBindingGroup, ["bindings"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLBindingGroup.comment="My instances hold other bindings, either actions or groups, and do not have actions by themselves.\x0a\x0aChildren are accessed with `atKey:` and added with the `add*` methods.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "activeBindings",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "activeBindings\x0a\x09^ self bindings select: [ :each | each isActive ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["select:", "bindings", "isActive"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._bindings())._select_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._isActive();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activeBindings",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingGroup);

$core.addMethod(
$core.method({
selector: "add:",
protocol: "adding",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBinding"],
source: "add: aBinding\x0a\x09^ self bindings add: aBinding",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["add:", "bindings"]
}, function ($methodClass){ return function (aBinding){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._bindings())._add_(aBinding);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"add:",{aBinding:aBinding})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingGroup);

$core.addMethod(
$core.method({
selector: "addActionKey:labelled:callback:",
protocol: "adding",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInteger", "aString", "aBlock"],
source: "addActionKey: anInteger labelled: aString callback: aBlock\x0a\x09self add: ((HLBindingAction on: anInteger labelled: aString)\x0a    \x09callback: aBlock;\x0a        yourself)",
referencedClasses: ["HLBindingAction"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["add:", "callback:", "on:labelled:", "yourself"]
}, function ($methodClass){ return function (anInteger,aString,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLBindingAction)._on_labelled_(anInteger,aString);
$recv($1)._callback_(aBlock);
$self._add_($recv($1)._yourself());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"addActionKey:labelled:callback:",{anInteger:anInteger,aString:aString,aBlock:aBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingGroup);

$core.addMethod(
$core.method({
selector: "addGroupKey:labelled:",
protocol: "add",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInteger", "aString"],
source: "addGroupKey: anInteger labelled: aString\x0a\x09self add: (HLBindingGroup on: anInteger labelled: aString)",
referencedClasses: ["HLBindingGroup"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["add:", "on:labelled:"]
}, function ($methodClass){ return function (anInteger,aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._add_($recv($globals.HLBindingGroup)._on_labelled_(anInteger,aString));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"addGroupKey:labelled:",{anInteger:anInteger,aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingGroup);

$core.addMethod(
$core.method({
selector: "at:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "at: aString\x0a\x09^ self bindings \x0a    \x09detect: [ :each | each label = aString ]\x0a      \x09ifNone: [ nil ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["detect:ifNone:", "bindings", "=", "label"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._bindings())._detect_ifNone_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv(each)._label()).__eq(aString);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}),(function(){
return nil;

}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"at:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingGroup);

$core.addMethod(
$core.method({
selector: "at:add:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString", "aBinding"],
source: "at: aString add: aBinding\x0a\x09| binding |\x0a\x09\x0a\x09binding := self at: aString.\x0a\x09binding ifNil: [ ^ self ].\x0a\x09\x09\x0a\x09binding add: aBinding",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["at:", "ifNil:", "add:"]
}, function ($methodClass){ return function (aString,aBinding){
var self=this,$self=this;
var binding;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
binding=$self._at_(aString);
$1=binding;
if($1 == null || $1.a$nil){
return self;
} else {
$1;
}
$recv(binding)._add_(aBinding);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"at:add:",{aString:aString,aBinding:aBinding,binding:binding})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingGroup);

$core.addMethod(
$core.method({
selector: "atKey:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInteger"],
source: "atKey: anInteger\x0a\x09^ self bindings \x0a    \x09detect: [ :each | each key = anInteger ]\x0a      \x09ifNone: [ nil ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["detect:ifNone:", "bindings", "=", "key"]
}, function ($methodClass){ return function (anInteger){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._bindings())._detect_ifNone_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv(each)._key()).__eq(anInteger);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}),(function(){
return nil;

}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"atKey:",{anInteger:anInteger})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingGroup);

$core.addMethod(
$core.method({
selector: "bindings",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "bindings\x0a\x09^ bindings ifNil: [ bindings := OrderedCollection new ]",
referencedClasses: ["OrderedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.bindings;
if($1 == null || $1.a$nil){
$self.bindings=$recv($globals.OrderedCollection)._new();
return $self.bindings;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"bindings",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingGroup);

$core.addMethod(
$core.method({
selector: "displayLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "displayLabel\x0a\x09^ super displayLabel, '...'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: [",", "displayLabel"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv([(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._displayLabel.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0]).__comma("...");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"displayLabel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingGroup);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ self activeBindings notEmpty",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["notEmpty", "activeBindings"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._activeBindings())._notEmpty();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingGroup);

$core.addMethod(
$core.method({
selector: "release",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "release\x0a\x09self bindings do: [ :each | each release ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "bindings", "release"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._bindings())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._release();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"release",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingGroup);

$core.addMethod(
$core.method({
selector: "renderOn:html:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBindingHelper", "html"],
source: "renderOn: aBindingHelper html: html\x0a\x09self isActive ifTrue: [\x0a\x09\x09aBindingHelper renderBindingGroup: self on: html ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "isActive", "renderBindingGroup:on:"]
}, function ($methodClass){ return function (aBindingHelper,html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($self._isActive())){
$recv(aBindingHelper)._renderBindingGroup_on_(self,html);
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderOn:html:",{aBindingHelper:aBindingHelper,html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingGroup);



$core.addClass("HLBindingActionInputWidget", $globals.Widget, "Helios-KeyBindings");
$core.setSlots($globals.HLBindingActionInputWidget, ["input", "callback", "status", "wrapper", "ghostText", "message", "inputCompletion", "defaultValue", "messageTag"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLBindingActionInputWidget.comment="My instances are built when a `HLBindingAction` that requires user input is applied.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "callback",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "callback\x0a\x09^ callback ifNil: [ callback := [ :value | ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.callback;
if($1 == null || $1.a$nil){
$self.callback=(function(value){

});
return $self.callback;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"callback",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingActionInputWidget);

$core.addMethod(
$core.method({
selector: "callback:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBlock"],
source: "callback: aBlock\x0a\x09callback := aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aBlock){
var self=this,$self=this;
$self.callback=aBlock;
return self;

}; }),
$globals.HLBindingActionInputWidget);

$core.addMethod(
$core.method({
selector: "clearStatus",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "clearStatus\x0a\x09self status: 'info'.\x0a\x09self message: ''.\x0a\x09self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["status:", "message:", "refresh"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._status_("info");
$self._message_("");
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"clearStatus",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingActionInputWidget);

$core.addMethod(
$core.method({
selector: "defaultValue",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "defaultValue\x0a\x09^ defaultValue ifNil: [ '' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.defaultValue;
if($1 == null || $1.a$nil){
return "";
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"defaultValue",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingActionInputWidget);

$core.addMethod(
$core.method({
selector: "defaultValue:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "defaultValue: aString\x0a\x09defaultValue := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.defaultValue=aString;
return self;

}; }),
$globals.HLBindingActionInputWidget);

$core.addMethod(
$core.method({
selector: "errorStatus",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "errorStatus\x0a\x09self status: 'error'.\x0a\x09self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["status:", "refresh"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._status_("error");
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"errorStatus",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingActionInputWidget);

$core.addMethod(
$core.method({
selector: "evaluate:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "evaluate: aString\x09\x0a\x09[ self callback value: aString ]\x0a\x09\x09on: Error\x0a\x09\x09do: [ :ex |\x0a\x09\x09\x09self input asJQuery \x0a\x09\x09\x09\x09one: 'keydown' \x0a\x09\x09\x09\x09do: [ self clearStatus ].\x0a\x09\x09\x09self message: ex messageText.\x0a\x09\x09\x09self errorStatus ]",
referencedClasses: ["Error"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:do:", "value:", "callback", "one:do:", "asJQuery", "input", "clearStatus", "message:", "messageText", "errorStatus"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._callback())._value_(aString);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._on_do_($globals.Error,(function(ex){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$recv($recv($self._input())._asJQuery())._one_do_("keydown",(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._clearStatus();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,3)});
//>>excludeEnd("ctx");
}));
$self._message_($recv(ex)._messageText());
return $self._errorStatus();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({ex:ex},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"evaluate:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingActionInputWidget);

$core.addMethod(
$core.method({
selector: "ghostText",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "ghostText\x0a\x09^ ghostText",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.ghostText;

}; }),
$globals.HLBindingActionInputWidget);

$core.addMethod(
$core.method({
selector: "ghostText:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aText"],
source: "ghostText: aText\x0a\x09ghostText := aText",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aText){
var self=this,$self=this;
$self.ghostText=aText;
return self;

}; }),
$globals.HLBindingActionInputWidget);

$core.addMethod(
$core.method({
selector: "input",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "input\x0a\x09^ input",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.input;

}; }),
$globals.HLBindingActionInputWidget);

$core.addMethod(
$core.method({
selector: "inputCompletion",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inputCompletion\x0a\x09^ inputCompletion ifNil: [ #() ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.inputCompletion;
if($1 == null || $1.a$nil){
return [];
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inputCompletion",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingActionInputWidget);

$core.addMethod(
$core.method({
selector: "inputCompletion:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection"],
source: "inputCompletion: aCollection\x0a\x09inputCompletion := aCollection",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aCollection){
var self=this,$self=this;
$self.inputCompletion=aCollection;
return self;

}; }),
$globals.HLBindingActionInputWidget);

$core.addMethod(
$core.method({
selector: "message",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "message\x0a\x09^ message ifNil: [ message := '' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.message;
if($1 == null || $1.a$nil){
$self.message="";
return $self.message;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"message",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingActionInputWidget);

$core.addMethod(
$core.method({
selector: "message:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "message: aString\x0a\x09message := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.message=aString;
return self;

}; }),
$globals.HLBindingActionInputWidget);

$core.addMethod(
$core.method({
selector: "refresh",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "refresh\x0a\x09wrapper ifNil: [ ^ self ].\x0a    \x0a\x09wrapper class: self status.\x0a\x09messageTag contents: self message",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "class:", "status", "contents:", "message"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.wrapper;
if($1 == null || $1.a$nil){
return self;
} else {
$1;
}
$recv($self.wrapper)._class_($self._status());
$recv($self.messageTag)._contents_($self._message());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"refresh",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingActionInputWidget);

$core.addMethod(
$core.method({
selector: "renderOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderOn: html\x0a\x09wrapper ifNil: [ wrapper := html span ].\x0a\x0a\x09wrapper \x0a\x09\x09class: self status;\x0a\x09\x09with: [\x0a\x09\x09\x09input := html input\x0a\x09\x09\x09\x09placeholder: self ghostText;\x0a\x09\x09\x09\x09value: self defaultValue;\x0a\x09\x09\x09\x09onKeyDown: [ :event | \x0a\x09\x09\x09\x09\x09event which = 13 ifTrue: [\x0a\x09\x09\x09\x09\x09\x09self evaluate: input asJQuery val ] ]\x0a\x09\x09\x09\x09yourself.\x0a\x09\x09\x09input asJQuery \x0a\x09\x09\x09\x09typeahead: #{ 'source' -> self inputCompletion }.\x0a\x09\x09\x09messageTag := (html span\x0a\x09\x09\x09\x09class: 'help-inline';\x0a\x09\x09\x09\x09with: self message;\x0a\x09\x09\x09\x09yourself) ].\x0a\x09\x0a\x09\x22Evaluate with a timeout to ensure focus.\x0a\x09Commands can be executed from a menu, clicking on the menu to\x0a\x09evaluate the command would give it the focus otherwise\x22\x0a\x09\x0a\x09[ input asJQuery focus; select ] fork",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "span", "class:", "status", "with:", "placeholder:", "input", "ghostText", "value:", "defaultValue", "onKeyDown:", "yourself", "ifTrue:", "=", "which", "evaluate:", "val", "asJQuery", "typeahead:", "inputCompletion", "message", "fork", "focus", "select"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3,$4,$5;
$1=$self.wrapper;
if($1 == null || $1.a$nil){
$self.wrapper=[$recv(html)._span()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["span"]=1
//>>excludeEnd("ctx");
][0];
$self.wrapper;
} else {
$1;
}
$2=$self.wrapper;
[$recv($2)._class_($self._status())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($2)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$3=$recv(html)._input();
$recv($3)._placeholder_($self._ghostText());
$recv($3)._value_($self._defaultValue());
$self.input=$recv($3)._onKeyDown_([$recv((function(event){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
if($core.assert($recv($recv(event)._which()).__eq((13)))){
return $self._evaluate_($recv([$recv($self.input)._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._val());
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({event:event},$ctx2,3)});
//>>excludeEnd("ctx");
}))._yourself()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["yourself"]=1
//>>excludeEnd("ctx");
][0]);
$recv([$recv($self.input)._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["asJQuery"]=2
//>>excludeEnd("ctx");
][0])._typeahead_($globals.HashedCollection._newFromPairs_(["source",$self._inputCompletion()]));
$4=$recv(html)._span();
$recv($4)._class_("help-inline");
$recv($4)._with_($self._message());
$self.messageTag=$recv($4)._yourself();
return $self.messageTag;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$5=$recv($self.input)._asJQuery();
$recv($5)._focus();
return $recv($5)._select();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,5)});
//>>excludeEnd("ctx");
}))._fork();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingActionInputWidget);

$core.addMethod(
$core.method({
selector: "status",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "status\x0a\x09^ status ifNil: [ status := 'info' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.status;
if($1 == null || $1.a$nil){
$self.status="info";
return $self.status;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"status",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBindingActionInputWidget);

$core.addMethod(
$core.method({
selector: "status:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aStatus"],
source: "status: aStatus\x0a\x09status := aStatus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aStatus){
var self=this,$self=this;
$self.status=aStatus;
return self;

}; }),
$globals.HLBindingActionInputWidget);



$core.addClass("HLKeyBinder", $globals.Object, "Helios-KeyBindings");
$core.setSlots($globals.HLKeyBinder, ["modifierKey", "helper", "bindings", "selectedBinding"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLKeyBinder.comment="My `current` instance holds keybindings for Helios actions and evaluate them.\x0a\x0aBindings can be nested by groups. The `bindings` instance variable holds the root of the key bindings tree.\x0a\x0aBindings are instances of a concrete subclass of `HLBinding`.\x0a\x0aI am always either in 'active' or 'inactive' state. In active state I capture key down events and my `helper` widget is displayed at the bottom of the window. My `selectedBinding`, if any, is displayed by the helper.\x0a\x0aBindings are evaluated through `applyBinding:`. If a binding is final (not a group of other bindings), evaluating it will result in deactivating the binder, and hiding the `helper` widget.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "activate",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "activate\x0a\x09self helper show",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["show", "helper"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._helper())._show();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activate",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "activateSpotlight",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "activateSpotlight\x0a\x09^ '.spotlight' asJQuery focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focus", "asJQuery"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv(".spotlight"._asJQuery())._focus();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activateSpotlight",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "activationKey",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "activationKey\x0a\x09\x22SPACE\x22\x0a\x09^ 32",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return (32);

}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "activationKeyLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "activationKeyLabel\x0a\x09^ 'ctrl + space'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "ctrl + space";

}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "applyBinding:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBinding"],
source: "applyBinding: aBinding\x0a\x09aBinding isActive ifFalse: [ ^ self ].\x0a\x09\x0a\x09self selectBinding: aBinding.\x0a    aBinding apply",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifFalse:", "isActive", "selectBinding:", "apply"]
}, function ($methodClass){ return function (aBinding){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if(!$core.assert($recv(aBinding)._isActive())){
return self;
}
$self._selectBinding_(aBinding);
$recv(aBinding)._apply();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"applyBinding:",{aBinding:aBinding})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "bindings",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "bindings\x0a\x09^ bindings ifNil: [ bindings := self defaultBindings ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "defaultBindings"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.bindings;
if($1 == null || $1.a$nil){
$self.bindings=$self._defaultBindings();
return $self.bindings;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"bindings",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "deactivate",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "deactivate\x0a\x09selectedBinding ifNotNil: [ selectedBinding release ].\x0a    selectedBinding := nil.\x0a\x09self helper hide",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "release", "hide", "helper"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.selectedBinding;
if($1 == null || $1.a$nil){
$1;
} else {
$recv($self.selectedBinding)._release();
}
$self.selectedBinding=nil;
$recv($self._helper())._hide();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"deactivate",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "defaultBindings",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "defaultBindings\x0a\x09| group |\x0a\x09\x0a\x09group := HLBindingGroup new\x0a\x09\x09add: HLCloseTabCommand new asBinding;\x0a\x09\x09add: HLSwitchTabCommand new asBinding;\x0a\x09\x09yourself.\x0a\x09\x09\x0a\x09HLOpenCommand registerConcreteClassesOn: group.\x0a\x09\x09\x09\x09\x0a\x09^ group",
referencedClasses: ["HLBindingGroup", "HLCloseTabCommand", "HLSwitchTabCommand", "HLOpenCommand"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["add:", "new", "asBinding", "yourself", "registerConcreteClassesOn:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var group;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=[$recv($globals.HLBindingGroup)._new()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["new"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._add_([$recv([$recv($globals.HLCloseTabCommand)._new()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["new"]=2
//>>excludeEnd("ctx");
][0])._asBinding()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asBinding"]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["add:"]=1
//>>excludeEnd("ctx");
][0];
$recv($1)._add_($recv($recv($globals.HLSwitchTabCommand)._new())._asBinding());
group=$recv($1)._yourself();
$recv($globals.HLOpenCommand)._registerConcreteClassesOn_(group);
return group;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"defaultBindings",{group:group})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "escapeKey",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "escapeKey\x0a\x09\x22ESC\x22\x0a\x09^ 27",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return (27);

}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "flushBindings",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "flushBindings\x0a\x09bindings := nil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
$self.bindings=nil;
return self;

}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "handleActiveKeyDown:",
protocol: "events",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["event"],
source: "handleActiveKeyDown: event\x0a\x0a\x09\x22ESC, ctrl+g ctrl+space deactivate the keyBinder\x22\x0a\x09(event which = self escapeKey or: [\x0a\x09\x09(event which = 71 or: [ event which = self activationKey ]) \x0a\x09\x09\x09and: [ event ctrlKey ] ])\x0a        \x09\x09ifTrue: [ \x0a           \x09\x09\x09self deactivate.\x0a\x09\x09\x09\x09\x09event preventDefault.\x0a\x09\x09\x09\x09\x09^ false ].\x0a            \x0a    \x22Handle the keybinding\x22\x0a    ^ self handleBindingFor: event",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "or:", "=", "which", "escapeKey", "and:", "activationKey", "ctrlKey", "deactivate", "preventDefault", "handleBindingFor:"]
}, function ($methodClass){ return function (event){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
if($core.assert([$recv([$recv(event)._which()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["which"]=1
//>>excludeEnd("ctx");
][0]).__eq($self._escapeKey())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["="]=1
//>>excludeEnd("ctx");
][0])){
$2=true;
} else {
if($core.assert([$recv([$recv(event)._which()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["which"]=2
//>>excludeEnd("ctx");
][0]).__eq((71))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["="]=2
//>>excludeEnd("ctx");
][0])){
$1=true;
} else {
$1=$recv($recv(event)._which()).__eq($self._activationKey());
}
if($core.assert($1)){
$2=$recv(event)._ctrlKey();
} else {
$2=false;
}
}
if($core.assert($2)){
$self._deactivate();
$recv(event)._preventDefault();
return false;
}
return $self._handleBindingFor_(event);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"handleActiveKeyDown:",{event:event})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "handleBindingFor:",
protocol: "events",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anEvent"],
source: "handleBindingFor: anEvent\x0a\x09| binding |\x0a    binding := self selectedBinding atKey: anEvent which.\x0a    \x0a    binding ifNotNil: [ \x0a    \x09self applyBinding: binding.\x0a\x09\x09anEvent preventDefault.\x0a\x09\x09^ false ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["atKey:", "selectedBinding", "which", "ifNotNil:", "applyBinding:", "preventDefault"]
}, function ($methodClass){ return function (anEvent){
var self=this,$self=this;
var binding;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
binding=$recv($self._selectedBinding())._atKey_($recv(anEvent)._which());
$1=binding;
if($1 == null || $1.a$nil){
$1;
} else {
$self._applyBinding_(binding);
$recv(anEvent)._preventDefault();
return false;
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"handleBindingFor:",{anEvent:anEvent,binding:binding})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "handleInactiveKeyDown:",
protocol: "events",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["event"],
source: "handleInactiveKeyDown: event\x0a\x09event which = self activationKey ifTrue: [\x0a    \x09event ctrlKey ifTrue: [\x0a\x09\x09\x09self activate. \x0a            event preventDefault. \x0a            ^ false ] ].\x0a\x09\x09\x09\x0a\x09event which = self spotlightActivationKey ifTrue: [\x0a    \x09event ctrlKey ifTrue: [\x0a\x09\x09\x09self activateSpotlight. \x0a            event preventDefault. \x0a            ^ false ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "=", "which", "activationKey", "ctrlKey", "activate", "preventDefault", "spotlightActivationKey", "activateSpotlight"]
}, function ($methodClass){ return function (event){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert([$recv([$recv(event)._which()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["which"]=1
//>>excludeEnd("ctx");
][0]).__eq($self._activationKey())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["="]=1
//>>excludeEnd("ctx");
][0])){
if($core.assert([$recv(event)._ctrlKey()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["ctrlKey"]=1
//>>excludeEnd("ctx");
][0])){
$self._activate();
[$recv(event)._preventDefault()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["preventDefault"]=1
//>>excludeEnd("ctx");
][0];
return false;
}
}
if($core.assert($recv($recv(event)._which()).__eq($self._spotlightActivationKey()))){
if($core.assert($recv(event)._ctrlKey())){
$self._activateSpotlight();
$recv(event)._preventDefault();
return false;
}
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"handleInactiveKeyDown:",{event:event})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "handleKeyDown:",
protocol: "events",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["event"],
source: "handleKeyDown: event\x0a\x09^ self isActive\x0a    \x09ifTrue: [ self handleActiveKeyDown: event ]\x0a      \x09ifFalse: [ self handleInactiveKeyDown: event ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:ifFalse:", "isActive", "handleActiveKeyDown:", "handleInactiveKeyDown:"]
}, function ($methodClass){ return function (event){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($self._isActive())){
return $self._handleActiveKeyDown_(event);
} else {
return $self._handleInactiveKeyDown_(event);
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"handleKeyDown:",{event:event})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "helper",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "helper\x0a\x09^ helper",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.helper;

}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "initialize",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initialize\x0a\x09super initialize.\x0a\x09helper := HLKeyBinderHelperWidget on: self",
referencedClasses: ["HLKeyBinderHelperWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initialize", "on:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._initialize.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$self.helper=$recv($globals.HLKeyBinderHelperWidget)._on_(self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initialize",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ ('.', self helper cssClass) asJQuery is: ':visible'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["is:", "asJQuery", ",", "cssClass", "helper"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv(".".__comma($recv($self._helper())._cssClass()))._asJQuery())._is_(":visible");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "selectBinding:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBinding"],
source: "selectBinding: aBinding\x0a\x09aBinding = selectedBinding ifTrue: [ ^ self ].\x0a\x09\x0a\x09selectedBinding := aBinding.\x0a\x09self helper refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "=", "refresh", "helper"]
}, function ($methodClass){ return function (aBinding){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv(aBinding).__eq($self.selectedBinding))){
return self;
}
$self.selectedBinding=aBinding;
$recv($self._helper())._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectBinding:",{aBinding:aBinding})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "selectedBinding",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectedBinding\x0a\x09^ selectedBinding ifNil: [ self bindings ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "bindings"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.selectedBinding;
if($1 == null || $1.a$nil){
return $self._bindings();
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectedBinding",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "setupEvents",
protocol: "events",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setupEvents\x0a\x09'body' asJQuery keydown: [ :event | self handleKeyDown: event ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["keydown:", "asJQuery", "handleKeyDown:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv("body"._asJQuery())._keydown_((function(event){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._handleKeyDown_(event);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({event:event},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setupEvents",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "spotlightActivationKey",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "spotlightActivationKey\x0a\x09\x22f\x22\x0a\x09^ 70",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return (70);

}; }),
$globals.HLKeyBinder);

$core.addMethod(
$core.method({
selector: "systemIsMac",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "systemIsMac\x0a\x09^ navigator platform match: 'Mac'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["match:", "platform"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv(navigator)._platform())._match_("Mac");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"systemIsMac",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder);


$core.setSlots($globals.HLKeyBinder.a$cls, ["current"]);
$core.addMethod(
$core.method({
selector: "current",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "current\x0a\x09^ current ifNil: [ current := super new ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.current;
if($1 == null || $1.a$nil){
$self.current=[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._new.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
return $self.current;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"current",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder.a$cls);

$core.addMethod(
$core.method({
selector: "new",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "new\x0a\x09self shouldNotImplement",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["shouldNotImplement"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._shouldNotImplement();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"new",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinder.a$cls);


$core.addClass("HLKeyBinderHelperWidget", $globals.HLWidget, "Helios-KeyBindings");
$core.setSlots($globals.HLKeyBinderHelperWidget, ["keyBinder"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLKeyBinderHelperWidget.comment="I am the widget responsible for displaying active keybindings in a bar at the bottom of the window. Each keybinding is an instance of `HLBinding`. \x0a\x0aRendering is done through a double dispatch, see `#renderSelectedBindingOn:`.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "cssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "cssClass\x0a\x09^ 'key_helper'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "key_helper";

}; }),
$globals.HLKeyBinderHelperWidget);

$core.addMethod(
$core.method({
selector: "deactivate",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "deactivate\x0a\x09self keyBinder deactivate",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["deactivate", "keyBinder"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._keyBinder())._deactivate();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"deactivate",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinderHelperWidget);

$core.addMethod(
$core.method({
selector: "hide",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "hide\x0a\x09('.', self cssClass) asJQuery remove.\x0a\x09'.helper_overlay' asJQuery remove.\x0a\x09self showCog",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["remove", "asJQuery", ",", "cssClass", "showCog"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$recv([$recv(".".__comma($self._cssClass()))._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._remove()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["remove"]=1
//>>excludeEnd("ctx");
][0];
$recv(".helper_overlay"._asJQuery())._remove();
$self._showCog();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"hide",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinderHelperWidget);

$core.addMethod(
$core.method({
selector: "hideCog",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "hideCog\x0a\x09'#cog-helper' asJQuery hide",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["hide", "asJQuery"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv("#cog-helper"._asJQuery())._hide();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"hideCog",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinderHelperWidget);

$core.addMethod(
$core.method({
selector: "keyBinder",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "keyBinder\x0a\x09^ keyBinder",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.keyBinder;

}; }),
$globals.HLKeyBinderHelperWidget);

$core.addMethod(
$core.method({
selector: "keyBinder:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aKeyBinder"],
source: "keyBinder: aKeyBinder\x0a\x09keyBinder := aKeyBinder",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aKeyBinder){
var self=this,$self=this;
$self.keyBinder=aKeyBinder;
return self;

}; }),
$globals.HLKeyBinderHelperWidget);

$core.addMethod(
$core.method({
selector: "mainId",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "mainId\x0a\x09^ 'binding-helper-main'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "binding-helper-main";

}; }),
$globals.HLKeyBinderHelperWidget);

$core.addMethod(
$core.method({
selector: "renderBindingActionFor:on:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBinding", "html"],
source: "renderBindingActionFor: aBinding on: html\x0a\x09html span class: 'command'; with: [\x0a\x09\x09html strong \x0a\x09\x09\x09class: 'label'; \x0a\x09\x09\x09with: aBinding shortcut asLowercase.\x0a  \x09\x09html a \x0a        \x09class: 'action'; \x0a            with: aBinding displayLabel;\x0a  \x09\x09\x09onClick: [ self keyBinder applyBinding: aBinding ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "span", "with:", "strong", "asLowercase", "shortcut", "a", "displayLabel", "onClick:", "applyBinding:", "keyBinder"]
}, function ($methodClass){ return function (aBinding,html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3;
$1=$recv(html)._span();
[$recv($1)._class_("command")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$2=$recv(html)._strong();
[$recv($2)._class_("label")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["class:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($2)._with_($recv($recv(aBinding)._shortcut())._asLowercase())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=2
//>>excludeEnd("ctx");
][0];
$3=$recv(html)._a();
$recv($3)._class_("action");
$recv($3)._with_($recv(aBinding)._displayLabel());
return $recv($3)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv($self._keyBinder())._applyBinding_(aBinding);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderBindingActionFor:on:",{aBinding:aBinding,html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinderHelperWidget);

$core.addMethod(
$core.method({
selector: "renderBindingGroup:on:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBindingGroup", "html"],
source: "renderBindingGroup: aBindingGroup on: html\x0a\x09(aBindingGroup activeBindings \x0a    \x09sorted: [ :a :b | a key < b key ])\x0a        do: [ :each | self renderBindingActionFor: each on: html ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "sorted:", "activeBindings", "<", "key", "renderBindingActionFor:on:"]
}, function ($methodClass){ return function (aBindingGroup,html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($recv(aBindingGroup)._activeBindings())._sorted_((function(a,b){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv([$recv(a)._key()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["key"]=1
//>>excludeEnd("ctx");
][0]).__lt($recv(b)._key());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({a:a,b:b},$ctx1,1)});
//>>excludeEnd("ctx");
})))._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._renderBindingActionFor_on_(each,html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderBindingGroup:on:",{aBindingGroup:aBindingGroup,html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinderHelperWidget);

$core.addMethod(
$core.method({
selector: "renderCloseOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderCloseOn: html\x0a\x09html a\x0a\x09\x09class: 'close';\x0a\x09\x09with: [ (html tag: 'i') class: 'glyphicon glyphicon-remove' ];\x0a\x09\x09onClick: [ self keyBinder deactivate ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "a", "with:", "tag:", "onClick:", "deactivate", "keyBinder"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._a();
[$recv($1)._class_("close")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv(html)._tag_("i"))._class_("glyphicon glyphicon-remove");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
$recv($1)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._keyBinder())._deactivate();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderCloseOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinderHelperWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09html div \x0a\x09\x09id: 'overlay';\x0a\x09\x09class: 'helper_overlay';\x0a\x09\x09onClick: [ self deactivate ].\x0a\x09\x0a\x09html div class: self cssClass; with: [\x0a      \x09self renderLabelOn: html.\x0a\x09\x09html div\x0a\x09\x09\x09id: self mainId;\x0a\x09\x09\x09with: [ self renderSelectedBindingOn: html ].\x0a\x09\x09self renderCloseOn: html ].\x0a\x09\x09\x0a\x09':focus' asJQuery blur",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["id:", "div", "class:", "onClick:", "deactivate", "cssClass", "with:", "renderLabelOn:", "mainId", "renderSelectedBindingOn:", "renderCloseOn:", "blur", "asJQuery"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3;
$1=[$recv(html)._div()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["div"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._id_("overlay")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["id:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._class_("helper_overlay")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
$recv($1)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._deactivate();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
$2=[$recv(html)._div()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["div"]=2
//>>excludeEnd("ctx");
][0];
$recv($2)._class_($self._cssClass());
[$recv($2)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$self._renderLabelOn_(html);
$3=$recv(html)._div();
$recv($3)._id_($self._mainId());
$recv($3)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._renderSelectedBindingOn_(html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,3)});
//>>excludeEnd("ctx");
}));
return $self._renderCloseOn_(html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
$recv(":focus"._asJQuery())._blur();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinderHelperWidget);

$core.addMethod(
$core.method({
selector: "renderLabelOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderLabelOn: html\x0a\x09\x09html span \x0a        \x09class: 'selected'; \x0a            with: (self selectedBinding label ifNil: [ 'Action' ])",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "span", "with:", "ifNil:", "label", "selectedBinding"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3;
$1=$recv(html)._span();
$recv($1)._class_("selected");
$2=$recv($self._selectedBinding())._label();
if($2 == null || $2.a$nil){
$3="Action";
} else {
$3=$2;
}
$recv($1)._with_($3);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderLabelOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinderHelperWidget);

$core.addMethod(
$core.method({
selector: "renderSelectedBindingOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderSelectedBindingOn: html\x0a\x09self selectedBinding renderOn: self html: html",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["renderOn:html:", "selectedBinding"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._selectedBinding())._renderOn_html_(self,html);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderSelectedBindingOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinderHelperWidget);

$core.addMethod(
$core.method({
selector: "selectedBinding",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectedBinding\x0a\x09^ self keyBinder selectedBinding",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectedBinding", "keyBinder"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._keyBinder())._selectedBinding();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectedBinding",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinderHelperWidget);

$core.addMethod(
$core.method({
selector: "show",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "show\x0a\x09self hideCog.\x0a\x09self appendToJQuery: 'body' asJQuery",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["hideCog", "appendToJQuery:", "asJQuery"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._hideCog();
$self._appendToJQuery_("body"._asJQuery());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"show",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinderHelperWidget);

$core.addMethod(
$core.method({
selector: "showCog",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "showCog\x0a\x09'#cog-helper' asJQuery show",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["show", "asJQuery"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv("#cog-helper"._asJQuery())._show();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"showCog",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinderHelperWidget);

$core.addMethod(
$core.method({
selector: "showWidget:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aWidget"],
source: "showWidget: aWidget\x0a\x09\x22Some actions need to display more info to the user or request input.\x0a\x09This method is the right place for that\x22\x0a\x09\x0a\x09('#', self mainId) asJQuery empty.\x0a\x09aWidget appendToJQuery: ('#', self mainId) asJQuery",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["empty", "asJQuery", ",", "mainId", "appendToJQuery:"]
}, function ($methodClass){ return function (aWidget){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv([$recv(["#".__comma([$self._mainId()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["mainId"]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx[","]=1
//>>excludeEnd("ctx");
][0])._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._empty();
$recv(aWidget)._appendToJQuery_($recv("#".__comma($self._mainId()))._asJQuery());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"showWidget:",{aWidget:aWidget})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinderHelperWidget);


$core.addMethod(
$core.method({
selector: "on:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aKeyBinder"],
source: "on: aKeyBinder\x0a\x09^ self new\x0a    \x09keyBinder: aKeyBinder;\x0a        yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["keyBinder:", "new", "yourself"]
}, function ($methodClass){ return function (aKeyBinder){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._keyBinder_(aKeyBinder);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{aKeyBinder:aKeyBinder})});
//>>excludeEnd("ctx");
}; }),
$globals.HLKeyBinderHelperWidget.a$cls);


$core.addClass("HLRepeatedKeyDownHandler", $globals.Object, "Helios-KeyBindings");
$core.setSlots($globals.HLRepeatedKeyDownHandler, ["repeatInterval", "delay", "interval", "keyBindings", "widget", "keyDown"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLRepeatedKeyDownHandler.comment="I am responsible for handling repeated key down actions for widgets.\x0a\x0a##Usage\x0a\x0a    (self on: aWidget)\x0a        whileKeyDown: 38 do: aBlock;\x0a        whileKeyDown: 40 do: anotherBlock;\x0a        bindKeys\x0a\x0aI perform an action block on a key press, wait for 300 ms and then preform the same action block every `repeatInterval` milliseconds until the key is released.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "bindKeys",
protocol: "binding",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "bindKeys\x0a\x09self widget \x0a\x09\x09bindKeyDown: [ :e | self handleKeyDown: e ] \x0a\x09\x09keyUp: [ :e | self handleKeyUp ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["bindKeyDown:keyUp:", "widget", "handleKeyDown:", "handleKeyUp"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._widget())._bindKeyDown_keyUp_((function(e){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._handleKeyDown_(e);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({e:e},$ctx1,1)});
//>>excludeEnd("ctx");
}),(function(e){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._handleKeyUp();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({e:e},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"bindKeys",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRepeatedKeyDownHandler);

$core.addMethod(
$core.method({
selector: "defaultRepeatInterval",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "defaultRepeatInterval\x0a\x09^ 70",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return (70);

}; }),
$globals.HLRepeatedKeyDownHandler);

$core.addMethod(
$core.method({
selector: "handleEvent:forKey:action:",
protocol: "events handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anEvent", "anInteger", "aBlock"],
source: "handleEvent: anEvent forKey: anInteger action: aBlock\x0a\x09(anEvent which = anInteger and: [ self isKeyDown not ])\x0a\x09\x09ifTrue: [ self whileKeyDownDo: aBlock ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "and:", "=", "which", "not", "isKeyDown", "whileKeyDownDo:"]
}, function ($methodClass){ return function (anEvent,anInteger,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
if($core.assert($recv($recv(anEvent)._which()).__eq(anInteger))){
$1=$recv($self._isKeyDown())._not();
} else {
$1=false;
}
if($core.assert($1)){
$self._whileKeyDownDo_(aBlock);
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"handleEvent:forKey:action:",{anEvent:anEvent,anInteger:anInteger,aBlock:aBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRepeatedKeyDownHandler);

$core.addMethod(
$core.method({
selector: "handleKeyDown:",
protocol: "events handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anEvent"],
source: "handleKeyDown: anEvent\x0a\x09self keyBindings keysAndValuesDo: [ :key :action | \x0a\x09\x09self handleEvent: anEvent forKey: key action: action ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["keysAndValuesDo:", "keyBindings", "handleEvent:forKey:action:"]
}, function ($methodClass){ return function (anEvent){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._keyBindings())._keysAndValuesDo_((function(key,action){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._handleEvent_forKey_action_(anEvent,key,action);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({key:key,action:action},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"handleKeyDown:",{anEvent:anEvent})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRepeatedKeyDownHandler);

$core.addMethod(
$core.method({
selector: "handleKeyUp",
protocol: "events handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "handleKeyUp\x0a\x09self isKeyDown ifTrue: [\x0a\x09\x09keyDown := false.\x0a\x09\x09interval ifNotNil: [ interval clearInterval ].\x0a\x09\x09delay ifNotNil: [ delay clearTimeout ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "isKeyDown", "ifNotNil:", "clearInterval", "clearTimeout"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
if($core.assert($self._isKeyDown())){
$self.keyDown=false;
$1=$self.interval;
if($1 == null || $1.a$nil){
$1;
} else {
$recv($self.interval)._clearInterval();
}
$2=$self.delay;
if($2 == null || $2.a$nil){
$2;
} else {
$recv($self.delay)._clearTimeout();
}
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"handleKeyUp",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRepeatedKeyDownHandler);

$core.addMethod(
$core.method({
selector: "isKeyDown",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isKeyDown\x0a\x09^ keyDown ifNil: [ false ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.keyDown;
if($1 == null || $1.a$nil){
return false;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isKeyDown",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRepeatedKeyDownHandler);

$core.addMethod(
$core.method({
selector: "keyBindings",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "keyBindings\x0a\x09^ keyBindings ifNil: [ keyBindings := Dictionary new ]",
referencedClasses: ["Dictionary"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.keyBindings;
if($1 == null || $1.a$nil){
$self.keyBindings=$recv($globals.Dictionary)._new();
return $self.keyBindings;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"keyBindings",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRepeatedKeyDownHandler);

$core.addMethod(
$core.method({
selector: "rebindKeys",
protocol: "binding",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "rebindKeys\x0a\x09self \x0a\x09\x09unbindKeys;\x0a\x09\x09bindKeys",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unbindKeys", "bindKeys"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._unbindKeys();
$self._bindKeys();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"rebindKeys",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRepeatedKeyDownHandler);

$core.addMethod(
$core.method({
selector: "repeatInterval",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "repeatInterval\x0a\x09^ repeatInterval ifNil: [ self defaultRepeatInterval ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "defaultRepeatInterval"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.repeatInterval;
if($1 == null || $1.a$nil){
return $self._defaultRepeatInterval();
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"repeatInterval",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRepeatedKeyDownHandler);

$core.addMethod(
$core.method({
selector: "repeatInterval:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInteger"],
source: "repeatInterval: anInteger\x0a\x09repeatInterval := anInteger",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anInteger){
var self=this,$self=this;
$self.repeatInterval=anInteger;
return self;

}; }),
$globals.HLRepeatedKeyDownHandler);

$core.addMethod(
$core.method({
selector: "startRepeatingAction:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBlock"],
source: "startRepeatingAction: aBlock\x0a\x09^ [ (self widget hasFocus)\x0a\x09\x09ifTrue: [ aBlock value ]\x0a\x09\x09ifFalse: [ self handleKeyUp ] ] valueWithInterval: self repeatInterval",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["valueWithInterval:", "ifTrue:ifFalse:", "hasFocus", "widget", "value", "handleKeyUp", "repeatInterval"]
}, function ($methodClass){ return function (aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if($core.assert($recv($self._widget())._hasFocus())){
return $recv(aBlock)._value();
} else {
return $self._handleKeyUp();
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._valueWithInterval_($self._repeatInterval());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"startRepeatingAction:",{aBlock:aBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRepeatedKeyDownHandler);

$core.addMethod(
$core.method({
selector: "unbindKeys",
protocol: "binding",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "unbindKeys\x0a\x09self widget unbindKeyDownKeyUp",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unbindKeyDownKeyUp", "widget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._widget())._unbindKeyDownKeyUp();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unbindKeys",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRepeatedKeyDownHandler);

$core.addMethod(
$core.method({
selector: "whileKeyDown:do:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aKey", "aBlock"],
source: "whileKeyDown: aKey do: aBlock\x0a\x09self keyBindings at: aKey put: aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["at:put:", "keyBindings"]
}, function ($methodClass){ return function (aKey,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._keyBindings())._at_put_(aKey,aBlock);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"whileKeyDown:do:",{aKey:aKey,aBlock:aBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRepeatedKeyDownHandler);

$core.addMethod(
$core.method({
selector: "whileKeyDownDo:",
protocol: "events handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBlock"],
source: "whileKeyDownDo: aBlock\x0a\x09keyDown := true.\x0a\x09aBlock value.\x0a\x09delay := [ interval := self startRepeatingAction: aBlock ] \x0a\x09\x09valueWithTimeout: 300",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["value", "valueWithTimeout:", "startRepeatingAction:"]
}, function ($methodClass){ return function (aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.keyDown=true;
$recv(aBlock)._value();
$self.delay=$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$self.interval=$self._startRepeatingAction_(aBlock);
return $self.interval;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._valueWithTimeout_((300));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"whileKeyDownDo:",{aBlock:aBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRepeatedKeyDownHandler);

$core.addMethod(
$core.method({
selector: "widget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "widget\x0a\x09^ widget",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.widget;

}; }),
$globals.HLRepeatedKeyDownHandler);

$core.addMethod(
$core.method({
selector: "widget:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aWidget"],
source: "widget: aWidget\x0a\x09widget := aWidget",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aWidget){
var self=this,$self=this;
$self.widget=aWidget;
return self;

}; }),
$globals.HLRepeatedKeyDownHandler);


$core.addMethod(
$core.method({
selector: "on:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aWidget"],
source: "on: aWidget\x0a\x09^ self new\x0a\x09\x09widget: aWidget;\x0a\x09\x09yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["widget:", "new", "yourself"]
}, function ($methodClass){ return function (aWidget){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._widget_(aWidget);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{aWidget:aWidget})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRepeatedKeyDownHandler.a$cls);

});
