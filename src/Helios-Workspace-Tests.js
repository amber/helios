define(["amber/boot", "require", "amber/core/SUnit"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-Workspace-Tests");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLCodeWidgetTest", $globals.TestCase, "Helios-Workspace-Tests");
$core.addMethod(
$core.method({
selector: "testKeyMap",
protocol: "tests",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testKeyMap\x0a\x09\x22Key maps are a collection of associations.\x22\x0a\x09self assert: (HLCodeWidget pcKeyMap isKindOf: HashedCollection).\x0a\x09self assert: (HLCodeWidget macKeyMap isKindOf: HashedCollection)",
referencedClasses: ["HLCodeWidget", "HashedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["assert:", "isKindOf:", "pcKeyMap", "macKeyMap"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$self._assert_([$recv($recv($globals.HLCodeWidget)._pcKeyMap())._isKindOf_($globals.HashedCollection)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["isKindOf:"]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["assert:"]=1
//>>excludeEnd("ctx");
][0];
$self._assert_($recv($recv($globals.HLCodeWidget)._macKeyMap())._isKindOf_($globals.HashedCollection));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testKeyMap",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidgetTest);


});
