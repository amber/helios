Smalltalk createPackage: 'Helios-Announcements'!
Object subclass: #HLAboutToChange
	slots: {#actionBlock}
	package: 'Helios-Announcements'!
!HLAboutToChange commentStamp!
I am announced whenever a change of context is about to be made, and unsaved changes could be lost.

I am used within `HLModel` to handle such user actions. See `HLModel >> withChangesDo:`.!

!HLAboutToChange methodsFor: 'accessing'!

actionBlock
	^ actionBlock
!

actionBlock: aBlock
	actionBlock := aBlock
! !

Object subclass: #HLAnnouncement
	slots: {}
	package: 'Helios-Announcements'!
!HLAnnouncement commentStamp!
I am the root of the announcement class hierarchy used in the Helios UI.!

!HLAnnouncement class methodsFor: 'helios'!

classTag
	^ 'announcement'
! !

HLAnnouncement subclass: #HLCodeHandled
	slots: {#code}
	package: 'Helios-Announcements'!
!HLCodeHandled commentStamp!
I am the root class of announcements emitted by `HLCodeWidget`s!

!HLCodeHandled methodsFor: 'accessing'!

code

	^ code
!

code: aModel

	code := aModel
! !

!HLCodeHandled class methodsFor: 'actions'!

on: aCodeModel

	^ self new 
    	code: aCodeModel;
        yourself
! !

HLCodeHandled subclass: #HLDoItExecuted
	slots: {}
	package: 'Helios-Announcements'!
!HLDoItExecuted commentStamp!
I am emitted by a `HLCodeWidget` after a DoIt has been executed.!

HLAnnouncement subclass: #HLDebuggerAnnouncement
	slots: {#context}
	package: 'Helios-Announcements'!
!HLDebuggerAnnouncement commentStamp!
I am the root class of debugger announcements, and hold onto the debugged `context`.!

!HLDebuggerAnnouncement methodsFor: 'accessing'!

context
	^ context
!

context: aContext
	context := aContext
! !

HLDebuggerAnnouncement subclass: #HLDebuggerContextSelected
	slots: {}
	package: 'Helios-Announcements'!
!HLDebuggerContextSelected commentStamp!
I am announced when a new context is selected in a debugger, to update the user interface.!

!HLDebuggerContextSelected methodsFor: 'accessing'!

context
	^ context
!

context: aContext
	context := aContext
! !

HLDebuggerAnnouncement subclass: #HLDebuggerProceeded
	slots: {}
	package: 'Helios-Announcements'!

HLDebuggerAnnouncement subclass: #HLDebuggerStepped
	slots: {}
	package: 'Helios-Announcements'!

HLDebuggerAnnouncement subclass: #HLDebuggerWhere
	slots: {}
	package: 'Helios-Announcements'!

HLAnnouncement subclass: #HLDiveRequested
	slots: {}
	package: 'Helios-Announcements'!

HLAnnouncement subclass: #HLEditComment
	slots: {}
	package: 'Helios-Announcements'!

HLAnnouncement subclass: #HLErrorRaised
	slots: {#error}
	package: 'Helios-Announcements'!

!HLErrorRaised methodsFor: 'accessing'!

error
	^ error
!

error: anError
	error := anError
! !

HLErrorRaised subclass: #HLCompileErrorRaised
	slots: {}
	package: 'Helios-Announcements'!

HLErrorRaised subclass: #HLParseErrorRaised
	slots: {#line. #column. #message}
	package: 'Helios-Announcements'!

!HLParseErrorRaised methodsFor: 'accessing'!

column
	^ column
!

column: anInteger
	column := anInteger
!

line
	^ line
!

line: anInteger
	line := anInteger
!

message
	^ message
!

message: aString
	message := aString
! !

HLErrorRaised subclass: #HLUnknownVariableErrorRaised
	slots: {}
	package: 'Helios-Announcements'!

HLAnnouncement subclass: #HLFocusRequested
	slots: {}
	package: 'Helios-Announcements'!

HLFocusRequested subclass: #HLClassesFocusRequested
	slots: {}
	package: 'Helios-Announcements'!

HLFocusRequested subclass: #HLDocumentationFocusRequested
	slots: {}
	package: 'Helios-Announcements'!

HLFocusRequested subclass: #HLMethodsFocusRequested
	slots: {}
	package: 'Helios-Announcements'!

HLFocusRequested subclass: #HLPackagesFocusRequested
	slots: {}
	package: 'Helios-Announcements'!

HLFocusRequested subclass: #HLProtocolsFocusRequested
	slots: {}
	package: 'Helios-Announcements'!

HLFocusRequested subclass: #HLSourceCodeFocusRequested
	slots: {}
	package: 'Helios-Announcements'!

HLAnnouncement subclass: #HLInstVarAdded
	slots: {#theClass. #variableName}
	package: 'Helios-Announcements'!

!HLInstVarAdded methodsFor: 'accessing'!

theClass
	^ theClass
!

theClass: aClass
	theClass := aClass
!

variableName
	^ variableName
!

variableName: aString
	variableName := aString
! !

HLAnnouncement subclass: #HLItemSelected
	slots: {#item. #soft}
	package: 'Helios-Announcements'!

!HLItemSelected methodsFor: 'accessing'!

beSoft
	soft := true
!

item
	^ item
!

item: anObject
	item := anObject
! !

!HLItemSelected methodsFor: 'initialization'!

initialize
	super initialize.
	item := nil.
	soft := false
! !

!HLItemSelected methodsFor: 'testing'!

isSoft
	^ soft
! !

!HLItemSelected class methodsFor: 'instance creation'!

on: anItem
	^ self new
    	item: anItem;
        yourself
!

softOn: anItem
	^ self new
    	item: anItem;
		beSoft;
        yourself
! !

HLItemSelected subclass: #HLClassSelected
	slots: {}
	package: 'Helios-Announcements'!

HLItemSelected subclass: #HLInstanceVariableSelected
	slots: {}
	package: 'Helios-Announcements'!

HLItemSelected subclass: #HLMethodSelected
	slots: {}
	package: 'Helios-Announcements'!

HLItemSelected subclass: #HLPackageSelected
	slots: {}
	package: 'Helios-Announcements'!

HLItemSelected subclass: #HLProtocolSelected
	slots: {}
	package: 'Helios-Announcements'!

HLAnnouncement subclass: #HLItemUnselected
	slots: {#item}
	package: 'Helios-Announcements'!

!HLItemUnselected methodsFor: 'accessing'!

item
	^ item
!

item: anObject
	item := anObject
! !

!HLItemUnselected class methodsFor: 'instance creation'!

on: anItem
	^ self new
    	item: anItem;
        yourself
! !

HLItemUnselected subclass: #HLClassUnselected
	slots: {}
	package: 'Helios-Announcements'!

HLItemUnselected subclass: #HLPackageUnselected
	slots: {}
	package: 'Helios-Announcements'!

HLAnnouncement subclass: #HLRunTests
	slots: {#testSuiteRunner}
	package: 'Helios-Announcements'!

!HLRunTests methodsFor: 'accessing'!

testSuiteRunner
	^ testSuiteRunner
!

testSuiteRunner: anObject
	testSuiteRunner := anObject
! !

!HLRunTests class methodsFor: 'instance creation'!

on: aTestSuiteRunner
	^self new
		testSuiteRunner: aTestSuiteRunner;
		yourself
! !

HLAnnouncement subclass: #HLSaveSourceCode
	slots: {}
	package: 'Helios-Announcements'!

HLAnnouncement subclass: #HLSearchReferences
	slots: {#searchString}
	package: 'Helios-Announcements'!

!HLSearchReferences methodsFor: 'accessing'!

searchString
	^ searchString
!

searchString: aString
	searchString := aString
! !

HLAnnouncement subclass: #HLShowCommentToggled
	slots: {}
	package: 'Helios-Announcements'!

HLAnnouncement subclass: #HLShowInstanceToggled
	slots: {}
	package: 'Helios-Announcements'!

HLAnnouncement subclass: #HLShowTemplate
	slots: {#template}
	package: 'Helios-Announcements'!

!HLShowTemplate methodsFor: 'accessing'!

template
	^ template
!

template: aString
	template := aString
! !

HLAnnouncement subclass: #HLSourceCodeSaved
	slots: {}
	package: 'Helios-Announcements'!

HLAnnouncement subclass: #HLTabLabelChanged
	slots: {#label. #widget}
	package: 'Helios-Announcements'!

!HLTabLabelChanged methodsFor: 'accessing'!

label
	^ label
!

label: aString
	label := aString
!

widget
	^ widget
!

widget: aWidget
	widget := aWidget
! !

