define(["amber/boot", "require", "amber/core/Kernel-Objects", "helios/Helios-Core"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-Transcript");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLTranscript", $globals.HLWidget, "Helios-Transcript");
$core.setSlots($globals.HLTranscript, ["textarea"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLTranscript.comment="I am a widget responsible for displaying transcript contents.\x0a\x0a## Transcript API\x0a\x0a    Transcript \x0a        show: 'hello world';\x0a        cr;\x0a        show: anObject.\x0a\x0a    Transcript clear.\x0a\x0aSee the `Transcript` service class.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "clear",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "clear\x0a\x09textarea asJQuery text: ''",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["text:", "asJQuery"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self.textarea)._asJQuery())._text_("");
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"clear",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTranscript);

$core.addMethod(
$core.method({
selector: "initialize",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initialize\x0a\x09super initialize.\x0a\x09self register",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initialize", "register"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._initialize.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$self._register();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initialize",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTranscript);

$core.addMethod(
$core.method({
selector: "register",
protocol: "registration",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "register\x0a\x09HLTranscriptHandler register: self",
referencedClasses: ["HLTranscriptHandler"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["register:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($globals.HLTranscriptHandler)._register_(self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"register",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTranscript);

$core.addMethod(
$core.method({
selector: "renderOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderOn: html\x0a\x09html div\x0a\x09\x09class: 'transcript';\x0a\x09\x09with: [ textarea := html textarea ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "with:", "textarea"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._div();
$recv($1)._class_("transcript");
$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$self.textarea=$recv(html)._textarea();
return $self.textarea;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTranscript);

$core.addMethod(
$core.method({
selector: "show:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "show: aString\x0a\x09textarea ifNotNil: [\x0a \x09\x09textarea append: aString asString ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "append:", "asString"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.textarea;
if($1 == null || $1.a$nil){
$1;
} else {
$recv($self.textarea)._append_($recv(aString)._asString());
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"show:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTranscript);

$core.addMethod(
$core.method({
selector: "unregister",
protocol: "registration",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "unregister\x0a\x09super unregister.\x0a\x09HLTranscriptHandler unregister: self",
referencedClasses: ["HLTranscriptHandler"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unregister", "unregister:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._unregister.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv($globals.HLTranscriptHandler)._unregister_(self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unregister",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTranscript);



$core.addClass("HLTranscriptHandler", $globals.Object, "Helios-Transcript");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLTranscriptHandler.comment="I handle transcript events, dispatching them to all instances of `HLTranscript`.\x0a\x0a## API\x0a\x0aUse the class-side method `#register:` to add transcript instances.";
//>>excludeEnd("ide");

$core.setSlots($globals.HLTranscriptHandler.a$cls, ["transcripts"]);
$core.addMethod(
$core.method({
selector: "clear",
protocol: "printing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "clear\x0a\x09self transcripts do: [ :each |\x0a\x09\x09each clear ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "transcripts", "clear"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._transcripts())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._clear();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"clear",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTranscriptHandler.a$cls);

$core.addMethod(
$core.method({
selector: "cr",
protocol: "printing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "cr\x0a\x09self transcripts do: [ :each | each cr ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "transcripts", "cr"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._transcripts())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._cr();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"cr",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTranscriptHandler.a$cls);

$core.addMethod(
$core.method({
selector: "register:",
protocol: "registration",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTranscript"],
source: "register: aTranscript\x0a\x09self transcripts add: aTranscript",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["add:", "transcripts"]
}, function ($methodClass){ return function (aTranscript){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._transcripts())._add_(aTranscript);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"register:",{aTranscript:aTranscript})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTranscriptHandler.a$cls);

$core.addMethod(
$core.method({
selector: "show:",
protocol: "printing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "show: aString\x0a\x09self transcripts do: [ :each |\x0a\x09\x09each show: aString ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "transcripts", "show:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._transcripts())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._show_(aString);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"show:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTranscriptHandler.a$cls);

$core.addMethod(
$core.method({
selector: "transcripts",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "transcripts\x0a\x09^ transcripts ifNil: [ transcripts := OrderedCollection new ]",
referencedClasses: ["OrderedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.transcripts;
if($1 == null || $1.a$nil){
$self.transcripts=$recv($globals.OrderedCollection)._new();
return $self.transcripts;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"transcripts",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTranscriptHandler.a$cls);

$core.addMethod(
$core.method({
selector: "unregister:",
protocol: "registration",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTranscript"],
source: "unregister: aTranscript\x0a\x09self transcripts remove: aTranscript",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["remove:", "transcripts"]
}, function ($methodClass){ return function (aTranscript){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._transcripts())._remove_(aTranscript);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unregister:",{aTranscript:aTranscript})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTranscriptHandler.a$cls);

});
