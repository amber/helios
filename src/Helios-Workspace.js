define(["amber/boot", "require", "amber/core/Kernel-Objects", "helios/Helios-Core"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-Workspace");
$pkg.context = function () { return {codeMirrorLib:codeMirrorLib}; };
$pkg.imports = ["codeMirrorLib=codemirror/lib/codemirror", "codemirror/addon/hint/show-hint", "codemirror/mode/smalltalk/smalltalk"];
//>>excludeStart("imports", pragmas.excludeImports);
var codeMirrorLib;
$pkg.isReady = new Promise(function (resolve, reject) { requirejs(["codemirror/lib/codemirror", "codemirror/addon/hint/show-hint", "codemirror/mode/smalltalk/smalltalk"], function ($1) {codeMirrorLib=$1; resolve();}, reject); });
//>>excludeEnd("imports");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLCodeModel", $globals.Object, "Helios-Workspace");
$core.setSlots($globals.HLCodeModel, ["announcer", "environment", "receiver"]);
$core.addMethod(
$core.method({
selector: "announcer",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "announcer\x0a\x09^ announcer ifNil: [ announcer := Announcer new ]",
referencedClasses: ["Announcer"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.announcer;
if($1 == null || $1.a$nil){
$self.announcer=$recv($globals.Announcer)._new();
return $self.announcer;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"announcer",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeModel);

$core.addMethod(
$core.method({
selector: "browse:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "browse: anObject\x0a\x09anObject browse",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["browse"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(anObject)._browse();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"browse:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeModel);

$core.addMethod(
$core.method({
selector: "defaultReceiver",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "defaultReceiver\x0a\x09^ self environment doItReceiver",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["doItReceiver", "environment"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._environment())._doItReceiver();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"defaultReceiver",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeModel);

$core.addMethod(
$core.method({
selector: "doIt:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "doIt: aString\x0a\x09^ self environment evaluate: aString for: self receiver",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["evaluate:for:", "environment", "receiver"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._environment())._evaluate_for_(aString,$self._receiver());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"doIt:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeModel);

$core.addMethod(
$core.method({
selector: "environment",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "environment\x0a\x09^ environment ifNil: [ HLManager current environment ]",
referencedClasses: ["HLManager"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "environment", "current"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.environment;
if($1 == null || $1.a$nil){
return $recv($recv($globals.HLManager)._current())._environment();
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"environment",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeModel);

$core.addMethod(
$core.method({
selector: "environment:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anEnvironment"],
source: "environment: anEnvironment\x0a\x09environment := anEnvironment",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anEnvironment){
var self=this,$self=this;
$self.environment=anEnvironment;
return self;

}; }),
$globals.HLCodeModel);

$core.addMethod(
$core.method({
selector: "inspect:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "inspect: anObject\x0a\x09self environment inspect: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["inspect:", "environment"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._environment())._inspect_(anObject);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inspect:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeModel);

$core.addMethod(
$core.method({
selector: "receiver",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "receiver\x0a\x09^ receiver ifNil: [ receiver := self defaultReceiver ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "defaultReceiver"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.receiver;
if($1 == null || $1.a$nil){
$self.receiver=$self._defaultReceiver();
return $self.receiver;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"receiver",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeModel);

$core.addMethod(
$core.method({
selector: "receiver:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "receiver: anObject\x0a\x09receiver := anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
$self.receiver=anObject;
return self;

}; }),
$globals.HLCodeModel);


$core.addMethod(
$core.method({
selector: "on:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anEnvironment"],
source: "on: anEnvironment\x0a\x0a\x09^ self new\x0a    \x09environment: anEnvironment;\x0a        yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["environment:", "new", "yourself"]
}, function ($methodClass){ return function (anEnvironment){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._environment_(anEnvironment);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{anEnvironment:anEnvironment})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeModel.a$cls);


$core.addClass("HLCodeWidget", $globals.HLWidget, "Helios-Workspace");
$core.setSlots($globals.HLCodeWidget, ["model", "code", "editor", "state"]);
$core.addMethod(
$core.method({
selector: "announcer",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "announcer\x0a\x09^ self model announcer",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._announcer();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"announcer",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "basicDoIt",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "basicDoIt\x0a\x09| result |\x0a\x0a\x09result := self model doIt: self currentLineOrSelection.\x0a\x09self model announcer announce: (HLDoItExecuted on: model).\x0a\x0a\x09^ result",
referencedClasses: ["HLDoItExecuted"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["doIt:", "model", "currentLineOrSelection", "announce:", "announcer", "on:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._doIt_($self._currentLineOrSelection());
$recv($recv($self._model())._announcer())._announce_($recv($globals.HLDoItExecuted)._on_($self.model));
return result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"basicDoIt",{result:result})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "browseIt",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "browseIt\x0a\x09[ self model browse: self basicDoIt ] fork",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["fork", "browse:", "model", "basicDoIt"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._model())._browse_($self._basicDoIt());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._fork();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"browseIt",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "canHaveFocus",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "canHaveFocus\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "clear",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "clear\x0a\x09self contents: ''",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["contents:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._contents_("");
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"clear",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "configureEditor",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "configureEditor\x0a\x09self editor at: 'amberCodeWidget' put: self.\x0a\x09self editor on: 'change' do: [ self onChange ].\x0a\x0a\x09'helios.code.font' settingValue ifNotNil: [ :font |\x0a\x09\x09(self wrapper asJQuery find: '.CodeMirror') css: 'font' put: font ].\x0a\x09self wrapper asJQuery on: 'mousedown' in: '.CodeMirror pre' do: [ :event | | position node |\x0a\x09\x09(event at: 'ctrlKey') ifTrue: [\x0a\x09\x09\x09position := self editor coordsChar: #{ \x0a\x09\x09\x09\x09'left' -> event clientX.\x0a\x09\x09\x09\x09'top' -> event clientY\x0a\x09\x09\x09}.\x0a\x09\x09\x09self onCtrlClickAt: (position line @ position ch) + 1.\x0a\x09\x09\x09event preventDefault ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["at:put:", "editor", "on:do:", "onChange", "ifNotNil:", "settingValue", "css:put:", "find:", "asJQuery", "wrapper", "on:in:do:", "ifTrue:", "at:", "coordsChar:", "clientX", "clientY", "onCtrlClickAt:", "+", "@", "line", "ch", "preventDefault"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$recv([$self._editor()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["editor"]=1
//>>excludeEnd("ctx");
][0])._at_put_("amberCodeWidget",self);
$recv([$self._editor()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["editor"]=2
//>>excludeEnd("ctx");
][0])._on_do_("change",(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._onChange();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
$1="helios.code.font"._settingValue();
if($1 == null || $1.a$nil){
$1;
} else {
var font;
font=$1;
$recv($recv([$recv([$self._wrapper()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["wrapper"]=1
//>>excludeEnd("ctx");
][0])._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._find_(".CodeMirror"))._css_put_("font",font);
}
$recv($recv($self._wrapper())._asJQuery())._on_in_do_("mousedown",".CodeMirror pre",(function(event){
var position,node;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if($core.assert($recv(event)._at_("ctrlKey"))){
position=$recv($self._editor())._coordsChar_($globals.HashedCollection._newFromPairs_(["left",$recv(event)._clientX(),"top",$recv(event)._clientY()]));
$self._onCtrlClickAt_($recv($recv($recv(position)._line()).__at($recv(position)._ch())).__plus((1)));
return $recv(event)._preventDefault();
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({event:event,position:position,node:node},$ctx1,3)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"configureEditor",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "contents",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "contents\x0a\x09^ editor getValue",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["getValue"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self.editor)._getValue();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"contents",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "contents:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "contents: aString\x0a\x09editor setValue: aString.\x0a\x09state ifNotNil: [ self updateState ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["setValue:", "ifNotNil:", "updateState"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$recv($self.editor)._setValue_(aString);
$1=$self.state;
if($1 == null || $1.a$nil){
$1;
} else {
$self._updateState();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"contents:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "currentLine",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "currentLine\x0a    ^editor getLine: (editor getCursor line)",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["getLine:", "line", "getCursor"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self.editor)._getLine_($recv($recv($self.editor)._getCursor())._line());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"currentLine",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "currentLineOrSelection",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "currentLineOrSelection\x0a    ^editor somethingSelected\x0a\x09\x09ifFalse: [ self currentLine ]\x0a\x09\x09ifTrue: [ self selection ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifFalse:ifTrue:", "somethingSelected", "currentLine", "selection"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv($self.editor)._somethingSelected())){
return $self._selection();
} else {
return $self._currentLine();
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"currentLineOrSelection",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "doIt",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "doIt\x0a\x09[ self basicDoIt ] fork",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["fork", "basicDoIt"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._basicDoIt();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._fork();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"doIt",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "editor",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "editor\x0a\x09^ editor",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.editor;

}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "editorOptions",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "editorOptions\x0a\x09^ #{\x0a\x09\x09'theme' -> ('helios.editorTheme' settingValueIfAbsent: 'default').\x0a\x09\x09'mode' -> 'text/x-stsrc'.\x0a\x09\x09'inputStyle' -> 'contenteditable'.\x0a        'lineNumbers' -> true.\x0a        'enterMode' -> 'flat'.\x0a        'indentWithTabs' -> true.\x0a\x09\x09'indentUnit' -> 4.\x0a        'matchBrackets' -> true.\x0a        'electricChars' -> false.\x0a\x09\x09'keyMap' -> 'Amber'.\x0a\x09\x09'extraKeys' -> (HashedCollection with: ('helios.completionKey' settingValueIfAbsent: 'Shift-Space') -> 'autocomplete')\x0a\x09}",
referencedClasses: ["HashedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["settingValueIfAbsent:", "with:", "->"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $globals.HashedCollection._newFromPairs_(["theme",["helios.editorTheme"._settingValueIfAbsent_("default")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["settingValueIfAbsent:"]=1
//>>excludeEnd("ctx");
][0],"mode","text/x-stsrc","inputStyle","contenteditable","lineNumbers",true,"enterMode","flat","indentWithTabs",true,"indentUnit",(4),"matchBrackets",true,"electricChars",false,"keyMap","Amber","extraKeys",$recv($globals.HashedCollection)._with_($recv("helios.completionKey"._settingValueIfAbsent_("Shift-Space")).__minus_gt("autocomplete"))]);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"editorOptions",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "focus",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focus\x0a\x09editor focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focus"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.editor)._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focus",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "hasFocus",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "hasFocus\x0a\x09^ code asJQuery is: ':active'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["is:", "asJQuery"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self.code)._asJQuery())._is_(":active");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"hasFocus",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "hasModification",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "hasModification\x0a\x09^ false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return false;

}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "inspectIt",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inspectIt\x0a\x09[ self model inspect: self basicDoIt ] fork",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["fork", "inspect:", "model", "basicDoIt"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._model())._inspect_($self._basicDoIt());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._fork();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inspectIt",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "messageHintFor:token:",
protocol: "hints",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anEditor", "aToken"],
source: "messageHintFor: anEditor token: aToken\x0a\x09^ (Smalltalk core allSelectors asArray \x0a\x09\x09select: [ :each | each includesSubString: aToken string ])\x0a\x09\x09reject: [ :each | each = aToken string ]",
referencedClasses: ["Smalltalk"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["reject:", "select:", "asArray", "allSelectors", "core", "includesSubString:", "string", "="]
}, function ($methodClass){ return function (anEditor,aToken){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($recv($recv($recv($globals.Smalltalk)._core())._allSelectors())._asArray())._select_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._includesSubString_([$recv(aToken)._string()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["string"]=1
//>>excludeEnd("ctx");
][0]);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
})))._reject_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each).__eq($recv(aToken)._string());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,2)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"messageHintFor:token:",{anEditor:anEditor,aToken:aToken})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "model",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "model\x0a\x09^ model ifNil: [ model := HLCodeModel new ]",
referencedClasses: ["HLCodeModel"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.model;
if($1 == null || $1.a$nil){
$self.model=$recv($globals.HLCodeModel)._new();
return $self.model;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"model",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "model:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aModel"],
source: "model: aModel\x0a\x09model := aModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aModel){
var self=this,$self=this;
$self.model=aModel;
return self;

}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "navigateTo:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "navigateTo: aString\x0a\x09Finder findString: aString",
referencedClasses: ["Finder"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["findString:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($globals.Finder)._findString_(aString);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"navigateTo:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "navigateToReference:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "navigateToReference: aString\x0a\x09(HLReferences openAsTab)\x0a\x09\x09search: aString",
referencedClasses: ["HLReferences"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["search:", "openAsTab"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($globals.HLReferences)._openAsTab())._search_(aString);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"navigateToReference:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "onChange",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onChange\x0a\x09self updateState",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["updateState"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._updateState();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onChange",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "onCtrlClickAt:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aPoint"],
source: "onCtrlClickAt: aPoint\x0a\x09| ast node |\x0a\x09\x0a\x09ast := [ Smalltalk parse: self editor getValue ] \x0a\x09\x09on: Error \x0a\x09\x09do: [ :error | ^ self ].\x0a\x09\x0a\x09node := ast \x0a\x09\x09navigationNodeAt: aPoint \x0a\x09\x09ifAbsent: [ ^ nil ].\x0a\x09\x09\x0a\x09self navigateTo: node navigationLink",
referencedClasses: ["Smalltalk", "Error"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:do:", "parse:", "getValue", "editor", "navigationNodeAt:ifAbsent:", "navigateTo:", "navigationLink"]
}, function ($methodClass){ return function (aPoint){
var self=this,$self=this;
var ast,node;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $early={};
try {
ast=$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($globals.Smalltalk)._parse_($recv($self._editor())._getValue());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._on_do_($globals.Error,(function(error){
throw $early=[self];

}));
node=$recv(ast)._navigationNodeAt_ifAbsent_(aPoint,(function(){
throw $early=[nil];

}));
$self._navigateTo_($recv(node)._navigationLink());
return self;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onCtrlClickAt:",{aPoint:aPoint,ast:ast,node:node})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "onInspectIt",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onInspectIt\x0a\x0a\x09self inspectIt",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["inspectIt"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._inspectIt();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onInspectIt",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "onPrintIt",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onPrintIt\x0a\x0a\x09self printIt",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["printIt"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._printIt();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onPrintIt",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "onSaveIt",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onSaveIt\x0a\x09\x22I do not do anything\x22",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return self;

}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "print:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "print: aString\x0a\x09| start stop currentLine |\x0a    currentLine := (editor getCursor: false) line.\x0a\x09start := HashedCollection new.\x0a\x09start at: 'line' put: currentLine.\x0a\x09start at: 'ch' put: (editor getCursor: false) ch.\x0a    (editor getSelection) ifEmpty: [\x0a    \x09\x22select current line if selection is empty\x22\x0a    \x09start at: 'ch' put: (editor getLine: currentLine) size.\x0a        editor setSelection: #{'line' -> currentLine. 'ch' -> 0} end: start.\x0a    ].\x0a\x09stop := HashedCollection new.\x0a\x09stop at: 'line' put: currentLine.\x0a\x09stop at: 'ch' put: ((start at: 'ch') + aString size + 2).\x0a\x0a\x09editor replaceSelection: (editor getSelection, ' ', aString, ' ').\x0a\x09editor setCursor: (editor getCursor: true).\x0a\x09editor setSelection: stop end: start",
referencedClasses: ["HashedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["line", "getCursor:", "new", "at:put:", "ch", "ifEmpty:", "getSelection", "size", "getLine:", "setSelection:end:", "+", "at:", "replaceSelection:", ",", "setCursor:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
var start,stop,currentLine;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
currentLine=$recv([$recv($self.editor)._getCursor_(false)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["getCursor:"]=1
//>>excludeEnd("ctx");
][0])._line();
start=[$recv($globals.HashedCollection)._new()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["new"]=1
//>>excludeEnd("ctx");
][0];
[$recv(start)._at_put_("line",currentLine)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=1
//>>excludeEnd("ctx");
][0];
[$recv(start)._at_put_("ch",$recv([$recv($self.editor)._getCursor_(false)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["getCursor:"]=2
//>>excludeEnd("ctx");
][0])._ch())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=2
//>>excludeEnd("ctx");
][0];
$recv([$recv($self.editor)._getSelection()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["getSelection"]=1
//>>excludeEnd("ctx");
][0])._ifEmpty_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
[$recv(start)._at_put_("ch",[$recv($recv($self.editor)._getLine_(currentLine))._size()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["size"]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["at:put:"]=3
//>>excludeEnd("ctx");
][0];
return [$recv($self.editor)._setSelection_end_($globals.HashedCollection._newFromPairs_(["line",currentLine,"ch",(0)]),start)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["setSelection:end:"]=1
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
stop=$recv($globals.HashedCollection)._new();
[$recv(stop)._at_put_("line",currentLine)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=4
//>>excludeEnd("ctx");
][0];
$recv(stop)._at_put_("ch",[$recv($recv($recv(start)._at_("ch")).__plus($recv(aString)._size())).__plus((2))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["+"]=1
//>>excludeEnd("ctx");
][0]);
$recv($self.editor)._replaceSelection_([$recv([$recv($recv($recv($self.editor)._getSelection()).__comma(" ")).__comma(aString)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx[","]=2
//>>excludeEnd("ctx");
][0]).__comma(" ")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx[","]=1
//>>excludeEnd("ctx");
][0]);
$recv($self.editor)._setCursor_($recv($self.editor)._getCursor_(true));
$recv($self.editor)._setSelection_end_(stop,start);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"print:",{aString:aString,start:start,stop:stop,currentLine:currentLine})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "printIt",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "printIt\x0a\x09[ self print: self basicDoIt printString ] fork",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["fork", "print:", "printString", "basicDoIt"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._print_($recv($self._basicDoIt())._printString());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._fork();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"printIt",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "receiver",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "receiver\x0a\x09^ self model receiver",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["receiver", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._receiver();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"receiver",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "receiver:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "receiver: anObject\x0a\x09self model receiver: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["receiver:", "model"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._receiver_(anObject);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"receiver:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "renderButtonsOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderButtonsOn: html\x0a\x09html button \x0a\x09\x09class: 'button';\x0a\x09\x09with: 'DoIt';\x0a\x09\x09onClick: [ self doIt ].\x0a\x09html button \x0a\x09\x09class: 'button';\x0a\x09\x09with: 'PrintIt';\x0a\x09\x09onClick: [ self printIt ].\x0a\x09html button \x0a\x09\x09class: 'button';\x0a\x09\x09with: 'InspectIt';\x0a\x09\x09onClick: [ self inspectIt ].\x0a\x09html button \x0a\x09\x09class: 'button';\x0a\x09\x09with: 'BrowseIt';\x0a\x09\x09onClick: [ self browseIt ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "button", "with:", "onClick:", "doIt", "printIt", "inspectIt", "browseIt"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3,$4;
$1=[$recv(html)._button()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["button"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._class_("button")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_("DoIt")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._doIt();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["onClick:"]=1
//>>excludeEnd("ctx");
][0];
$2=[$recv(html)._button()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["button"]=2
//>>excludeEnd("ctx");
][0];
[$recv($2)._class_("button")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($2)._with_("PrintIt")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($2)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._printIt();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["onClick:"]=2
//>>excludeEnd("ctx");
][0];
$3=[$recv(html)._button()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["button"]=3
//>>excludeEnd("ctx");
][0];
[$recv($3)._class_("button")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=3
//>>excludeEnd("ctx");
][0];
[$recv($3)._with_("InspectIt")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=3
//>>excludeEnd("ctx");
][0];
[$recv($3)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._inspectIt();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,3)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["onClick:"]=3
//>>excludeEnd("ctx");
][0];
$4=$recv(html)._button();
$recv($4)._class_("button");
$recv($4)._with_("BrowseIt");
$recv($4)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._browseIt();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,4)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderButtonsOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09html div class: 'editor'; with: [\x0a\x09\x09code := html textarea ].\x0a\x09state := html div class: 'state'.\x0a\x09\x0a\x09html div \x0a\x09\x09class: 'buttons_bar';\x0a\x09\x09with: [ self renderButtonsOn: html ].\x0a\x09\x0a\x09self \x0a\x09\x09setEditorOn: code asDomNode;\x0a\x09\x09configureEditor;\x0a\x09\x09updateState",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "with:", "textarea", "renderButtonsOn:", "setEditorOn:", "asDomNode", "configureEditor", "updateState"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=[$recv(html)._div()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["div"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._class_("editor")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$self.code=$recv(html)._textarea();
return $self.code;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
$self.state=[$recv([$recv(html)._div()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["div"]=2
//>>excludeEnd("ctx");
][0])._class_("state")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=2
//>>excludeEnd("ctx");
][0];
$2=$recv(html)._div();
$recv($2)._class_("buttons_bar");
$recv($2)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._renderButtonsOn_(html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
$self._setEditorOn_($recv($self.code)._asDomNode());
$self._configureEditor();
$self._updateState();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "saveIt",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "saveIt\x0a\x09\x22I do not do anything\x22",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return self;

}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "selection",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selection\x0a\x09^editor getSelection",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["getSelection"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self.editor)._getSelection();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selection",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "selectionEnd",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectionEnd\x0a   ^ code asDomNode selectionEnd",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectionEnd", "asDomNode"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self.code)._asDomNode())._selectionEnd();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectionEnd",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "selectionEnd:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInteger"],
source: "selectionEnd: anInteger\x0a   code asDomNode selectionEnd: anInteger",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectionEnd:", "asDomNode"]
}, function ($methodClass){ return function (anInteger){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self.code)._asDomNode())._selectionEnd_(anInteger);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectionEnd:",{anInteger:anInteger})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "selectionStart",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectionStart\x0a   ^ code asDomNode selectionStart",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectionStart", "asDomNode"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self.code)._asDomNode())._selectionStart();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectionStart",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "selectionStart:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInteger"],
source: "selectionStart: anInteger\x0a   code asDomNode selectionStart: anInteger",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectionStart:", "asDomNode"]
}, function ($methodClass){ return function (anInteger){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self.code)._asDomNode())._selectionStart_(anInteger);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectionStart:",{anInteger:anInteger})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "setEditorOn:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTextarea"],
source: "setEditorOn: aTextarea\x0a\x09editor := codeMirrorLib provided fromTextArea: aTextarea options: self editorOptions",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["fromTextArea:options:", "provided", "editorOptions"]
}, function ($methodClass){ return function (aTextarea){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.editor=$recv($recv(codeMirrorLib)._provided())._fromTextArea_options_(aTextarea,$self._editorOptions());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setEditorOn:",{aTextarea:aTextarea})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "updateState",
protocol: "updating",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "updateState\x0a\x09self hasModification \x0a\x09\x09ifTrue: [ state asJQuery addClass: 'modified' ]\x0a\x09\x09ifFalse: [ state asJQuery removeClass: 'modified' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:ifFalse:", "hasModification", "addClass:", "asJQuery", "removeClass:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($self._hasModification())){
$recv([$recv($self.state)._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._addClass_("modified");
} else {
$recv($recv($self.state)._asJQuery())._removeClass_("modified");
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"updateState",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);

$core.addMethod(
$core.method({
selector: "variableHintFor:token:",
protocol: "hints",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anEditor", "aToken"],
source: "variableHintFor: anEditor token: aToken\x0a\x09| variables classNames pseudoVariables |\x0a\x09\x0a\x09variables := (anEditor display wrapper asJQuery find: 'span.cm-variable') get\x0a\x09\x09collect: [ :each | each asJQuery html ].\x0a\x09\x0a\x09classNames := Smalltalk classes collect: [ :each | each name ].\x0a\x09pseudoVariables := Smalltalk pseudoVariableNames.\x0a\x09\x0a\x09^ ((variables, classNames, pseudoVariables) asSet asArray sort\x0a\x09\x09select: [ :each | each includesSubString: aToken string ])\x0a\x09\x09reject: [ :each | each = aToken string ]",
referencedClasses: ["Smalltalk"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["collect:", "get", "find:", "asJQuery", "wrapper", "display", "html", "classes", "name", "pseudoVariableNames", "reject:", "select:", "sort", "asArray", "asSet", ",", "includesSubString:", "string", "="]
}, function ($methodClass){ return function (anEditor,aToken){
var self=this,$self=this;
var variables,classNames,pseudoVariables;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
variables=[$recv($recv($recv([$recv($recv($recv(anEditor)._display())._wrapper())._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._find_("span.cm-variable"))._get())._collect_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv(each)._asJQuery())._html();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["collect:"]=1
//>>excludeEnd("ctx");
][0];
classNames=$recv($recv($globals.Smalltalk)._classes())._collect_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._name();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,2)});
//>>excludeEnd("ctx");
}));
pseudoVariables=$recv($globals.Smalltalk)._pseudoVariableNames();
return $recv($recv($recv($recv($recv([$recv($recv(variables).__comma(classNames)).__comma(pseudoVariables)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx[","]=1
//>>excludeEnd("ctx");
][0])._asSet())._asArray())._sort())._select_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._includesSubString_([$recv(aToken)._string()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["string"]=1
//>>excludeEnd("ctx");
][0]);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,3)});
//>>excludeEnd("ctx");
})))._reject_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each).__eq($recv(aToken)._string());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,4)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"variableHintFor:token:",{anEditor:anEditor,aToken:aToken,variables:variables,classNames:classNames,pseudoVariables:pseudoVariables})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget);


$core.addMethod(
$core.method({
selector: "hintFor:options:",
protocol: "hints",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anEditor", "options"],
source: "hintFor: anEditor options: options\x0a\x09| cursor token completions |\x0a\x09\x0a\x09cursor := anEditor getCursor.\x0a\x09token := anEditor getTokenAt: cursor.\x0a\x09token at: 'state' put: ((codeMirrorLib basicAt: 'innerMode')\x0a\x09\x09value: anEditor getMode value: (token at: 'state')) state.\x0a\x09\x0a\x09completions := token type = 'variable' \x0a\x09\x09ifTrue: [ HLCodeWidget variableHintFor: anEditor token: token ]\x0a\x09\x09ifFalse: [ HLCodeWidget messageHintFor: anEditor token: token ].\x0a\x09\x0a\x09^ #{\x0a\x09\x09'list' -> completions.\x0a\x09\x09'from' -> ((codeMirrorLib basicAt: 'Pos') value: cursor line value: token end).\x0a\x09\x09'to' -> ((codeMirrorLib basicAt: 'Pos') value: cursor line value: token start)\x0a\x09}",
referencedClasses: ["HLCodeWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["getCursor", "getTokenAt:", "at:put:", "state", "value:value:", "basicAt:", "getMode", "at:", "ifTrue:ifFalse:", "=", "type", "variableHintFor:token:", "messageHintFor:token:", "line", "end", "start"]
}, function ($methodClass){ return function (anEditor,options){
var self=this,$self=this;
var cursor,token,completions;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
cursor=$recv(anEditor)._getCursor();
token=$recv(anEditor)._getTokenAt_(cursor);
$recv(token)._at_put_("state",$recv([$recv([$recv(codeMirrorLib)._basicAt_("innerMode")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["basicAt:"]=1
//>>excludeEnd("ctx");
][0])._value_value_($recv(anEditor)._getMode(),$recv(token)._at_("state"))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["value:value:"]=1
//>>excludeEnd("ctx");
][0])._state());
if($core.assert($recv($recv(token)._type()).__eq("variable"))){
completions=$recv($globals.HLCodeWidget)._variableHintFor_token_(anEditor,token);
} else {
completions=$recv($globals.HLCodeWidget)._messageHintFor_token_(anEditor,token);
}
return $globals.HashedCollection._newFromPairs_(["list",completions,"from",[$recv([$recv(codeMirrorLib)._basicAt_("Pos")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["basicAt:"]=2
//>>excludeEnd("ctx");
][0])._value_value_([$recv(cursor)._line()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["line"]=1
//>>excludeEnd("ctx");
][0],$recv(token)._end())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["value:value:"]=2
//>>excludeEnd("ctx");
][0],"to",$recv($recv(codeMirrorLib)._basicAt_("Pos"))._value_value_($recv(cursor)._line(),$recv(token)._start())]);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"hintFor:options:",{anEditor:anEditor,options:options,cursor:cursor,token:token,completions:completions})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget.a$cls);

$core.addMethod(
$core.method({
selector: "initialize",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initialize\x0a\x09super initialize.\x0a\x09self \x0a\x09\x09setupCodeMirror;\x0a\x09\x09setupCommands;\x0a\x09\x09setupKeyMaps.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initialize", "setupCodeMirror", "setupCommands", "setupKeyMaps"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._initialize.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$self._setupCodeMirror();
$self._setupCommands();
$self._setupKeyMaps();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initialize",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget.a$cls);

$core.addMethod(
$core.method({
selector: "keyMap",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "keyMap\x0a\x09^ HLManager current keyBinder systemIsMac\x0a\x09\x09ifTrue: [ self macKeyMap ]\x0a\x09\x09ifFalse: [ self pcKeyMap ]",
referencedClasses: ["HLManager"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:ifFalse:", "systemIsMac", "keyBinder", "current", "macKeyMap", "pcKeyMap"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv($recv($recv($globals.HLManager)._current())._keyBinder())._systemIsMac())){
return $self._macKeyMap();
} else {
return $self._pcKeyMap();
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"keyMap",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget.a$cls);

$core.addMethod(
$core.method({
selector: "macKeyMap",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "macKeyMap\x0a\x09^ #{\x0a\x09\x09'Alt-Backspace'\x09\x09\x09-> 'delWordBefore'.\x0a\x09\x09'Alt-Delete'\x09\x09\x09-> 'delWordAfter'. \x0a\x09\x09'Alt-Left'\x09\x09\x09\x09-> 'goWordLeft'.\x0a\x09\x09'Alt-Right'\x09\x09\x09\x09-> 'goWordRight'. \x0a\x09\x09'Cmd-A'\x09\x09\x09\x09\x09-> 'selectAll'. \x0a\x09\x09'Cmd-Alt-F'\x09\x09\x09\x09-> 'replace'. \x0a\x09\x09'Cmd-D'\x09\x09\x09\x09\x09-> 'doIt'. \x0a\x09\x09'Cmd-B'\x09\x09\x09\x09\x09-> 'browseIt'. \x0a\x09\x09'Cmd-Down'\x09\x09\x09\x09-> 'goDocEnd'. \x0a\x09\x09'Cmd-End'\x09\x09\x09\x09-> 'goDocEnd'. \x0a\x09\x09'Cmd-F'\x09\x09\x09\x09\x09-> 'find'.\x0a\x09\x09'Cmd-G'\x09\x09\x09\x09\x09-> 'findNext'. \x0a\x09\x09'Cmd-I'\x09\x09\x09\x09\x09-> 'inspectIt'. \x0a\x09\x09'Cmd-Left'\x09\x09\x09\x09-> 'goLineStart'. \x0a\x09\x09'Cmd-P'\x09\x09\x09\x09\x09-> 'printIt'. \x0a\x09\x09'Cmd-Right'\x09\x09\x09\x09-> 'goLineEnd'. \x0a\x09\x09'Cmd-S'\x09\x09\x09\x09\x09-> 'saveIt'. \x0a\x09\x09'Cmd-Up'\x09\x09\x09\x09-> 'goDocStart'. \x0a\x09\x09'Cmd-Y'\x09\x09\x09\x09\x09-> 'redo'.\x0a\x09\x09'Cmd-Z'\x09\x09\x09\x09\x09-> 'undo'. \x0a\x09\x09'Cmd-['\x09\x09\x09\x09\x09-> 'indentLess'. \x0a\x09\x09'Cmd-]'\x09\x09\x09\x09\x09-> 'indentMore'.\x0a\x09\x09'Ctrl-Alt-Backspace'\x09-> 'delWordAfter'. \x0a\x09\x09'Shift-Cmd-Alt-F'\x09\x09-> 'replaceAll'.\x0a\x09\x09'Shift-Cmd-G'\x09\x09\x09-> 'findPrev'. \x0a\x09\x09'Shift-Cmd-Z'\x09\x09\x09-> 'redo'. \x0a    \x09'fallthrough' \x09\x09\x09-> { 'basic'. 'emacsy' }\x0a  }",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $globals.HashedCollection._newFromPairs_(["Alt-Backspace","delWordBefore","Alt-Delete","delWordAfter","Alt-Left","goWordLeft","Alt-Right","goWordRight","Cmd-A","selectAll","Cmd-Alt-F","replace","Cmd-D","doIt","Cmd-B","browseIt","Cmd-Down","goDocEnd","Cmd-End","goDocEnd","Cmd-F","find","Cmd-G","findNext","Cmd-I","inspectIt","Cmd-Left","goLineStart","Cmd-P","printIt","Cmd-Right","goLineEnd","Cmd-S","saveIt","Cmd-Up","goDocStart","Cmd-Y","redo","Cmd-Z","undo","Cmd-[","indentLess","Cmd-]","indentMore","Ctrl-Alt-Backspace","delWordAfter","Shift-Cmd-Alt-F","replaceAll","Shift-Cmd-G","findPrev","Shift-Cmd-Z","redo","fallthrough",["basic","emacsy"]]);

}; }),
$globals.HLCodeWidget.a$cls);

$core.addMethod(
$core.method({
selector: "messageHintFor:token:",
protocol: "hints",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anEditor", "aToken"],
source: "messageHintFor: anEditor token: aToken\x0a\x09^ (anEditor at: 'amberCodeWidget')\x0a\x09\x09messageHintFor: anEditor token: aToken",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["messageHintFor:token:", "at:"]
}, function ($methodClass){ return function (anEditor,aToken){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv(anEditor)._at_("amberCodeWidget"))._messageHintFor_token_(anEditor,aToken);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"messageHintFor:token:",{anEditor:anEditor,aToken:aToken})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget.a$cls);

$core.addMethod(
$core.method({
selector: "pcKeyMap",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "pcKeyMap\x0a\x09^ #{\x0a\x09\x09'Alt-Left' -> \x09\x09'goLineStart'. \x0a\x09\x09'Alt-Right' -> \x09\x09'goLineEnd'.\x0a\x09\x09'Alt-Up' -> \x09\x09'goDocStart'. \x0a\x09\x09'Ctrl-A' -> \x09\x09'selectAll'. \x0a\x09\x09'Ctrl-Backspace' -> 'delWordBefore'. \x0a\x09\x09'Ctrl-D' -> \x09\x09'doIt'. \x0a\x09\x09'Ctrl-B' -> \x09\x09'browseIt'. \x0a\x09\x09'Ctrl-Delete' -> \x09\x09'delWordAfter'. \x0a\x09\x09'Ctrl-Down' -> \x09\x09'goDocEnd'.\x0a\x09\x09'Ctrl-End' -> \x09\x09'goDocEnd'. \x0a\x09\x09'Ctrl-F' -> \x09\x09'find'.\x0a\x09\x09'Ctrl-G' -> \x09\x09'findNext'. \x0a\x09\x09'Ctrl-I' -> \x09\x09'inspectIt'.\x0a\x09\x09'Ctrl-Home' -> \x09\x09'goDocStart'. \x0a\x09\x09'Ctrl-Left' -> \x09\x09'goWordLeft'. \x0a\x09\x09'Ctrl-P' -> \x09\x09'printIt'.\x0a\x09\x09'Ctrl-Right' -> \x09'goWordRight'. \x0a\x09\x09'Ctrl-S' -> \x09\x09'saveIt'. \x0a\x09\x09'Ctrl-Y' -> \x09\x09'redo'.\x0a\x09\x09'Ctrl-Z' -> \x09\x09'undo'. \x0a\x09\x09'Ctrl-[' -> \x09\x09'indentLess'. \x0a\x09\x09'Ctrl-]' -> \x09\x09'indentMore'.\x0a\x09\x09'Shift-Ctrl-F' -> \x09'replace'. \x0a\x09\x09'Shift-Ctrl-G' -> \x09'findPrev'. \x0a\x09\x09'Shift-Ctrl-R' -> \x09'replaceAll'.\x0a\x09\x09'Shift-Ctrl-Z' -> \x09'redo'. \x0a\x09\x09'fallthrough' -> \x09#('basic')\x0a}",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $globals.HashedCollection._newFromPairs_(["Alt-Left","goLineStart","Alt-Right","goLineEnd","Alt-Up","goDocStart","Ctrl-A","selectAll","Ctrl-Backspace","delWordBefore","Ctrl-D","doIt","Ctrl-B","browseIt","Ctrl-Delete","delWordAfter","Ctrl-Down","goDocEnd","Ctrl-End","goDocEnd","Ctrl-F","find","Ctrl-G","findNext","Ctrl-I","inspectIt","Ctrl-Home","goDocStart","Ctrl-Left","goWordLeft","Ctrl-P","printIt","Ctrl-Right","goWordRight","Ctrl-S","saveIt","Ctrl-Y","redo","Ctrl-Z","undo","Ctrl-[","indentLess","Ctrl-]","indentMore","Shift-Ctrl-F","replace","Shift-Ctrl-G","findPrev","Shift-Ctrl-R","replaceAll","Shift-Ctrl-Z","redo","fallthrough",["basic"]]);

}; }),
$globals.HLCodeWidget.a$cls);

$core.addMethod(
$core.method({
selector: "setupCodeMirror",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setupCodeMirror\x0a\x09<inlineJS: '\x0a\x09\x09codeMirrorLib.keyMap[\x22default\x22].fallthrough = [\x22basic\x22];\x0a\x09\x09codeMirrorLib.commands.autocomplete = function(cm) {\x0a\x09\x09\x09codeMirrorLib.showHint(cm, $self._hintFor_options_.bind($self));\x0a\x09\x09}\x0a\x09'>",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [["inlineJS:", ["\x0a\x09\x09codeMirrorLib.keyMap[\x22default\x22].fallthrough = [\x22basic\x22];\x0a\x09\x09codeMirrorLib.commands.autocomplete = function(cm) {\x0a\x09\x09\x09codeMirrorLib.showHint(cm, $self._hintFor_options_.bind($self));\x0a\x09\x09}\x0a\x09"]]],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");

		codeMirrorLib.keyMap["default"].fallthrough = ["basic"];
		codeMirrorLib.commands.autocomplete = function(cm) {
			codeMirrorLib.showHint(cm, $self._hintFor_options_.bind($self));
		}
	;
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setupCodeMirror",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget.a$cls);

$core.addMethod(
$core.method({
selector: "setupCommands",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setupCommands\x0a\x09(codeMirrorLib basicAt: 'commands') \x0a\x09\x09at: 'doIt' put: [ :cm | cm amberCodeWidget doIt ];\x0a\x09\x09at: 'inspectIt' put: [ :cm | cm amberCodeWidget inspectIt ];\x0a\x09\x09at: 'printIt' put: [ :cm | cm amberCodeWidget printIt ];\x0a\x09\x09at: 'saveIt' put: [ :cm | cm amberCodeWidget saveIt ];\x0a\x09\x09at: 'browseIt' put: [ :cm | cm amberCodeWidget browseIt ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["at:put:", "basicAt:", "doIt", "amberCodeWidget", "inspectIt", "printIt", "saveIt", "browseIt"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(codeMirrorLib)._basicAt_("commands");
[$recv($1)._at_put_("doIt",(function(cm){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv([$recv(cm)._amberCodeWidget()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["amberCodeWidget"]=1
//>>excludeEnd("ctx");
][0])._doIt();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({cm:cm},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._at_put_("inspectIt",(function(cm){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv([$recv(cm)._amberCodeWidget()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["amberCodeWidget"]=2
//>>excludeEnd("ctx");
][0])._inspectIt();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({cm:cm},$ctx1,2)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($1)._at_put_("printIt",(function(cm){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv([$recv(cm)._amberCodeWidget()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["amberCodeWidget"]=3
//>>excludeEnd("ctx");
][0])._printIt();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({cm:cm},$ctx1,3)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=3
//>>excludeEnd("ctx");
][0];
[$recv($1)._at_put_("saveIt",(function(cm){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv([$recv(cm)._amberCodeWidget()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["amberCodeWidget"]=4
//>>excludeEnd("ctx");
][0])._saveIt();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({cm:cm},$ctx1,4)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=4
//>>excludeEnd("ctx");
][0];
$recv($1)._at_put_("browseIt",(function(cm){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv(cm)._amberCodeWidget())._browseIt();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({cm:cm},$ctx1,5)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setupCommands",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget.a$cls);

$core.addMethod(
$core.method({
selector: "setupKeyMaps",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setupKeyMaps\x0a\x09<inlineJS: 'codeMirrorLib.keyMap[\x22Amber\x22] = $self._keyMap()'>",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [["inlineJS:", ["codeMirrorLib.keyMap[\x22Amber\x22] = $self._keyMap()"]]],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
codeMirrorLib.keyMap["Amber"] = $self._keyMap();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setupKeyMaps",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget.a$cls);

$core.addMethod(
$core.method({
selector: "variableHintFor:token:",
protocol: "hints",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anEditor", "aToken"],
source: "variableHintFor: anEditor token: aToken\x0a\x09^ (anEditor at: 'amberCodeWidget')\x0a\x09\x09variableHintFor: anEditor token: aToken",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["variableHintFor:token:", "at:"]
}, function ($methodClass){ return function (anEditor,aToken){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv(anEditor)._at_("amberCodeWidget"))._variableHintFor_token_(anEditor,aToken);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"variableHintFor:token:",{anEditor:anEditor,aToken:aToken})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCodeWidget.a$cls);


$core.addClass("HLNavigationCodeWidget", $globals.HLCodeWidget, "Helios-Workspace");
$core.setSlots($globals.HLNavigationCodeWidget, ["methodContents"]);
$core.addMethod(
$core.method({
selector: "configureEditor",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "configureEditor\x0a\x09super configureEditor.\x0a\x09self contents: self methodContents",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["configureEditor", "contents:", "methodContents"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._configureEditor.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$self._contents_($self._methodContents());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"configureEditor",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLNavigationCodeWidget);

$core.addMethod(
$core.method({
selector: "contents:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "contents: aString\x0a\x09self methodContents: aString.\x0a\x09super contents: aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["methodContents:", "contents:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._methodContents_(aString);
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._contents_.call($self,aString))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"contents:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLNavigationCodeWidget);

$core.addMethod(
$core.method({
selector: "hasModification",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "hasModification\x0a\x09^ (self methodContents = self contents) not",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["not", "=", "methodContents", "contents"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._methodContents()).__eq($self._contents()))._not();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"hasModification",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLNavigationCodeWidget);

$core.addMethod(
$core.method({
selector: "methodContents",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "methodContents\x0a\x09^ methodContents ifNil: [ '' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.methodContents;
if($1 == null || $1.a$nil){
return "";
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"methodContents",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLNavigationCodeWidget);

$core.addMethod(
$core.method({
selector: "methodContents:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "methodContents: aString\x0a\x09^ methodContents := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.methodContents=aString;
return $self.methodContents;

}; }),
$globals.HLNavigationCodeWidget);

$core.addMethod(
$core.method({
selector: "previous",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "previous\x0a\x09\x22for browser lists widget\x22",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return self;

}; }),
$globals.HLNavigationCodeWidget);

$core.addMethod(
$core.method({
selector: "previous:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aWidget"],
source: "previous: aWidget\x0a\x09\x22for browser lists widget\x22",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aWidget){
var self=this,$self=this;
return self;

}; }),
$globals.HLNavigationCodeWidget);


$core.addMethod(
$core.method({
selector: "canBeOpenAsTab",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "canBeOpenAsTab\x0a\x09^ false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return false;

}; }),
$globals.HLNavigationCodeWidget.a$cls);

$core.addMethod(
$core.method({
selector: "on:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBrowserModel"],
source: "on: aBrowserModel\x0a\x09^ self new\x0a\x09\x09browserModel: aBrowserModel;\x0a\x09\x09yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["browserModel:", "new", "yourself"]
}, function ($methodClass){ return function (aBrowserModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._browserModel_(aBrowserModel);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{aBrowserModel:aBrowserModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLNavigationCodeWidget.a$cls);


$core.addClass("HLBrowserCodeWidget", $globals.HLNavigationCodeWidget, "Helios-Workspace");
$core.setSlots($globals.HLBrowserCodeWidget, ["browserModel"]);
$core.addMethod(
$core.method({
selector: "browserModel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "browserModel\x0a\x09^ browserModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.browserModel;

}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "browserModel:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBrowserModel"],
source: "browserModel: aBrowserModel\x0a\x09browserModel := aBrowserModel.\x0a\x09self \x0a\x09\x09observeSystem;\x0a\x09\x09observeBrowserModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["observeSystem", "observeBrowserModel"]
}, function ($methodClass){ return function (aBrowserModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.browserModel=aBrowserModel;
$self._observeSystem();
$self._observeBrowserModel();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"browserModel:",{aBrowserModel:aBrowserModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "observeBrowserModel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeBrowserModel\x0a\x09self browserModel announcer\x0a\x09\x09on: HLSaveSourceCode\x0a\x09\x09send: #onSaveIt\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLShowInstanceToggled\x0a\x09\x09send: #onShowInstanceToggled\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLSourceCodeSaved\x0a\x09\x09send: #onSourceCodeSaved\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLAboutToChange\x0a\x09\x09send: #onBrowserAboutToChange:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLParseErrorRaised\x0a\x09\x09send: #onParseError:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLCompileErrorRaised\x0a\x09\x09send: #onCompileError:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLUnknownVariableErrorRaised\x0a\x09\x09send: #onUnknownVariableError:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLInstVarAdded \x0a\x09\x09send: #onInstVarAdded\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLMethodSelected \x0a\x09\x09send: #onMethodSelected:\x0a\x09\x09to: self;\x0a\x09\x09\x0a    \x09on: HLClassSelected \x0a\x09\x09send: #onClassSelected:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLPackageSelected \x0a\x09\x09send: #onPackageSelected:\x0a\x09\x09to: self;\x0a\x09\x09\x0a    \x09on: HLProtocolSelected \x0a\x09\x09send: #onProtocolSelected: \x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLSourceCodeFocusRequested \x0a\x09\x09send: #onSourceCodeFocusRequested\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLShowTemplate\x0a\x09\x09send: #onShowTemplate:\x0a\x09\x09to: self",
referencedClasses: ["HLSaveSourceCode", "HLShowInstanceToggled", "HLSourceCodeSaved", "HLAboutToChange", "HLParseErrorRaised", "HLCompileErrorRaised", "HLUnknownVariableErrorRaised", "HLInstVarAdded", "HLMethodSelected", "HLClassSelected", "HLPackageSelected", "HLProtocolSelected", "HLSourceCodeFocusRequested", "HLShowTemplate"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "announcer", "browserModel"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._browserModel())._announcer();
[$recv($1)._on_send_to_($globals.HLSaveSourceCode,"onSaveIt",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLShowInstanceToggled,"onShowInstanceToggled",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLSourceCodeSaved,"onSourceCodeSaved",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=3
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLAboutToChange,"onBrowserAboutToChange:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=4
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLParseErrorRaised,"onParseError:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=5
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLCompileErrorRaised,"onCompileError:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=6
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLUnknownVariableErrorRaised,"onUnknownVariableError:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=7
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLInstVarAdded,"onInstVarAdded",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=8
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLMethodSelected,"onMethodSelected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=9
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLClassSelected,"onClassSelected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=10
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLPackageSelected,"onPackageSelected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=11
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLProtocolSelected,"onProtocolSelected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=12
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLSourceCodeFocusRequested,"onSourceCodeFocusRequested",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=13
//>>excludeEnd("ctx");
][0];
$recv($1)._on_send_to_($globals.HLShowTemplate,"onShowTemplate:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeBrowserModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "observeSystem",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeSystem\x0a\x09self browserModel systemAnnouncer\x0a    \x09on: MethodModified\x0a        send: #onMethodModified:\x0a\x09\x09to: self",
referencedClasses: ["MethodModified"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "systemAnnouncer", "browserModel"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self._browserModel())._systemAnnouncer())._on_send_to_($globals.MethodModified,"onMethodModified:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeSystem",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "onBrowserAboutToChange:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onBrowserAboutToChange: anAnnouncement\x0a\x09| block |\x0a\x09\x0a\x09block := anAnnouncement actionBlock.\x0a\x09\x0a\x09self hasModification\x0a\x09\x09ifTrue: [\x0a\x09\x09\x09self \x0a\x09\x09\x09\x09confirm: 'Changes have not been saved. Do you want to discard these changes?' \x0a\x09\x09\x09\x09ifTrue: [\x0a\x09\x09\x09\x09\x09\x22Don't ask twice\x22\x0a\x09\x09\x09\x09\x09self methodContents: self contents.\x0a\x09\x09\x09\x09\x09block value ].\x0a\x09\x09\x09\x0a\x09\x09\x09\x0a\x09\x09\x09HLChangeForbidden signal ]",
referencedClasses: ["HLChangeForbidden"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["actionBlock", "ifTrue:", "hasModification", "confirm:ifTrue:", "methodContents:", "contents", "value", "signal"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var block;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
block=$recv(anAnnouncement)._actionBlock();
if($core.assert($self._hasModification())){
$self._confirm_ifTrue_("Changes have not been saved. Do you want to discard these changes?",(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$self._methodContents_($self._contents());
return $recv(block)._value();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
$recv($globals.HLChangeForbidden)._signal();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onBrowserAboutToChange:",{anAnnouncement:anAnnouncement,block:block})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "onClassSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onClassSelected: anAnnouncement\x0a\x09| class |\x0a\x09\x0a\x09class:= anAnnouncement item.\x0a\x09\x0a\x09class ifNil: [ ^ self contents: '' ].\x0a\x09self contents: class definition",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["item", "ifNil:", "contents:", "definition"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var class_;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
class_=$recv(anAnnouncement)._item();
$1=class_;
if($1 == null || $1.a$nil){
return [$self._contents_("")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["contents:"]=1
//>>excludeEnd("ctx");
][0];
} else {
$1;
}
$self._contents_($recv(class_)._definition());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassSelected:",{anAnnouncement:anAnnouncement,class_:class_})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "onCompileError:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onCompileError: anAnnouncement\x0a\x09Terminal alert: anAnnouncement error messageText",
referencedClasses: ["Terminal"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["alert:", "messageText", "error"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($globals.Terminal)._alert_($recv($recv(anAnnouncement)._error())._messageText());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onCompileError:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "onInstVarAdded",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onInstVarAdded\x0a\x09self browserModel save: self contents",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["save:", "browserModel", "contents"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._browserModel())._save_($self._contents());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onInstVarAdded",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "onMethodModified:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onMethodModified: anAnnouncement\x0a\x09| method |\x0a\x09\x0a\x09method := anAnnouncement method.\x0a\x09\x0a\x09self browserModel selectedClass = method methodClass ifFalse: [ ^ self ].\x0a\x09self browserModel selectedMethod ifNil: [ ^ self ].\x0a\x09self browserModel selectedMethod selector = method selector ifFalse: [ ^ self ].\x0a\x0a\x09self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["method", "ifFalse:", "=", "selectedClass", "browserModel", "methodClass", "ifNil:", "selectedMethod", "selector", "refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var method;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
method=$recv(anAnnouncement)._method();
if(!$core.assert([$recv($recv([$self._browserModel()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["browserModel"]=1
//>>excludeEnd("ctx");
][0])._selectedClass()).__eq($recv(method)._methodClass())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["="]=1
//>>excludeEnd("ctx");
][0])){
return self;
}
$1=[$recv([$self._browserModel()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["browserModel"]=2
//>>excludeEnd("ctx");
][0])._selectedMethod()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedMethod"]=1
//>>excludeEnd("ctx");
][0];
if($1 == null || $1.a$nil){
return self;
} else {
$1;
}
if(!$core.assert($recv([$recv($recv($self._browserModel())._selectedMethod())._selector()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selector"]=1
//>>excludeEnd("ctx");
][0]).__eq($recv(method)._selector()))){
return self;
}
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onMethodModified:",{anAnnouncement:anAnnouncement,method:method})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "onMethodSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onMethodSelected: anAnnouncement\x0a\x09| method |\x0a\x09\x0a\x09method := anAnnouncement item.\x0a\x09\x0a\x09method ifNil: [ ^ self contents: '' ].\x0a\x09self contents: method source",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["item", "ifNil:", "contents:", "source"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var method;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
method=$recv(anAnnouncement)._item();
$1=method;
if($1 == null || $1.a$nil){
return [$self._contents_("")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["contents:"]=1
//>>excludeEnd("ctx");
][0];
} else {
$1;
}
$self._contents_($recv(method)._source());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onMethodSelected:",{anAnnouncement:anAnnouncement,method:method})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "onPackageSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onPackageSelected: anAnnouncement\x0a\x09| package |\x0a\x09\x0a\x09package := anAnnouncement item.\x0a\x09\x0a\x09package ifNil: [ ^ self contents: '' ].\x0a\x09self contents: package definition",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["item", "ifNil:", "contents:", "definition"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var package_;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
package_=$recv(anAnnouncement)._item();
$1=package_;
if($1 == null || $1.a$nil){
return [$self._contents_("")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["contents:"]=1
//>>excludeEnd("ctx");
][0];
} else {
$1;
}
$self._contents_($recv(package_)._definition());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onPackageSelected:",{anAnnouncement:anAnnouncement,package_:package_})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "onParseError:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onParseError: anAnnouncement\x0a\x09| lineIndex newContents |\x0a\x09\x0a\x09lineIndex := 1.\x0a\x09\x0a\x09self contents: (String streamContents: [ :stream |\x0a\x09\x09self contents linesDo: [ :each |\x0a\x09\x09\x09lineIndex = anAnnouncement line \x0a\x09\x09\x09\x09ifTrue: [ \x0a\x09\x09\x09\x09\x09stream \x0a\x09\x09\x09\x09\x09\x09nextPutAll: (each copyFrom: 1 to: anAnnouncement column);\x0a\x09\x09\x09\x09\x09\x09nextPutAll: '<- ';\x0a\x09\x09\x09\x09\x09\x09nextPutAll: anAnnouncement message;\x0a\x09\x09\x09\x09\x09\x09nextPutAll: ' ';\x0a\x09\x09\x09\x09\x09\x09nextPutAll: (each copyFrom: anAnnouncement column + 1 to: each size) ]\x0a\x09\x09\x09\x09ifFalse: [ stream nextPutAll: each ].\x0a\x09\x09\x09stream nextPutAll: String cr.\x0a\x09\x09\x09lineIndex := lineIndex + 1 ] ])",
referencedClasses: ["String"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["contents:", "streamContents:", "linesDo:", "contents", "ifTrue:ifFalse:", "=", "line", "nextPutAll:", "copyFrom:to:", "column", "message", "+", "size", "cr"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var lineIndex,newContents;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
lineIndex=(1);
$self._contents_($recv($globals.String)._streamContents_((function(stream){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._contents())._linesDo_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
if($core.assert($recv(lineIndex).__eq($recv(anAnnouncement)._line()))){
[$recv(stream)._nextPutAll_([$recv(each)._copyFrom_to_((1),[$recv(anAnnouncement)._column()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["column"]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["copyFrom:to:"]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["nextPutAll:"]=1
//>>excludeEnd("ctx");
][0];
[$recv(stream)._nextPutAll_("<- ")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["nextPutAll:"]=2
//>>excludeEnd("ctx");
][0];
[$recv(stream)._nextPutAll_($recv(anAnnouncement)._message())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["nextPutAll:"]=3
//>>excludeEnd("ctx");
][0];
[$recv(stream)._nextPutAll_(" ")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["nextPutAll:"]=4
//>>excludeEnd("ctx");
][0];
[$recv(stream)._nextPutAll_($recv(each)._copyFrom_to_([$recv($recv(anAnnouncement)._column()).__plus((1))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["+"]=1
//>>excludeEnd("ctx");
][0],$recv(each)._size()))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["nextPutAll:"]=5
//>>excludeEnd("ctx");
][0];
} else {
[$recv(stream)._nextPutAll_(each)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["nextPutAll:"]=6
//>>excludeEnd("ctx");
][0];
}
$recv(stream)._nextPutAll_($recv($globals.String)._cr());
lineIndex=$recv(lineIndex).__plus((1));
return lineIndex;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({each:each},$ctx2,2)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({stream:stream},$ctx1,1)});
//>>excludeEnd("ctx");
})));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onParseError:",{anAnnouncement:anAnnouncement,lineIndex:lineIndex,newContents:newContents})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "onProtocolSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onProtocolSelected: anAnnouncement\x0a\x09self browserModel selectedClass ifNil: [ ^ self contents: '' ].\x0a\x09self contents: self browserModel selectedClass definition",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "selectedClass", "browserModel", "contents:", "definition"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=[$recv([$self._browserModel()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["browserModel"]=1
//>>excludeEnd("ctx");
][0])._selectedClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedClass"]=1
//>>excludeEnd("ctx");
][0];
if($1 == null || $1.a$nil){
return [$self._contents_("")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["contents:"]=1
//>>excludeEnd("ctx");
][0];
} else {
$1;
}
$self._contents_($recv($recv($self._browserModel())._selectedClass())._definition());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onProtocolSelected:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "onSaveIt",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onSaveIt\x0a\x09self browserModel save: self contents",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["save:", "browserModel", "contents"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._browserModel())._save_($self._contents());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onSaveIt",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "onShowInstanceToggled",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onShowInstanceToggled\x0a\x09self browserModel selectedClass ifNil: [ ^ self contents: '' ].\x0a    \x0a\x09self contents: self browserModel selectedClass definition",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "selectedClass", "browserModel", "contents:", "definition"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=[$recv([$self._browserModel()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["browserModel"]=1
//>>excludeEnd("ctx");
][0])._selectedClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedClass"]=1
//>>excludeEnd("ctx");
][0];
if($1 == null || $1.a$nil){
return [$self._contents_("")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["contents:"]=1
//>>excludeEnd("ctx");
][0];
} else {
$1;
}
$self._contents_($recv($recv($self._browserModel())._selectedClass())._definition());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onShowInstanceToggled",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "onShowTemplate:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onShowTemplate: anAnnouncement\x0a\x09self contents: anAnnouncement template",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["contents:", "template"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._contents_($recv(anAnnouncement)._template());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onShowTemplate:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "onSourceCodeFocusRequested",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onSourceCodeFocusRequested\x0a\x09self focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focus"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onSourceCodeFocusRequested",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "onSourceCodeSaved",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onSourceCodeSaved\x0a\x09self methodContents: self contents.\x0a\x09self updateState",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["methodContents:", "contents", "updateState"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._methodContents_($self._contents());
$self._updateState();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onSourceCodeSaved",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "onUnknownVariableError:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onUnknownVariableError: anAnnouncement\x0a\x09| error |\x0a\x09\x0a\x09error := anAnnouncement error.\x0a\x09\x0a\x09self \x0a\x09\x09confirm: (String streamContents: [ :stream |\x0a\x09\x09\x09stream \x0a\x09\x09\x09\x09nextPutAll: error messageText;\x0a\x09\x09\x09\x09nextPutAll: String cr;\x0a\x09\x09\x09\x09nextPutAll: 'Would you like to define an instance variable?' ])\x0a\x09\x09ifTrue: [\x0a\x09\x09\x09self browserModel addInstVarNamed: error variableName ]",
referencedClasses: ["String"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["error", "confirm:ifTrue:", "streamContents:", "nextPutAll:", "messageText", "cr", "addInstVarNamed:", "browserModel", "variableName"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var error;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
error=$recv(anAnnouncement)._error();
$self._confirm_ifTrue_($recv($globals.String)._streamContents_((function(stream){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
[$recv(stream)._nextPutAll_($recv(error)._messageText())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["nextPutAll:"]=1
//>>excludeEnd("ctx");
][0];
[$recv(stream)._nextPutAll_($recv($globals.String)._cr())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["nextPutAll:"]=2
//>>excludeEnd("ctx");
][0];
return $recv(stream)._nextPutAll_("Would you like to define an instance variable?");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({stream:stream},$ctx1,1)});
//>>excludeEnd("ctx");
})),(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._browserModel())._addInstVarNamed_($recv(error)._variableName());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onUnknownVariableError:",{anAnnouncement:anAnnouncement,error:error})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "refresh",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "refresh\x0a\x09self hasModification ifTrue: [ ^ self ].\x0a    self hasFocus ifTrue: [ ^ self ].\x0a\x0a\x09self contents: self browserModel selectedMethod source",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "hasModification", "hasFocus", "contents:", "source", "selectedMethod", "browserModel"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($self._hasModification())){
return self;
}
if($core.assert($self._hasFocus())){
return self;
}
$self._contents_($recv($recv($self._browserModel())._selectedMethod())._source());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"refresh",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "renderButtonsOn:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderButtonsOn: html\x0a\x09html button \x0a\x09\x09class: 'button';\x0a\x09\x09with: 'SaveIt';\x0a\x09\x09onClick: [ self saveIt ].\x0a\x09super renderButtonsOn: html",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "button", "with:", "onClick:", "saveIt", "renderButtonsOn:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._button();
$recv($1)._class_("button");
$recv($1)._with_("SaveIt");
$recv($1)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._saveIt();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._renderButtonsOn_.call($self,html))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderButtonsOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "saveIt",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "saveIt\x0a\x09self browserModel saveSourceCode",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["saveSourceCode", "browserModel"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._browserModel())._saveSourceCode();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"saveIt",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);

$core.addMethod(
$core.method({
selector: "unregister",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "unregister\x0a\x09super unregsiter.\x0a\x09\x0a\x09self browserModel announcer unsubscribe: self.\x0a\x09self browserModel systemAnnouncer unsubscribe: self",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unregsiter", "unsubscribe:", "announcer", "browserModel", "systemAnnouncer"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._unregsiter.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
[$recv($recv([$self._browserModel()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["browserModel"]=1
//>>excludeEnd("ctx");
][0])._announcer())._unsubscribe_(self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["unsubscribe:"]=1
//>>excludeEnd("ctx");
][0];
$recv($recv($self._browserModel())._systemAnnouncer())._unsubscribe_(self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unregister",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget);


$core.addMethod(
$core.method({
selector: "canBeOpenAsTab",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "canBeOpenAsTab\x0a\x09^ false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return false;

}; }),
$globals.HLBrowserCodeWidget.a$cls);

$core.addMethod(
$core.method({
selector: "on:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBrowserModel"],
source: "on: aBrowserModel\x0a\x09^ self new\x0a\x09\x09browserModel: aBrowserModel;\x0a\x09\x09yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["browserModel:", "new", "yourself"]
}, function ($methodClass){ return function (aBrowserModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._browserModel_(aBrowserModel);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{aBrowserModel:aBrowserModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserCodeWidget.a$cls);


$core.addClass("HLWorkspace", $globals.HLWidget, "Helios-Workspace");
$core.setSlots($globals.HLWorkspace, ["codeWidget", "transcript"]);
$core.addMethod(
$core.method({
selector: "canHaveFocus",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "canHaveFocus\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLWorkspace);

$core.addMethod(
$core.method({
selector: "codeWidget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "codeWidget\x0a\x09^ codeWidget ifNil: [ codeWidget := HLCodeWidget new ]",
referencedClasses: ["HLCodeWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.codeWidget;
if($1 == null || $1.a$nil){
$self.codeWidget=$recv($globals.HLCodeWidget)._new();
return $self.codeWidget;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"codeWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWorkspace);

$core.addMethod(
$core.method({
selector: "focus",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focus\x0a\x09^ self codeWidget focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focus", "codeWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._codeWidget())._focus();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focus",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWorkspace);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09html with: (HLContainer with: (HLHorizontalSplitter\x0a\x09\x09with: self codeWidget\x0a\x09\x09with: [ :canvas | self renderTranscriptOn: canvas ]))",
referencedClasses: ["HLContainer", "HLHorizontalSplitter"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "with:with:", "codeWidget", "renderTranscriptOn:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$recv(html)._with_($recv($globals.HLContainer)._with_($recv($globals.HLHorizontalSplitter)._with_with_($self._codeWidget(),(function(canvas){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._renderTranscriptOn_(canvas);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({canvas:canvas},$ctx1,1)});
//>>excludeEnd("ctx");
}))))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWorkspace);

$core.addMethod(
$core.method({
selector: "renderTranscriptOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderTranscriptOn: html\x0a\x09html div \x0a\x09\x09class: 'transcript-container';\x0a\x09\x09with: [\x0a\x09\x09\x09html div\x0a\x09\x09\x09\x09class: 'list-label';\x0a\x09\x09\x09\x09with: 'Transcript'.\x0a\x09\x09\x09self transcript renderOn: html ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "with:", "renderOn:", "transcript"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=[$recv(html)._div()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["div"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._class_("transcript-container")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$2=$recv(html)._div();
$recv($2)._class_("list-label");
$recv($2)._with_("Transcript");
return $recv($self._transcript())._renderOn_(html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderTranscriptOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWorkspace);

$core.addMethod(
$core.method({
selector: "transcript",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "transcript\x0a\x09^ transcript ifNil: [ transcript := HLTranscript new ]",
referencedClasses: ["HLTranscript"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.transcript;
if($1 == null || $1.a$nil){
$self.transcript=$recv($globals.HLTranscript)._new();
return $self.transcript;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"transcript",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWorkspace);

$core.addMethod(
$core.method({
selector: "unregister",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "unregister\x0a\x09super unregister.\x0a\x09self transcript unregister",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unregister", "transcript"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._unregister.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["unregister"]=1,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv($self._transcript())._unregister();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unregister",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWorkspace);


$core.addMethod(
$core.method({
selector: "canBeOpenAsTab",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "canBeOpenAsTab\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLWorkspace.a$cls);

$core.addMethod(
$core.method({
selector: "tabClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabClass\x0a\x09^ 'workspace'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "workspace";

}; }),
$globals.HLWorkspace.a$cls);

$core.addMethod(
$core.method({
selector: "tabLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabLabel\x0a\x09^ 'Workspace'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Workspace";

}; }),
$globals.HLWorkspace.a$cls);

$core.addMethod(
$core.method({
selector: "tabPriority",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabPriority\x0a\x09^ 10",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return (10);

}; }),
$globals.HLWorkspace.a$cls);

});
