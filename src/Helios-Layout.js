define(["amber/boot", "require", "amber/core/Kernel-Objects", "amber/web/Web", "helios/Helios-Core"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-Layout");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLContainer", $globals.HLWidget, "Helios-Layout");
$core.setSlots($globals.HLContainer, ["splitter"]);
$core.addMethod(
$core.method({
selector: "renderOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderOn: html\x0a\x09html div \x0a    \x09class: 'tool_container'; \x0a        with: self splitter",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "with:", "splitter"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._div();
$recv($1)._class_("tool_container");
$recv($1)._with_($self._splitter());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLContainer);

$core.addMethod(
$core.method({
selector: "splitter",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "splitter\x0a\x09^ splitter",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.splitter;

}; }),
$globals.HLContainer);

$core.addMethod(
$core.method({
selector: "splitter:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSplitter"],
source: "splitter: aSplitter\x0a\x09splitter := aSplitter",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aSplitter){
var self=this,$self=this;
$self.splitter=aSplitter;
return self;

}; }),
$globals.HLContainer);


$core.addMethod(
$core.method({
selector: "with:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSplitter"],
source: "with: aSplitter\x0a\x09^ self new \x0a    \x09splitter: aSplitter; \x0a        yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["splitter:", "new", "yourself"]
}, function ($methodClass){ return function (aSplitter){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._splitter_(aSplitter);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"with:",{aSplitter:aSplitter})});
//>>excludeEnd("ctx");
}; }),
$globals.HLContainer.a$cls);


$core.addClass("HLSplitter", $globals.Widget, "Helios-Layout");
$core.setSlots($globals.HLSplitter, ["firstWidget", "secondWidget", "firstPane", "secondPane", "splitter"]);
$core.addMethod(
$core.method({
selector: "cssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "cssClass\x0a\x09^ 'splitter'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "splitter";

}; }),
$globals.HLSplitter);

$core.addMethod(
$core.method({
selector: "firstWidget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "firstWidget\x0a\x09^ firstWidget",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.firstWidget;

}; }),
$globals.HLSplitter);

$core.addMethod(
$core.method({
selector: "firstWidget:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aWidget"],
source: "firstWidget: aWidget\x0a\x09firstWidget := aWidget",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aWidget){
var self=this,$self=this;
$self.firstWidget=aWidget;
return self;

}; }),
$globals.HLSplitter);

$core.addMethod(
$core.method({
selector: "isHeliosSplitter",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isHeliosSplitter\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLSplitter);

$core.addMethod(
$core.method({
selector: "panesCssClass",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "panesCssClass\x0a\x09^ 'panes'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "panes";

}; }),
$globals.HLSplitter);

$core.addMethod(
$core.method({
selector: "renderOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderOn: html\x0a\x09html div class: self panesCssClass; with: [\x0a\x09\x09firstPane := html div class: 'pane'; with: self firstWidget.\x0a    \x09splitter := html div class: self cssClass.\x0a    \x09secondPane := html div class: 'pane'; with: self secondWidget ].\x0a        \x0a\x09self setupSplitter",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "panesCssClass", "with:", "firstWidget", "cssClass", "secondWidget", "setupSplitter"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3;
$1=[$recv(html)._div()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["div"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._class_($self._panesCssClass())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$2=[$recv(html)._div()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["div"]=2
//>>excludeEnd("ctx");
][0];
[$recv($2)._class_("pane")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["class:"]=2
//>>excludeEnd("ctx");
][0];
$self.firstPane=[$recv($2)._with_($self._firstWidget())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=2
//>>excludeEnd("ctx");
][0];
$self.splitter=[$recv([$recv(html)._div()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["div"]=3
//>>excludeEnd("ctx");
][0])._class_($self._cssClass())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["class:"]=3
//>>excludeEnd("ctx");
][0];
$3=$recv(html)._div();
$recv($3)._class_("pane");
$self.secondPane=$recv($3)._with_($self._secondWidget());
return $self.secondPane;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
$self._setupSplitter();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSplitter);

$core.addMethod(
$core.method({
selector: "resize",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "resize",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return self;

}; }),
$globals.HLSplitter);

$core.addMethod(
$core.method({
selector: "secondWidget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "secondWidget\x0a\x09^ secondWidget",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.secondWidget;

}; }),
$globals.HLSplitter);

$core.addMethod(
$core.method({
selector: "secondWidget:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aWidget"],
source: "secondWidget: aWidget\x0a\x09secondWidget := aWidget",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aWidget){
var self=this,$self=this;
$self.secondWidget=aWidget;
return self;

}; }),
$globals.HLSplitter);

$core.addMethod(
$core.method({
selector: "setupSplitter",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setupSplitter",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return self;

}; }),
$globals.HLSplitter);


$core.addMethod(
$core.method({
selector: "with:with:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aWidget", "anotherWidget"],
source: "with: aWidget with: anotherWidget\x0a\x09^ self new\x0a    \x09\x09firstWidget: aWidget;\x0a            secondWidget: anotherWidget;\x0a            yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["firstWidget:", "new", "secondWidget:", "yourself"]
}, function ($methodClass){ return function (aWidget,anotherWidget){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._firstWidget_(aWidget);
$recv($1)._secondWidget_(anotherWidget);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"with:with:",{aWidget:aWidget,anotherWidget:anotherWidget})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSplitter.a$cls);


$core.addClass("HLHorizontalSplitter", $globals.HLSplitter, "Helios-Layout");
$core.addMethod(
$core.method({
selector: "cssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "cssClass\x0a\x09^ super cssClass, ' horizontal'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: [",", "cssClass"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv([(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._cssClass.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0]).__comma(" horizontal");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"cssClass",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLHorizontalSplitter);

$core.addMethod(
$core.method({
selector: "panesCssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "panesCssClass\x0a\x09^ super panesCssClass, ' horizontal'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: [",", "panesCssClass"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv([(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._panesCssClass.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0]).__comma(" horizontal");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"panesCssClass",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLHorizontalSplitter);

$core.addMethod(
$core.method({
selector: "resize",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "resize\x0a\x09self resize: (splitter asJQuery css: 'top')",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["resize:", "css:", "asJQuery"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._resize_($recv($recv($self.splitter)._asJQuery())._css_("top"));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"resize",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLHorizontalSplitter);

$core.addMethod(
$core.method({
selector: "resize:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInteger"],
source: "resize: anInteger\x0a\x09| container size offset percentage |\x0a    \x0a    container := firstPane asJQuery parent.\x0a\x09offset := firstPane asJQuery offset top.\x0a    size := container height.\x0a\x09\x0a\x09percentage := (size - (anInteger - offset)) / size * 100.\x0a\x09percentage := 80 min: (percentage max: 20).\x0a\x09\x0a    firstPane asJQuery css: 'bottom' put: percentage asString, '%'.\x0a\x09\x0a\x09splitter asJQuery css: 'top' put: (100 - percentage) asString, '%'.\x0a\x09secondPane asJQuery css: 'top' put: (100 - percentage) asString, '%'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["parent", "asJQuery", "top", "offset", "height", "*", "/", "-", "min:", "max:", "css:put:", ",", "asString"]
}, function ($methodClass){ return function (anInteger){
var self=this,$self=this;
var container,size,offset,percentage;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
container=$recv([$recv($self.firstPane)._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._parent();
offset=$recv($recv([$recv($self.firstPane)._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=2
//>>excludeEnd("ctx");
][0])._offset())._top();
size=$recv(container)._height();
percentage=$recv($recv([$recv(size).__minus([$recv(anInteger).__minus(offset)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["-"]=2
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["-"]=1
//>>excludeEnd("ctx");
][0]).__slash(size)).__star((100));
percentage=(80)._min_($recv(percentage)._max_((20)));
[$recv([$recv($self.firstPane)._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=3
//>>excludeEnd("ctx");
][0])._css_put_("bottom",[$recv([$recv(percentage)._asString()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asString"]=1
//>>excludeEnd("ctx");
][0]).__comma("%")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx[","]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["css:put:"]=1
//>>excludeEnd("ctx");
][0];
[$recv([$recv($self.splitter)._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=4
//>>excludeEnd("ctx");
][0])._css_put_("top",[$recv([$recv([(100).__minus(percentage)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["-"]=3
//>>excludeEnd("ctx");
][0])._asString()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asString"]=2
//>>excludeEnd("ctx");
][0]).__comma("%")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx[","]=2
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["css:put:"]=2
//>>excludeEnd("ctx");
][0];
$recv($recv($self.secondPane)._asJQuery())._css_put_("top",$recv($recv((100).__minus(percentage))._asString()).__comma("%"));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"resize:",{anInteger:anInteger,container:container,size:size,offset:offset,percentage:percentage})});
//>>excludeEnd("ctx");
}; }),
$globals.HLHorizontalSplitter);

$core.addMethod(
$core.method({
selector: "setupSplitter",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setupSplitter\x0a\x09splitter asJQuery draggable: #{ \x0a    \x09'axis' -> 'y'. \x0a        'containment' -> splitter asJQuery parent.\x0a        'helper' -> 'clone'.\x0a        'start' -> [ :e :ui | self startResizing: ui helper ].\x0a        'drag' -> [ :e :ui | self resize: ui offset top ] }",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["draggable:", "asJQuery", "parent", "startResizing:", "helper", "resize:", "top", "offset"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv([$recv($self.splitter)._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._draggable_($globals.HashedCollection._newFromPairs_(["axis","y","containment",$recv($recv($self.splitter)._asJQuery())._parent(),"helper","clone","start",(function(e,ui){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._startResizing_($recv(ui)._helper());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({e:e,ui:ui},$ctx1,1)});
//>>excludeEnd("ctx");
}),"drag",(function(e,ui){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._resize_($recv($recv(ui)._offset())._top());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({e:e,ui:ui},$ctx1,2)});
//>>excludeEnd("ctx");
})]));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setupSplitter",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLHorizontalSplitter);

$core.addMethod(
$core.method({
selector: "startResizing:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSplitter"],
source: "startResizing: aSplitter\x0a\x09aSplitter width: splitter asJQuery width",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["width:", "width", "asJQuery"]
}, function ($methodClass){ return function (aSplitter){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(aSplitter)._width_($recv($recv($self.splitter)._asJQuery())._width());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"startResizing:",{aSplitter:aSplitter})});
//>>excludeEnd("ctx");
}; }),
$globals.HLHorizontalSplitter);



$core.addClass("HLVerticalSplitter", $globals.HLSplitter, "Helios-Layout");
$core.addMethod(
$core.method({
selector: "cssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "cssClass\x0a\x09^ super cssClass, ' vertical'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: [",", "cssClass"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv([(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._cssClass.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0]).__comma(" vertical");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"cssClass",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLVerticalSplitter);

$core.addMethod(
$core.method({
selector: "panesCssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "panesCssClass\x0a\x09^ super panesCssClass, ' vertical'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: [",", "panesCssClass"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv([(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._panesCssClass.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0]).__comma(" vertical");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"panesCssClass",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLVerticalSplitter);

$core.addMethod(
$core.method({
selector: "resize",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "resize\x0a\x09self resize: (splitter asJQuery css: 'left')",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["resize:", "css:", "asJQuery"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._resize_($recv($recv($self.splitter)._asJQuery())._css_("left"));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"resize",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLVerticalSplitter);

$core.addMethod(
$core.method({
selector: "resize:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInteger"],
source: "resize: anInteger\x0a\x09| container size offset percentage |\x0a    \x0a    container := firstPane asJQuery parent.\x0a\x09offset := firstPane asJQuery offset left.\x0a    size := container width.\x0a\x09\x0a\x09percentage := (size - (anInteger - offset)) / size * 100.\x0a\x09percentage := 80 min: (percentage max: 20).\x0a\x09\x0a    firstPane asJQuery css: 'right' put: percentage asString, '%'.\x0a\x09\x0a\x09splitter asJQuery css: 'left' put: (100 - percentage) asString, '%'.\x0a\x09secondPane asJQuery css: 'left' put: (100 - percentage) asString, '%'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["parent", "asJQuery", "left", "offset", "width", "*", "/", "-", "min:", "max:", "css:put:", ",", "asString"]
}, function ($methodClass){ return function (anInteger){
var self=this,$self=this;
var container,size,offset,percentage;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
container=$recv([$recv($self.firstPane)._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._parent();
offset=$recv($recv([$recv($self.firstPane)._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=2
//>>excludeEnd("ctx");
][0])._offset())._left();
size=$recv(container)._width();
percentage=$recv($recv([$recv(size).__minus([$recv(anInteger).__minus(offset)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["-"]=2
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["-"]=1
//>>excludeEnd("ctx");
][0]).__slash(size)).__star((100));
percentage=(80)._min_($recv(percentage)._max_((20)));
[$recv([$recv($self.firstPane)._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=3
//>>excludeEnd("ctx");
][0])._css_put_("right",[$recv([$recv(percentage)._asString()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asString"]=1
//>>excludeEnd("ctx");
][0]).__comma("%")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx[","]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["css:put:"]=1
//>>excludeEnd("ctx");
][0];
[$recv([$recv($self.splitter)._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=4
//>>excludeEnd("ctx");
][0])._css_put_("left",[$recv([$recv([(100).__minus(percentage)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["-"]=3
//>>excludeEnd("ctx");
][0])._asString()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asString"]=2
//>>excludeEnd("ctx");
][0]).__comma("%")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx[","]=2
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["css:put:"]=2
//>>excludeEnd("ctx");
][0];
$recv($recv($self.secondPane)._asJQuery())._css_put_("left",$recv($recv((100).__minus(percentage))._asString()).__comma("%"));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"resize:",{anInteger:anInteger,container:container,size:size,offset:offset,percentage:percentage})});
//>>excludeEnd("ctx");
}; }),
$globals.HLVerticalSplitter);

$core.addMethod(
$core.method({
selector: "setupSplitter",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setupSplitter\x0a\x09splitter asJQuery draggable: #{ \x0a    \x09'axis' -> 'x'. \x0a        'containment' -> splitter asJQuery parent.\x0a        'helper' -> 'clone'.\x0a        'start' -> [ :e :ui | self startResizing: ui helper ].\x0a        'drag' -> [ :e :ui | self resize: (ui offset left) ] }",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["draggable:", "asJQuery", "parent", "startResizing:", "helper", "resize:", "left", "offset"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv([$recv($self.splitter)._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._draggable_($globals.HashedCollection._newFromPairs_(["axis","x","containment",$recv($recv($self.splitter)._asJQuery())._parent(),"helper","clone","start",(function(e,ui){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._startResizing_($recv(ui)._helper());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({e:e,ui:ui},$ctx1,1)});
//>>excludeEnd("ctx");
}),"drag",(function(e,ui){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._resize_($recv($recv(ui)._offset())._left());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({e:e,ui:ui},$ctx1,2)});
//>>excludeEnd("ctx");
})]));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setupSplitter",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLVerticalSplitter);

$core.addMethod(
$core.method({
selector: "startResizing:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSplitter"],
source: "startResizing: aSplitter\x0a\x09aSplitter height: splitter asJQuery height",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["height:", "height", "asJQuery"]
}, function ($methodClass){ return function (aSplitter){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(aSplitter)._height_($recv($recv($self.splitter)._asJQuery())._height());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"startResizing:",{aSplitter:aSplitter})});
//>>excludeEnd("ctx");
}; }),
$globals.HLVerticalSplitter);


$core.addMethod(
$core.method({
selector: "isHeliosSplitter",
protocol: "*Helios-Layout",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isHeliosSplitter\x0a\x09^ false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return false;

}; }),
$globals.Object);

});
