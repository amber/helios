define(["amber/boot", "require", "helios/Helios-Core"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-SUnit");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLMultiSelectToolListWidget", $globals.HLToolListWidget, "Helios-SUnit");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLMultiSelectToolListWidget.comment="This is a list that handles multiple selection";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "activeItemCssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "activeItemCssClass\x0a\x09^'selector'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "selector";

}; }),
$globals.HLMultiSelectToolListWidget);

$core.addMethod(
$core.method({
selector: "isSelected:",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "isSelected: anObject\x0a\x09self subclassResponsibility",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["subclassResponsibility"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._subclassResponsibility();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isSelected:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMultiSelectToolListWidget);

$core.addMethod(
$core.method({
selector: "listCssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "listCssClass \x0a\x09^'nav nav-multiselect nav-pills nav-stacked'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "nav nav-multiselect nav-pills nav-stacked";

}; }),
$globals.HLMultiSelectToolListWidget);

$core.addMethod(
$core.method({
selector: "listCssClassForItem:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "listCssClassForItem: anObject\x0a\x09^(super listCssClassForItem: anObject), ((self isSelected: anObject)\x0a\x09\x09ifTrue: [' active']\x0a\x09\x09ifFalse: ['']).",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: [",", "listCssClassForItem:", "ifTrue:ifFalse:", "isSelected:"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._listCssClassForItem_.call($self,anObject))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
if($core.assert($self._isSelected_(anObject))){
$2=" active";
} else {
$2="";
}
return $recv($1).__comma($2);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"listCssClassForItem:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMultiSelectToolListWidget);

$core.addMethod(
$core.method({
selector: "reselectItem:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "reselectItem: anObject\x0a\x09anObject ifNil: [^self].\x0a\x09self toggleSelection: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "toggleSelection:"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if(anObject == null || anObject.a$nil){
return self;
} else {
anObject;
}
$self._toggleSelection_(anObject);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"reselectItem:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMultiSelectToolListWidget);

$core.addMethod(
$core.method({
selector: "select:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "select: anObject\x0a\x09self subclassResponsibility",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["subclassResponsibility"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._subclassResponsibility();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"select:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMultiSelectToolListWidget);

$core.addMethod(
$core.method({
selector: "toggleListItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aListItem"],
source: "toggleListItem: aListItem\x0a\x09| item |\x0a\x09\x0a\x09(aListItem get: 0) ifNil: [ ^ self ].\x0a\x09\x22Find item\x22\x0a\x09item := aListItem data: 'item'.\x0a\x09self toggleSelection: item",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "get:", "data:", "toggleSelection:"]
}, function ($methodClass){ return function (aListItem){
var self=this,$self=this;
var item;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(aListItem)._get_((0));
if($1 == null || $1.a$nil){
return self;
} else {
$1;
}
item=$recv(aListItem)._data_("item");
$self._toggleSelection_(item);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"toggleListItem:",{aListItem:aListItem,item:item})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMultiSelectToolListWidget);

$core.addMethod(
$core.method({
selector: "toggleSelection:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "toggleSelection: anObject\x0a\x09(self isSelected: anObject) \x0a\x09\x09ifTrue: [ self unselect: anObject ]\x0a\x09\x09ifFalse: [self select: anObject ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:ifFalse:", "isSelected:", "unselect:", "select:"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($self._isSelected_(anObject))){
$self._unselect_(anObject);
} else {
$self._select_(anObject);
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"toggleSelection:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMultiSelectToolListWidget);

$core.addMethod(
$core.method({
selector: "unselect:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "unselect: anObject\x0a\x09self subclassResponsibility",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["subclassResponsibility"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._subclassResponsibility();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unselect:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMultiSelectToolListWidget);



$core.addClass("HLSUnitClassesListWidget", $globals.HLMultiSelectToolListWidget, "Helios-SUnit");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnitClassesListWidget.comment="I display a list of  classes (subclasses of `TestCase`).";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "buttonsDivCssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "buttonsDivCssClass\x0a\x09^ 'buttons_bar'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "buttons_bar";

}; }),
$globals.HLSUnitClassesListWidget);

$core.addMethod(
$core.method({
selector: "cssClassForItem:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass"],
source: "cssClassForItem: aClass\x0a\x09^ aClass theNonMetaClass classTag",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["classTag", "theNonMetaClass"]
}, function ($methodClass){ return function (aClass){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv(aClass)._theNonMetaClass())._classTag();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"cssClassForItem:",{aClass:aClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitClassesListWidget);

$core.addMethod(
$core.method({
selector: "initializeItems",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initializeItems\x0a\x09^items := model testClasses",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["testClasses"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.items=$recv($self.model)._testClasses();
return $self.items;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initializeItems",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitClassesListWidget);

$core.addMethod(
$core.method({
selector: "isSelected:",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "isSelected: anObject\x0a\x09^model selectedClasses includes: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["includes:", "selectedClasses"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self.model)._selectedClasses())._includes_(anObject);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isSelected:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitClassesListWidget);

$core.addMethod(
$core.method({
selector: "items",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "items\x0a\x09^ items ifNil: [ self initializeItems ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "initializeItems"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.items;
if($1 == null || $1.a$nil){
return $self._initializeItems();
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"items",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitClassesListWidget);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Classes'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Classes";

}; }),
$globals.HLSUnitClassesListWidget);

$core.addMethod(
$core.method({
selector: "observeModel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeModel\x0a    self model announcer \x0a\x09\x09on: HLPackageSelected\x0a\x09\x09send: #onPackageSelected:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLPackageUnselected\x0a\x09\x09send: #onPackageUnselected:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLClassSelected\x0a\x09\x09send: #onClassSelected:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLClassUnselected\x0a\x09\x09send: #onClassUnselected:\x0a\x09\x09to: self.",
referencedClasses: ["HLPackageSelected", "HLPackageUnselected", "HLClassSelected", "HLClassUnselected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._model())._announcer();
[$recv($1)._on_send_to_($globals.HLPackageSelected,"onPackageSelected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLPackageUnselected,"onPackageUnselected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLClassSelected,"onClassSelected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=3
//>>excludeEnd("ctx");
][0];
$recv($1)._on_send_to_($globals.HLClassUnselected,"onClassUnselected:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitClassesListWidget);

$core.addMethod(
$core.method({
selector: "observeSystem",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeSystem\x0a    self model systemAnnouncer \x0a\x09\x09on: ClassAdded \x0a\x09\x09send: #onClassAdded:\x0a\x09\x09to: self.",
referencedClasses: ["ClassAdded"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "systemAnnouncer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self._model())._systemAnnouncer())._on_send_to_($globals.ClassAdded,"onClassAdded:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeSystem",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitClassesListWidget);

$core.addMethod(
$core.method({
selector: "onClassAdded:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onClassAdded: anAnnouncement\x09\x0a\x09(self model selectedPackages includes: anAnnouncement theClass package)\x0a\x09\x09ifTrue: [ \x0a\x09\x09\x09self \x0a\x09\x09\x09\x09initializeItems;\x0a\x09\x09\x09\x09refresh ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "includes:", "selectedPackages", "model", "package", "theClass", "initializeItems", "refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv($recv($self._model())._selectedPackages())._includes_($recv($recv(anAnnouncement)._theClass())._package()))){
$self._initializeItems();
$self._refresh();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassAdded:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitClassesListWidget);

$core.addMethod(
$core.method({
selector: "onClassSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onClassSelected: anAnnouncement\x0a\x09| listItem |\x0a\x09listItem := self findListItemFor: anAnnouncement item.\x0a\x09listItem addClass: 'active'.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["findListItemFor:", "item", "addClass:"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var listItem;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
listItem=$self._findListItemFor_($recv(anAnnouncement)._item());
$recv(listItem)._addClass_("active");
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassSelected:",{anAnnouncement:anAnnouncement,listItem:listItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitClassesListWidget);

$core.addMethod(
$core.method({
selector: "onClassUnselected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onClassUnselected: anAnnouncement\x0a\x09| listItem |\x0a\x09listItem := self findListItemFor: anAnnouncement item.\x0a\x09listItem removeClass: 'active'.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["findListItemFor:", "item", "removeClass:"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var listItem;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
listItem=$self._findListItemFor_($recv(anAnnouncement)._item());
$recv(listItem)._removeClass_("active");
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassUnselected:",{anAnnouncement:anAnnouncement,listItem:listItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitClassesListWidget);

$core.addMethod(
$core.method({
selector: "onPackageSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onPackageSelected: anAnnouncement\x0a\x09self initializeItems;\x0a\x09\x09refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initializeItems", "refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._initializeItems();
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onPackageSelected:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitClassesListWidget);

$core.addMethod(
$core.method({
selector: "onPackageUnselected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onPackageUnselected: anAnnouncement\x0a\x09self initializeItems;\x0a\x09\x09refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initializeItems", "refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._initializeItems();
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onPackageUnselected:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitClassesListWidget);

$core.addMethod(
$core.method({
selector: "renderButtonsOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderButtonsOn: html\x0a\x09html button\x0a\x09\x09class: 'button'; \x0a\x09\x09with: 'Select all';\x0a\x09\x09onClick: [ self model selectAllClasses ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "button", "with:", "onClick:", "selectAllClasses", "model"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._button();
$recv($1)._class_("button");
$recv($1)._with_("Select all");
$recv($1)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._model())._selectAllClasses();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderButtonsOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitClassesListWidget);

$core.addMethod(
$core.method({
selector: "renderItemLabel:on:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass", "html"],
source: "renderItemLabel: aClass on: html\x0a\x09html with: aClass name",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "name"]
}, function ($methodClass){ return function (aClass,html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(html)._with_($recv(aClass)._name());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderItemLabel:on:",{aClass:aClass,html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitClassesListWidget);

$core.addMethod(
$core.method({
selector: "select:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "select: anObject\x0a\x09model selectClass: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectClass:"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.model)._selectClass_(anObject);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"select:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitClassesListWidget);

$core.addMethod(
$core.method({
selector: "unselect:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "unselect: anObject\x0a\x09model unselectClass: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unselectClass:"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.model)._unselectClass_(anObject);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unselect:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitClassesListWidget);



$core.addClass("HLSUnitPackagesListWidget", $globals.HLMultiSelectToolListWidget, "Helios-SUnit");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnitPackagesListWidget.comment="I display a list of packages for which unit tests are associated (packages containing subclasses of `TestCase`).";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "buttonsDivCssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "buttonsDivCssClass\x0a\x09^ 'buttons_bar'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "buttons_bar";

}; }),
$globals.HLSUnitPackagesListWidget);

$core.addMethod(
$core.method({
selector: "cssClassForItem:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anItem"],
source: "cssClassForItem: anItem\x09\x0a\x09^ anItem isDirty \x0a\x09\x09ifTrue: [ 'package_dirty' ]\x0a\x09\x09ifFalse: [ 'package' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:ifFalse:", "isDirty"]
}, function ($methodClass){ return function (anItem){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv(anItem)._isDirty())){
return "package_dirty";
} else {
return "package";
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"cssClassForItem:",{anItem:anItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitPackagesListWidget);

$core.addMethod(
$core.method({
selector: "initializeItems",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initializeItems\x0a\x09^items := model testPackages \x0a\x09\x09sort: [:a :b | a name < b name]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["sort:", "testPackages", "<", "name"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.items=$recv($recv($self.model)._testPackages())._sort_((function(a,b){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv([$recv(a)._name()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["name"]=1
//>>excludeEnd("ctx");
][0]).__lt($recv(b)._name());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({a:a,b:b},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return $self.items;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initializeItems",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitPackagesListWidget);

$core.addMethod(
$core.method({
selector: "isSelected:",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "isSelected: anObject\x0a\x09^model selectedPackages includes: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["includes:", "selectedPackages"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self.model)._selectedPackages())._includes_(anObject);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isSelected:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitPackagesListWidget);

$core.addMethod(
$core.method({
selector: "items",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "items\x0a\x09^ items ifNil: [ self initializeItems ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "initializeItems"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.items;
if($1 == null || $1.a$nil){
return $self._initializeItems();
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"items",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitPackagesListWidget);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Packages'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Packages";

}; }),
$globals.HLSUnitPackagesListWidget);

$core.addMethod(
$core.method({
selector: "observeModel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeModel\x0a    self model announcer \x0a\x09\x09on: HLPackageSelected\x0a\x09\x09send: #onPackageSelected:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLPackageUnselected\x0a\x09\x09send: #onPackageUnselected:\x0a\x09\x09to: self",
referencedClasses: ["HLPackageSelected", "HLPackageUnselected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._model())._announcer();
[$recv($1)._on_send_to_($globals.HLPackageSelected,"onPackageSelected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
$recv($1)._on_send_to_($globals.HLPackageUnselected,"onPackageUnselected:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitPackagesListWidget);

$core.addMethod(
$core.method({
selector: "observeSystem",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeSystem\x0a    self model systemAnnouncer \x0a\x09\x09on: ClassAdded \x0a\x09\x09send: #onClassAdded:\x0a\x09\x09to: self.",
referencedClasses: ["ClassAdded"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "systemAnnouncer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self._model())._systemAnnouncer())._on_send_to_($globals.ClassAdded,"onClassAdded:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeSystem",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitPackagesListWidget);

$core.addMethod(
$core.method({
selector: "onClassAdded:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onClassAdded: anAnnouncement\x0a\x09((self items includes: anAnnouncement theClass package) not and: [anAnnouncement theClass package isTestPackage])\x0a\x09\x09ifTrue: [ \x0a\x09\x09\x09self \x0a\x09\x09\x09\x09initializeItems;\x0a\x09\x09\x09\x09refresh ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "and:", "not", "includes:", "items", "package", "theClass", "isTestPackage", "initializeItems", "refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
if($core.assert($recv($recv($self._items())._includes_([$recv([$recv(anAnnouncement)._theClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["theClass"]=1
//>>excludeEnd("ctx");
][0])._package()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["package"]=1
//>>excludeEnd("ctx");
][0]))._not())){
$1=$recv($recv($recv(anAnnouncement)._theClass())._package())._isTestPackage();
} else {
$1=false;
}
if($core.assert($1)){
$self._initializeItems();
$self._refresh();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassAdded:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitPackagesListWidget);

$core.addMethod(
$core.method({
selector: "onPackageSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onPackageSelected: anAnnouncement\x0a\x09| listItem |\x0a\x09listItem := self findListItemFor: anAnnouncement item.\x0a\x09listItem addClass: 'active'.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["findListItemFor:", "item", "addClass:"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var listItem;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
listItem=$self._findListItemFor_($recv(anAnnouncement)._item());
$recv(listItem)._addClass_("active");
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onPackageSelected:",{anAnnouncement:anAnnouncement,listItem:listItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitPackagesListWidget);

$core.addMethod(
$core.method({
selector: "onPackageUnselected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onPackageUnselected: anAnnouncement\x0a\x09| listItem |\x0a\x09listItem := self findListItemFor: anAnnouncement item.\x0a\x09listItem removeClass: 'active'.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["findListItemFor:", "item", "removeClass:"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var listItem;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
listItem=$self._findListItemFor_($recv(anAnnouncement)._item());
$recv(listItem)._removeClass_("active");
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onPackageUnselected:",{anAnnouncement:anAnnouncement,listItem:listItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitPackagesListWidget);

$core.addMethod(
$core.method({
selector: "renderButtonsOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderButtonsOn: html\x0a\x09html button\x0a\x09\x09class: 'button';\x0a\x09\x09with: 'Run Tests';\x0a\x09\x09onClick: [ self model runTests ].\x0a\x09html button\x0a\x09\x09class: 'button';\x0a\x09\x09with: 'Select all';\x0a\x09\x09onClick: [ self model selectAllPackages ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "button", "with:", "onClick:", "runTests", "model", "selectAllPackages"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=[$recv(html)._button()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["button"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._class_("button")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_("Run Tests")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._runTests();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["onClick:"]=1
//>>excludeEnd("ctx");
][0];
$2=$recv(html)._button();
$recv($2)._class_("button");
$recv($2)._with_("Select all");
$recv($2)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._model())._selectAllPackages();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderButtonsOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitPackagesListWidget);

$core.addMethod(
$core.method({
selector: "renderItemLabel:on:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aPackage", "html"],
source: "renderItemLabel: aPackage on: html\x0a\x09html with: aPackage name",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "name"]
}, function ($methodClass){ return function (aPackage,html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(html)._with_($recv(aPackage)._name());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderItemLabel:on:",{aPackage:aPackage,html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitPackagesListWidget);

$core.addMethod(
$core.method({
selector: "select:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "select: anObject\x0a\x09model selectPackage: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectPackage:"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.model)._selectPackage_(anObject);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"select:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitPackagesListWidget);

$core.addMethod(
$core.method({
selector: "unselect:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "unselect: anObject\x0a\x09model unselectPackage: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unselectPackage:"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.model)._unselectPackage_(anObject);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unselect:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitPackagesListWidget);



$core.addClass("HLSUnit", $globals.HLWidget, "Helios-SUnit");
$core.setSlots($globals.HLSUnit, ["model", "packagesListWidget", "classesListWidget", "resultWidget", "failuresWidget", "errorsWidget"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnit.comment="I am the main widget for running unit tests in Helios.\x0a\x0aI provide the ability to select set of tests to run per package, and a detailed result log with passed tests, failed tests and errors.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "classesListWidget",
protocol: "widgets",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "classesListWidget\x0a\x09^ classesListWidget ifNil: [ \x0a\x09\x09classesListWidget := HLSUnitClassesListWidget on: self model.\x0a\x09\x09classesListWidget next: self failuresWidget ]",
referencedClasses: ["HLSUnitClassesListWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "on:", "model", "next:", "failuresWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.classesListWidget;
if($1 == null || $1.a$nil){
$self.classesListWidget=$recv($globals.HLSUnitClassesListWidget)._on_($self._model());
return $recv($self.classesListWidget)._next_($self._failuresWidget());
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"classesListWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnit);

$core.addMethod(
$core.method({
selector: "errorsWidget",
protocol: "widgets",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "errorsWidget\x0a\x09^ errorsWidget ifNil: [errorsWidget := HLSUnitErrorsListWidget on: self model]",
referencedClasses: ["HLSUnitErrorsListWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "on:", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.errorsWidget;
if($1 == null || $1.a$nil){
$self.errorsWidget=$recv($globals.HLSUnitErrorsListWidget)._on_($self._model());
return $self.errorsWidget;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"errorsWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnit);

$core.addMethod(
$core.method({
selector: "failuresWidget",
protocol: "widgets",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "failuresWidget\x0a\x09^ failuresWidget ifNil: [\x0a\x09\x09failuresWidget := HLSUnitFailuresListWidget on: self model.\x0a\x09\x09failuresWidget next: self errorsWidget]",
referencedClasses: ["HLSUnitFailuresListWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "on:", "model", "next:", "errorsWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.failuresWidget;
if($1 == null || $1.a$nil){
$self.failuresWidget=$recv($globals.HLSUnitFailuresListWidget)._on_($self._model());
return $recv($self.failuresWidget)._next_($self._errorsWidget());
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"failuresWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnit);

$core.addMethod(
$core.method({
selector: "model",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "model\x0a\x09^ model ifNil: [ model := HLSUnitModel new ]",
referencedClasses: ["HLSUnitModel"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.model;
if($1 == null || $1.a$nil){
$self.model=$recv($globals.HLSUnitModel)._new();
return $self.model;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"model",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnit);

$core.addMethod(
$core.method({
selector: "packagesListWidget",
protocol: "widgets",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "packagesListWidget\x0a\x09^ packagesListWidget ifNil: [ \x0a\x09\x09packagesListWidget := HLSUnitPackagesListWidget on: self model.\x0a\x09\x09packagesListWidget next: self classesListWidget]",
referencedClasses: ["HLSUnitPackagesListWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "on:", "model", "next:", "classesListWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.packagesListWidget;
if($1 == null || $1.a$nil){
$self.packagesListWidget=$recv($globals.HLSUnitPackagesListWidget)._on_($self._model());
return $recv($self.packagesListWidget)._next_($self._classesListWidget());
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"packagesListWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnit);

$core.addMethod(
$core.method({
selector: "registerBindingsOn:",
protocol: "keybindings",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBindingGroup"],
source: "registerBindingsOn: aBindingGroup\x0a\x09HLToolCommand \x0a\x09\x09registerConcreteClassesOn: aBindingGroup \x0a\x09\x09for: self model",
referencedClasses: ["HLToolCommand"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["registerConcreteClassesOn:for:", "model"]
}, function ($methodClass){ return function (aBindingGroup){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($globals.HLToolCommand)._registerConcreteClassesOn_for_(aBindingGroup,$self._model());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"registerBindingsOn:",{aBindingGroup:aBindingGroup})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnit);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09| resultSection |\x0a\x09html with: (HLContainer with:  (\x0a\x09\x09HLVerticalSplitter \x0a\x09\x09\x09with: (HLVerticalSplitter\x0a\x09\x09\x09\x09with: self packagesListWidget \x0a        \x09\x09with: self classesListWidget)\x0a\x09\x09\x09with: (resultSection := self resultSection))).\x0a\x09\x0a\x09[resultSection resize: 0] valueWithTimeout: 100.\x0a\x09\x0a\x09self packagesListWidget focus",
referencedClasses: ["HLContainer", "HLVerticalSplitter"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "with:with:", "packagesListWidget", "classesListWidget", "resultSection", "valueWithTimeout:", "resize:", "focus"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
var resultSection;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3;
$1=$globals.HLContainer;
$2=$globals.HLVerticalSplitter;
$3=$recv($globals.HLVerticalSplitter)._with_with_([$self._packagesListWidget()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["packagesListWidget"]=1
//>>excludeEnd("ctx");
][0],$self._classesListWidget());
resultSection=$self._resultSection();
[$recv(html)._with_($recv($1)._with_([$recv($2)._with_with_($3,resultSection)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:with:"]=1
//>>excludeEnd("ctx");
][0]))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(resultSection)._resize_((0));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._valueWithTimeout_((100));
$recv($self._packagesListWidget())._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html,resultSection:resultSection})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnit);

$core.addMethod(
$core.method({
selector: "resultSection",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "resultSection\x0a\x09^HLHorizontalSplitter \x0a\x09\x09with: self resultWidget\x0a\x09\x09with: (HLHorizontalSplitter \x0a\x09\x09\x09with: self failuresWidget\x0a\x09\x09\x09with: self errorsWidget)",
referencedClasses: ["HLHorizontalSplitter"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:with:", "resultWidget", "failuresWidget", "errorsWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return [$recv($globals.HLHorizontalSplitter)._with_with_($self._resultWidget(),$recv($globals.HLHorizontalSplitter)._with_with_($self._failuresWidget(),$self._errorsWidget()))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:with:"]=1
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"resultSection",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnit);

$core.addMethod(
$core.method({
selector: "resultWidget",
protocol: "widgets",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "resultWidget\x0a\x09^ resultWidget ifNil: [\x0a\x09\x09resultWidget := HLSUnitResults new\x0a\x09\x09\x09model: self model;\x0a\x09\x09\x09yourself]",
referencedClasses: ["HLSUnitResults"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "model:", "new", "model", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$self.resultWidget;
if($1 == null || $1.a$nil){
$2=$recv($globals.HLSUnitResults)._new();
$recv($2)._model_($self._model());
$self.resultWidget=$recv($2)._yourself();
return $self.resultWidget;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"resultWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnit);

$core.addMethod(
$core.method({
selector: "unregister",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "unregister\x0a\x09super unregister.\x0a\x0a\x09{ \x0a\x09\x09self packagesListWidget.\x0a\x09\x09self classesListWidget.\x0a\x09\x09self resultWidget.\x0a\x09\x09self errorsWidget.\x0a\x09\x09self failuresWidget\x0a\x09} \x0a\x09\x09do: [ :each | each unregister ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unregister", "do:", "packagesListWidget", "classesListWidget", "resultWidget", "errorsWidget", "failuresWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._unregister.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["unregister"]=1,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv([$self._packagesListWidget(),$self._classesListWidget(),$self._resultWidget(),$self._errorsWidget(),$self._failuresWidget()])._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._unregister();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unregister",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnit);


$core.addMethod(
$core.method({
selector: "canBeOpenAsTab",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "canBeOpenAsTab\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLSUnit.a$cls);

$core.addMethod(
$core.method({
selector: "tabClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabClass\x0a\x09^ 'sunit'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "sunit";

}; }),
$globals.HLSUnit.a$cls);

$core.addMethod(
$core.method({
selector: "tabLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabLabel\x0a\x09^ 'SUnit'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "SUnit";

}; }),
$globals.HLSUnit.a$cls);

$core.addMethod(
$core.method({
selector: "tabPriority",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabPriority\x0a\x09^ 1000",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return (1000);

}; }),
$globals.HLSUnit.a$cls);


$core.addClass("HLSUnitModel", $globals.HLModel, "Helios-SUnit");
$core.setSlots($globals.HLSUnitModel, ["selectedPackages", "selectedClasses", "testResult", "currentSuite"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnitModel.comment="I am the model for running unit tests in Helios.\x0a\x0aI provide the ability to select set of tests to run per package, and a detailed result log with passed tests, failed tests and errors.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "currentSuite",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "currentSuite\x0a\x09^currentSuite",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.currentSuite;

}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "invertSelectedClasses",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "invertSelectedClasses\x0a\x09self testClasses do: [:each | \x0a\x09\x09(self unfilteredSelectedClasses includes: each)\x0a\x09\x09\x09ifTrue: [ self unselectClass: each ]\x0a\x09\x09\x09ifFalse: [ self selectClass: each ]].",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "testClasses", "ifTrue:ifFalse:", "includes:", "unfilteredSelectedClasses", "unselectClass:", "selectClass:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._testClasses())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if($core.assert($recv($self._unfilteredSelectedClasses())._includes_(each))){
return $self._unselectClass_(each);
} else {
return $self._selectClass_(each);
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"invertSelectedClasses",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "invertSelectedPackages",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "invertSelectedPackages\x0a\x09self testPackages do: [:each | \x0a\x09\x09(self selectedPackages includes: each)\x0a\x09\x09\x09ifTrue: [ self unselectPackage: each ]\x0a\x09\x09\x09ifFalse: [ self selectPackage: each ]].",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "testPackages", "ifTrue:ifFalse:", "includes:", "selectedPackages", "unselectPackage:", "selectPackage:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._testPackages())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if($core.assert($recv($self._selectedPackages())._includes_(each))){
return $self._unselectPackage_(each);
} else {
return $self._selectPackage_(each);
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"invertSelectedPackages",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "onResultAnnouncement:",
protocol: "reacting",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["announcement"],
source: "onResultAnnouncement: announcement\x0a\x09\x22Propogate announcement\x22\x0a\x09self announcer announce: announcement.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer"]
}, function ($methodClass){ return function (announcement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._announcer())._announce_(announcement);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onResultAnnouncement:",{announcement:announcement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "runTests",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "runTests\x0a\x09| worker |\x0a\x09worker := (self environment classNamed: #TestSuiteRunner) on: self testCases.\x0a\x09testResult := worker result.\x0a\x09self announcer announce: (HLRunTests on: worker).\x0a\x09self subscribeToTestSuite: worker.\x0a\x09worker run",
referencedClasses: ["HLRunTests"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:", "classNamed:", "environment", "testCases", "result", "announce:", "announcer", "subscribeToTestSuite:", "run"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var worker;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
worker=[$recv($recv($self._environment())._classNamed_("TestSuiteRunner"))._on_($self._testCases())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:"]=1
//>>excludeEnd("ctx");
][0];
$self.testResult=$recv(worker)._result();
$recv($self._announcer())._announce_($recv($globals.HLRunTests)._on_(worker));
$self._subscribeToTestSuite_(worker);
$recv(worker)._run();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"runTests",{worker:worker})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "selectAllClasses",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectAllClasses\x0a\x09self testClasses do: [:each | self selectClass: each].",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "testClasses", "selectClass:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._testClasses())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._selectClass_(each);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectAllClasses",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "selectAllPackages",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectAllPackages\x0a\x09self testPackages do: [:each | self selectPackage: each].",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "testPackages", "selectPackage:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._testPackages())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._selectPackage_(each);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectAllPackages",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "selectClass:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass"],
source: "selectClass: aClass\x0a\x09self unfilteredSelectedClasses add: aClass.\x0a\x09self announcer announce: (HLClassSelected on: aClass).",
referencedClasses: ["HLClassSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["add:", "unfilteredSelectedClasses", "announce:", "announcer", "on:"]
}, function ($methodClass){ return function (aClass){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._unfilteredSelectedClasses())._add_(aClass);
$recv($self._announcer())._announce_($recv($globals.HLClassSelected)._on_(aClass));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectClass:",{aClass:aClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "selectPackage:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aPackage"],
source: "selectPackage: aPackage\x0a\x09self selectedPackages add: aPackage.\x0a\x09self announcer announce: (HLPackageSelected on: aPackage).",
referencedClasses: ["HLPackageSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["add:", "selectedPackages", "announce:", "announcer", "on:"]
}, function ($methodClass){ return function (aPackage){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._selectedPackages())._add_(aPackage);
$recv($self._announcer())._announce_($recv($globals.HLPackageSelected)._on_(aPackage));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectPackage:",{aPackage:aPackage})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "selectedClasses",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectedClasses\x0a\x09^ (self unfilteredSelectedClasses) select: [ :each |\x0a\x09\x09self selectedPackages includes: each package ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["select:", "unfilteredSelectedClasses", "includes:", "selectedPackages", "package"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._unfilteredSelectedClasses())._select_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._selectedPackages())._includes_($recv(each)._package());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectedClasses",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "selectedPackages",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectedPackages\x0a\x09^ selectedPackages ifNil: [ selectedPackages := Set new ]",
referencedClasses: ["Set"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.selectedPackages;
if($1 == null || $1.a$nil){
$self.selectedPackages=$recv($globals.Set)._new();
return $self.selectedPackages;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectedPackages",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "subscribeToTestSuite:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTestSuiteRunner"],
source: "subscribeToTestSuite: aTestSuiteRunner\x0a\x09currentSuite ifNotNil: [ currentSuite announcer unsubscribe: self].\x0a\x09currentSuite := aTestSuiteRunner.\x0a\x09currentSuite announcer \x0a\x09\x09on: ResultAnnouncement\x0a\x09\x09send: #onResultAnnouncement:\x0a\x09\x09to: self",
referencedClasses: ["ResultAnnouncement"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "unsubscribe:", "announcer", "on:send:to:"]
}, function ($methodClass){ return function (aTestSuiteRunner){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.currentSuite;
if($1 == null || $1.a$nil){
$1;
} else {
$recv([$recv($self.currentSuite)._announcer()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["announcer"]=1
//>>excludeEnd("ctx");
][0])._unsubscribe_(self);
}
$self.currentSuite=aTestSuiteRunner;
$recv($recv($self.currentSuite)._announcer())._on_send_to_($globals.ResultAnnouncement,"onResultAnnouncement:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"subscribeToTestSuite:",{aTestSuiteRunner:aTestSuiteRunner})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "testCases",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testCases\x0a\x09| testCases |\x0a\x09testCases := #().\x0a\x09self selectedClasses\x0a\x09\x09do: [ :each | testCases addAll: each buildSuite ].\x0a\x09^ testCases",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "selectedClasses", "addAll:", "buildSuite"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var testCases;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
testCases=[];
$recv($self._selectedClasses())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(testCases)._addAll_($recv(each)._buildSuite());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return testCases;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testCases",{testCases:testCases})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "testClasses",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testClasses\x0a\x09\x22Answer all concrete subclasses of TestCase in selected packages\x22\x0a\x09\x0a\x09| stream |\x0a\x09stream := Array new writeStream.\x0a\x09self selectedPackages do: [ :package |\x0a\x09\x09stream nextPutAll: (package classes select:  [ :each |\x0a\x09\x09\x09each isTestClass ] ) ].\x0a\x09^ stream contents",
referencedClasses: ["Array"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["writeStream", "new", "do:", "selectedPackages", "nextPutAll:", "select:", "classes", "isTestClass", "contents"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var stream;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
stream=$recv($recv($globals.Array)._new())._writeStream();
$recv($self._selectedPackages())._do_((function(package_){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(stream)._nextPutAll_($recv($recv(package_)._classes())._select_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv(each)._isTestClass();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({each:each},$ctx2,2)});
//>>excludeEnd("ctx");
})));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({package_:package_},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return $recv(stream)._contents();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testClasses",{stream:stream})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "testPackages",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testPackages\x0a\x09\x22Answer all packages containing concrete subclasses of TestCase\x22\x0a\x09\x0a\x09^ self environment packages \x0a\x09\x09select: [ :each | each isTestPackage ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["select:", "packages", "environment", "isTestPackage"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._environment())._packages())._select_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._isTestPackage();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testPackages",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "testResult",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testResult\x0a\x09^testResult ifNil: [testResult := TestResult new]",
referencedClasses: ["TestResult"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.testResult;
if($1 == null || $1.a$nil){
$self.testResult=$recv($globals.TestResult)._new();
return $self.testResult;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testResult",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "unfilteredSelectedClasses",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "unfilteredSelectedClasses\x0a\x09^ (selectedClasses ifNil: [ selectedClasses := Set new ])",
referencedClasses: ["Set"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.selectedClasses;
if($1 == null || $1.a$nil){
$self.selectedClasses=$recv($globals.Set)._new();
return $self.selectedClasses;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unfilteredSelectedClasses",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "unselectClass:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass"],
source: "unselectClass: aClass\x0a\x09self unfilteredSelectedClasses remove: aClass ifAbsent: [^self].\x0a\x09self announcer announce: (HLClassUnselected on: aClass).",
referencedClasses: ["HLClassUnselected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["remove:ifAbsent:", "unfilteredSelectedClasses", "announce:", "announcer", "on:"]
}, function ($methodClass){ return function (aClass){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $early={};
try {
$recv($self._unfilteredSelectedClasses())._remove_ifAbsent_(aClass,(function(){
throw $early=[self];

}));
$recv($self._announcer())._announce_($recv($globals.HLClassUnselected)._on_(aClass));
return self;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unselectClass:",{aClass:aClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);

$core.addMethod(
$core.method({
selector: "unselectPackage:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aPackage"],
source: "unselectPackage: aPackage\x0a\x09self selectedPackages remove: aPackage ifAbsent: [^self].\x0a\x09self announcer announce: (HLPackageUnselected on: aPackage).",
referencedClasses: ["HLPackageUnselected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["remove:ifAbsent:", "selectedPackages", "announce:", "announcer", "on:"]
}, function ($methodClass){ return function (aPackage){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $early={};
try {
$recv($self._selectedPackages())._remove_ifAbsent_(aPackage,(function(){
throw $early=[self];

}));
$recv($self._announcer())._announce_($recv($globals.HLPackageUnselected)._on_(aPackage));
return self;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unselectPackage:",{aPackage:aPackage})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitModel);



$core.addClass("HLSUnitResultListWidget", $globals.HLToolListWidget, "Helios-SUnit");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnitResultListWidget.comment="I group the lists that display test results";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "observeModel",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeModel\x0a\x09self model announcer \x0a\x09\x09on: ResultAnnouncement\x0a\x09\x09send: #onResultAnnouncement:\x0a\x09\x09to: self",
referencedClasses: ["ResultAnnouncement"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self._model())._announcer())._on_send_to_($globals.ResultAnnouncement,"onResultAnnouncement:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultListWidget);

$core.addMethod(
$core.method({
selector: "onResultAnnouncement:",
protocol: "reacting",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["announcement"],
source: "onResultAnnouncement: announcement\x0a\x09self refresh.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["refresh"]
}, function ($methodClass){ return function (announcement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onResultAnnouncement:",{announcement:announcement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultListWidget);

$core.addMethod(
$core.method({
selector: "performFailure:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTestCase"],
source: "performFailure: aTestCase\x0a\x09aTestCase runCase",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["runCase"]
}, function ($methodClass){ return function (aTestCase){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(aTestCase)._runCase();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"performFailure:",{aTestCase:aTestCase})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultListWidget);

$core.addMethod(
$core.method({
selector: "renderItemLabel:on:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "html"],
source: "renderItemLabel: anObject on: html\x0a\x09html with: anObject class name, ' >> ', anObject selector",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", ",", "name", "class", "selector"]
}, function ($methodClass){ return function (anObject,html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(html)._with_([$recv($recv($recv($recv(anObject)._class())._name()).__comma(" >> ")).__comma($recv(anObject)._selector())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx[","]=1
//>>excludeEnd("ctx");
][0]);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderItemLabel:on:",{anObject:anObject,html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultListWidget);

$core.addMethod(
$core.method({
selector: "reselectItem:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "reselectItem: anObject\x0a\x09self performFailure: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["performFailure:"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._performFailure_(anObject);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"reselectItem:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultListWidget);



$core.addClass("HLSUnitErrorsListWidget", $globals.HLSUnitResultListWidget, "Helios-SUnit");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnitErrorsListWidget.comment="I display a list of tests that have errors";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "items",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "items\x0a\x09^self model testResult errors",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["errors", "testResult", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._model())._testResult())._errors();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"items",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitErrorsListWidget);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^'Errors'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Errors";

}; }),
$globals.HLSUnitErrorsListWidget);



$core.addClass("HLSUnitFailuresListWidget", $globals.HLSUnitResultListWidget, "Helios-SUnit");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnitFailuresListWidget.comment="I display a list of tests that have failures";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "items",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "items\x0a\x09^self model testResult failures",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["failures", "testResult", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._model())._testResult())._failures();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"items",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitFailuresListWidget);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^'Failures'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Failures";

}; }),
$globals.HLSUnitFailuresListWidget);



$core.addClass("HLSUnitResultStatus", $globals.HLWidget, "Helios-SUnit");
$core.setSlots($globals.HLSUnitResultStatus, ["model"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnitResultStatus.comment="I display the status of the previous test run\x0a\x0a1. How many tests where run.\x0a* How many tests passed.\x0a* How many tests failed.\x0a* How many tests resulted in an error.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "model",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "model\x0a\x09^ model ifNil: [model := TestResult new]",
referencedClasses: ["TestResult"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.model;
if($1 == null || $1.a$nil){
$self.model=$recv($globals.TestResult)._new();
return $self.model;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"model",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultStatus);

$core.addMethod(
$core.method({
selector: "model:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "model: anObject\x0a\x09model := anObject.\x0a\x09self observeModel.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["observeModel"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.model=anObject;
$self._observeModel();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"model:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultStatus);

$core.addMethod(
$core.method({
selector: "observeModel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeModel\x0a\x09self model announcer \x0a\x09\x09on: ResultAnnouncement\x0a\x09\x09send: #onResultAnnouncement:\x0a\x09\x09to: self",
referencedClasses: ["ResultAnnouncement"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self._model())._announcer())._on_send_to_($globals.ResultAnnouncement,"onResultAnnouncement:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultStatus);

$core.addMethod(
$core.method({
selector: "onResultAnnouncement:",
protocol: "reacting",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["announcement"],
source: "onResultAnnouncement: announcement\x0a\x09self refresh.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["refresh"]
}, function ($methodClass){ return function (announcement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onResultAnnouncement:",{announcement:announcement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultStatus);

$core.addMethod(
$core.method({
selector: "printErrors",
protocol: "printing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "printErrors\x0a\x09^ self result errors size asString , ' errors, '",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: [",", "asString", "size", "errors", "result"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($recv($recv($self._result())._errors())._size())._asString()).__comma(" errors, ");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"printErrors",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultStatus);

$core.addMethod(
$core.method({
selector: "printFailures",
protocol: "printing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "printFailures\x0a\x09^ self result failures size asString, ' failures'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: [",", "asString", "size", "failures", "result"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($recv($recv($self._result())._failures())._size())._asString()).__comma(" failures");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"printFailures",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultStatus);

$core.addMethod(
$core.method({
selector: "printPasses",
protocol: "printing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "printPasses\x0a\x09^ (self result runs - self result errors size - self result failures size) asString , ' passes, '",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: [",", "asString", "-", "runs", "result", "size", "errors", "failures"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv([$recv($recv($recv([$self._result()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["result"]=1
//>>excludeEnd("ctx");
][0])._runs()).__minus([$recv($recv([$self._result()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["result"]=2
//>>excludeEnd("ctx");
][0])._errors())._size()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["size"]=1
//>>excludeEnd("ctx");
][0])).__minus($recv($recv($self._result())._failures())._size())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["-"]=1
//>>excludeEnd("ctx");
][0])._asString()).__comma(" passes, ");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"printPasses",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultStatus);

$core.addMethod(
$core.method({
selector: "printTotal",
protocol: "printing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "printTotal\x0a\x09^ self result total asString, ' runs, '",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: [",", "asString", "total", "result"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($recv($self._result())._total())._asString()).__comma(" runs, ");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"printTotal",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultStatus);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09html div\x0a\x09\x09class: self statusCssClass;\x0a\x09\x09with: [ html span with: self statusInfo ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "statusCssClass", "with:", "span", "statusInfo"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._div();
$recv($1)._class_($self._statusCssClass());
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv(html)._span())._with_($self._statusInfo());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultStatus);

$core.addMethod(
$core.method({
selector: "result",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "result\x0a\x09^ self model testResult",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["testResult", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._testResult();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"result",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultStatus);

$core.addMethod(
$core.method({
selector: "statusCssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "statusCssClass\x0a\x09^'sunit status ', self result status",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: [",", "status", "result"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return "sunit status ".__comma($recv($self._result())._status());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"statusCssClass",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultStatus);

$core.addMethod(
$core.method({
selector: "statusInfo",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "statusInfo\x0a\x09^ self printTotal, self printPasses, self printErrors, self printFailures",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: [",", "printTotal", "printPasses", "printErrors", "printFailures"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return [$recv([$recv($recv($self._printTotal()).__comma($self._printPasses())).__comma($self._printErrors())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx[","]=2
//>>excludeEnd("ctx");
][0]).__comma($self._printFailures())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx[","]=1
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"statusInfo",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultStatus);

$core.addMethod(
$core.method({
selector: "unregister",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "unregister\x0a\x09super unregister.\x0a\x09self model announcer unsubscribe: self.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unregister", "unsubscribe:", "announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._unregister.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv($recv($self._model())._announcer())._unsubscribe_(self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unregister",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResultStatus);



$core.addClass("HLSUnitResults", $globals.HLWidget, "Helios-SUnit");
$core.setSlots($globals.HLSUnitResults, ["model", "progressBarWidget", "resultStatusWidget"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnitResults.comment="I am the widget that displays the test results for a previous test run in Helios.\x0a\x0aI display.\x0a\x0a1. The status of the tests.\x0a* Progress of the currently running test suite.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "model",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "model\x0a\x09^model",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.model;

}; }),
$globals.HLSUnitResults);

$core.addMethod(
$core.method({
selector: "model:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "model: anObject\x0a\x09model := anObject.\x0a\x09self observeModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["observeModel"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.model=anObject;
$self._observeModel();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"model:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResults);

$core.addMethod(
$core.method({
selector: "observeModel",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeModel\x0a    self model announcer \x0a\x09\x09on: HLRunTests\x0a\x09\x09send: #onRunTests:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: ResultAnnouncement\x0a\x09\x09send: #onResultAnnouncement:\x0a\x09\x09to: self",
referencedClasses: ["HLRunTests", "ResultAnnouncement"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._model())._announcer();
[$recv($1)._on_send_to_($globals.HLRunTests,"onRunTests:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
$recv($1)._on_send_to_($globals.ResultAnnouncement,"onResultAnnouncement:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResults);

$core.addMethod(
$core.method({
selector: "onResultAnnouncement:",
protocol: "reacting",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["announcement"],
source: "onResultAnnouncement: announcement\x0a\x09[self progressBarWidget \x0a\x09\x09updateProgress: (self model testResult runs / self model testResult total * 100) rounded] fork",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["fork", "updateProgress:", "progressBarWidget", "rounded", "*", "/", "runs", "testResult", "model", "total"]
}, function ($methodClass){ return function (announcement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._progressBarWidget())._updateProgress_($recv($recv($recv($recv([$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._testResult()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["testResult"]=1
//>>excludeEnd("ctx");
][0])._runs()).__slash($recv($recv($self._model())._testResult())._total())).__star((100)))._rounded());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._fork();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onResultAnnouncement:",{announcement:announcement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResults);

$core.addMethod(
$core.method({
selector: "onRunTests:",
protocol: "reacting",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["announcement"],
source: "onRunTests: announcement\x0a\x09self progressBarWidget updateProgress: 0;\x0a\x09\x09refresh.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["updateProgress:", "progressBarWidget", "refresh"]
}, function ($methodClass){ return function (announcement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._progressBarWidget();
$recv($1)._updateProgress_((0));
$recv($1)._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onRunTests:",{announcement:announcement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResults);

$core.addMethod(
$core.method({
selector: "progressBarWidget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "progressBarWidget\x0a\x09^progressBarWidget ifNil: [progressBarWidget := HLProgressBarWidget new\x0a\x09\x09label: '';\x0a\x09\x09yourself]",
referencedClasses: ["HLProgressBarWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "label:", "new", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$self.progressBarWidget;
if($1 == null || $1.a$nil){
$2=$recv($globals.HLProgressBarWidget)._new();
$recv($2)._label_("");
$self.progressBarWidget=$recv($2)._yourself();
return $self.progressBarWidget;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"progressBarWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResults);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09html with: self resultStatusWidget;\x0a\x09\x09with: self progressBarWidget",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "resultStatusWidget", "progressBarWidget"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$recv(html)._with_($self._resultStatusWidget())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
$recv(html)._with_($self._progressBarWidget());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResults);

$core.addMethod(
$core.method({
selector: "resultStatusWidget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "resultStatusWidget\x0a\x09^resultStatusWidget ifNil: [resultStatusWidget := HLSUnitResultStatus new\x0a\x09\x09model: self model;\x0a\x09\x09yourself]",
referencedClasses: ["HLSUnitResultStatus"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "model:", "new", "model", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$self.resultStatusWidget;
if($1 == null || $1.a$nil){
$2=$recv($globals.HLSUnitResultStatus)._new();
$recv($2)._model_($self._model());
$self.resultStatusWidget=$recv($2)._yourself();
return $self.resultStatusWidget;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"resultStatusWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResults);

$core.addMethod(
$core.method({
selector: "unregister",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "unregister\x0a\x09super unregister.\x0a\x09self model announcer unsubscribe: self.\x0a\x09self resultStatusWidget unregister.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unregister", "unsubscribe:", "announcer", "model", "resultStatusWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._unregister.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["unregister"]=1,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv($recv($self._model())._announcer())._unsubscribe_(self);
$recv($self._resultStatusWidget())._unregister();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unregister",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitResults);


});
