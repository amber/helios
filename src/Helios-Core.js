define(["amber/boot", "require", "amber/core/Kernel-Objects", "amber/web/Web"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-Core");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLModel", $globals.Object, "Helios-Core");
$core.setSlots($globals.HLModel, ["announcer", "environment"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLModel.comment="I am the abstract superclass of all models of Helios.\x0aI am the \x22Model\x22 part of the MVC pattern implementation in Helios.\x0a\x0aI provide access to an `Environment` object and both a local (model-specific) and global (system-specific) announcer.\x0a\x0aThe `#withChangesDo:` method is handy for performing model changes ensuring that all widgets are aware of the change and can prevent it from happening.\x0a\x0aModifications of the system should be done via commands (see `HLCommand` and subclasses).";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "announcer",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "announcer\x0a\x09^ announcer ifNil: [ announcer := Announcer new ]",
referencedClasses: ["Announcer"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.announcer;
if($1 == null || $1.a$nil){
$self.announcer=$recv($globals.Announcer)._new();
return $self.announcer;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"announcer",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLModel);

$core.addMethod(
$core.method({
selector: "environment",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "environment\x0a\x09^ environment ifNil: [ self manager environment ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "environment", "manager"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.environment;
if($1 == null || $1.a$nil){
return $recv($self._manager())._environment();
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"environment",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLModel);

$core.addMethod(
$core.method({
selector: "environment:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anEnvironment"],
source: "environment: anEnvironment\x0a\x09environment := anEnvironment",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anEnvironment){
var self=this,$self=this;
$self.environment=anEnvironment;
return self;

}; }),
$globals.HLModel);

$core.addMethod(
$core.method({
selector: "isBrowserModel",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isBrowserModel\x0a\x09^ false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return false;

}; }),
$globals.HLModel);

$core.addMethod(
$core.method({
selector: "isReferencesModel",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isReferencesModel\x0a\x09^ false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return false;

}; }),
$globals.HLModel);

$core.addMethod(
$core.method({
selector: "isToolModel",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isToolModel\x0a\x09^ false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return false;

}; }),
$globals.HLModel);

$core.addMethod(
$core.method({
selector: "manager",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "manager\x0a\x09^ HLManager current",
referencedClasses: ["HLManager"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["current"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.HLManager)._current();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"manager",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLModel);

$core.addMethod(
$core.method({
selector: "systemAnnouncer",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "systemAnnouncer\x0a\x09^ self environment systemAnnouncer",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["systemAnnouncer", "environment"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._environment())._systemAnnouncer();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"systemAnnouncer",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLModel);

$core.addMethod(
$core.method({
selector: "withChangesDo:",
protocol: "error handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBlock"],
source: "withChangesDo: aBlock\x0a\x09[ \x0a\x09\x09self announcer announce: (HLAboutToChange new\x0a\x09\x09\x09actionBlock: aBlock;\x0a\x09\x09\x09yourself).\x0a\x09\x09aBlock value.\x0a\x09]\x0a\x09\x09on: HLChangeForbidden \x0a\x09\x09do: [ :ex | ]",
referencedClasses: ["HLAboutToChange", "HLChangeForbidden"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:do:", "announce:", "announcer", "actionBlock:", "new", "yourself", "value"]
}, function ($methodClass){ return function (aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$1=$self._announcer();
$2=$recv($globals.HLAboutToChange)._new();
$recv($2)._actionBlock_(aBlock);
$recv($1)._announce_($recv($2)._yourself());
return $recv(aBlock)._value();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._on_do_($globals.HLChangeForbidden,(function(ex){

}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"withChangesDo:",{aBlock:aBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLModel);



$core.addClass("HLFinder", $globals.HLModel, "Helios-Core");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLFinder.comment="I am the `Finder` service handler of Helios.\x0a\x0aFinding a class will open a new class browser, while finding a method will open a references browser.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "findClass:",
protocol: "finding",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass"],
source: "findClass: aClass\x0a\x09HLBrowser openAsTab openClassNamed: aClass name",
referencedClasses: ["HLBrowser"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["openClassNamed:", "openAsTab", "name"]
}, function ($methodClass){ return function (aClass){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($globals.HLBrowser)._openAsTab())._openClassNamed_($recv(aClass)._name());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"findClass:",{aClass:aClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLFinder);

$core.addMethod(
$core.method({
selector: "findMethod:",
protocol: "finding",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCompiledMethod"],
source: "findMethod: aCompiledMethod\x0a\x09HLBrowser openAsTab openMethod: aCompiledMethod",
referencedClasses: ["HLBrowser"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["openMethod:", "openAsTab"]
}, function ($methodClass){ return function (aCompiledMethod){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($globals.HLBrowser)._openAsTab())._openMethod_(aCompiledMethod);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"findMethod:",{aCompiledMethod:aCompiledMethod})});
//>>excludeEnd("ctx");
}; }),
$globals.HLFinder);

$core.addMethod(
$core.method({
selector: "findString:",
protocol: "finding",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "findString: aString\x0a\x09| foundClass |\x0a\x09\x0a\x09foundClass := self environment classes \x0a\x09\x09detect: [ :each | each name = aString ]\x0a\x09\x09ifNone: [ nil ].\x0a\x09\x0a\x09foundClass \x0a\x09\x09ifNil: [ HLReferences openAsTab search: aString ]\x0a\x09\x09ifNotNil: [ self findClass: foundClass ]",
referencedClasses: ["HLReferences"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["detect:ifNone:", "classes", "environment", "=", "name", "ifNil:ifNotNil:", "search:", "openAsTab", "findClass:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
var foundClass;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
foundClass=$recv($recv($self._environment())._classes())._detect_ifNone_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv(each)._name()).__eq(aString);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}),(function(){
return nil;

}));
$1=foundClass;
if($1 == null || $1.a$nil){
$recv($recv($globals.HLReferences)._openAsTab())._search_(aString);
} else {
$self._findClass_(foundClass);
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"findString:",{aString:aString,foundClass:foundClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLFinder);



$core.addClass("HLToolModel", $globals.HLModel, "Helios-Core");
$core.setSlots($globals.HLToolModel, ["selectedClass", "selectedPackage", "selectedProtocol", "selectedSelector"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLToolModel.comment="I am a model specific to package and class manipulation. All browsers should either use me or a subclass as their model.\x0a\x0aI provide methods for package, class, protocol and method manipulation and access, forwarding to my environment.\x0a\x0aI also handle compilation of classes and methods as well as compilation and parsing errors.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "addInstVarNamed:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "addInstVarNamed: aString\x0a\x09self environment addInstVarNamed: aString to: self selectedClass.\x0a\x09self announcer announce: (HLInstVarAdded new\x0a\x09\x09theClass: self selectedClass;\x0a\x09\x09variableName: aString;\x0a\x09\x09yourself)",
referencedClasses: ["HLInstVarAdded"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["addInstVarNamed:to:", "environment", "selectedClass", "announce:", "announcer", "theClass:", "new", "variableName:", "yourself"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$recv($self._environment())._addInstVarNamed_to_(aString,[$self._selectedClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedClass"]=1
//>>excludeEnd("ctx");
][0]);
$1=$self._announcer();
$2=$recv($globals.HLInstVarAdded)._new();
$recv($2)._theClass_($self._selectedClass());
$recv($2)._variableName_(aString);
$recv($1)._announce_($recv($2)._yourself());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"addInstVarNamed:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "allProtocol",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "allProtocol\x0a\x09^ '-- all --'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "-- all --";

}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "allSelectors",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "allSelectors\x0a\x09^ self environment allSelectors",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["allSelectors", "environment"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._environment())._allSelectors();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"allSelectors",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "availableClassNames",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "availableClassNames\x0a\x09^ self environment availableClassNames",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["availableClassNames", "environment"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._environment())._availableClassNames();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"availableClassNames",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "availablePackageNames",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "availablePackageNames\x0a\x09^ self environment availablePackageNames",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["availablePackageNames", "environment"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._environment())._availablePackageNames();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"availablePackageNames",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "availablePackages",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "availablePackages\x0a\x09^ self environment availablePackageNames",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["availablePackageNames", "environment"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._environment())._availablePackageNames();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"availablePackages",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "availableProtocols",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "availableProtocols\x0a\x09^ self environment availableProtocolsFor: self selectedClass",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["availableProtocolsFor:", "environment", "selectedClass"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._environment())._availableProtocolsFor_($self._selectedClass());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"availableProtocols",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "commitPackageOnSuccess:onError:",
protocol: "commands actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBlock", "anotherBlock"],
source: "commitPackageOnSuccess: aBlock onError: anotherBlock\x0a\x09self environment \x0a\x09\x09commitPackage: self packageToCommit\x0a\x09\x09onSuccess: aBlock\x0a\x09\x09onError: anotherBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["commitPackage:onSuccess:onError:", "environment", "packageToCommit"]
}, function ($methodClass){ return function (aBlock,anotherBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._environment())._commitPackage_onSuccess_onError_($self._packageToCommit(),aBlock,anotherBlock);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"commitPackageOnSuccess:onError:",{aBlock:aBlock,anotherBlock:anotherBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "compilationProtocol",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "compilationProtocol\x0a\x09| currentProtocol |\x0a\x09\x0a\x09currentProtocol := self selectedProtocol.\x0a\x09currentProtocol ifNil: [ currentProtocol := self unclassifiedProtocol ].\x0a\x09self selectedMethod ifNotNil: [ currentProtocol := self selectedMethod protocol ].\x0a\x0a\x09^ currentProtocol = self allProtocol\x0a\x09\x09ifTrue: [ self unclassifiedProtocol ]\x0a\x09\x09ifFalse: [ currentProtocol ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectedProtocol", "ifNil:", "unclassifiedProtocol", "ifNotNil:", "selectedMethod", "protocol", "ifTrue:ifFalse:", "=", "allProtocol"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var currentProtocol;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
currentProtocol=$self._selectedProtocol();
$1=currentProtocol;
if($1 == null || $1.a$nil){
currentProtocol=[$self._unclassifiedProtocol()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["unclassifiedProtocol"]=1
//>>excludeEnd("ctx");
][0];
currentProtocol;
} else {
$1;
}
$2=[$self._selectedMethod()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedMethod"]=1
//>>excludeEnd("ctx");
][0];
if($2 == null || $2.a$nil){
$2;
} else {
currentProtocol=$recv($self._selectedMethod())._protocol();
currentProtocol;
}
if($core.assert($recv(currentProtocol).__eq($self._allProtocol()))){
return $self._unclassifiedProtocol();
} else {
return currentProtocol;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"compilationProtocol",{currentProtocol:currentProtocol})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "compileClassComment:",
protocol: "compiling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "compileClassComment: aString\x0a\x09self environment \x0a\x09\x09compileClassComment: aString \x0a\x09\x09for: self selectedClass",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["compileClassComment:for:", "environment", "selectedClass"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._environment())._compileClassComment_for_(aString,$self._selectedClass());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"compileClassComment:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "compileClassDefinition:",
protocol: "compiling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "compileClassDefinition: aString\x0a\x09self environment compileClassDefinition: aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["compileClassDefinition:", "environment"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._environment())._compileClassDefinition_(aString);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"compileClassDefinition:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "compileMethod:",
protocol: "compiling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "compileMethod: aString\x0a\x09| method |\x0a\x09\x0a\x09self withCompileErrorHandling: [ \x0a\x09\x09method := self environment \x0a\x09\x09\x09compileMethod: aString \x0a\x09\x09\x09for: self selectedClass\x0a\x09\x09\x09protocol: self compilationProtocol.\x0a\x0a\x09\x09self selectedMethod: method ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withCompileErrorHandling:", "compileMethod:for:protocol:", "environment", "selectedClass", "compilationProtocol", "selectedMethod:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
var method;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._withCompileErrorHandling_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
method=$recv($self._environment())._compileMethod_for_protocol_(aString,$self._selectedClass(),$self._compilationProtocol());
return $self._selectedMethod_(method);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"compileMethod:",{aString:aString,method:method})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "copyClassTo:",
protocol: "commands actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClassName"],
source: "copyClassTo: aClassName\x0a\x09self withChangesDo: [ \x0a\x09\x09self environment \x0a\x09\x09\x09copyClass: self selectedClass theNonMetaClass\x0a\x09\x09\x09to: aClassName.\x0a\x09\x09self selectedClass: (self environment classNamed: aClassName) ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "copyClass:to:", "environment", "theNonMetaClass", "selectedClass", "selectedClass:", "classNamed:"]
}, function ($methodClass){ return function (aClassName){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$recv([$self._environment()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["environment"]=1
//>>excludeEnd("ctx");
][0])._copyClass_to_($recv($self._selectedClass())._theNonMetaClass(),aClassName);
return $self._selectedClass_($recv($self._environment())._classNamed_(aClassName));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"copyClassTo:",{aClassName:aClassName})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "handleCompileError:",
protocol: "error handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anError"],
source: "handleCompileError: anError\x0a\x09self announcer announce: (HLCompileErrorRaised new\x0a\x09\x09error: anError;\x0a\x09\x09yourself)",
referencedClasses: ["HLCompileErrorRaised"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "error:", "new", "yourself"]
}, function ($methodClass){ return function (anError){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$self._announcer();
$2=$recv($globals.HLCompileErrorRaised)._new();
$recv($2)._error_(anError);
$recv($1)._announce_($recv($2)._yourself());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"handleCompileError:",{anError:anError})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "handleParseError:",
protocol: "error handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anError"],
source: "handleParseError: anError\x0a\x09| split line column messageToInsert |\x0a\x09\x0a\x09split := anError messageText tokenize: ' : '.\x0a\x09messageToInsert := split second.\x0a\x0a\x09\x2221 = 'Parse error on line ' size + 1\x22\x0a\x09split := split first copyFrom: 21 to: split first size.\x0a\x09\x0a\x09split := split tokenize: ' column '.\x0a\x09line := split first.\x0a\x09column := split second.\x0a\x09\x0a\x09self announcer announce: (HLParseErrorRaised new\x0a\x09\x09line: line asNumber;\x0a\x09\x09column: column asNumber;\x0a\x09\x09message: messageToInsert;\x0a\x09\x09error: anError;\x0a\x09\x09yourself)",
referencedClasses: ["HLParseErrorRaised"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["tokenize:", "messageText", "second", "copyFrom:to:", "first", "size", "announce:", "announcer", "line:", "new", "asNumber", "column:", "message:", "error:", "yourself"]
}, function ($methodClass){ return function (anError){
var self=this,$self=this;
var split,line,column,messageToInsert;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
split=[$recv($recv(anError)._messageText())._tokenize_(" : ")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["tokenize:"]=1
//>>excludeEnd("ctx");
][0];
messageToInsert=[$recv(split)._second()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["second"]=1
//>>excludeEnd("ctx");
][0];
split=$recv([$recv(split)._first()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["first"]=1
//>>excludeEnd("ctx");
][0])._copyFrom_to_((21),$recv([$recv(split)._first()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["first"]=2
//>>excludeEnd("ctx");
][0])._size());
split=$recv(split)._tokenize_(" column ");
line=$recv(split)._first();
column=$recv(split)._second();
$1=$self._announcer();
$2=$recv($globals.HLParseErrorRaised)._new();
$recv($2)._line_([$recv(line)._asNumber()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asNumber"]=1
//>>excludeEnd("ctx");
][0]);
$recv($2)._column_($recv(column)._asNumber());
$recv($2)._message_(messageToInsert);
$recv($2)._error_(anError);
$recv($1)._announce_($recv($2)._yourself());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"handleParseError:",{anError:anError,split:split,line:line,column:column,messageToInsert:messageToInsert})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "handleUnkownVariableError:",
protocol: "error handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anError"],
source: "handleUnkownVariableError: anError\x0a\x09self announcer announce: (HLUnknownVariableErrorRaised new\x0a\x09\x09error: anError;\x0a\x09\x09yourself)",
referencedClasses: ["HLUnknownVariableErrorRaised"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "error:", "new", "yourself"]
}, function ($methodClass){ return function (anError){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$self._announcer();
$2=$recv($globals.HLUnknownVariableErrorRaised)._new();
$recv($2)._error_(anError);
$recv($1)._announce_($recv($2)._yourself());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"handleUnkownVariableError:",{anError:anError})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "isToolModel",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isToolModel\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "moveClassToPackage:",
protocol: "commands actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aPackageName"],
source: "moveClassToPackage: aPackageName\x0a\x09self withChangesDo: [\x0a\x09\x09self environment \x0a\x09\x09\x09moveClass: self selectedClass theNonMetaClass\x0a\x09\x09\x09toPackage: aPackageName ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "moveClass:toPackage:", "environment", "theNonMetaClass", "selectedClass"]
}, function ($methodClass){ return function (aPackageName){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._environment())._moveClass_toPackage_($recv($self._selectedClass())._theNonMetaClass(),aPackageName);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"moveClassToPackage:",{aPackageName:aPackageName})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "moveMethodToClass:",
protocol: "commands actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClassName"],
source: "moveMethodToClass: aClassName\x0a\x09self withChangesDo: [\x0a\x09\x09self environment \x0a\x09\x09\x09moveMethod: self selectedMethod \x0a\x09\x09\x09toClass: aClassName ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "moveMethod:toClass:", "environment", "selectedMethod"]
}, function ($methodClass){ return function (aClassName){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._environment())._moveMethod_toClass_($self._selectedMethod(),aClassName);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"moveMethodToClass:",{aClassName:aClassName})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "moveMethodToProtocol:",
protocol: "commands actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aProtocol"],
source: "moveMethodToProtocol: aProtocol\x0a\x09self withChangesDo: [\x0a\x09\x09self environment \x0a\x09\x09\x09moveMethod: self selectedMethod \x0a\x09\x09\x09toProtocol: aProtocol ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "moveMethod:toProtocol:", "environment", "selectedMethod"]
}, function ($methodClass){ return function (aProtocol){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._environment())._moveMethod_toProtocol_($self._selectedMethod(),aProtocol);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"moveMethodToProtocol:",{aProtocol:aProtocol})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "openClassNamed:",
protocol: "commands actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "openClassNamed: aString\x0a\x09| class |\x0a\x09\x0a\x09self withChangesDo: [\x0a\x09\x09class := self environment classNamed: aString.\x0a\x09\x09self selectedPackage: class package.\x0a\x09\x09self selectedClass: class ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "classNamed:", "environment", "selectedPackage:", "package", "selectedClass:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
var class_;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
class_=$recv($self._environment())._classNamed_(aString);
$self._selectedPackage_($recv(class_)._package());
return $self._selectedClass_(class_);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"openClassNamed:",{aString:aString,class_:class_})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "packageToCommit",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "packageToCommit\x0a\x09\x22Answer the package to commit depending on the context:\x0a\x09- if a Method is selected, answer its package\x0a\x09- else answer the `selectedPackage`\x22\x0a\x09\x0a\x09^ self selectedMethod \x0a\x09\x09ifNil: [ self selectedPackage ]\x0a\x09\x09ifNotNil: [ :method | method package ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:ifNotNil:", "selectedMethod", "selectedPackage", "package"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._selectedMethod();
if($1 == null || $1.a$nil){
return $self._selectedPackage();
} else {
var method;
method=$1;
return $recv(method)._package();
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"packageToCommit",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "packages",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "packages\x0a\x09^ self environment packages",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["packages", "environment"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._environment())._packages();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"packages",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "removeClass",
protocol: "commands actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "removeClass\x0a\x09self withChangesDo: [\x0a\x09\x09self manager \x0a\x09\x09\x09confirm: 'Do you REALLY want to remove class ', self selectedClass theNonMetaClass name\x0a\x09\x09\x09ifTrue: [ self environment removeClass: self selectedClass theNonMetaClass ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "confirm:ifTrue:", "manager", ",", "name", "theNonMetaClass", "selectedClass", "removeClass:", "environment"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._manager())._confirm_ifTrue_("Do you REALLY want to remove class ".__comma($recv([$recv([$self._selectedClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["selectedClass"]=1
//>>excludeEnd("ctx");
][0])._theNonMetaClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["theNonMetaClass"]=1
//>>excludeEnd("ctx");
][0])._name()),(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv($self._environment())._removeClass_($recv($self._selectedClass())._theNonMetaClass());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"removeClass",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "removeMethod",
protocol: "commands actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "removeMethod\x0a\x09self withChangesDo: [\x0a\x09\x09self manager \x0a\x09\x09\x09confirm: 'Do you REALLY want to remove method ', self selectedMethod origin name,' >> #', self selectedMethod selector\x0a\x09\x09\x09ifTrue: [ self environment removeMethod: self selectedMethod ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "confirm:ifTrue:", "manager", ",", "name", "origin", "selectedMethod", "selector", "removeMethod:", "environment"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._manager())._confirm_ifTrue_([$recv([$recv("Do you REALLY want to remove method ".__comma($recv($recv([$self._selectedMethod()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["selectedMethod"]=1
//>>excludeEnd("ctx");
][0])._origin())._name())).__comma(" >> #")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx[","]=2
//>>excludeEnd("ctx");
][0]).__comma($recv([$self._selectedMethod()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["selectedMethod"]=2
//>>excludeEnd("ctx");
][0])._selector())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx[","]=1
//>>excludeEnd("ctx");
][0],(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv($self._environment())._removeMethod_($self._selectedMethod());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"removeMethod",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "removeProtocol",
protocol: "commands actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "removeProtocol\x0a\x09self withChangesDo: [\x0a\x09\x09self manager \x0a\x09\x09\x09confirm: 'Do you REALLY want to remove protocol ', self selectedProtocol\x0a\x09\x09\x09ifTrue: [ self environment \x0a\x09\x09\x09\x09removeProtocol: self selectedProtocol \x0a\x09\x09\x09\x09from: self selectedClass ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "confirm:ifTrue:", "manager", ",", "selectedProtocol", "removeProtocol:from:", "environment", "selectedClass"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._manager())._confirm_ifTrue_("Do you REALLY want to remove protocol ".__comma([$self._selectedProtocol()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["selectedProtocol"]=1
//>>excludeEnd("ctx");
][0]),(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv($self._environment())._removeProtocol_from_($self._selectedProtocol(),$self._selectedClass());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"removeProtocol",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "renameClassTo:",
protocol: "commands actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClassName"],
source: "renameClassTo: aClassName\x0a\x09self withChangesDo: [\x0a\x09\x09self environment \x0a\x09\x09\x09renameClass: self selectedClass theNonMetaClass\x0a\x09\x09\x09to: aClassName ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "renameClass:to:", "environment", "theNonMetaClass", "selectedClass"]
}, function ($methodClass){ return function (aClassName){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._environment())._renameClass_to_($recv($self._selectedClass())._theNonMetaClass(),aClassName);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renameClassTo:",{aClassName:aClassName})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "renamePackageTo:",
protocol: "commands actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aPackageName"],
source: "renamePackageTo: aPackageName\x0a\x09self withChangesDo: [\x0a\x09\x09self environment\x0a\x09\x09\x09renamePackage: self selectedPackage name\x0a\x09\x09\x09to: aPackageName ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "renamePackage:to:", "environment", "name", "selectedPackage"]
}, function ($methodClass){ return function (aPackageName){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._environment())._renamePackage_to_($recv($self._selectedPackage())._name(),aPackageName);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renamePackageTo:",{aPackageName:aPackageName})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "renameProtocolTo:",
protocol: "commands actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "renameProtocolTo: aString\x0a\x09self withChangesDo: [\x0a\x09\x09self environment \x0a\x09\x09\x09renameProtocol: self selectedProtocol\x0a\x09\x09\x09to: aString\x0a\x09\x09\x09in: self selectedClass ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "renameProtocol:to:in:", "environment", "selectedProtocol", "selectedClass"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._environment())._renameProtocol_to_in_($self._selectedProtocol(),aString,$self._selectedClass());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renameProtocolTo:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "save:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "save: aString\x0a\x09self announcer announce: HLSourceCodeSaved new.\x0a\x09\x0a\x09(self shouldCompileDefinition: aString)\x0a\x09\x09ifTrue: [ self compileClassDefinition: aString ]\x0a\x09\x09ifFalse: [ self compileMethod: aString ]",
referencedClasses: ["HLSourceCodeSaved"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "new", "ifTrue:ifFalse:", "shouldCompileDefinition:", "compileClassDefinition:", "compileMethod:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._announcer())._announce_($recv($globals.HLSourceCodeSaved)._new());
if($core.assert($self._shouldCompileDefinition_(aString))){
$self._compileClassDefinition_(aString);
} else {
$self._compileMethod_(aString);
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"save:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "saveSourceCode",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "saveSourceCode\x0a\x09self announcer announce: HLSaveSourceCode new",
referencedClasses: ["HLSaveSourceCode"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._announcer())._announce_($recv($globals.HLSaveSourceCode)._new());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"saveSourceCode",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "selectedClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectedClass\x0a\x09^ selectedClass",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.selectedClass;

}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "selectedClass:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass"],
source: "selectedClass: aClass\x0a\x09(self selectedClass = aClass and: [ aClass isNil ]) \x0a\x09\x09ifTrue: [ ^ self ].\x0a\x09\x0a\x09self withChangesDo: [\x0a\x09\x09aClass \x0a   \x09\x09\x09ifNil: [ selectedClass := nil ]\x0a    \x09\x09ifNotNil: [\x0a\x09\x09\x09\x09self selectedPackage: aClass theNonMetaClass package.\x0a\x09\x09\x09\x09self showInstance \x0a   \x09\x09\x09\x09\x09ifTrue: [ selectedClass := aClass theNonMetaClass ]\x0a     \x09\x09\x09\x09ifFalse: [ selectedClass := aClass theMetaClass ] ].\x0a\x09\x09selectedProtocol := nil.\x0a\x09\x09self selectedProtocol: self allProtocol.\x0a\x09\x09self announcer announce: (HLClassSelected on: self selectedClass) ]",
referencedClasses: ["HLClassSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "and:", "=", "selectedClass", "isNil", "withChangesDo:", "ifNil:ifNotNil:", "selectedPackage:", "package", "theNonMetaClass", "ifTrue:ifFalse:", "showInstance", "theMetaClass", "selectedProtocol:", "allProtocol", "announce:", "announcer", "on:"]
}, function ($methodClass){ return function (aClass){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
if($core.assert($recv([$self._selectedClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedClass"]=1
//>>excludeEnd("ctx");
][0]).__eq(aClass))){
$1=$recv(aClass)._isNil();
} else {
$1=false;
}
if($core.assert($1)){
return self;
}
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if(aClass == null || aClass.a$nil){
$self.selectedClass=nil;
$self.selectedClass;
} else {
$self._selectedPackage_($recv([$recv(aClass)._theNonMetaClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["theNonMetaClass"]=1
//>>excludeEnd("ctx");
][0])._package());
if($core.assert($self._showInstance())){
$self.selectedClass=$recv(aClass)._theNonMetaClass();
$self.selectedClass;
} else {
$self.selectedClass=$recv(aClass)._theMetaClass();
$self.selectedClass;
}
}
$self.selectedProtocol=nil;
$self._selectedProtocol_($self._allProtocol());
return $recv($self._announcer())._announce_($recv($globals.HLClassSelected)._on_($self._selectedClass()));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,3)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectedClass:",{aClass:aClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "selectedMethod",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectedMethod\x0a\x09^ self selectedClass ifNotNil: [ \x0a\x09\x09self selectedClass methodDictionary \x0a\x09\x09\x09at: selectedSelector \x0a\x09\x09\x09ifAbsent: [ nil ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "selectedClass", "at:ifAbsent:", "methodDictionary"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=[$self._selectedClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedClass"]=1
//>>excludeEnd("ctx");
][0];
if($1 == null || $1.a$nil){
return $1;
} else {
return $recv($recv($self._selectedClass())._methodDictionary())._at_ifAbsent_($self.selectedSelector,(function(){
return nil;

}));
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectedMethod",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "selectedMethod:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCompiledMethod"],
source: "selectedMethod: aCompiledMethod\x0a\x09selectedSelector = aCompiledMethod ifTrue: [ ^ self ].\x0a    \x0a    self withChangesDo: [\x0a\x09\x09aCompiledMethod\x0a    \x09\x09ifNil: [ selectedSelector := nil ]\x0a      \x09\x09ifNotNil: [\x0a\x09\x09\x09\x09selectedSelector := aCompiledMethod selector.\x0a\x09\x09\x09\x09(selectedClass notNil and: [ selectedClass methodDictionary includesKey: selectedSelector ]) ifFalse: [\x0a\x09\x09\x09\x09\x09selectedClass := aCompiledMethod methodClass.\x0a\x09\x09\x09\x09\x09selectedPackage := selectedClass theNonMetaClass package ] ].\x0a\x0a\x09\x09self announcer announce: (HLMethodSelected on: aCompiledMethod) ]",
referencedClasses: ["HLMethodSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "=", "withChangesDo:", "ifNil:ifNotNil:", "selector", "ifFalse:", "and:", "notNil", "includesKey:", "methodDictionary", "methodClass", "package", "theNonMetaClass", "announce:", "announcer", "on:"]
}, function ($methodClass){ return function (aCompiledMethod){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
if($core.assert($recv($self.selectedSelector).__eq(aCompiledMethod))){
return self;
}
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if(aCompiledMethod == null || aCompiledMethod.a$nil){
$self.selectedSelector=nil;
$self.selectedSelector;
} else {
$self.selectedSelector=$recv(aCompiledMethod)._selector();
if($core.assert($recv($self.selectedClass)._notNil())){
$1=$recv($recv($self.selectedClass)._methodDictionary())._includesKey_($self.selectedSelector);
} else {
$1=false;
}
if(!$core.assert($1)){
$self.selectedClass=$recv(aCompiledMethod)._methodClass();
$self.selectedPackage=$recv($recv($self.selectedClass)._theNonMetaClass())._package();
$self.selectedPackage;
}
}
return $recv($self._announcer())._announce_($recv($globals.HLMethodSelected)._on_(aCompiledMethod));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectedMethod:",{aCompiledMethod:aCompiledMethod})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "selectedPackage",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectedPackage\x0a\x09^ selectedPackage",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.selectedPackage;

}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "selectedPackage:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aPackage"],
source: "selectedPackage: aPackage\x0a\x09selectedPackage = aPackage ifTrue: [ ^ self ].\x0a\x0a\x09self withChangesDo: [\x0a\x09\x09selectedPackage := aPackage.\x0a\x09\x09self selectedClass: nil.\x0a\x09\x09self announcer announce: (HLPackageSelected on: aPackage) ]",
referencedClasses: ["HLPackageSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "=", "withChangesDo:", "selectedClass:", "announce:", "announcer", "on:"]
}, function ($methodClass){ return function (aPackage){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv($self.selectedPackage).__eq(aPackage))){
return self;
}
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$self.selectedPackage=aPackage;
$self._selectedClass_(nil);
return $recv($self._announcer())._announce_($recv($globals.HLPackageSelected)._on_(aPackage));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectedPackage:",{aPackage:aPackage})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "selectedProtocol",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectedProtocol\x0a\x09^ selectedProtocol",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.selectedProtocol;

}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "selectedProtocol:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "selectedProtocol: aString\x0a\x09selectedProtocol = aString ifTrue: [ ^ self ].\x0a\x0a\x09self withChangesDo: [\x0a\x09\x09selectedProtocol := aString.\x0a\x09\x09self selectedMethod: nil.\x0a\x09\x09self announcer announce: (HLProtocolSelected on: aString) ]",
referencedClasses: ["HLProtocolSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "=", "withChangesDo:", "selectedMethod:", "announce:", "announcer", "on:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv($self.selectedProtocol).__eq(aString))){
return self;
}
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$self.selectedProtocol=aString;
$self._selectedMethod_(nil);
return $recv($self._announcer())._announce_($recv($globals.HLProtocolSelected)._on_(aString));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectedProtocol:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "shouldCompileDefinition:",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "shouldCompileDefinition: aString\x0a\x09^ self selectedClass isNil or: [\x0a\x09\x09aString match: '^\x5cs*[A-Z]' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["or:", "isNil", "selectedClass", "match:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv($self._selectedClass())._isNil())){
return true;
} else {
return $recv(aString)._match_("^\x5cs*[A-Z]");
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"shouldCompileDefinition:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "softSelectedClass:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass"],
source: "softSelectedClass: aClass\x0a\x09self announcer announce: (HLClassSelected softOn: aClass)",
referencedClasses: ["HLClassSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "softOn:"]
}, function ($methodClass){ return function (aClass){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._announcer())._announce_($recv($globals.HLClassSelected)._softOn_(aClass));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"softSelectedClass:",{aClass:aClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "softSelectedMethod:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aMethod"],
source: "softSelectedMethod: aMethod\x0a\x09self announcer announce: (HLMethodSelected softOn: aMethod)",
referencedClasses: ["HLMethodSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "softOn:"]
}, function ($methodClass){ return function (aMethod){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._announcer())._announce_($recv($globals.HLMethodSelected)._softOn_(aMethod));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"softSelectedMethod:",{aMethod:aMethod})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "softSelectedPackage:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aPackage"],
source: "softSelectedPackage: aPackage\x0a\x09self announcer announce: (HLPackageSelected softOn: aPackage)",
referencedClasses: ["HLPackageSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "softOn:"]
}, function ($methodClass){ return function (aPackage){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._announcer())._announce_($recv($globals.HLPackageSelected)._softOn_(aPackage));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"softSelectedPackage:",{aPackage:aPackage})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "softSelectedProtocol:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aProtocol"],
source: "softSelectedProtocol: aProtocol\x0a\x09self announcer announce: (HLProtocolSelected softOn: aProtocol)",
referencedClasses: ["HLProtocolSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "softOn:"]
}, function ($methodClass){ return function (aProtocol){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._announcer())._announce_($recv($globals.HLProtocolSelected)._softOn_(aProtocol));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"softSelectedProtocol:",{aProtocol:aProtocol})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "unclassifiedProtocol",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "unclassifiedProtocol\x0a\x09^ 'as yet unclassified'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "as yet unclassified";

}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "withCompileErrorHandling:",
protocol: "error handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBlock"],
source: "withCompileErrorHandling: aBlock\x0a\x09self environment\x0a\x09\x09evaluate: [\x0a\x09\x09\x09self environment \x0a\x09\x09\x09evaluate: [\x0a\x09\x09\x09\x09self environment \x0a\x09\x09\x09\x09\x09evaluate: aBlock\x0a\x09\x09\x09\x09\x09on: ParseError\x0a\x09\x09\x09\x09\x09do: [ :ex | self handleParseError: ex ] ]\x0a\x09\x09\x09on: UnknownVariableError\x0a\x09\x09\x09do: [ :ex | self handleUnkownVariableError: ex ] ]\x0a\x09\x09on: CompilerError\x0a\x09\x09do: [ :ex | self handleCompileError: ex ]",
referencedClasses: ["ParseError", "UnknownVariableError", "CompilerError"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["evaluate:on:do:", "environment", "handleParseError:", "handleUnkownVariableError:", "handleCompileError:"]
}, function ($methodClass){ return function (aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$recv([$self._environment()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["environment"]=1
//>>excludeEnd("ctx");
][0])._evaluate_on_do_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return [$recv([$self._environment()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["environment"]=2
//>>excludeEnd("ctx");
][0])._evaluate_on_do_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv($self._environment())._evaluate_on_do_(aBlock,$globals.ParseError,(function(ex){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx4) {
//>>excludeEnd("ctx");
return $self._handleParseError_(ex);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx4) {$ctx4.fillBlock({ex:ex},$ctx3,3)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}),$globals.UnknownVariableError,(function(ex){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._handleUnkownVariableError_(ex);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({ex:ex},$ctx2,4)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["evaluate:on:do:"]=2
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}),$globals.CompilerError,(function(ex){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._handleCompileError_(ex);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({ex:ex},$ctx1,5)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["evaluate:on:do:"]=1
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"withCompileErrorHandling:",{aBlock:aBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);

$core.addMethod(
$core.method({
selector: "withHelperLabelled:do:",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString", "aBlock"],
source: "withHelperLabelled: aString do: aBlock\x0a\x09\x22TODO: doesn't belong here\x22\x0a\x0a\x09'#helper' asJQuery remove.\x0a\x0a\x09[ :html |\x0a\x09\x09html div \x0a\x09\x09\x09id: 'helper';\x0a\x09\x09\x09with: aString ] appendToJQuery: 'body' asJQuery.\x0a\x09\x0a\x09[\x0a\x09\x09aBlock value.\x0a\x09\x09'#helper' asJQuery remove ] fork",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["remove", "asJQuery", "appendToJQuery:", "id:", "div", "with:", "fork", "value"]
}, function ($methodClass){ return function (aString,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
[$recv(["#helper"._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._remove()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["remove"]=1
//>>excludeEnd("ctx");
][0];
$recv((function(html){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$1=$recv(html)._div();
$recv($1)._id_("helper");
return $recv($1)._with_(aString);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({html:html},$ctx1,1)});
//>>excludeEnd("ctx");
}))._appendToJQuery_(["body"._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=2
//>>excludeEnd("ctx");
][0]);
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$recv(aBlock)._value();
return $recv("#helper"._asJQuery())._remove();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}))._fork();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"withHelperLabelled:do:",{aString:aString,aBlock:aBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel);


$core.addMethod(
$core.method({
selector: "on:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anEnvironment"],
source: "on: anEnvironment\x0a\x0a\x09^ self new\x0a    \x09environment: anEnvironment;\x0a        yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["environment:", "new", "yourself"]
}, function ($methodClass){ return function (anEnvironment){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._environment_(anEnvironment);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{anEnvironment:anEnvironment})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolModel.a$cls);


$core.addClass("HLProgressHandler", $globals.Object, "Helios-Core");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLProgressHandler.comment="I am a specific progress handler for Helios, displaying progresses in a modal window.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "do:on:displaying:",
protocol: "progress handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBlock", "aCollection", "aString"],
source: "do: aBlock on: aCollection displaying: aString\x0a\x09HLProgressWidget default\x0a\x09\x09do: aBlock \x0a\x09\x09on: aCollection \x0a\x09\x09displaying: aString",
referencedClasses: ["HLProgressWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:on:displaying:", "default"]
}, function ($methodClass){ return function (aBlock,aCollection,aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($globals.HLProgressWidget)._default())._do_on_displaying_(aBlock,aCollection,aString);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"do:on:displaying:",{aBlock:aBlock,aCollection:aCollection,aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProgressHandler);



$core.addClass("HLWidget", $globals.Widget, "Helios-Core");
$core.setSlots($globals.HLWidget, ["wrapper"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLWidget.comment="I am the abstract superclass of all Helios widgets.\x0a\x0aI provide common methods, additional behavior to widgets useful for Helios, like dialog creation, command execution and tab creation.\x0a\x0a## API\x0a\x0a1. Rendering\x0a\x0a    Instead of overriding `#renderOn:` as with other Widget subclasses, my subclasses should override `#renderContentOn:`.\x0a\x0a2. Refreshing\x0a\x0a    To re-render a widget, use `#refresh`.\x0a\x0a3. Key bindings registration and tabs\x0a\x0a    When displayed as a tab, the widget has a chance to register keybindings with the `#registerBindingsOn:` hook method.\x0a    \x0a4. Unregistration\x0a\x0a    When a widget has subscribed to announcements or other actions that need to be cleared when closing the tab, the hook method `#unregister` will be called by helios.\x0a\x0a5. Tabs\x0a\x0a   To enable a widget class to be open as a tab, override the class-side `#canBeOpenAsTab` method to answer `true`. `#tabClass` and `#tabPriority` can be overridden too to respectively change the css class of the tab and the order of tabs in the main menu.\x0a\x0a6. Command execution\x0a\x0a    An helios command (instance of `HLCommand` or one of its subclass) can be executed with `#execute:`.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "bindKeyDown:keyUp:",
protocol: "keybindings",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["keyDownBlock", "keyUpBlock"],
source: "bindKeyDown: keyDownBlock keyUp: keyUpBlock\x0a\x09self wrapper ifNotNil: [ :wrappr | wrappr asJQuery\x0a\x09\x09keydown: keyDownBlock;\x0a\x09\x09keyup: keyUpBlock ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "wrapper", "keydown:", "asJQuery", "keyup:"]
}, function ($methodClass){ return function (keyDownBlock,keyUpBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$self._wrapper();
if($1 == null || $1.a$nil){
$1;
} else {
var wrappr;
wrappr=$1;
$2=$recv(wrappr)._asJQuery();
$recv($2)._keydown_(keyDownBlock);
$recv($2)._keyup_(keyUpBlock);
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"bindKeyDown:keyUp:",{keyDownBlock:keyDownBlock,keyUpBlock:keyUpBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "canHaveFocus",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "canHaveFocus\x0a\x09^ false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return false;

}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "confirm:ifTrue:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString", "aBlock"],
source: "confirm: aString ifTrue: aBlock\x0a\x09self manager confirm: aString ifTrue: aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["confirm:ifTrue:", "manager"]
}, function ($methodClass){ return function (aString,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._manager())._confirm_ifTrue_(aString,aBlock);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"confirm:ifTrue:",{aString:aString,aBlock:aBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "confirm:ifTrue:ifFalse:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString", "aBlock", "anotherBlock"],
source: "confirm: aString ifTrue: aBlock ifFalse: anotherBlock\x0a\x09self manager \x0a\x09\x09confirm: aString \x0a\x09\x09ifTrue: aBlock\x0a\x09\x09ifFalse: anotherBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["confirm:ifTrue:ifFalse:", "manager"]
}, function ($methodClass){ return function (aString,aBlock,anotherBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._manager())._confirm_ifTrue_ifFalse_(aString,aBlock,anotherBlock);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"confirm:ifTrue:ifFalse:",{aString:aString,aBlock:aBlock,anotherBlock:anotherBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "cssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "cssClass\x0a\x09^ 'hl_widget'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "hl_widget";

}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "defaultTabLabel",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "defaultTabLabel\x0a\x09^ self class tabLabel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["tabLabel", "class"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._class())._tabLabel();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"defaultTabLabel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "execute:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCommand"],
source: "execute: aCommand\x0a\x09HLManager current keyBinder\x0a\x09\x09activate;\x0a\x09\x09applyBinding: aCommand asBinding",
referencedClasses: ["HLManager"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["activate", "keyBinder", "current", "applyBinding:", "asBinding"]
}, function ($methodClass){ return function (aCommand){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($recv($globals.HLManager)._current())._keyBinder();
$recv($1)._activate();
$recv($1)._applyBinding_($recv(aCommand)._asBinding());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute:",{aCommand:aCommand})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "inform:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "inform: aString\x0a\x09self manager inform: aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["inform:", "manager"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._manager())._inform_(aString);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inform:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "manager",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "manager\x0a\x09^ HLManager current",
referencedClasses: ["HLManager"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["current"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.HLManager)._current();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"manager",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "openAsTab",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "openAsTab\x0a\x09(HLTabWidget on: self labelled: self defaultTabLabel)\x0a\x09\x09add",
referencedClasses: ["HLTabWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["add", "on:labelled:", "defaultTabLabel"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($globals.HLTabWidget)._on_labelled_(self,$self._defaultTabLabel()))._add();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"openAsTab",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "refresh",
protocol: "updating",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "refresh\x0a\x09self wrapper\x0a\x09\x09ifNotNil: [ :wrap | wrap contents: [ :html | self renderContentOn: html ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "wrapper", "contents:", "renderContentOn:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._wrapper();
if($1 == null || $1.a$nil){
$1;
} else {
var wrap;
wrap=$1;
$recv(wrap)._contents_((function(html){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._renderContentOn_(html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({html:html},$ctx1,2)});
//>>excludeEnd("ctx");
}));
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"refresh",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "registerBindings",
protocol: "keybindings",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "registerBindings\x0a\x09self registerBindingsOn: self manager keyBinder bindings",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["registerBindingsOn:", "bindings", "keyBinder", "manager"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._registerBindingsOn_($recv($recv($self._manager())._keyBinder())._bindings());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"registerBindings",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "registerBindingsOn:",
protocol: "keybindings",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBindingGroup"],
source: "registerBindingsOn: aBindingGroup",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aBindingGroup){
var self=this,$self=this;
return self;

}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "removeTab",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "removeTab\x0a\x09self manager removeTabForWidget: self",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["removeTabForWidget:", "manager"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._manager())._removeTabForWidget_(self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"removeTab",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (html){
var self=this,$self=this;
return self;

}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "renderOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderOn: html\x0a\x09wrapper := html div\x0a\x09\x09class: self cssClass;\x0a\x09\x09yourself.\x0a\x09\x22must do this later, as renderContentOn may want to use self wrapper\x22\x0a\x09wrapper with: [ self renderContentOn: html ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "cssClass", "yourself", "with:", "renderContentOn:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._div();
$recv($1)._class_($self._cssClass());
$self.wrapper=$recv($1)._yourself();
$recv($self.wrapper)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._renderContentOn_(html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "request:do:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString", "aBlock"],
source: "request: aString do: aBlock\x0a\x09self manager request: aString do: aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["request:do:", "manager"]
}, function ($methodClass){ return function (aString,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._manager())._request_do_(aString,aBlock);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"request:do:",{aString:aString,aBlock:aBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "request:value:do:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString", "valueString", "aBlock"],
source: "request: aString value: valueString do: aBlock\x0a\x09self manager \x0a\x09\x09request: aString \x0a\x09\x09value: valueString\x0a\x09\x09do: aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["request:value:do:", "manager"]
}, function ($methodClass){ return function (aString,valueString,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._manager())._request_value_do_(aString,valueString,aBlock);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"request:value:do:",{aString:aString,valueString:valueString,aBlock:aBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "setTabLabel:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "setTabLabel: aString\x0a\x09self manager announcer announce: (HLTabLabelChanged new\x0a\x09\x09widget: self;\x0a\x09\x09label: aString;\x0a\x09\x09yourself)",
referencedClasses: ["HLTabLabelChanged"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "manager", "widget:", "new", "label:", "yourself"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$recv($self._manager())._announcer();
$2=$recv($globals.HLTabLabelChanged)._new();
$recv($2)._widget_(self);
$recv($2)._label_(aString);
$recv($1)._announce_($recv($2)._yourself());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setTabLabel:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "tabClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabClass\x0a\x09^ self class tabClass",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["tabClass", "class"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._class())._tabClass();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"tabClass",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "unbindKeyDownKeyUp",
protocol: "keybindings",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "unbindKeyDownKeyUp\x0a\x09self wrapper asJQuery\x0a\x09\x09unbind: 'keydown';\x0a\x09\x09unbind: 'keyup'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unbind:", "asJQuery", "wrapper"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._wrapper())._asJQuery();
[$recv($1)._unbind_("keydown")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["unbind:"]=1
//>>excludeEnd("ctx");
][0];
$recv($1)._unbind_("keyup");
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unbindKeyDownKeyUp",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "unregister",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "unregister\x0a\x09\x22This method is called whenever the receiver is closed (as a tab).\x0a\x09Widgets subscribing to announcements should unregister there\x22",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return self;

}; }),
$globals.HLWidget);

$core.addMethod(
$core.method({
selector: "wrapper",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "wrapper\x0a\x09^ wrapper",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.wrapper;

}; }),
$globals.HLWidget);


$core.addMethod(
$core.method({
selector: "canBeOpenAsTab",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "canBeOpenAsTab\x0a\x09^ false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return false;

}; }),
$globals.HLWidget.a$cls);

$core.addMethod(
$core.method({
selector: "openAsTab",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "openAsTab\x0a\x09| instance |\x0a\x09\x0a\x09instance := self new.\x0a\x09(HLTabWidget \x0a\x09\x09on: instance \x0a\x09\x09labelled: instance defaultTabLabel) add.\x0a\x09^ instance",
referencedClasses: ["HLTabWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["new", "add", "on:labelled:", "defaultTabLabel"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var instance;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
instance=$self._new();
$recv($recv($globals.HLTabWidget)._on_labelled_(instance,$recv(instance)._defaultTabLabel()))._add();
return instance;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"openAsTab",{instance:instance})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWidget.a$cls);

$core.addMethod(
$core.method({
selector: "tabClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabClass\x0a\x09^ ''",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "";

}; }),
$globals.HLWidget.a$cls);

$core.addMethod(
$core.method({
selector: "tabLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabLabel\x0a\x09^ 'Tab'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Tab";

}; }),
$globals.HLWidget.a$cls);

$core.addMethod(
$core.method({
selector: "tabPriority",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabPriority\x0a\x09^ 500",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return (500);

}; }),
$globals.HLWidget.a$cls);


$core.addClass("HLFocusableWidget", $globals.HLWidget, "Helios-Core");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLFocusableWidget.comment="I am a widget that can be focused.\x0a\x0a## API \x0a\x0aInstead of overriding `#renderOn:` as with other `Widget` subclasses, my subclasses should override `#renderContentOn:`.\x0a\x0aTo bring the focus to the widget, use the `#focus` method.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "blur",
protocol: "events",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "blur\x0a\x09self wrapper asJQuery blur",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["blur", "asJQuery", "wrapper"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self._wrapper())._asJQuery())._blur();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"blur",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLFocusableWidget);

$core.addMethod(
$core.method({
selector: "canHaveFocus",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "canHaveFocus\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLFocusableWidget);

$core.addMethod(
$core.method({
selector: "focus",
protocol: "events",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focus\x0a\x09self wrapper asJQuery focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focus", "asJQuery", "wrapper"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self._wrapper())._asJQuery())._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focus",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLFocusableWidget);

$core.addMethod(
$core.method({
selector: "focusClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focusClass\x0a\x09^ 'focused'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "focused";

}; }),
$globals.HLFocusableWidget);

$core.addMethod(
$core.method({
selector: "hasFocus",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "hasFocus\x0a\x09^ self wrapper notNil and: [ self wrapper asJQuery hasClass: self focusClass ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["and:", "notNil", "wrapper", "hasClass:", "asJQuery", "focusClass"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv([$self._wrapper()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["wrapper"]=1
//>>excludeEnd("ctx");
][0])._notNil())){
return $recv($recv($self._wrapper())._asJQuery())._hasClass_($self._focusClass());
} else {
return false;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"hasFocus",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLFocusableWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (html){
var self=this,$self=this;
return self;

}; }),
$globals.HLFocusableWidget);

$core.addMethod(
$core.method({
selector: "renderOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderOn: html\x0a    wrapper := html div\x0a    \x09class: self cssClass;\x0a\x09\x09at: 'tabindex' put: '0';\x0a\x09\x09onBlur: [ self wrapper asJQuery removeClass: self focusClass ];\x0a        onFocus: [ self wrapper asJQuery addClass: self focusClass ];\x0a\x09\x09yourself.\x0a\x09\x22must do this later, as renderContentOn may want to use self wrapper\x22\x09\x0a\x09wrapper with: [ self renderContentOn: html ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "cssClass", "at:put:", "onBlur:", "removeClass:", "asJQuery", "wrapper", "focusClass", "onFocus:", "addClass:", "yourself", "with:", "renderContentOn:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._div();
$recv($1)._class_($self._cssClass());
$recv($1)._at_put_("tabindex","0");
$recv($1)._onBlur_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv([$recv([$self._wrapper()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["wrapper"]=1
//>>excludeEnd("ctx");
][0])._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._removeClass_([$self._focusClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["focusClass"]=1
//>>excludeEnd("ctx");
][0]);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
$recv($1)._onFocus_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv($self._wrapper())._asJQuery())._addClass_($self._focusClass());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
$self.wrapper=$recv($1)._yourself();
$recv($self.wrapper)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._renderContentOn_(html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,3)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLFocusableWidget);



$core.addClass("HLListWidget", $globals.HLFocusableWidget, "Helios-Core");
$core.setSlots($globals.HLListWidget, ["items", "selectedItem"]);
$core.addMethod(
$core.method({
selector: "activateFirstListItem",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "activateFirstListItem\x0a\x09self activateListItem: ((wrapper asJQuery find: 'li.inactive') eq: 0)",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["activateListItem:", "eq:", "find:", "asJQuery"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._activateListItem_($recv($recv($recv($self.wrapper)._asJQuery())._find_("li.inactive"))._eq_((0)));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activateFirstListItem",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "activateItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "activateItem: anObject\x0a\x09self activateListItem: (self findListItemFor: anObject)",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["activateListItem:", "findListItemFor:"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._activateListItem_($self._findListItemFor_(anObject));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activateItem:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "activateListItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aListItem"],
source: "activateListItem: aListItem\x0a\x09| item |\x0a\x09\x0a\x09(aListItem get: 0) ifNil: [ ^ self ].\x0a\x09aListItem parent children removeClass: self activeItemCssClass.\x0a\x09aListItem addClass: self activeItemCssClass.\x0a    \x0a\x09self ensureVisible: aListItem.\x0a    \x0a   \x22Activate the corresponding item\x22\x0a   item := aListItem data: 'item'.\x0a   self selectedItem == item ifFalse: [\x0a\x09   self selectItem: item ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "get:", "removeClass:", "children", "parent", "activeItemCssClass", "addClass:", "ensureVisible:", "data:", "ifFalse:", "==", "selectedItem", "selectItem:"]
}, function ($methodClass){ return function (aListItem){
var self=this,$self=this;
var item;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(aListItem)._get_((0));
if($1 == null || $1.a$nil){
return self;
} else {
$1;
}
$recv($recv($recv(aListItem)._parent())._children())._removeClass_([$self._activeItemCssClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["activeItemCssClass"]=1
//>>excludeEnd("ctx");
][0]);
$recv(aListItem)._addClass_($self._activeItemCssClass());
$self._ensureVisible_(aListItem);
item=$recv(aListItem)._data_("item");
if(!$core.assert($recv($self._selectedItem()).__eq_eq(item))){
$self._selectItem_(item);
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activateListItem:",{aListItem:aListItem,item:item})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "activateNextListItem",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "activateNextListItem\x0a\x09self activateListItem: (self wrapper asJQuery find: ('li.', self activeItemCssClass)) next.\x0a\x09\x0a\x09\x22select the first item if none is selected\x22\x0a\x09(self wrapper asJQuery find: (' .', self activeItemCssClass)) get ifEmpty: [\x0a\x09\x09self activateFirstListItem ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["activateListItem:", "next", "find:", "asJQuery", "wrapper", ",", "activeItemCssClass", "ifEmpty:", "get", "activateFirstListItem"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._activateListItem_($recv([$recv([$recv([$self._wrapper()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["wrapper"]=1
//>>excludeEnd("ctx");
][0])._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._find_(["li.".__comma([$self._activeItemCssClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["activeItemCssClass"]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx[","]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["find:"]=1
//>>excludeEnd("ctx");
][0])._next());
$recv($recv($recv($recv($self._wrapper())._asJQuery())._find_(" .".__comma($self._activeItemCssClass())))._get())._ifEmpty_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._activateFirstListItem();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activateNextListItem",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "activatePreviousListItem",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "activatePreviousListItem\x0a\x09self activateListItem: (self wrapper asJQuery find:  ('li.', self activeItemCssClass)) prev",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["activateListItem:", "prev", "find:", "asJQuery", "wrapper", ",", "activeItemCssClass"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._activateListItem_($recv($recv($recv($self._wrapper())._asJQuery())._find_("li.".__comma($self._activeItemCssClass())))._prev());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activatePreviousListItem",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "activeItemCssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "activeItemCssClass\x0a\x09^'active'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "active";

}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "buttonsDivCssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "buttonsDivCssClass\x0a\x09^ 'pane_actions form-group'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "pane_actions form-group";

}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "cssClassForItem:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "cssClassForItem: anObject\x0a\x09^ ''",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
return "";

}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "defaultItems",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "defaultItems\x0a\x09^ #()",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return [];

}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "ensureVisible:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aListItem"],
source: "ensureVisible: aListItem\x09\x0a\x09\x22Move the scrollbar to show the active element\x22\x0a\x09\x0a\x09| parent position |\x0a\x09(aListItem get: 0) ifNil: [ ^ self ].\x0a\x09position := self positionOf: aListItem.\x0a\x09parent := aListItem parent.\x0a\x09\x0a    aListItem position top < 0 ifTrue: [\x0a\x09\x09(parent get: 0) scrollTop: ((parent get: 0) scrollTop + aListItem position top - 10) ].\x0a    aListItem position top + aListItem height > parent height ifTrue: [ \x0a\x09\x09(parent get: 0) scrollTop: ((parent get: 0) scrollTop + aListItem height - (parent height - aListItem position top)) +10 ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "get:", "positionOf:", "parent", "ifTrue:", "<", "top", "position", "scrollTop:", "-", "+", "scrollTop", ">", "height"]
}, function ($methodClass){ return function (aListItem){
var self=this,$self=this;
var parent,position;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=[$recv(aListItem)._get_((0))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["get:"]=1
//>>excludeEnd("ctx");
][0];
if($1 == null || $1.a$nil){
return self;
} else {
$1;
}
position=$self._positionOf_(aListItem);
parent=$recv(aListItem)._parent();
if($core.assert($recv([$recv([$recv(aListItem)._position()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["position"]=1
//>>excludeEnd("ctx");
][0])._top()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["top"]=1
//>>excludeEnd("ctx");
][0]).__lt((0)))){
[$recv([$recv(parent)._get_((0))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["get:"]=2
//>>excludeEnd("ctx");
][0])._scrollTop_([$recv([$recv([$recv([$recv(parent)._get_((0))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["get:"]=3
//>>excludeEnd("ctx");
][0])._scrollTop()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["scrollTop"]=1
//>>excludeEnd("ctx");
][0]).__plus([$recv([$recv(aListItem)._position()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["position"]=2
//>>excludeEnd("ctx");
][0])._top()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["top"]=2
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["+"]=1
//>>excludeEnd("ctx");
][0]).__minus((10))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["-"]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["scrollTop:"]=1
//>>excludeEnd("ctx");
][0];
}
if($core.assert($recv([$recv([$recv([$recv(aListItem)._position()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["position"]=3
//>>excludeEnd("ctx");
][0])._top()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["top"]=3
//>>excludeEnd("ctx");
][0]).__plus([$recv(aListItem)._height()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["height"]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["+"]=2
//>>excludeEnd("ctx");
][0]).__gt([$recv(parent)._height()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["height"]=2
//>>excludeEnd("ctx");
][0]))){
$recv([$recv(parent)._get_((0))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["get:"]=4
//>>excludeEnd("ctx");
][0])._scrollTop_([$recv([$recv($recv($recv($recv(parent)._get_((0)))._scrollTop()).__plus([$recv(aListItem)._height()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["height"]=3
//>>excludeEnd("ctx");
][0])).__minus($recv($recv(parent)._height()).__minus($recv($recv(aListItem)._position())._top()))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["-"]=2
//>>excludeEnd("ctx");
][0]).__plus((10))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["+"]=3
//>>excludeEnd("ctx");
][0]);
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"ensureVisible:",{aListItem:aListItem,parent:parent,position:position})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "findListItemFor:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "findListItemFor: anObject\x0a\x09^ (((wrapper asJQuery find: 'li') \x0a\x09\x09filter: [ :thisArg :otherArg | (thisArg asJQuery data: 'item') = anObject ] currySelf) eq: 0)",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["eq:", "filter:", "find:", "asJQuery", "currySelf", "=", "data:"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($recv([$recv($self.wrapper)._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._find_("li"))._filter_($recv((function(thisArg,otherArg){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv($recv(thisArg)._asJQuery())._data_("item")).__eq(anObject);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({thisArg:thisArg,otherArg:otherArg},$ctx1,1)});
//>>excludeEnd("ctx");
}))._currySelf()))._eq_((0));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"findListItemFor:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "focus",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focus\x0a\x09super focus.\x0a    self items ifNotEmpty: [ \x0a\x09\x09self selectedItem ifNil: [ self activateFirstListItem ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focus", "ifNotEmpty:", "items", "ifNil:", "selectedItem", "activateFirstListItem"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._focus.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv($self._items())._ifNotEmpty_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$1=$self._selectedItem();
if($1 == null || $1.a$nil){
return $self._activateFirstListItem();
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focus",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "items",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "items\x0a\x09^ items ifNil: [ items := self defaultItems ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "defaultItems"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.items;
if($1 == null || $1.a$nil){
$self.items=$self._defaultItems();
return $self.items;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"items",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "items:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection"],
source: "items: aCollection\x0a\x09items := aCollection",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aCollection){
var self=this,$self=this;
$self.items=aCollection;
return self;

}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "listCssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "listCssClass \x0a\x09^'nav nav-pills nav-stacked'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "nav nav-pills nav-stacked";

}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "listCssClassForItem:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "listCssClassForItem: anObject\x0a\x09^ self selectedItem = anObject\x0a\x09\x09ifTrue: [ self activeItemCssClass ]\x0a\x09\x09ifFalse: [ 'inactive' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:ifFalse:", "=", "selectedItem", "activeItemCssClass"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv($self._selectedItem()).__eq(anObject))){
return $self._activeItemCssClass();
} else {
return "inactive";
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"listCssClassForItem:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "positionOf:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aListItem"],
source: "positionOf: aListItem\x0a\x09<inlineJS: '\x0a    \x09return aListItem.parent().children().get().indexOf(aListItem.get(0)) + 1\x0a\x09'>",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [["inlineJS:", ["\x0a    \x09return aListItem.parent().children().get().indexOf(aListItem.get(0)) + 1\x0a\x09"]]],
messageSends: []
}, function ($methodClass){ return function (aListItem){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");

    	return aListItem.parent().children().get().indexOf(aListItem.get(0)) + 1
	;
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"positionOf:",{aListItem:aListItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "reactivateListItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aListItem"],
source: "reactivateListItem: aListItem\x0a\x09self activateListItem: aListItem.\x0a\x09self reselectItem: self selectedItem",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["activateListItem:", "reselectItem:", "selectedItem"]
}, function ($methodClass){ return function (aListItem){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._activateListItem_(aListItem);
$self._reselectItem_($self._selectedItem());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"reactivateListItem:",{aListItem:aListItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "refresh",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "refresh\x0a\x09super refresh.\x0a\x09self selectedItem ifNotNil: [self ensureVisible: (self findListItemFor: self selectedItem)].",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["refresh", "ifNotNil:", "selectedItem", "ensureVisible:", "findListItemFor:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._refresh.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$1=[$self._selectedItem()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedItem"]=1
//>>excludeEnd("ctx");
][0];
if($1 == null || $1.a$nil){
$1;
} else {
$self._ensureVisible_($self._findListItemFor_($self._selectedItem()));
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"refresh",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "renderButtonsOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderButtonsOn: html",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (html){
var self=this,$self=this;
return self;

}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x0a\x09|ul|\x0a\x09\x0a\x09ul := html ul \x0a    \x09\x09class: self listCssClass;\x0a        \x09with: [ self renderListOn: html ];\x0a\x09\x09\x09onClick: [ self focus ].\x0a\x09\x09\x09\x0a\x09ul asJQuery contextmenu: [ self toggleCommandsMenu. false].\x0a\x09\x09\x0a    html div class: self buttonsDivCssClass; with: [\x0a      \x09self renderButtonsOn: html ].\x0a        \x0a   self setupKeyBindings",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "ul", "listCssClass", "with:", "renderListOn:", "onClick:", "focus", "contextmenu:", "asJQuery", "toggleCommandsMenu", "div", "buttonsDivCssClass", "renderButtonsOn:", "setupKeyBindings"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
var ul;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$recv(html)._ul();
[$recv($1)._class_($self._listCssClass())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._renderListOn_(html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
ul=$recv($1)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._focus();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
$recv($recv(ul)._asJQuery())._contextmenu_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$self._toggleCommandsMenu();
return false;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,3)});
//>>excludeEnd("ctx");
}));
$2=$recv(html)._div();
$recv($2)._class_($self._buttonsDivCssClass());
$recv($2)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._renderButtonsOn_(html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,4)});
//>>excludeEnd("ctx");
}));
$self._setupKeyBindings();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html,ul:ul})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "renderItemLabel:on:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "html"],
source: "renderItemLabel: anObject on: html\x0a\x09html with: anObject asString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "asString"]
}, function ($methodClass){ return function (anObject,html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(html)._with_($recv(anObject)._asString());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderItemLabel:on:",{anObject:anObject,html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "renderItemModel:viewModel:on:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "anotherObject", "html"],
source: "renderItemModel: anObject viewModel: anotherObject on: html\x0a\x09| li |\x0a    \x0a\x09li := html li.\x0a\x09li asJQuery data: 'item' put: anObject.\x0a    li\x0a\x09\x09class: (self listCssClassForItem: anObject);\x0a        with: [ \x0a        \x09html a\x0a            \x09with: [ \x0a            \x09\x09(html tag: 'i') class: (self cssClassForItem: anObject).\x0a  \x09\x09\x09\x09\x09self renderItemLabel: anotherObject on: html ];\x0a\x09\x09\x09\x09onClick: [\x0a                  \x09self reactivateListItem: li asJQuery ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["li", "data:put:", "asJQuery", "class:", "listCssClassForItem:", "with:", "a", "tag:", "cssClassForItem:", "renderItemLabel:on:", "onClick:", "reactivateListItem:"]
}, function ($methodClass){ return function (anObject,anotherObject,html){
var self=this,$self=this;
var li;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
li=$recv(html)._li();
$recv([$recv(li)._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._data_put_("item",anObject);
$1=li;
[$recv($1)._class_($self._listCssClassForItem_(anObject))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$2=$recv(html)._a();
$recv($2)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
$recv($recv(html)._tag_("i"))._class_($self._cssClassForItem_(anObject));
return $self._renderItemLabel_on_(anotherObject,html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}));
return $recv($2)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._reactivateListItem_($recv(li)._asJQuery());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,3)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderItemModel:viewModel:on:",{anObject:anObject,anotherObject:anotherObject,html:html,li:li})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "renderListOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderListOn: html\x0a\x09self items do: [ :each  | \x0a    \x09self renderItemModel: each viewModel: each on: html ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "items", "renderItemModel:viewModel:on:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._items())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._renderItemModel_viewModel_on_(each,each,html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderListOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "reselectItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "reselectItem: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
return self;

}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "selectItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "selectItem: anObject\x0a\x09self selectedItem: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectedItem:"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._selectedItem_(anObject);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectItem:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "selectedItem",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectedItem\x0a\x09^ selectedItem",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.selectedItem;

}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "selectedItem:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "selectedItem: anObject\x0a\x09selectedItem := anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
$self.selectedItem=anObject;
return self;

}; }),
$globals.HLListWidget);

$core.addMethod(
$core.method({
selector: "setupKeyBindings",
protocol: "events",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setupKeyBindings \x0a\x09(HLRepeatedKeyDownHandler on: self)\x0a\x09\x09whileKeyDown: 38 do: [ self activatePreviousListItem ];\x0a\x09\x09whileKeyDown: 40 do: [ self activateNextListItem ];\x0a\x09\x09rebindKeys.\x0a\x09\x09\x0a\x09self wrapper asJQuery keydown: [ :e |\x0a        e which = 13 ifTrue: [ \x0a        \x09self reselectItem: self selectedItem ] ]",
referencedClasses: ["HLRepeatedKeyDownHandler"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["whileKeyDown:do:", "on:", "activatePreviousListItem", "activateNextListItem", "rebindKeys", "keydown:", "asJQuery", "wrapper", "ifTrue:", "=", "which", "reselectItem:", "selectedItem"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLRepeatedKeyDownHandler)._on_(self);
[$recv($1)._whileKeyDown_do_((38),(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._activatePreviousListItem();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["whileKeyDown:do:"]=1
//>>excludeEnd("ctx");
][0];
$recv($1)._whileKeyDown_do_((40),(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._activateNextListItem();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
$recv($1)._rebindKeys();
$recv($recv($self._wrapper())._asJQuery())._keydown_((function(e){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if($core.assert($recv($recv(e)._which()).__eq((13)))){
return $self._reselectItem_($self._selectedItem());
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({e:e},$ctx1,3)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setupKeyBindings",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLListWidget);



$core.addClass("HLNavigationListWidget", $globals.HLListWidget, "Helios-Core");
$core.setSlots($globals.HLNavigationListWidget, ["previous", "next"]);
$core.addMethod(
$core.method({
selector: "next",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "next\x0a\x09^ next",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.next;

}; }),
$globals.HLNavigationListWidget);

$core.addMethod(
$core.method({
selector: "next:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aWidget"],
source: "next: aWidget\x0a\x09next := aWidget.\x0a    aWidget previous = self ifFalse: [ aWidget previous: self ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifFalse:", "=", "previous", "previous:"]
}, function ($methodClass){ return function (aWidget){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.next=aWidget;
if(!$core.assert($recv($recv(aWidget)._previous()).__eq(self))){
$recv(aWidget)._previous_(self);
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"next:",{aWidget:aWidget})});
//>>excludeEnd("ctx");
}; }),
$globals.HLNavigationListWidget);

$core.addMethod(
$core.method({
selector: "nextFocus",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "nextFocus\x0a\x09self next ifNotNil: [ self next focus ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "next", "focus"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=[$self._next()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["next"]=1
//>>excludeEnd("ctx");
][0];
if($1 == null || $1.a$nil){
$1;
} else {
$recv($self._next())._focus();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"nextFocus",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLNavigationListWidget);

$core.addMethod(
$core.method({
selector: "previous",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "previous\x0a\x09^ previous",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.previous;

}; }),
$globals.HLNavigationListWidget);

$core.addMethod(
$core.method({
selector: "previous:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aWidget"],
source: "previous: aWidget\x0a\x09previous := aWidget.\x0a    aWidget next = self ifFalse: [ aWidget next: self ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifFalse:", "=", "next", "next:"]
}, function ($methodClass){ return function (aWidget){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.previous=aWidget;
if(!$core.assert($recv($recv(aWidget)._next()).__eq(self))){
$recv(aWidget)._next_(self);
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"previous:",{aWidget:aWidget})});
//>>excludeEnd("ctx");
}; }),
$globals.HLNavigationListWidget);

$core.addMethod(
$core.method({
selector: "previousFocus",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "previousFocus\x0a\x09self previous ifNotNil: [ self previous focus ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "previous", "focus"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=[$self._previous()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["previous"]=1
//>>excludeEnd("ctx");
][0];
if($1 == null || $1.a$nil){
$1;
} else {
$recv($self._previous())._focus();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"previousFocus",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLNavigationListWidget);

$core.addMethod(
$core.method({
selector: "setupKeyBindings",
protocol: "events",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setupKeyBindings\x0a\x09super setupKeyBindings.\x0a\x0a\x09self wrapper ifNotNil: [ wrapper onKeyDown: [ :e |\x0a        e which = 39 ifTrue: [ \x0a        \x09self nextFocus ].\x0a\x09\x09e which = 37 ifTrue: [ \x0a        \x09self previousFocus ] ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["setupKeyBindings", "ifNotNil:", "wrapper", "onKeyDown:", "ifTrue:", "=", "which", "nextFocus", "previousFocus"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._setupKeyBindings.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$1=$self._wrapper();
if($1 == null || $1.a$nil){
$1;
} else {
$recv($self.wrapper)._onKeyDown_((function(e){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if($core.assert([$recv([$recv(e)._which()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["which"]=1
//>>excludeEnd("ctx");
][0]).__eq((39))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["="]=1
//>>excludeEnd("ctx");
][0])){
$self._nextFocus();
}
if($core.assert($recv($recv(e)._which()).__eq((37)))){
return $self._previousFocus();
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({e:e},$ctx1,2)});
//>>excludeEnd("ctx");
}));
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setupKeyBindings",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLNavigationListWidget);



$core.addClass("HLToolListWidget", $globals.HLNavigationListWidget, "Helios-Core");
$core.setSlots($globals.HLToolListWidget, ["model", "commandsMenuElement"]);
$core.addMethod(
$core.method({
selector: "activateListItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anItem"],
source: "activateListItem: anItem\x0a\x09self model withChangesDo: [ super activateListItem: anItem ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "model", "activateListItem:"]
}, function ($methodClass){ return function (anItem){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return [(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._activateListItem_.call($self,anItem))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.supercall = false
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activateListItem:",{anItem:anItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolListWidget);

$core.addMethod(
$core.method({
selector: "activateNextListItem",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "activateNextListItem\x0a\x09self model withChangesDo: [ super activateNextListItem ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "model", "activateNextListItem"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return [(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._activateNextListItem.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.supercall = false
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activateNextListItem",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolListWidget);

$core.addMethod(
$core.method({
selector: "activatePreviousListItem",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "activatePreviousListItem\x0a\x09self model withChangesDo: [ super activatePreviousListItem ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "model", "activatePreviousListItem"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return [(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._activatePreviousListItem.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.supercall = false
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activatePreviousListItem",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolListWidget);

$core.addMethod(
$core.method({
selector: "commandCategory",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "commandCategory\x0a\x09^ self label",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["label"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $self._label();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"commandCategory",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolListWidget);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'List'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "List";

}; }),
$globals.HLToolListWidget);

$core.addMethod(
$core.method({
selector: "menuCommands",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "menuCommands\x0a\x09\x22Answer a collection of commands to be put in the cog menu\x22\x0a\x09\x0a\x09^ ((HLToolCommand concreteClasses\x0a\x09\x09select: [ :each | each isValidFor: self model ])\x0a\x09\x09\x09collect: [ :each | each for: self model ])\x0a\x09\x09\x09select: [ :each | \x0a\x09\x09\x09\x09each category = self commandCategory and: [ \x0a\x09\x09\x09\x09\x09each isAction and: [ each isActive ] ] ]",
referencedClasses: ["HLToolCommand"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["select:", "collect:", "concreteClasses", "isValidFor:", "model", "for:", "and:", "=", "category", "commandCategory", "isAction", "isActive"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return [$recv($recv($recv($recv($globals.HLToolCommand)._concreteClasses())._select_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._isValidFor_([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0]);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
})))._collect_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._for_($self._model());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,2)});
//>>excludeEnd("ctx");
})))._select_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if($core.assert($recv($recv(each)._category()).__eq($self._commandCategory()))){
if($core.assert($recv(each)._isAction())){
return $recv(each)._isActive();
} else {
return false;
}
} else {
return false;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,3)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["select:"]=1
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"menuCommands",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolListWidget);

$core.addMethod(
$core.method({
selector: "model",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "model\x0a\x09^ model",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.model;

}; }),
$globals.HLToolListWidget);

$core.addMethod(
$core.method({
selector: "model:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBrowserModel"],
source: "model: aBrowserModel\x0a\x09model := aBrowserModel.\x0a    \x0a    self \x0a\x09\x09observeSystem;\x0a\x09\x09observeModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["observeSystem", "observeModel"]
}, function ($methodClass){ return function (aBrowserModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.model=aBrowserModel;
$self._observeSystem();
$self._observeModel();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"model:",{aBrowserModel:aBrowserModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolListWidget);

$core.addMethod(
$core.method({
selector: "observeModel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return self;

}; }),
$globals.HLToolListWidget);

$core.addMethod(
$core.method({
selector: "observeSystem",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeSystem",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return self;

}; }),
$globals.HLToolListWidget);

$core.addMethod(
$core.method({
selector: "reactivateListItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anItem"],
source: "reactivateListItem: anItem\x0a\x09self model withChangesDo: [ super reactivateListItem: anItem ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "model", "reactivateListItem:"]
}, function ($methodClass){ return function (anItem){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return [(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._reactivateListItem_.call($self,anItem))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.supercall = false
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"reactivateListItem:",{anItem:anItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolListWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09self renderHeadOn: html.\x09\x0a\x09super renderContentOn: html",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["renderHeadOn:", "renderContentOn:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._renderHeadOn_(html);
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._renderContentOn_.call($self,html))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolListWidget);

$core.addMethod(
$core.method({
selector: "renderHeadOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderHeadOn: html\x0a\x09html div \x0a\x09\x09class: 'list-label';\x0a\x09\x09with: [\x0a\x09\x09\x09html with: self label.\x0a\x09\x09\x09self renderMenuOn: html ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "with:", "label", "renderMenuOn:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._div();
$recv($1)._class_("list-label");
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$recv(html)._with_($self._label());
return $self._renderMenuOn_(html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderHeadOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolListWidget);

$core.addMethod(
$core.method({
selector: "renderMenuOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderMenuOn: html\x0a\x09| commands |\x0a\x09\x0a\x09commands := self menuCommands.\x0a\x09commands ifEmpty: [ ^ self ].\x0a\x09\x0a\x09html div \x0a\x09\x09class: 'btn-group cog';\x0a\x09\x09with: [\x0a\x09\x09\x09commandsMenuElement := html a\x0a\x09\x09\x09\x09class: 'btn btn-default dropdown-toggle';\x0a\x09\x09\x09\x09at: 'data-toggle' put: 'dropdown';\x0a\x09\x09\x09\x09with: [ (html tag: 'i') class: 'glyphicon glyphicon-chevron-down' ].\x0a\x09\x09html ul \x0a\x09\x09\x09class: 'dropdown-menu pull-right';\x0a\x09\x09\x09with: [ \x0a\x09\x09\x09\x09commands do: [ :each | \x0a\x09\x09\x09\x09\x09html li with: [ html a \x0a\x09\x09\x09\x09\x09\x09with: each menuLabel;\x0a\x09\x09\x09\x09\x09\x09onClick: [ self execute: each ] ] ] ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["menuCommands", "ifEmpty:", "class:", "div", "with:", "a", "at:put:", "tag:", "ul", "do:", "li", "menuLabel", "onClick:", "execute:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
var commands;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3,$4;
var $early={};
try {
commands=$self._menuCommands();
$recv(commands)._ifEmpty_((function(){
throw $early=[self];

}));
$1=$recv(html)._div();
[$recv($1)._class_("btn-group cog")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$2=[$recv(html)._a()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["a"]=1
//>>excludeEnd("ctx");
][0];
[$recv($2)._class_("btn btn-default dropdown-toggle")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["class:"]=2
//>>excludeEnd("ctx");
][0];
$recv($2)._at_put_("data-toggle","dropdown");
$self.commandsMenuElement=[$recv($2)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return [$recv($recv(html)._tag_("i"))._class_("glyphicon glyphicon-chevron-down")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["class:"]=3
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,3)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=2
//>>excludeEnd("ctx");
][0];
$3=$recv(html)._ul();
$recv($3)._class_("dropdown-menu pull-right");
return [$recv($3)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv(commands)._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx4) {
//>>excludeEnd("ctx");
return [$recv($recv(html)._li())._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx5) {
//>>excludeEnd("ctx");
$4=$recv(html)._a();
$recv($4)._with_($recv(each)._menuLabel());
return $recv($4)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx6) {
//>>excludeEnd("ctx");
return $self._execute_(each);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx6) {$ctx6.fillBlock({},$ctx5,7)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx5) {$ctx5.fillBlock({},$ctx4,6)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx4.sendIdx["with:"]=4
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx4) {$ctx4.fillBlock({each:each},$ctx3,5)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,4)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=3
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
return self;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderMenuOn:",{html:html,commands:commands})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolListWidget);

$core.addMethod(
$core.method({
selector: "selectedItem:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anItem"],
source: "selectedItem: anItem\x0a\x09\x22Selection changed, update the cog menu\x22\x0a\x09\x0a\x09super selectedItem: anItem.\x0a\x09self updateMenu",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectedItem:", "updateMenu"]
}, function ($methodClass){ return function (anItem){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._selectedItem_.call($self,anItem))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$self._updateMenu();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectedItem:",{anItem:anItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolListWidget);

$core.addMethod(
$core.method({
selector: "toggleCommandsMenu",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "toggleCommandsMenu\x0a\x0a\x09commandsMenuElement asJQuery dropdown: 'toggle'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["dropdown:", "asJQuery"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self.commandsMenuElement)._asJQuery())._dropdown_("toggle");
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"toggleCommandsMenu",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolListWidget);

$core.addMethod(
$core.method({
selector: "unregister",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "unregister\x0a\x09super unregister.\x0a\x09\x0a\x09self model announcer unsubscribe: self.\x0a\x09self model systemAnnouncer unsubscribe: self",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unregister", "unsubscribe:", "announcer", "model", "systemAnnouncer"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._unregister.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
[$recv($recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._announcer())._unsubscribe_(self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["unsubscribe:"]=1
//>>excludeEnd("ctx");
][0];
$recv($recv($self._model())._systemAnnouncer())._unsubscribe_(self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unregister",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolListWidget);

$core.addMethod(
$core.method({
selector: "updateMenu",
protocol: "updating",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "updateMenu\x0a\x09(self wrapper asJQuery find: '.cog') remove.\x0a\x09\x0a\x09[ :html | self renderMenuOn: html ] \x0a\x09\x09appendToJQuery: (self wrapper asJQuery find: '.list-label')",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["remove", "find:", "asJQuery", "wrapper", "appendToJQuery:", "renderMenuOn:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv([$recv([$recv([$self._wrapper()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["wrapper"]=1
//>>excludeEnd("ctx");
][0])._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._find_(".cog")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["find:"]=1
//>>excludeEnd("ctx");
][0])._remove();
$recv((function(html){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._renderMenuOn_(html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({html:html},$ctx1,1)});
//>>excludeEnd("ctx");
}))._appendToJQuery_($recv($recv($self._wrapper())._asJQuery())._find_(".list-label"));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"updateMenu",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolListWidget);


$core.addMethod(
$core.method({
selector: "on:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aModel"],
source: "on: aModel\x0a\x09^ self new \x0a    \x09model: aModel;\x0a        yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["model:", "new", "yourself"]
}, function ($methodClass){ return function (aModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._model_(aModel);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{aModel:aModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolListWidget.a$cls);


$core.addClass("HLTabListWidget", $globals.HLListWidget, "Helios-Core");
$core.setSlots($globals.HLTabListWidget, ["callback"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLTabListWidget.comment="I am a widget used to display a list of helios tabs.\x0a\x0aWhen a tab is selected, `callback` is evaluated with the selected tab as argument.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "callback",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "callback\x0a\x09^ callback ifNil: [ [] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.callback;
if($1 == null || $1.a$nil){
return (function(){

});
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"callback",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabListWidget);

$core.addMethod(
$core.method({
selector: "callback:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBlock"],
source: "callback: aBlock\x0a\x09callback := aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aBlock){
var self=this,$self=this;
$self.callback=aBlock;
return self;

}; }),
$globals.HLTabListWidget);

$core.addMethod(
$core.method({
selector: "renderItemLabel:on:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTab", "html"],
source: "renderItemLabel: aTab on: html\x0a\x09html span\x0a\x09\x09class: aTab cssClass;\x0a\x09\x09with: aTab label",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "span", "cssClass", "with:", "label"]
}, function ($methodClass){ return function (aTab,html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._span();
$recv($1)._class_($recv(aTab)._cssClass());
$recv($1)._with_($recv(aTab)._label());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderItemLabel:on:",{aTab:aTab,html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabListWidget);

$core.addMethod(
$core.method({
selector: "selectItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTab"],
source: "selectItem: aTab\x0a\x09super selectItem: aTab.\x0a\x09self callback value: aTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectItem:", "value:", "callback"]
}, function ($methodClass){ return function (aTab){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._selectItem_.call($self,aTab))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv($self._callback())._value_(aTab);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectItem:",{aTab:aTab})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabListWidget);



$core.addClass("HLInformationWidget", $globals.HLWidget, "Helios-Core");
$core.setSlots($globals.HLInformationWidget, ["informationString"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLInformationWidget.comment="I display an information dialog.\x0a\x0a## API\x0a\x0a`HLWidget >> #inform:` is a convenience method for creating information dialogs.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "informationString",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "informationString\x0a\x09^ informationString ifNil: [ '' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.informationString;
if($1 == null || $1.a$nil){
return "";
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"informationString",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInformationWidget);

$core.addMethod(
$core.method({
selector: "informationString:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "informationString: anObject\x0a\x09informationString := anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
$self.informationString=anObject;
return self;

}; }),
$globals.HLInformationWidget);

$core.addMethod(
$core.method({
selector: "remove",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "remove\x0a\x09[ \x0a\x09\x09self wrapper asJQuery fadeOut: 100.\x0a\x09\x09[ self wrapper asJQuery remove ]\x0a\x09\x09\x09valueWithTimeout: 400.\x0a\x09]\x0a\x09\x09valueWithTimeout: 1500",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["valueWithTimeout:", "fadeOut:", "asJQuery", "wrapper", "remove"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$recv([$recv([$self._wrapper()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["wrapper"]=1
//>>excludeEnd("ctx");
][0])._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._fadeOut_((100));
return $recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv($recv($self._wrapper())._asJQuery())._remove();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}))._valueWithTimeout_((400));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._valueWithTimeout_((1500))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["valueWithTimeout:"]=1
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"remove",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInformationWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09html div \x0a\x09\x09class: 'growl'; \x0a\x09\x09with: self informationString.\x0a\x09\x09\x0a\x09self remove",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "with:", "informationString", "remove"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._div();
$recv($1)._class_("growl");
$recv($1)._with_($self._informationString());
$self._remove();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInformationWidget);

$core.addMethod(
$core.method({
selector: "show",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "show\x0a\x09self appendToJQuery: 'body' asJQuery",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["appendToJQuery:", "asJQuery"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._appendToJQuery_("body"._asJQuery());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"show",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInformationWidget);



$core.addClass("HLManager", $globals.HLWidget, "Helios-Core");
$core.setSlots($globals.HLManager, ["tabsWidget", "environment", "history", "announcer", "rendered"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLManager.comment="HLManager is the entry point Class of Helios.\x0a\x0aIts `singleton` instance is created on startup, and rendered on body.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "activate:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTab"],
source: "activate: aTab\x0a\x09self tabsWidget activate: aTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["activate:", "tabsWidget"]
}, function ($methodClass){ return function (aTab){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._tabsWidget())._activate_(aTab);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activate:",{aTab:aTab})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "activeTab",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "activeTab\x0a\x09^ self tabsWidget activeTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["activeTab", "tabsWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._tabsWidget())._activeTab();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activeTab",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "addTab:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTab"],
source: "addTab: aTab\x0a\x09self tabsWidget addTab: aTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["addTab:", "tabsWidget"]
}, function ($methodClass){ return function (aTab){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._tabsWidget())._addTab_(aTab);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"addTab:",{aTab:aTab})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "announcer",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "announcer\x0a\x09^ announcer ifNil: [ announcer := Announcer new ]",
referencedClasses: ["Announcer"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.announcer;
if($1 == null || $1.a$nil){
$self.announcer=$recv($globals.Announcer)._new();
return $self.announcer;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"announcer",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "confirm:ifFalse:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString", "aBlock"],
source: "confirm: aString ifFalse: aBlock\x0a\x09self \x0a\x09\x09confirm: aString\x0a\x09\x09ifTrue: []\x0a\x09\x09ifFalse: aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["confirm:ifTrue:ifFalse:"]
}, function ($methodClass){ return function (aString,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._confirm_ifTrue_ifFalse_(aString,(function(){

}),aBlock);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"confirm:ifFalse:",{aString:aString,aBlock:aBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "confirm:ifTrue:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString", "aBlock"],
source: "confirm: aString ifTrue: aBlock\x0a\x09self \x0a\x09\x09confirm: aString\x0a\x09\x09ifTrue: aBlock\x0a\x09\x09ifFalse: []",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["confirm:ifTrue:ifFalse:"]
}, function ($methodClass){ return function (aString,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._confirm_ifTrue_ifFalse_(aString,aBlock,(function(){

}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"confirm:ifTrue:",{aString:aString,aBlock:aBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "confirm:ifTrue:ifFalse:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString", "aBlock", "anotherBlock"],
source: "confirm: aString ifTrue: aBlock ifFalse: anotherBlock\x0a\x09HLConfirmationWidget new\x0a\x09\x09confirmationString: aString;\x0a\x09\x09actionBlock: aBlock;\x0a\x09\x09cancelBlock: anotherBlock;\x0a\x09\x09show",
referencedClasses: ["HLConfirmationWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["confirmationString:", "new", "actionBlock:", "cancelBlock:", "show"]
}, function ($methodClass){ return function (aString,aBlock,anotherBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLConfirmationWidget)._new();
$recv($1)._confirmationString_(aString);
$recv($1)._actionBlock_(aBlock);
$recv($1)._cancelBlock_(anotherBlock);
$recv($1)._show();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"confirm:ifTrue:ifFalse:",{aString:aString,aBlock:aBlock,anotherBlock:anotherBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "defaultEnvironment",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "defaultEnvironment\x0a\x09\x22If helios is loaded from within a frame, answer the parent window environment\x22\x0a\x09\x0a\x09| parent parentSmalltalkGlobals |\x0a\x09\x0a\x09parent := window opener ifNil: [ window parent ].\x0a\x09parent ifNil: [ ^ Environment new ].\x0a\x09\x0a\x09parentSmalltalkGlobals := ((parent at: 'requirejs') value: 'amber/boot') at: 'globals'.\x0a\x09parentSmalltalkGlobals ifNil: [ ^ Environment new ].\x0a\x09\x0a\x09self handleLossOfEnvironmentWithParent: parent.\x0a\x09\x0a\x09^ (parentSmalltalkGlobals at: 'Environment') new",
referencedClasses: ["Environment"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "opener", "parent", "new", "at:", "value:", "handleLossOfEnvironmentWithParent:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var parent,parentSmalltalkGlobals;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3;
$1=$recv(window)._opener();
if($1 == null || $1.a$nil){
parent=$recv(window)._parent();
} else {
parent=$1;
}
$2=parent;
if($2 == null || $2.a$nil){
return [$recv($globals.Environment)._new()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["new"]=1
//>>excludeEnd("ctx");
][0];
} else {
$2;
}
parentSmalltalkGlobals=[$recv($recv([$recv(parent)._at_("requirejs")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:"]=2
//>>excludeEnd("ctx");
][0])._value_("amber/boot"))._at_("globals")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:"]=1
//>>excludeEnd("ctx");
][0];
$3=parentSmalltalkGlobals;
if($3 == null || $3.a$nil){
return [$recv($globals.Environment)._new()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["new"]=2
//>>excludeEnd("ctx");
][0];
} else {
$3;
}
$self._handleLossOfEnvironmentWithParent_(parent);
return $recv($recv(parentSmalltalkGlobals)._at_("Environment"))._new();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"defaultEnvironment",{parent:parent,parentSmalltalkGlobals:parentSmalltalkGlobals})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "environment",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "environment\x0a\x09\x22The default environment used by all Helios objects\x22\x0a    \x0a\x09^ environment ifNil: [ environment := self defaultEnvironment ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "defaultEnvironment"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.environment;
if($1 == null || $1.a$nil){
$self.environment=$self._defaultEnvironment();
return $self.environment;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"environment",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "environment:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anEnvironment"],
source: "environment: anEnvironment\x0a\x09environment := anEnvironment",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anEnvironment){
var self=this,$self=this;
$self.environment=anEnvironment;
return self;

}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "handleLossOfEnvironmentWithParent:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["parent"],
source: "handleLossOfEnvironmentWithParent: parent\x0a\x09parent at: 'onunload' put: [ \x0a\x09\x09self removeBeforeUnloadMessage.\x0a\x09\x09window close ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["at:put:", "removeBeforeUnloadMessage", "close"]
}, function ($methodClass){ return function (parent){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(parent)._at_put_("onunload",(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$self._removeBeforeUnloadMessage();
return $recv(window)._close();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"handleLossOfEnvironmentWithParent:",{parent:parent})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "history",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "history\x0a\x09^ history ifNil: [ history := OrderedCollection new ]",
referencedClasses: ["OrderedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.history;
if($1 == null || $1.a$nil){
$self.history=$recv($globals.OrderedCollection)._new();
return $self.history;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"history",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "history:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection"],
source: "history: aCollection\x0a\x09history := aCollection",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aCollection){
var self=this,$self=this;
$self.history=aCollection;
return self;

}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "inform:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "inform: aString\x0a\x09HLInformationWidget new\x0a\x09\x09informationString: aString;\x0a\x09\x09show",
referencedClasses: ["HLInformationWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["informationString:", "new", "show"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLInformationWidget)._new();
$recv($1)._informationString_(aString);
$recv($1)._show();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inform:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "initialize",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initialize\x0a\x09super initialize.\x0a\x09rendered := false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initialize"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._initialize.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$self.rendered=false;
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initialize",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "keyBinder",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "keyBinder\x0a\x09^ HLKeyBinder current",
referencedClasses: ["HLKeyBinder"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["current"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.HLKeyBinder)._current();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"keyBinder",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "registerErrorHandler",
protocol: "services",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "registerErrorHandler\x0a\x09| handler |\x0a\x09handler := HLErrorHandler new.\x0a\x09self environment registerErrorHandler: handler.\x0a\x09ErrorHandler register: handler",
referencedClasses: ["HLErrorHandler", "ErrorHandler"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["new", "registerErrorHandler:", "environment", "register:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var handler;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
handler=$recv($globals.HLErrorHandler)._new();
$recv($self._environment())._registerErrorHandler_(handler);
$recv($globals.ErrorHandler)._register_(handler);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"registerErrorHandler",{handler:handler})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "registerFinder",
protocol: "services",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "registerFinder\x0a\x09self environment registerFinder: HLFinder new.\x0a\x09Finder register: HLFinder new",
referencedClasses: ["HLFinder", "Finder"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["registerFinder:", "environment", "new", "register:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._environment())._registerFinder_([$recv($globals.HLFinder)._new()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["new"]=1
//>>excludeEnd("ctx");
][0]);
$recv($globals.Finder)._register_($recv($globals.HLFinder)._new());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"registerFinder",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "registerInspector",
protocol: "services",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "registerInspector\x0a\x09self environment registerInspector: HLInspector.\x0a\x09Inspector register: HLInspector",
referencedClasses: ["HLInspector", "Inspector"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["registerInspector:", "environment", "register:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._environment())._registerInspector_($globals.HLInspector);
$recv($globals.Inspector)._register_($globals.HLInspector);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"registerInspector",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "registerProgressHandler",
protocol: "services",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "registerProgressHandler\x0a\x09self environment registerProgressHandler: HLProgressHandler new.\x0a\x09ProgressHandler register: HLProgressHandler new",
referencedClasses: ["HLProgressHandler", "ProgressHandler"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["registerProgressHandler:", "environment", "new", "register:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._environment())._registerProgressHandler_([$recv($globals.HLProgressHandler)._new()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["new"]=1
//>>excludeEnd("ctx");
][0]);
$recv($globals.ProgressHandler)._register_($recv($globals.HLProgressHandler)._new());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"registerProgressHandler",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "registerServices",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "registerServices\x0a\x09self\x0a\x09\x09registerInspector;\x0a\x09\x09registerErrorHandler;\x0a\x09\x09registerProgressHandler;\x0a\x09\x09registerTranscript;\x0a\x09\x09registerFinder",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["registerInspector", "registerErrorHandler", "registerProgressHandler", "registerTranscript", "registerFinder"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._registerInspector();
$self._registerErrorHandler();
$self._registerProgressHandler();
$self._registerTranscript();
$self._registerFinder();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"registerServices",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "registerTranscript",
protocol: "services",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "registerTranscript\x0a\x09self environment registerTranscript: HLTranscriptHandler",
referencedClasses: ["HLTranscriptHandler"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["registerTranscript:", "environment"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._environment())._registerTranscript_($globals.HLTranscriptHandler);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"registerTranscript",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "removeActiveTab",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "removeActiveTab\x0a\x09self tabsWidget removeActiveTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["removeActiveTab", "tabsWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._tabsWidget())._removeActiveTab();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"removeActiveTab",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "removeBeforeUnloadMessage",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "removeBeforeUnloadMessage\x0a\x09<inlineJS: 'window.onbeforeunload = null'>",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [["inlineJS:", ["window.onbeforeunload = null"]]],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
window.onbeforeunload = null;
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"removeBeforeUnloadMessage",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "removeTabForWidget:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aWidget"],
source: "removeTabForWidget: aWidget\x0a\x09self tabsWidget removeTabForWidget: aWidget",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["removeTabForWidget:", "tabsWidget"]
}, function ($methodClass){ return function (aWidget){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._tabsWidget())._removeTabForWidget_(aWidget);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"removeTabForWidget:",{aWidget:aWidget})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09html with: self tabsWidget.\x0a\x09html with: HLWelcomeWidget new.\x0a\x09\x0a\x09self renderDefaultTabs.\x0a\x09rendered := true",
referencedClasses: ["HLWelcomeWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "tabsWidget", "new", "renderDefaultTabs"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$recv(html)._with_($self._tabsWidget())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
$recv(html)._with_($recv($globals.HLWelcomeWidget)._new());
$self._renderDefaultTabs();
$self.rendered=true;
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "renderDefaultTabs",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "renderDefaultTabs\x0a\x09rendered ifFalse: [\x0a\x09\x09HLWorkspace openAsTab.\x0a\x09\x09HLBrowser openAsTab ]",
referencedClasses: ["HLWorkspace", "HLBrowser"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifFalse:", "openAsTab"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if(!$core.assert($self.rendered)){
[$recv($globals.HLWorkspace)._openAsTab()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["openAsTab"]=1
//>>excludeEnd("ctx");
][0];
$recv($globals.HLBrowser)._openAsTab();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderDefaultTabs",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "request:do:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString", "aBlock"],
source: "request: aString do: aBlock\x0a\x09self \x0a\x09\x09request: aString\x0a\x09\x09value: ''\x0a\x09\x09do: aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["request:value:do:"]
}, function ($methodClass){ return function (aString,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._request_value_do_(aString,"",aBlock);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"request:do:",{aString:aString,aBlock:aBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "request:value:do:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString", "valueString", "aBlock"],
source: "request: aString value: valueString do: aBlock\x0a\x09HLRequestWidget new\x0a\x09\x09confirmationString: aString;\x0a\x09\x09actionBlock: aBlock;\x0a\x09\x09value: valueString;\x0a\x09\x09show",
referencedClasses: ["HLRequestWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["confirmationString:", "new", "actionBlock:", "value:", "show"]
}, function ($methodClass){ return function (aString,valueString,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLRequestWidget)._new();
$recv($1)._confirmationString_(aString);
$recv($1)._actionBlock_(aBlock);
$recv($1)._value_(valueString);
$recv($1)._show();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"request:value:do:",{aString:aString,valueString:valueString,aBlock:aBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "setEditorTheme:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTheme"],
source: "setEditorTheme: aTheme\x0a\x0a\x09'helios.editorTheme' asSetting value: aTheme",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["value:", "asSetting"]
}, function ($methodClass){ return function (aTheme){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv("helios.editorTheme"._asSetting())._value_(aTheme);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setEditorTheme:",{aTheme:aTheme})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "setTheme:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTheme"],
source: "setTheme: aTheme\x0a\x09| currentTheme |\x0a\x0a\x09currentTheme := 'helios.theme' asSettingIfAbsent: 'default'.\x0a\x09\x0a\x09'body' asJQuery\x0a\x09\x09removeClass: currentTheme value;\x0a\x09\x09addClass: aTheme.\x0a\x09\x09\x0a\x09\x0a\x09'helios.theme' asSetting value: aTheme",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["asSettingIfAbsent:", "removeClass:", "asJQuery", "value", "addClass:", "value:", "asSetting"]
}, function ($methodClass){ return function (aTheme){
var self=this,$self=this;
var currentTheme;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
currentTheme="helios.theme"._asSettingIfAbsent_("default");
$1="body"._asJQuery();
$recv($1)._removeClass_($recv(currentTheme)._value());
$recv($1)._addClass_(aTheme);
$recv("helios.theme"._asSetting())._value_(aTheme);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setTheme:",{aTheme:aTheme,currentTheme:currentTheme})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "setup",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setup\x0a\x09self \x0a\x09\x09registerServices;\x0a\x09\x09setupEvents.\x0a    self keyBinder setupEvents.\x0a\x09self tabsWidget setupEvents.\x0a\x09self setupTheme.\x0a\x09\x0a\x09\x0a\x09'#helper' asJQuery fadeOut",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["registerServices", "setupEvents", "keyBinder", "tabsWidget", "setupTheme", "fadeOut", "asJQuery"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._registerServices();
[$self._setupEvents()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["setupEvents"]=1
//>>excludeEnd("ctx");
][0];
[$recv($self._keyBinder())._setupEvents()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["setupEvents"]=2
//>>excludeEnd("ctx");
][0];
$recv($self._tabsWidget())._setupEvents();
$self._setupTheme();
$recv("#helper"._asJQuery())._fadeOut();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setup",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "setupEvents",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setupEvents\x0a\x09'body' asJQuery keydown: [ :event |\x0a\x09\x09\x09\x0a\x09\x09\x22On ctrl keydown, adds a 'navigation' css class to <body>\x0a\x09\x09for the CodeMirror navigation links. See `HLCodeWidget`.\x22\x0a\x09\x09event ctrlKey ifTrue: [\x0a\x09\x09\x09'body' asJQuery addClass: 'navigation' ] ].\x0a\x09\x09\x09\x0a\x09'body' asJQuery keyup: [ :event |\x0a\x09\x09'body' asJQuery removeClass: 'navigation' ].\x0a\x09\x09\x0a\x09window asJQuery resize: [ :event |\x0a\x09\x09self refresh ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["keydown:", "asJQuery", "ifTrue:", "ctrlKey", "addClass:", "keyup:", "removeClass:", "resize:", "refresh"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(["body"._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._keydown_((function(event){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if($core.assert($recv(event)._ctrlKey())){
return $recv(["body"._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["asJQuery"]=2
//>>excludeEnd("ctx");
][0])._addClass_("navigation");
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({event:event},$ctx1,1)});
//>>excludeEnd("ctx");
}));
$recv(["body"._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=3
//>>excludeEnd("ctx");
][0])._keyup_((function(event){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(["body"._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["asJQuery"]=4
//>>excludeEnd("ctx");
][0])._removeClass_("navigation");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({event:event},$ctx1,3)});
//>>excludeEnd("ctx");
}));
$recv($recv(window)._asJQuery())._resize_((function(event){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._refresh();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({event:event},$ctx1,4)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setupEvents",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "setupTheme",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setupTheme\x0a\x09self setTheme: ('helios.theme' settingValueIfAbsent: 'default')",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["setTheme:", "settingValueIfAbsent:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._setTheme_("helios.theme"._settingValueIfAbsent_("default"));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setupTheme",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "tabWidth",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabWidth\x0a\x09^ (window asJQuery width - 90) / self tabs size",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["/", "-", "width", "asJQuery", "size", "tabs"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($recv($recv(window)._asJQuery())._width()).__minus((90))).__slash($recv($self._tabs())._size());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"tabWidth",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "tabs",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabs\x0a\x09^ self tabsWidget tabs",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["tabs", "tabsWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._tabsWidget())._tabs();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"tabs",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);

$core.addMethod(
$core.method({
selector: "tabsWidget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabsWidget\x0a\x09^ tabsWidget ifNil: [ tabsWidget := HLTabsWidget new ]",
referencedClasses: ["HLTabsWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.tabsWidget;
if($1 == null || $1.a$nil){
$self.tabsWidget=$recv($globals.HLTabsWidget)._new();
return $self.tabsWidget;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"tabsWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager);


$core.setSlots($globals.HLManager.a$cls, ["current"]);
$core.addMethod(
$core.method({
selector: "current",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "current\x0a\x09^ current ifNil: [ current := self basicNew initialize ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "initialize", "basicNew"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.current;
if($1 == null || $1.a$nil){
$self.current=$recv($self._basicNew())._initialize();
return $self.current;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"current",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager.a$cls);

$core.addMethod(
$core.method({
selector: "new",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "new\x0a\x09\x22Use current instead\x22\x0a\x0a\x09self shouldNotImplement",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["shouldNotImplement"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._shouldNotImplement();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"new",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager.a$cls);

$core.addMethod(
$core.method({
selector: "setup",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setup\x0a\x09self current \x0a\x09\x09setup;\x0a\x09\x09appendToJQuery: 'body' asJQuery.\x0a\x09\x0a\x09('helios.confirmOnExit' settingValueIfAbsent: true) ifTrue: [\x0a\x09\x09window onbeforeunload: [ 'Do you want to close Amber? All uncommitted changes will be lost.' ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["setup", "current", "appendToJQuery:", "asJQuery", "ifTrue:", "settingValueIfAbsent:", "onbeforeunload:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._current();
$recv($1)._setup();
$recv($1)._appendToJQuery_("body"._asJQuery());
if($core.assert("helios.confirmOnExit"._settingValueIfAbsent_(true))){
$recv(window)._onbeforeunload_((function(){
return "Do you want to close Amber? All uncommitted changes will be lost.";

}));
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setup",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLManager.a$cls);


$core.addClass("HLModalWidget", $globals.HLWidget, "Helios-Core");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLModalWidget.comment="I implement an abstract modal widget.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "giveFocusToButton:",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aButton"],
source: "giveFocusToButton: aButton\x0a\x09aButton asJQuery focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focus", "asJQuery"]
}, function ($methodClass){ return function (aButton){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv(aButton)._asJQuery())._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"giveFocusToButton:",{aButton:aButton})});
//>>excludeEnd("ctx");
}; }),
$globals.HLModalWidget);

$core.addMethod(
$core.method({
selector: "hasButtons",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "hasButtons\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLModalWidget);

$core.addMethod(
$core.method({
selector: "remove",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "remove\x0a\x09'.dialog' asJQuery removeClass: 'active'.\x0a\x09HLModalWidget current: nil.\x0a\x09'#overlay' asJQuery remove.\x0a\x09[ \x0a\x09\x09wrapper asJQuery remove\x0a\x09] valueWithTimeout: 300",
referencedClasses: ["HLModalWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["removeClass:", "asJQuery", "current:", "remove", "valueWithTimeout:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv([".dialog"._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._removeClass_("active");
$recv($globals.HLModalWidget)._current_(nil);
[$recv(["#overlay"._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=2
//>>excludeEnd("ctx");
][0])._remove()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["remove"]=1
//>>excludeEnd("ctx");
][0];
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv($self.wrapper)._asJQuery())._remove();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._valueWithTimeout_((300));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"remove",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLModalWidget);

$core.addMethod(
$core.method({
selector: "renderButtonsOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderButtonsOn: html",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (html){
var self=this,$self=this;
return self;

}; }),
$globals.HLModalWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09| confirmButton |\x0a\x09\x0a\x09html div id: 'overlay'.\x0a\x09\x0a\x09html div \x0a\x09\x09class: 'dialog ', self cssClass;\x0a\x09\x09with: [\x0a\x09\x09\x09self renderMainOn: html.\x0a\x09\x09\x09self hasButtons ifTrue: [ \x0a\x09\x09\x09\x09self renderButtonsOn: html ] ].\x0a\x0a\x09'.dialog' asJQuery addClass: 'active'.\x0a\x09self setupKeyBindings",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["id:", "div", "class:", ",", "cssClass", "with:", "renderMainOn:", "ifTrue:", "hasButtons", "renderButtonsOn:", "addClass:", "asJQuery", "setupKeyBindings"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
var confirmButton;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$recv([$recv(html)._div()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["div"]=1
//>>excludeEnd("ctx");
][0])._id_("overlay");
$1=$recv(html)._div();
$recv($1)._class_("dialog ".__comma($self._cssClass()));
$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$self._renderMainOn_(html);
if($core.assert($self._hasButtons())){
return $self._renderButtonsOn_(html);
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
$recv(".dialog"._asJQuery())._addClass_("active");
$self._setupKeyBindings();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html,confirmButton:confirmButton})});
//>>excludeEnd("ctx");
}; }),
$globals.HLModalWidget);

$core.addMethod(
$core.method({
selector: "renderMainOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderMainOn: html",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (html){
var self=this,$self=this;
return self;

}; }),
$globals.HLModalWidget);

$core.addMethod(
$core.method({
selector: "setupKeyBindings",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setupKeyBindings\x0a\x09'.dialog' asJQuery keyup: [ :e |\x0a\x09\x09e keyCode = String esc asciiValue ifTrue: [ self cancel ] ]",
referencedClasses: ["String"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["keyup:", "asJQuery", "ifTrue:", "=", "keyCode", "asciiValue", "esc", "cancel"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(".dialog"._asJQuery())._keyup_((function(e){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if($core.assert($recv($recv(e)._keyCode()).__eq($recv($recv($globals.String)._esc())._asciiValue()))){
return $self._cancel();
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({e:e},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setupKeyBindings",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLModalWidget);

$core.addMethod(
$core.method({
selector: "show",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "show\x0a\x09HLModalWidget current ifNotNil: [ :old | old remove ].\x0a\x09HLModalWidget current: self.\x0a\x09self appendToJQuery: 'body' asJQuery",
referencedClasses: ["HLModalWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "current", "remove", "current:", "appendToJQuery:", "asJQuery"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLModalWidget)._current();
if($1 == null || $1.a$nil){
$1;
} else {
var old;
old=$1;
$recv(old)._remove();
}
$recv($globals.HLModalWidget)._current_(self);
$self._appendToJQuery_("body"._asJQuery());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"show",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLModalWidget);


$core.setSlots($globals.HLModalWidget.a$cls, ["current"]);
$core.addMethod(
$core.method({
selector: "current",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "current\x0a\x09^ current",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.current;

}; }),
$globals.HLModalWidget.a$cls);

$core.addMethod(
$core.method({
selector: "current:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anHLModelWidget"],
source: "current: anHLModelWidget\x0a\x09current := anHLModelWidget",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anHLModelWidget){
var self=this,$self=this;
$self.current=anHLModelWidget;
return self;

}; }),
$globals.HLModalWidget.a$cls);


$core.addClass("HLConfirmationWidget", $globals.HLModalWidget, "Helios-Core");
$core.setSlots($globals.HLConfirmationWidget, ["cancelButtonLabel", "confirmButtonLabel", "confirmationString", "actionBlock", "cancelBlock"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLConfirmationWidget.comment="I display confirmation dialog. \x0a\x0a## API\x0a\x0aHLWidget contains convenience methods like `HLWidget >> #confirm:ifTrue:` for creating confirmation dialogs.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "actionBlock",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "actionBlock\x0a\x09^ actionBlock ifNil: [ [] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.actionBlock;
if($1 == null || $1.a$nil){
return (function(){

});
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"actionBlock",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLConfirmationWidget);

$core.addMethod(
$core.method({
selector: "actionBlock:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBlock"],
source: "actionBlock: aBlock\x0a\x09actionBlock := aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aBlock){
var self=this,$self=this;
$self.actionBlock=aBlock;
return self;

}; }),
$globals.HLConfirmationWidget);

$core.addMethod(
$core.method({
selector: "cancel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "cancel\x0a\x09self cancelBlock value.\x0a\x09self remove",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["value", "cancelBlock", "remove"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._cancelBlock())._value();
$self._remove();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"cancel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLConfirmationWidget);

$core.addMethod(
$core.method({
selector: "cancelBlock",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "cancelBlock\x0a\x09^ cancelBlock ifNil: [ [] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.cancelBlock;
if($1 == null || $1.a$nil){
return (function(){

});
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"cancelBlock",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLConfirmationWidget);

$core.addMethod(
$core.method({
selector: "cancelBlock:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBlock"],
source: "cancelBlock: aBlock\x0a\x09cancelBlock := aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aBlock){
var self=this,$self=this;
$self.cancelBlock=aBlock;
return self;

}; }),
$globals.HLConfirmationWidget);

$core.addMethod(
$core.method({
selector: "cancelButtonLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "cancelButtonLabel\x0a\x09^ cancelButtonLabel ifNil: [ 'Cancel' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.cancelButtonLabel;
if($1 == null || $1.a$nil){
return "Cancel";
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"cancelButtonLabel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLConfirmationWidget);

$core.addMethod(
$core.method({
selector: "cancelButtonLabel:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "cancelButtonLabel: aString\x0a\x09^ cancelButtonLabel := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.cancelButtonLabel=aString;
return $self.cancelButtonLabel;

}; }),
$globals.HLConfirmationWidget);

$core.addMethod(
$core.method({
selector: "confirm",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "confirm\x0a\x09self remove.\x0a\x09self actionBlock value",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["remove", "value", "actionBlock"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._remove();
$recv($self._actionBlock())._value();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"confirm",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLConfirmationWidget);

$core.addMethod(
$core.method({
selector: "confirmButtonLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "confirmButtonLabel\x0a\x09^ confirmButtonLabel ifNil: [ 'Confirm' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.confirmButtonLabel;
if($1 == null || $1.a$nil){
return "Confirm";
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"confirmButtonLabel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLConfirmationWidget);

$core.addMethod(
$core.method({
selector: "confirmButtonLabel:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "confirmButtonLabel: aString\x0a\x09^ confirmButtonLabel := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.confirmButtonLabel=aString;
return $self.confirmButtonLabel;

}; }),
$globals.HLConfirmationWidget);

$core.addMethod(
$core.method({
selector: "confirmationString",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "confirmationString\x0a\x09^ confirmationString ifNil: [ 'Confirm' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.confirmationString;
if($1 == null || $1.a$nil){
return "Confirm";
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"confirmationString",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLConfirmationWidget);

$core.addMethod(
$core.method({
selector: "confirmationString:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "confirmationString: aString\x0a\x09confirmationString := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.confirmationString=aString;
return self;

}; }),
$globals.HLConfirmationWidget);

$core.addMethod(
$core.method({
selector: "renderButtonsOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderButtonsOn: html\x0a\x09| confirmButton |\x0a\x09\x0a\x09html div \x0a\x09\x09class: 'buttons';\x0a\x09\x09with: [\x0a\x09\x09\x09html button\x0a\x09\x09\x09\x09class: 'button';\x0a\x09\x09\x09\x09with: self cancelButtonLabel;\x0a\x09\x09\x09\x09onClick: [ self cancel ].\x0a\x09\x09\x09confirmButton := html button\x0a\x09\x09\x09\x09class: 'button default';\x0a\x09\x09\x09\x09with: self confirmButtonLabel;\x0a\x09\x09\x09\x09onClick: [ self confirm ] ].\x0a\x0a\x09self giveFocusToButton:confirmButton",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "with:", "button", "cancelButtonLabel", "onClick:", "cancel", "confirmButtonLabel", "confirm", "giveFocusToButton:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
var confirmButton;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3;
$1=$recv(html)._div();
[$recv($1)._class_("buttons")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$2=[$recv(html)._button()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["button"]=1
//>>excludeEnd("ctx");
][0];
[$recv($2)._class_("button")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["class:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($2)._with_($self._cancelButtonLabel())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($2)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._cancel();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["onClick:"]=1
//>>excludeEnd("ctx");
][0];
$3=$recv(html)._button();
$recv($3)._class_("button default");
$recv($3)._with_($self._confirmButtonLabel());
confirmButton=$recv($3)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._confirm();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,3)});
//>>excludeEnd("ctx");
}));
return confirmButton;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
$self._giveFocusToButton_(confirmButton);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderButtonsOn:",{html:html,confirmButton:confirmButton})});
//>>excludeEnd("ctx");
}; }),
$globals.HLConfirmationWidget);

$core.addMethod(
$core.method({
selector: "renderMainOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderMainOn: html\x0a\x09html span \x0a\x09\x09class: 'head'; \x0a\x09\x09with: self confirmationString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "span", "with:", "confirmationString"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._span();
$recv($1)._class_("head");
$recv($1)._with_($self._confirmationString());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderMainOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLConfirmationWidget);



$core.addClass("HLRequestWidget", $globals.HLConfirmationWidget, "Helios-Core");
$core.setSlots($globals.HLRequestWidget, ["input", "multiline", "value"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLRequestWidget.comment="I display a modal window requesting user input.\x0a\x0a## API\x0a\x0a`HLWidget >> #request:do:` and `#request:value:do:` are convenience methods for creating modal request dialogs.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "beMultiline",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "beMultiline\x0a\x09multiline := true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
$self.multiline=true;
return self;

}; }),
$globals.HLRequestWidget);

$core.addMethod(
$core.method({
selector: "beSingleline",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "beSingleline\x0a\x09multiline := false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
$self.multiline=false;
return self;

}; }),
$globals.HLRequestWidget);

$core.addMethod(
$core.method({
selector: "confirm",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "confirm\x0a\x09| val |\x0a\x09val := input asJQuery val.\x0a\x09self remove.\x0a\x09self actionBlock value: val",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["val", "asJQuery", "remove", "value:", "actionBlock"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var val;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
val=$recv($recv($self.input)._asJQuery())._val();
$self._remove();
$recv($self._actionBlock())._value_(val);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"confirm",{val:val})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRequestWidget);

$core.addMethod(
$core.method({
selector: "cssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "cssClass\x0a\x09^ 'large'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "large";

}; }),
$globals.HLRequestWidget);

$core.addMethod(
$core.method({
selector: "giveFocusToButton:",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aButton"],
source: "giveFocusToButton: aButton",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aButton){
var self=this,$self=this;
return self;

}; }),
$globals.HLRequestWidget);

$core.addMethod(
$core.method({
selector: "isMultiline",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isMultiline\x0a\x09^ multiline ifNil: [ true ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.multiline;
if($1 == null || $1.a$nil){
return true;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isMultiline",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRequestWidget);

$core.addMethod(
$core.method({
selector: "renderMainOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderMainOn: html\x0a\x09super renderMainOn: html.\x0a\x09self isMultiline\x0a\x09\x09ifTrue: [ input := html textarea ]\x0a\x09\x09ifFalse: [ input := html input \x0a\x09\x09\x09type: 'text';\x0a\x09\x09\x09onKeyDown: [ :event |\x0a\x09\x09\x09\x09event keyCode = 13 ifTrue: [\x0a\x09\x09\x09\x09\x09self confirm ] ];\x0a\x09\x09\x09yourself ].\x0a\x09input asJQuery \x0a\x09\x09val: self value;\x0a\x09\x09focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["renderMainOn:", "ifTrue:ifFalse:", "isMultiline", "textarea", "type:", "input", "onKeyDown:", "ifTrue:", "=", "keyCode", "confirm", "yourself", "val:", "asJQuery", "value", "focus"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._renderMainOn_.call($self,html))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
if($core.assert($self._isMultiline())){
$self.input=$recv(html)._textarea();
$self.input;
} else {
$1=$recv(html)._input();
$recv($1)._type_("text");
$recv($1)._onKeyDown_((function(event){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if($core.assert($recv($recv(event)._keyCode()).__eq((13)))){
return $self._confirm();
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({event:event},$ctx1,3)});
//>>excludeEnd("ctx");
}));
$self.input=$recv($1)._yourself();
$self.input;
}
$2=$recv($self.input)._asJQuery();
$recv($2)._val_($self._value());
$recv($2)._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderMainOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRequestWidget);

$core.addMethod(
$core.method({
selector: "value",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "value\x0a\x09^ value ifNil: [ '' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.value;
if($1 == null || $1.a$nil){
return "";
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"value",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRequestWidget);

$core.addMethod(
$core.method({
selector: "value:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "value: aString\x0a\x09value := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.value=aString;
return self;

}; }),
$globals.HLRequestWidget);



$core.addClass("HLProgressWidget", $globals.HLModalWidget, "Helios-Core");
$core.setSlots($globals.HLProgressWidget, ["progressBars", "visible"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLProgressWidget.comment="I am a widget used to display progress modal dialogs.\x0a\x0aMy default instance is accessed with `HLProgressWidget class >> #default`.\x0a\x0aSee `HLProgressHandler` for usage.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "addProgressBar:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aProgressBar"],
source: "addProgressBar: aProgressBar\x0a\x09self show.\x0a\x09self progressBars add: aProgressBar.\x0a\x09aProgressBar appendToJQuery: (self wrapper asJQuery find: '.dialog')",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["show", "add:", "progressBars", "appendToJQuery:", "find:", "asJQuery", "wrapper"]
}, function ($methodClass){ return function (aProgressBar){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._show();
$recv($self._progressBars())._add_(aProgressBar);
$recv(aProgressBar)._appendToJQuery_($recv($recv($self._wrapper())._asJQuery())._find_(".dialog"));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"addProgressBar:",{aProgressBar:aProgressBar})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProgressWidget);

$core.addMethod(
$core.method({
selector: "do:on:displaying:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBlock", "aCollection", "aString"],
source: "do: aBlock on: aCollection displaying: aString\x0a\x09| progressBar |\x0a\x09\x0a\x09progressBar := HLProgressBarWidget new\x0a\x09\x09parent: self;\x0a\x09\x09label: aString;\x0a\x09\x09workBlock: aBlock;\x0a\x09\x09collection: aCollection;\x0a\x09\x09yourself.\x0a\x09\x0a\x09self addProgressBar: progressBar.\x0a\x09progressBar start",
referencedClasses: ["HLProgressBarWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["parent:", "new", "label:", "workBlock:", "collection:", "yourself", "addProgressBar:", "start"]
}, function ($methodClass){ return function (aBlock,aCollection,aString){
var self=this,$self=this;
var progressBar;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLProgressBarWidget)._new();
$recv($1)._parent_(self);
$recv($1)._label_(aString);
$recv($1)._workBlock_(aBlock);
$recv($1)._collection_(aCollection);
progressBar=$recv($1)._yourself();
$self._addProgressBar_(progressBar);
$recv(progressBar)._start();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"do:on:displaying:",{aBlock:aBlock,aCollection:aCollection,aString:aString,progressBar:progressBar})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProgressWidget);

$core.addMethod(
$core.method({
selector: "flush",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "flush\x0a\x09self progressBars do: [ :each |\x0a\x09\x09self removeProgressBar: each ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "progressBars", "removeProgressBar:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._progressBars())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._removeProgressBar_(each);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"flush",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProgressWidget);

$core.addMethod(
$core.method({
selector: "hasButtons",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "hasButtons\x0a\x09^ false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return false;

}; }),
$globals.HLProgressWidget);

$core.addMethod(
$core.method({
selector: "isVisible",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isVisible\x0a\x09^ visible ifNil: [ false ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.visible;
if($1 == null || $1.a$nil){
return false;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isVisible",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProgressWidget);

$core.addMethod(
$core.method({
selector: "progressBars",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "progressBars\x0a\x09^ progressBars ifNil: [ progressBars := OrderedCollection new ]",
referencedClasses: ["OrderedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.progressBars;
if($1 == null || $1.a$nil){
$self.progressBars=$recv($globals.OrderedCollection)._new();
return $self.progressBars;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"progressBars",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProgressWidget);

$core.addMethod(
$core.method({
selector: "remove",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "remove\x0a\x09self isVisible ifTrue: [\x0a\x09\x09visible := false.\x0a\x09\x09super remove ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "isVisible", "remove"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($self._isVisible())){
$self.visible=false;
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._remove.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"remove",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProgressWidget);

$core.addMethod(
$core.method({
selector: "removeProgressBar:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aProgressBar"],
source: "removeProgressBar: aProgressBar\x0a\x09self progressBars remove: aProgressBar ifAbsent: [].\x0a\x09aProgressBar wrapper asJQuery remove.\x0a\x09\x0a\x09self progressBars ifEmpty: [ self remove ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["remove:ifAbsent:", "progressBars", "remove", "asJQuery", "wrapper", "ifEmpty:"]
}, function ($methodClass){ return function (aProgressBar){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv([$self._progressBars()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["progressBars"]=1
//>>excludeEnd("ctx");
][0])._remove_ifAbsent_(aProgressBar,(function(){

}));
[$recv($recv($recv(aProgressBar)._wrapper())._asJQuery())._remove()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["remove"]=1
//>>excludeEnd("ctx");
][0];
$recv($self._progressBars())._ifEmpty_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._remove();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"removeProgressBar:",{aProgressBar:aProgressBar})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProgressWidget);

$core.addMethod(
$core.method({
selector: "renderMainOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderMainOn: html\x0a\x09self progressBars do: [ :each |\x0a\x09\x09html with: each ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "progressBars", "with:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._progressBars())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(html)._with_(each);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderMainOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProgressWidget);

$core.addMethod(
$core.method({
selector: "show",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "show\x0a\x09self isVisible ifFalse: [\x0a\x09\x09visible := true.\x0a\x09\x09super show ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifFalse:", "isVisible", "show"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if(!$core.assert($self._isVisible())){
$self.visible=true;
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._show.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"show",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProgressWidget);


$core.setSlots($globals.HLProgressWidget.a$cls, ["default"]);
$core.addMethod(
$core.method({
selector: "default",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "default\x0a\x09^ default ifNil: [ default := self new ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.default;
if($1 == null || $1.a$nil){
$self.default=$self._new();
return $self.default;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"default",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProgressWidget.a$cls);


$core.addClass("HLTabSelectionWidget", $globals.HLModalWidget, "Helios-Core");
$core.setSlots($globals.HLTabSelectionWidget, ["tabs", "tabList", "selectedTab", "selectCallback", "cancelCallback", "confirmCallback"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLTabSelectionWidget.comment="I am a modal window used to select or create tabs.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "cancel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "cancel\x0a\x09self remove.\x0a\x09self cancelCallback value",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["remove", "value", "cancelCallback"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._remove();
$recv($self._cancelCallback())._value();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"cancel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "cancelCallback",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "cancelCallback\x0a\x09^ cancelCallback ifNil: [ [] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.cancelCallback;
if($1 == null || $1.a$nil){
return (function(){

});
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"cancelCallback",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "cancelCallback:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBlock"],
source: "cancelCallback: aBlock\x0a\x09cancelCallback := aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aBlock){
var self=this,$self=this;
$self.cancelCallback=aBlock;
return self;

}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "confirm",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "confirm\x0a\x09self remove.\x0a\x09self confirmCallback value: self selectedTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["remove", "value:", "confirmCallback", "selectedTab"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._remove();
$recv($self._confirmCallback())._value_($self._selectedTab());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"confirm",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "confirmCallback",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "confirmCallback\x0a\x09^ confirmCallback ifNil: [ [] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.confirmCallback;
if($1 == null || $1.a$nil){
return (function(){

});
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"confirmCallback",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "confirmCallback:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBlock"],
source: "confirmCallback: aBlock\x0a\x09confirmCallback := aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aBlock){
var self=this,$self=this;
$self.confirmCallback=aBlock;
return self;

}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "renderButtonsOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderButtonsOn: html\x0a\x09| confirmButton |\x0a\x09\x0a\x09html div \x0a\x09\x09class: 'buttons';\x0a\x09\x09with: [\x0a\x09\x09\x09html button\x0a\x09\x09\x09\x09class: 'button';\x0a\x09\x09\x09\x09with: 'Cancel';\x0a\x09\x09\x09\x09onClick: [ self cancel ].\x0a\x09\x09\x09confirmButton := html button\x0a\x09\x09\x09\x09class: 'button default';\x0a\x09\x09\x09\x09with: 'Select tab';\x0a\x09\x09\x09\x09onClick: [ self confirm ] ].\x0a\x0a\x09self giveFocusToButton:confirmButton",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "with:", "button", "onClick:", "cancel", "confirm", "giveFocusToButton:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
var confirmButton;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3;
$1=$recv(html)._div();
[$recv($1)._class_("buttons")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$2=[$recv(html)._button()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["button"]=1
//>>excludeEnd("ctx");
][0];
[$recv($2)._class_("button")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["class:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($2)._with_("Cancel")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($2)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._cancel();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["onClick:"]=1
//>>excludeEnd("ctx");
][0];
$3=$recv(html)._button();
$recv($3)._class_("button default");
$recv($3)._with_("Select tab");
confirmButton=$recv($3)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._confirm();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,3)});
//>>excludeEnd("ctx");
}));
return confirmButton;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
$self._giveFocusToButton_(confirmButton);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderButtonsOn:",{html:html,confirmButton:confirmButton})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09super renderContentOn: html.\x0a\x09self tabList focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["renderContentOn:", "focus", "tabList"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._renderContentOn_.call($self,html))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv($self._tabList())._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "renderMainOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderMainOn: html\x0a\x09html div \x0a\x09\x09class: 'title'; \x0a\x09\x09with: 'Tab selection'.\x0a\x09\x0a\x09html with: self tabList",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "with:", "tabList"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._div();
$recv($1)._class_("title");
[$recv($1)._with_("Tab selection")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
$recv(html)._with_($self._tabList());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderMainOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "renderTab:on:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTab", "html"],
source: "renderTab: aTab on: html\x0a\x09html \x0a\x09\x09span \x0a\x09\x09\x09class: aTab cssClass;\x0a\x09\x09\x09with: aTab label",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "span", "cssClass", "with:", "label"]
}, function ($methodClass){ return function (aTab,html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._span();
$recv($1)._class_($recv(aTab)._cssClass());
$recv($1)._with_($recv(aTab)._label());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderTab:on:",{aTab:aTab,html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "renderTabsOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderTabsOn: html\x0a\x09self tabs do: [ :each |\x0a\x09\x09html li with: [ \x0a\x09\x09\x09html a \x0a\x09\x09\x09\x09with: [ \x0a\x09\x09\x09\x09\x09self renderTab: each on: html ];\x0a\x09\x09\x09\x09onClick: [ self selectTab: each ] ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "tabs", "with:", "li", "a", "renderTab:on:", "onClick:", "selectTab:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$recv($self._tabs())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return [$recv($recv(html)._li())._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
$1=$recv(html)._a();
$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx4) {
//>>excludeEnd("ctx");
return $self._renderTab_on_(each,html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx4) {$ctx4.fillBlock({},$ctx3,3)});
//>>excludeEnd("ctx");
}));
return $recv($1)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx4) {
//>>excludeEnd("ctx");
return $self._selectTab_(each);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx4) {$ctx4.fillBlock({},$ctx3,4)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderTabsOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "selectCallback",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectCallback\x0a\x09^ selectCallback ifNil: [ [] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.selectCallback;
if($1 == null || $1.a$nil){
return (function(){

});
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectCallback",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "selectCallback:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBlock"],
source: "selectCallback: aBlock\x0a\x09selectCallback := aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aBlock){
var self=this,$self=this;
$self.selectCallback=aBlock;
return self;

}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "selectTab:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTab"],
source: "selectTab: aTab\x0a\x09self selectedTab: aTab.\x0a\x09self selectCallback value: aTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectedTab:", "value:", "selectCallback"]
}, function ($methodClass){ return function (aTab){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._selectedTab_(aTab);
$recv($self._selectCallback())._value_(aTab);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectTab:",{aTab:aTab})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "selectedTab",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectedTab\x0a\x09^ selectedTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.selectedTab;

}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "selectedTab:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTab"],
source: "selectedTab: aTab\x0a\x09selectedTab := aTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aTab){
var self=this,$self=this;
$self.selectedTab=aTab;
return self;

}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "setupKeyBindings",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setupKeyBindings\x0a\x09super setupKeyBindings.\x0a\x09'.dialog' asJQuery keyup: [ :e |\x0a\x09\x09e keyCode = String cr asciiValue ifTrue: [ self confirm ] ]",
referencedClasses: ["String"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["setupKeyBindings", "keyup:", "asJQuery", "ifTrue:", "=", "keyCode", "asciiValue", "cr", "confirm"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._setupKeyBindings.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv(".dialog"._asJQuery())._keyup_((function(e){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if($core.assert($recv($recv(e)._keyCode()).__eq($recv($recv($globals.String)._cr())._asciiValue()))){
return $self._confirm();
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({e:e},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setupKeyBindings",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "tabList",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabList\x0a\x09tabList ifNil: [ \x0a\x09\x09tabList := HLTabListWidget new.\x0a\x09\x09tabList\x0a\x09\x09\x09callback: [ :tab | self selectTab: tab. tabList focus ];\x0a\x09\x09\x09selectedItem: self selectedTab;\x0a\x09\x09\x09items: self tabs ].\x0a\x09\x0a\x09^ tabList",
referencedClasses: ["HLTabListWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new", "callback:", "selectTab:", "focus", "selectedItem:", "selectedTab", "items:", "tabs"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$self.tabList;
if($1 == null || $1.a$nil){
$self.tabList=$recv($globals.HLTabListWidget)._new();
$2=$self.tabList;
$recv($2)._callback_((function(tab){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$self._selectTab_(tab);
return $recv($self.tabList)._focus();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({tab:tab},$ctx1,2)});
//>>excludeEnd("ctx");
}));
$recv($2)._selectedItem_($self._selectedTab());
$recv($2)._items_($self._tabs());
} else {
$1;
}
return $self.tabList;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"tabList",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "tabs",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabs\x0a\x09^ tabs ifNil: [ #() ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.tabs;
if($1 == null || $1.a$nil){
return [];
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"tabs",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabSelectionWidget);

$core.addMethod(
$core.method({
selector: "tabs:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection"],
source: "tabs: aCollection\x0a\x09tabs := aCollection",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aCollection){
var self=this,$self=this;
$self.tabs=aCollection;
return self;

}; }),
$globals.HLTabSelectionWidget);



$core.addClass("HLProgressBarWidget", $globals.HLWidget, "Helios-Core");
$core.setSlots($globals.HLProgressBarWidget, ["label", "parent", "workBlock", "collection", "bar"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLProgressBarWidget.comment="I am a widget used to display a progress bar while iterating over a collection.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "collection",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "collection\x0a\x09^ collection",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.collection;

}; }),
$globals.HLProgressBarWidget);

$core.addMethod(
$core.method({
selector: "collection:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection"],
source: "collection: aCollection\x0a\x09collection := aCollection",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aCollection){
var self=this,$self=this;
$self.collection=aCollection;
return self;

}; }),
$globals.HLProgressBarWidget);

$core.addMethod(
$core.method({
selector: "evaluateAt:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInteger"],
source: "evaluateAt: anInteger\x0a\x09self updateProgress: ((anInteger / self collection size) * 100) rounded.\x0a\x09anInteger <= self collection size\x0a\x09\x09ifTrue: [ \x0a\x09\x09\x09[ \x0a\x09\x09\x09\x09self workBlock value: (self collection at: anInteger).\x0a\x09\x09\x09\x09self evaluateAt: anInteger + 1 ] fork ]\x0a\x09\x09ifFalse: [ [ self remove ] valueWithTimeout: 500 ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["updateProgress:", "rounded", "*", "/", "size", "collection", "ifTrue:ifFalse:", "<=", "fork", "value:", "workBlock", "at:", "evaluateAt:", "+", "valueWithTimeout:", "remove"]
}, function ($methodClass){ return function (anInteger){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._updateProgress_($recv($recv($recv(anInteger).__slash([$recv([$self._collection()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["collection"]=1
//>>excludeEnd("ctx");
][0])._size()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["size"]=1
//>>excludeEnd("ctx");
][0])).__star((100)))._rounded());
if($core.assert($recv(anInteger).__lt_eq($recv([$self._collection()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["collection"]=2
//>>excludeEnd("ctx");
][0])._size()))){
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$recv($self._workBlock())._value_($recv($self._collection())._at_(anInteger));
return $self._evaluateAt_($recv(anInteger).__plus((1)));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}))._fork();
} else {
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._remove();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,4)});
//>>excludeEnd("ctx");
}))._valueWithTimeout_((500));
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"evaluateAt:",{anInteger:anInteger})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProgressBarWidget);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ label",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.label;

}; }),
$globals.HLProgressBarWidget);

$core.addMethod(
$core.method({
selector: "label:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "label: aString\x0a\x09label := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.label=aString;
return self;

}; }),
$globals.HLProgressBarWidget);

$core.addMethod(
$core.method({
selector: "parent",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "parent\x0a\x09^ parent",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.parent;

}; }),
$globals.HLProgressBarWidget);

$core.addMethod(
$core.method({
selector: "parent:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aProgress"],
source: "parent: aProgress\x0a\x09parent := aProgress",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aProgress){
var self=this,$self=this;
$self.parent=aProgress;
return self;

}; }),
$globals.HLProgressBarWidget);

$core.addMethod(
$core.method({
selector: "remove",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "remove\x0a\x09self parent removeProgressBar: self",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["removeProgressBar:", "parent"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._parent())._removeProgressBar_(self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"remove",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProgressBarWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09html span with: self label.\x0a\x09html div \x0a\x09\x09class: 'progress';\x0a\x09\x09with: [\x0a\x09\x09\x09bar := html div \x0a\x09\x09\x09\x09class: 'progress-bar';\x0a\x09\x09\x09\x09style: 'width: 0%' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "span", "label", "class:", "div", "style:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
[$recv($recv(html)._span())._with_($self._label())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
$1=[$recv(html)._div()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["div"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._class_("progress")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$2=$recv(html)._div();
$recv($2)._class_("progress-bar");
$self.bar=$recv($2)._style_("width: 0%");
return $self.bar;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProgressBarWidget);

$core.addMethod(
$core.method({
selector: "start",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "start\x0a\x09\x22Make sure the UI has some time to update itself between each iteration\x22\x0a\x09\x0a\x09self evaluateAt: 1",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["evaluateAt:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._evaluateAt_((1));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"start",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProgressBarWidget);

$core.addMethod(
$core.method({
selector: "updateProgress:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInteger"],
source: "updateProgress: anInteger\x0a\x09bar asJQuery css: 'width' put: anInteger asString, '%'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["css:put:", "asJQuery", ",", "asString"]
}, function ($methodClass){ return function (anInteger){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self.bar)._asJQuery())._css_put_("width",$recv($recv(anInteger)._asString()).__comma("%"));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"updateProgress:",{anInteger:anInteger})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProgressBarWidget);

$core.addMethod(
$core.method({
selector: "workBlock",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "workBlock\x0a\x09^ workBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.workBlock;

}; }),
$globals.HLProgressBarWidget);

$core.addMethod(
$core.method({
selector: "workBlock:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBlock"],
source: "workBlock: aBlock\x0a\x09workBlock := aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aBlock){
var self=this,$self=this;
$self.workBlock=aBlock;
return self;

}; }),
$globals.HLProgressBarWidget);


$core.setSlots($globals.HLProgressBarWidget.a$cls, ["default"]);
$core.addMethod(
$core.method({
selector: "default",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "default\x0a\x09^ default ifNil: [ default := self new ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.default;
if($1 == null || $1.a$nil){
$self.default=$self._new();
return $self.default;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"default",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProgressBarWidget.a$cls);


$core.addClass("HLSpotlightWidget", $globals.HLWidget, "Helios-Core");
$core.setSlots($globals.HLSpotlightWidget, ["input"]);
$core.addMethod(
$core.method({
selector: "findMatches:andRender:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aQueryString", "aRenderCallback"],
source: "findMatches: aQueryString andRender: aRenderCallback\x0a\x09| matches |\x0a\x09matches := self inputCompletion select: [ :each | each match: aQueryString ].\x0a\x09aRenderCallback value: matches",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["select:", "inputCompletion", "match:", "value:"]
}, function ($methodClass){ return function (aQueryString,aRenderCallback){
var self=this,$self=this;
var matches;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
matches=$recv($self._inputCompletion())._select_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._match_(aQueryString);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
$recv(aRenderCallback)._value_(matches);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"findMatches:andRender:",{aQueryString:aQueryString,aRenderCallback:aRenderCallback,matches:matches})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSpotlightWidget);

$core.addMethod(
$core.method({
selector: "ghostText",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "ghostText\x0a\x09^ 'Search... (Ctrl+F)'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Search... (Ctrl+F)";

}; }),
$globals.HLSpotlightWidget);

$core.addMethod(
$core.method({
selector: "inputCompletion",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inputCompletion\x0a\x09^ self manager environment availableClassNames, self manager environment allSelectors",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: [",", "availableClassNames", "environment", "manager", "allSelectors"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv([$recv([$self._manager()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["manager"]=1
//>>excludeEnd("ctx");
][0])._environment()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["environment"]=1
//>>excludeEnd("ctx");
][0])._availableClassNames()).__comma($recv($recv($self._manager())._environment())._allSelectors());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inputCompletion",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSpotlightWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09input := html input\x0a\x09\x09class: 'spotlight typeahead';\x0a\x09\x09placeholder: self ghostText;\x0a\x09\x09onKeyDown: [ :event | \x0a\x09\x09\x09event which = 13 ifTrue: [\x0a\x09\x09\x09\x09self search: input asJQuery val ] ]\x0a\x09\x09yourself.\x0a\x09\x09\x09\x0a\x09input asJQuery \x0a\x09\x09typeahead: #{ 'hint' -> true }\x0a\x09\x09value: #{ 'name' -> 'classesAndSelectors'.\x0a\x09\x09\x09'displayKey' -> [ :suggestion | suggestion asString ].\x0a\x09\x09\x09'source' -> [ :query :callback | self findMatches: query andRender: callback ]}.\x0a\x09\x09\x22use additional datasets for grouping into classes and selectors\x22",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "input", "placeholder:", "ghostText", "onKeyDown:", "yourself", "ifTrue:", "=", "which", "search:", "val", "asJQuery", "typeahead:value:", "asString", "findMatches:andRender:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._input();
$recv($1)._class_("spotlight typeahead");
$recv($1)._placeholder_($self._ghostText());
$self.input=$recv($1)._onKeyDown_($recv((function(event){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if($core.assert($recv($recv(event)._which()).__eq((13)))){
return $self._search_($recv([$recv($self.input)._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0])._val());
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({event:event},$ctx1,1)});
//>>excludeEnd("ctx");
}))._yourself());
$recv($recv($self.input)._asJQuery())._typeahead_value_($globals.HashedCollection._newFromPairs_(["hint",true]),$globals.HashedCollection._newFromPairs_(["name","classesAndSelectors","displayKey",(function(suggestion){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(suggestion)._asString();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({suggestion:suggestion},$ctx1,3)});
//>>excludeEnd("ctx");
}),"source",(function(query,callback){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._findMatches_andRender_(query,callback);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({query:query,callback:callback},$ctx1,4)});
//>>excludeEnd("ctx");
})]));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSpotlightWidget);

$core.addMethod(
$core.method({
selector: "search:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "search: aString\x0a\x09\x22open a new Browser pointing to aString\x22\x0a\x09aString ifNotEmpty: [\x0a\x09\x09Finder findString: aString ]",
referencedClasses: ["Finder"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotEmpty:", "findString:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(aString)._ifNotEmpty_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($globals.Finder)._findString_(aString);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"search:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSpotlightWidget);



$core.addClass("HLTabWidget", $globals.HLWidget, "Helios-Core");
$core.setSlots($globals.HLTabWidget, ["widget", "label", "root"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLTabWidget.comment="I am a widget specialized into building another widget as an Helios tab.\x0a\x0aI should not be used directly, `HLWidget class >> #openAsTab` should be used instead.\x0a\x0a## Example\x0a\x0a    HLWorkspace openAsTab";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "activate",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "activate\x0a\x09self manager activate: self",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["activate:", "manager"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._manager())._activate_(self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activate",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "add",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "add\x0a\x09self manager addTab: self.\x0a\x09self observeManager",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["addTab:", "manager", "observeManager"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._manager())._addTab_(self);
$self._observeManager();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"add",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "cssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "cssClass\x0a\x09^ self widget tabClass",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["tabClass", "widget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._widget())._tabClass();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"cssClass",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "focus",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focus\x0a\x09self widget canHaveFocus ifTrue: [\x0a\x09\x09self widget focus ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "canHaveFocus", "widget", "focus"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv([$self._widget()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["widget"]=1
//>>excludeEnd("ctx");
][0])._canHaveFocus())){
$recv($self._widget())._focus();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focus",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "hide",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "hide\x0a\x09root ifNotNil: [ root asJQuery css: 'visibility' put: 'hidden' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "css:put:", "asJQuery"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.root;
if($1 == null || $1.a$nil){
$1;
} else {
$recv($recv($self.root)._asJQuery())._css_put_("visibility","hidden");
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"hide",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ self manager activeTab = self",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["=", "activeTab", "manager"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._manager())._activeTab()).__eq(self);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ label ifNil: [ '' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.label;
if($1 == null || $1.a$nil){
return "";
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"label",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "label:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "label: aString\x0a\x09label := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.label=aString;
return self;

}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "manager",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "manager\x0a\x09^ HLManager current",
referencedClasses: ["HLManager"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["current"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.HLManager)._current();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"manager",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "observeManager",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeManager\x0a\x09self manager announcer \x0a\x09\x09on: HLTabLabelChanged\x0a\x09\x09send: #onTabLabelChanged:\x0a\x09\x09to: self",
referencedClasses: ["HLTabLabelChanged"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "announcer", "manager"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self._manager())._announcer())._on_send_to_($globals.HLTabLabelChanged,"onTabLabelChanged:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeManager",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "onTabLabelChanged:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onTabLabelChanged: anAnnouncement\x0a\x09anAnnouncement widget = self widget ifTrue: [\x0a\x09\x09self label = anAnnouncement label ifFalse: [\x0a\x09\x09\x09self label: anAnnouncement label.\x0a\x09\x09\x09self manager refresh ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "=", "widget", "ifFalse:", "label", "label:", "refresh", "manager"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert([$recv([$recv(anAnnouncement)._widget()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["widget"]=1
//>>excludeEnd("ctx");
][0]).__eq($self._widget())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["="]=1
//>>excludeEnd("ctx");
][0])){
if(!$core.assert($recv([$self._label()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["label"]=1
//>>excludeEnd("ctx");
][0]).__eq([$recv(anAnnouncement)._label()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["label"]=2
//>>excludeEnd("ctx");
][0]))){
$self._label_($recv(anAnnouncement)._label());
$recv($self._manager())._refresh();
}
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onTabLabelChanged:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "registerBindings",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "registerBindings\x0a\x09self widget registerBindings",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["registerBindings", "widget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._widget())._registerBindings();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"registerBindings",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "remove",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "remove\x0a\x09self unregister.\x0a\x09self widget unregister.\x0a\x09root ifNotNil: [ root asJQuery remove ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unregister", "widget", "ifNotNil:", "remove", "asJQuery"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
[$self._unregister()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["unregister"]=1
//>>excludeEnd("ctx");
][0];
$recv($self._widget())._unregister();
$1=$self.root;
if($1 == null || $1.a$nil){
$1;
} else {
$recv($recv($self.root)._asJQuery())._remove();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"remove",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "renderOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderOn: html\x0a\x09root := html div\x0a\x09\x09class: 'tab';\x0a\x09\x09yourself.\x0a\x09self renderTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "yourself", "renderTab"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._div();
$recv($1)._class_("tab");
$self.root=$recv($1)._yourself();
$self._renderTab();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "renderTab",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "renderTab\x0a\x09root contents: [ :html |\x0a\x09\x09html div\x0a\x09\x09\x09class: 'amber_box';\x0a\x09\x09\x09with: [ self widget renderOn: html ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["contents:", "class:", "div", "with:", "renderOn:", "widget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$recv($self.root)._contents_((function(html){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$1=$recv(html)._div();
$recv($1)._class_("amber_box");
return $recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv($self._widget())._renderOn_(html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({html:html},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderTab",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "show",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "show\x0a\x09root\x0a\x09\x09ifNil: [ self appendToJQuery: 'body' asJQuery ]\x0a\x09\x09ifNotNil: [ root asJQuery css: 'visibility' put: 'visible' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:ifNotNil:", "appendToJQuery:", "asJQuery", "css:put:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.root;
if($1 == null || $1.a$nil){
$self._appendToJQuery_(["body"._asJQuery()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["asJQuery"]=1
//>>excludeEnd("ctx");
][0]);
} else {
$recv($recv($self.root)._asJQuery())._css_put_("visibility","visible");
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"show",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "unregister",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "unregister\x0a\x09self manager announcer unsubscribe: self",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unsubscribe:", "announcer", "manager"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self._manager())._announcer())._unsubscribe_(self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unregister",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "widget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "widget\x0a\x09^ widget",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.widget;

}; }),
$globals.HLTabWidget);

$core.addMethod(
$core.method({
selector: "widget:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aWidget"],
source: "widget: aWidget\x0a\x09widget := aWidget",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aWidget){
var self=this,$self=this;
$self.widget=aWidget;
return self;

}; }),
$globals.HLTabWidget);


$core.addMethod(
$core.method({
selector: "on:labelled:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aWidget", "aString"],
source: "on: aWidget labelled: aString\x0a\x09^ self new\x0a\x09\x09widget: aWidget;\x0a\x09\x09label: aString;\x0a\x09\x09yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["widget:", "new", "label:", "yourself"]
}, function ($methodClass){ return function (aWidget,aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._widget_(aWidget);
$recv($1)._label_(aString);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:labelled:",{aWidget:aWidget,aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabWidget.a$cls);


$core.addClass("HLTabsWidget", $globals.HLWidget, "Helios-Core");
$core.setSlots($globals.HLTabsWidget, ["tabs", "activeTab", "history", "selectionDisabled", "spotlight"]);
$core.addMethod(
$core.method({
selector: "activate:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTab"],
source: "activate: aTab\x0a\x09self isSelectionDisabled ifTrue: [ ^ self ].\x0a\x09(self tabs includes: aTab) ifFalse: [ ^ self ].\x0a\x0a\x09self manager keyBinder flushBindings.\x0a\x09aTab registerBindings.\x0a\x09activeTab := aTab.\x0a\x09\x0a\x09self \x0a\x09\x09refresh;\x0a\x09\x09addToHistory: aTab;\x0a\x09\x09show: aTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "isSelectionDisabled", "ifFalse:", "includes:", "tabs", "flushBindings", "keyBinder", "manager", "registerBindings", "refresh", "addToHistory:", "show:"]
}, function ($methodClass){ return function (aTab){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($self._isSelectionDisabled())){
return self;
}
if(!$core.assert($recv($self._tabs())._includes_(aTab))){
return self;
}
$recv($recv($self._manager())._keyBinder())._flushBindings();
$recv(aTab)._registerBindings();
$self.activeTab=aTab;
$self._refresh();
$self._addToHistory_(aTab);
$self._show_(aTab);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activate:",{aTab:aTab})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "activateNextTab",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "activateNextTab\x0a\x09| nextTab |\x0a\x09\x0a\x09self tabs ifEmpty: [ ^ self ].\x0a\x09\x0a\x09nextTab := self tabs \x0a\x09\x09at: (self tabs indexOf: self activeTab) + 1 \x0a\x09\x09ifAbsent: [ self tabs first ].\x0a\x09\x09\x0a\x09self activate: nextTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifEmpty:", "tabs", "at:ifAbsent:", "+", "indexOf:", "activeTab", "first", "activate:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var nextTab;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $early={};
try {
$recv([$self._tabs()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["tabs"]=1
//>>excludeEnd("ctx");
][0])._ifEmpty_((function(){
throw $early=[self];

}));
nextTab=$recv([$self._tabs()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["tabs"]=2
//>>excludeEnd("ctx");
][0])._at_ifAbsent_($recv($recv([$self._tabs()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["tabs"]=3
//>>excludeEnd("ctx");
][0])._indexOf_($self._activeTab())).__plus((1)),(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._tabs())._first();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
$self._activate_(nextTab);
return self;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activateNextTab",{nextTab:nextTab})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "activatePreviousTab",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "activatePreviousTab\x0a\x09| previousTab |\x0a\x09\x0a\x09self tabs ifEmpty: [ ^ self ].\x0a\x09\x0a\x09previousTab := self tabs \x0a\x09\x09at: (self tabs indexOf: self activeTab) - 1 \x0a\x09\x09ifAbsent: [ self tabs last ].\x0a\x09\x09\x0a\x09self activate: previousTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifEmpty:", "tabs", "at:ifAbsent:", "-", "indexOf:", "activeTab", "last", "activate:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var previousTab;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $early={};
try {
$recv([$self._tabs()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["tabs"]=1
//>>excludeEnd("ctx");
][0])._ifEmpty_((function(){
throw $early=[self];

}));
previousTab=$recv([$self._tabs()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["tabs"]=2
//>>excludeEnd("ctx");
][0])._at_ifAbsent_($recv($recv([$self._tabs()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["tabs"]=3
//>>excludeEnd("ctx");
][0])._indexOf_($self._activeTab())).__minus((1)),(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._tabs())._last();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
$self._activate_(previousTab);
return self;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"activatePreviousTab",{previousTab:previousTab})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "activeTab",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "activeTab\x0a\x09^ activeTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.activeTab;

}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "addTab:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTab"],
source: "addTab: aTab\x0a\x09self tabs add: aTab.\x0a    self activate: aTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["add:", "tabs", "activate:"]
}, function ($methodClass){ return function (aTab){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._tabs())._add_(aTab);
$self._activate_(aTab);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"addTab:",{aTab:aTab})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "addToHistory:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTab"],
source: "addToHistory: aTab\x0a\x09self removeFromHistory: aTab.\x0a\x09self history add: aTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["removeFromHistory:", "add:", "history"]
}, function ($methodClass){ return function (aTab){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._removeFromHistory_(aTab);
$recv($self._history())._add_(aTab);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"addToHistory:",{aTab:aTab})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "disableSelection",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "disableSelection\x0a\x09selectionDisabled := true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
$self.selectionDisabled=true;
return self;

}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "enableSelection",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "enableSelection\x0a\x09selectionDisabled := false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
$self.selectionDisabled=false;
return self;

}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "history",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "history\x0a\x09^ history ifNil: [ history := OrderedCollection new ]",
referencedClasses: ["OrderedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.history;
if($1 == null || $1.a$nil){
$self.history=$recv($globals.OrderedCollection)._new();
return $self.history;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"history",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "history:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection"],
source: "history: aCollection\x0a\x09history := aCollection",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aCollection){
var self=this,$self=this;
$self.history=aCollection;
return self;

}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "isSelectionDisabled",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isSelectionDisabled\x0a\x09^ selectionDisabled ifNil: [ false ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.selectionDisabled;
if($1 == null || $1.a$nil){
return false;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isSelectionDisabled",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "removeActiveTab",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "removeActiveTab\x0a\x09self removeTab: self activeTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["removeTab:", "activeTab"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._removeTab_($self._activeTab());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"removeActiveTab",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "removeFromHistory:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTab"],
source: "removeFromHistory: aTab\x0a\x09self history: (self history reject: [ :each | each == aTab ])",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["history:", "reject:", "history", "=="]
}, function ($methodClass){ return function (aTab){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._history_($recv($self._history())._reject_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each).__eq_eq(aTab);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
})));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"removeFromHistory:",{aTab:aTab})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "removeTab:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTab"],
source: "removeTab: aTab\x0a\x09(self tabs includes: aTab) ifFalse: [ ^ self ].\x0a\x0a\x09self removeFromHistory: aTab.\x0a\x09self tabs remove: aTab.\x0a\x09self manager keyBinder flushBindings.\x0a\x09aTab remove.\x0a\x09self refresh.\x0a\x09self history ifNotEmpty: [\x0a\x09\x09self history last activate ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifFalse:", "includes:", "tabs", "removeFromHistory:", "remove:", "flushBindings", "keyBinder", "manager", "remove", "refresh", "ifNotEmpty:", "history", "activate", "last"]
}, function ($methodClass){ return function (aTab){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if(!$core.assert($recv([$self._tabs()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["tabs"]=1
//>>excludeEnd("ctx");
][0])._includes_(aTab))){
return self;
}
$self._removeFromHistory_(aTab);
$recv($self._tabs())._remove_(aTab);
$recv($recv($self._manager())._keyBinder())._flushBindings();
$recv(aTab)._remove();
$self._refresh();
$recv([$self._history()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["history"]=1
//>>excludeEnd("ctx");
][0])._ifNotEmpty_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv($self._history())._last())._activate();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"removeTab:",{aTab:aTab})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "removeTabForWidget:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aWidget"],
source: "removeTabForWidget: aWidget\x0a\x09self removeTab: (self tabs \x0a\x09\x09detect: [ :each | each widget = aWidget ]\x0a\x09\x09ifNone: [ ^ self ])",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["removeTab:", "detect:ifNone:", "tabs", "=", "widget"]
}, function ($methodClass){ return function (aWidget){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $early={};
try {
$self._removeTab_($recv($self._tabs())._detect_ifNone_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv(each)._widget()).__eq(aWidget);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}),(function(){
throw $early=[self];

})));
return self;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"removeTabForWidget:",{aWidget:aWidget})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "renderAddOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderAddOn: html\x0a    html div \x0a    \x09class: 'dropdown new_tab';\x0a        with: [ \x0a\x09\x09\x09html a \x0a        \x09\x09class: 'dropdown-toggle';\x0a           \x09 \x09at: 'data-toggle' put: 'dropdown';\x0a            \x09with: [\x0a  \x09\x09\x09\x09\x09(html tag: 'b') class: 'caret' ].\x0a           html ul \x0a           \x09\x09class: 'dropdown-menu';\x0a                with: [\x0a                  \x09((HLWidget withAllSubclasses\x0a                    \x09select: [ :each | each canBeOpenAsTab ])\x0a                        sorted: [ :a :b | a tabPriority < b tabPriority ])\x0a                        do: [ :each |\x0a  \x09\x09\x09\x09\x09\x09\x09html li with: [\x0a                      \x09\x09\x09html a \x0a                                \x09with: each tabLabel;\x0a      \x09\x09\x09\x09\x09\x09\x09\x09onClick: [ each openAsTab ] ] ] ] ]",
referencedClasses: ["HLWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "with:", "a", "at:put:", "tag:", "ul", "do:", "sorted:", "select:", "withAllSubclasses", "canBeOpenAsTab", "<", "tabPriority", "li", "tabLabel", "onClick:", "openAsTab"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3,$4;
$1=$recv(html)._div();
[$recv($1)._class_("dropdown new_tab")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$2=[$recv(html)._a()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["a"]=1
//>>excludeEnd("ctx");
][0];
[$recv($2)._class_("dropdown-toggle")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["class:"]=2
//>>excludeEnd("ctx");
][0];
$recv($2)._at_put_("data-toggle","dropdown");
[$recv($2)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return [$recv($recv(html)._tag_("b"))._class_("caret")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["class:"]=3
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=2
//>>excludeEnd("ctx");
][0];
$3=$recv(html)._ul();
$recv($3)._class_("dropdown-menu");
return [$recv($3)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv($recv($recv($recv($globals.HLWidget)._withAllSubclasses())._select_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx4) {
//>>excludeEnd("ctx");
return $recv(each)._canBeOpenAsTab();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx4) {$ctx4.fillBlock({each:each},$ctx3,4)});
//>>excludeEnd("ctx");
})))._sorted_((function(a,b){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx4) {
//>>excludeEnd("ctx");
return $recv([$recv(a)._tabPriority()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx4.sendIdx["tabPriority"]=1
//>>excludeEnd("ctx");
][0]).__lt($recv(b)._tabPriority());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx4) {$ctx4.fillBlock({a:a,b:b},$ctx3,5)});
//>>excludeEnd("ctx");
})))._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx4) {
//>>excludeEnd("ctx");
return [$recv($recv(html)._li())._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx5) {
//>>excludeEnd("ctx");
$4=$recv(html)._a();
$recv($4)._with_($recv(each)._tabLabel());
return $recv($4)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx6) {
//>>excludeEnd("ctx");
return $recv(each)._openAsTab();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx6) {$ctx6.fillBlock({},$ctx5,8)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx5) {$ctx5.fillBlock({},$ctx4,7)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx4.sendIdx["with:"]=4
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx4) {$ctx4.fillBlock({each:each},$ctx3,6)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,3)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=3
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderAddOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09html div \x0a\x09\x09class: 'navbar navbar-fixed-top';\x0a\x09\x09with: [ html div \x0a\x09\x09\x09class: 'navbar-header';\x0a\x09\x09\x09at: 'role' put: 'tabpanel';\x0a\x09\x09\x09with: [ self renderTabsOn: html ] ].\x0a\x09\x09\x09\x0a\x09html with: self spotlight.\x0a\x09self renderAddOn: html",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "with:", "at:put:", "renderTabsOn:", "spotlight", "renderAddOn:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=[$recv(html)._div()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["div"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._class_("navbar navbar-fixed-top")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$2=$recv(html)._div();
$recv($2)._class_("navbar-header");
$recv($2)._at_put_("role","tabpanel");
return [$recv($2)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._renderTabsOn_(html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=2
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
$recv(html)._with_($self._spotlight());
$self._renderAddOn_(html);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "renderTab:on:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTab", "html"],
source: "renderTab: aTab on: html\x0a\x09| li |\x0a\x09li := html li \x0a\x09\x09style: 'width: ', self tabWidth asString, 'px';\x0a\x09\x09class: (aTab isActive ifTrue: [ 'tab active' ] ifFalse: [ 'tab inactive' ]);\x0a\x09\x09with: [\x0a\x09\x09\x09html a\x0a\x09\x09\x09with: [\x0a\x09\x09\x09\x09((html tag: 'i') class: 'close')\x0a\x09\x09\x09\x09\x09onClick: [ self removeTab: aTab ].\x0a\x09\x09\x09\x09html span \x0a\x09\x09\x09\x09\x09class: aTab cssClass;\x0a\x09\x09\x09\x09\x09title: aTab label;\x0a\x09\x09\x09\x09\x09with: aTab label ];\x0a\x09\x09\x09at: 'role' put: 'tab'];\x0a\x09\x09onClick: [ aTab activate ].\x0a\x09\x0a\x09li asDomNode at: 'tab-data' put: aTab",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["style:", "li", ",", "asString", "tabWidth", "class:", "ifTrue:ifFalse:", "isActive", "with:", "a", "onClick:", "tag:", "removeTab:", "span", "cssClass", "title:", "label", "at:put:", "activate", "asDomNode"]
}, function ($methodClass){ return function (aTab,html){
var self=this,$self=this;
var li;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3,$4;
$1=$recv(html)._li();
$recv($1)._style_([$recv("width: ".__comma($recv($self._tabWidth())._asString())).__comma("px")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx[","]=1
//>>excludeEnd("ctx");
][0]);
if($core.assert($recv(aTab)._isActive())){
$2="tab active";
} else {
$2="tab inactive";
}
[$recv($1)._class_($2)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$3=$recv(html)._a();
[$recv($3)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
[$recv([$recv($recv(html)._tag_("i"))._class_("close")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["class:"]=2
//>>excludeEnd("ctx");
][0])._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx4) {
//>>excludeEnd("ctx");
return $self._removeTab_(aTab);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx4) {$ctx4.fillBlock({},$ctx3,5)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["onClick:"]=1
//>>excludeEnd("ctx");
][0];
$4=$recv(html)._span();
$recv($4)._class_($recv(aTab)._cssClass());
$recv($4)._title_([$recv(aTab)._label()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["label"]=1
//>>excludeEnd("ctx");
][0]);
return $recv($4)._with_($recv(aTab)._label());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,4)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=2
//>>excludeEnd("ctx");
][0];
return [$recv($3)._at_put_("role","tab")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["at:put:"]=1
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,3)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
li=$recv($1)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(aTab)._activate();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,6)});
//>>excludeEnd("ctx");
}));
$recv($recv(li)._asDomNode())._at_put_("tab-data",aTab);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderTab:on:",{aTab:aTab,html:html,li:li})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "renderTabsOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderTabsOn: html\x0a\x09| ul |\x0a\x09ul := html ul \x0a\x09\x09class: 'nav navbar-nav nav-tabs';\x0a\x09\x09at: 'role' put: 'tablist';\x0a\x09\x09with: [ \x0a        \x09self tabs do: [ :each |\x0a\x09\x09\x09\x09self renderTab: each on: html ] ].\x0a\x09\x09\x0a\x09ul asJQuery sortable: #{\x0a\x09\x09'containment' -> 'parent'.\x0a\x09\x09'start' -> [ self disableSelection ].\x0a\x09\x09'stop' -> [ [ self enableSelection] valueWithTimeout: 300 ].\x0a\x09\x09'update' -> [ self updateTabsOrder ]\x0a\x09}",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "ul", "at:put:", "with:", "do:", "tabs", "renderTab:on:", "sortable:", "asJQuery", "disableSelection", "valueWithTimeout:", "enableSelection", "updateTabsOrder"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
var ul;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._ul();
$recv($1)._class_("nav navbar-nav nav-tabs");
$recv($1)._at_put_("role","tablist");
ul=$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._tabs())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._renderTab_on_(each,html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({each:each},$ctx2,2)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
$recv($recv(ul)._asJQuery())._sortable_($globals.HashedCollection._newFromPairs_(["containment","parent","start",(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._disableSelection();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,3)});
//>>excludeEnd("ctx");
}),"stop",(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._enableSelection();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,5)});
//>>excludeEnd("ctx");
}))._valueWithTimeout_((300));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,4)});
//>>excludeEnd("ctx");
}),"update",(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._updateTabsOrder();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,6)});
//>>excludeEnd("ctx");
})]));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderTabsOn:",{html:html,ul:ul})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "setupEvents",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setupEvents\x0a\x09'body' asJQuery keydown: [ :event |\x0a\x09\x0a\x09\x09\x22ctrl+> and ctrl+<\x22\x0a\x09\x09(event ctrlKey and: [ event which = 188 ]) ifTrue: [\x0a\x09\x09\x09self activatePreviousTab.\x0a\x09\x09\x09event preventDefault ].\x0a\x09\x09(event ctrlKey and: [ event which = 190 ]) ifTrue: [\x0a\x09\x09\x09self activateNextTab.\x0a\x09\x09\x09event preventDefault ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["keydown:", "asJQuery", "ifTrue:", "and:", "ctrlKey", "=", "which", "activatePreviousTab", "preventDefault", "activateNextTab"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$recv("body"._asJQuery())._keydown_((function(event){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if($core.assert([$recv(event)._ctrlKey()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["ctrlKey"]=1
//>>excludeEnd("ctx");
][0])){
$1=[$recv([$recv(event)._which()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["which"]=1
//>>excludeEnd("ctx");
][0]).__eq((188))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["="]=1
//>>excludeEnd("ctx");
][0];
} else {
$1=false;
}
if($core.assert($1)){
$self._activatePreviousTab();
[$recv(event)._preventDefault()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["preventDefault"]=1
//>>excludeEnd("ctx");
][0];
}
if($core.assert($recv(event)._ctrlKey())){
$2=$recv($recv(event)._which()).__eq((190));
} else {
$2=false;
}
if($core.assert($2)){
$self._activateNextTab();
return $recv(event)._preventDefault();
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({event:event},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setupEvents",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "show:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aTab"],
source: "show: aTab\x0a\x09self tabs do: [ :each | each hide ].\x0a\x09aTab show; focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "tabs", "hide", "show", "focus"]
}, function ($methodClass){ return function (aTab){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._tabs())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._hide();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
$recv(aTab)._show();
$recv(aTab)._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"show:",{aTab:aTab})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "spotlight",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "spotlight\x0a\x09^ spotlight ifNil: [ spotlight := HLSpotlightWidget new ]",
referencedClasses: ["HLSpotlightWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.spotlight;
if($1 == null || $1.a$nil){
$self.spotlight=$recv($globals.HLSpotlightWidget)._new();
return $self.spotlight;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"spotlight",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "tabWidth",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabWidth\x0a\x09^ (window asJQuery width - 250) / self tabs size",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["/", "-", "width", "asJQuery", "size", "tabs"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($recv($recv(window)._asJQuery())._width()).__minus((250))).__slash($recv($self._tabs())._size());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"tabWidth",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "tabs",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabs\x0a\x09^ tabs ifNil: [ tabs := OrderedCollection new ]",
referencedClasses: ["OrderedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.tabs;
if($1 == null || $1.a$nil){
$self.tabs=$recv($globals.OrderedCollection)._new();
return $self.tabs;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"tabs",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);

$core.addMethod(
$core.method({
selector: "updateTabsOrder",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "updateTabsOrder\x0a\x09tabs := '.nav-tabs li' asJQuery toArray\x0a\x09\x09collect: [ :each | each at: 'tab-data' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["collect:", "toArray", "asJQuery", "at:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.tabs=$recv($recv(".nav-tabs li"._asJQuery())._toArray())._collect_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._at_("tab-data");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"updateTabsOrder",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLTabsWidget);


$core.setSlots($globals.HLTabsWidget.a$cls, ["current"]);

$core.addClass("HLWelcomeWidget", $globals.HLWidget, "Helios-Core");
$core.addMethod(
$core.method({
selector: "cssClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "cssClass\x0a\x09^ 'welcome'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "welcome";

}; }),
$globals.HLWelcomeWidget);

$core.addMethod(
$core.method({
selector: "openClassBrowser",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "openClassBrowser\x0a\x09HLBrowser openAsTab",
referencedClasses: ["HLBrowser"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["openAsTab"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($globals.HLBrowser)._openAsTab();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"openClassBrowser",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWelcomeWidget);

$core.addMethod(
$core.method({
selector: "openHelp",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "openHelp",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return self;

}; }),
$globals.HLWelcomeWidget);

$core.addMethod(
$core.method({
selector: "openTestRunner",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "openTestRunner\x0a\x09HLSUnit openAsTab",
referencedClasses: ["HLSUnit"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["openAsTab"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($globals.HLSUnit)._openAsTab();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"openTestRunner",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWelcomeWidget);

$core.addMethod(
$core.method({
selector: "openWorkspace",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "openWorkspace\x0a\x09HLWorkspace openAsTab",
referencedClasses: ["HLWorkspace"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["openAsTab"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($globals.HLWorkspace)._openAsTab();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"openWorkspace",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWelcomeWidget);

$core.addMethod(
$core.method({
selector: "renderButtonsOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderButtonsOn: html\x0a\x09html button\x0a\x09\x09class: 'button';\x0a\x09\x09with: 'Class Browser';\x0a\x09\x09onClick: [ self openClassBrowser ].\x0a\x09html button\x0a\x09\x09class: 'button';\x0a\x09\x09with: 'Workspace';\x0a\x09\x09onClick: [ self openWorkspace ].\x0a\x09html button\x0a\x09\x09class: 'button';\x0a\x09\x09with: 'Test Runner';\x0a\x09\x09onClick: [ self openTestRunner ].\x0a\x09\x22html button\x0a\x09\x09class: 'button';\x0a\x09\x09with: 'Help';\x0a\x09\x09onClick: [ self openHelp ]\x22",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "button", "with:", "onClick:", "openClassBrowser", "openWorkspace", "openTestRunner"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3;
$1=[$recv(html)._button()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["button"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._class_("button")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_("Class Browser")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._openClassBrowser();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["onClick:"]=1
//>>excludeEnd("ctx");
][0];
$2=[$recv(html)._button()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["button"]=2
//>>excludeEnd("ctx");
][0];
[$recv($2)._class_("button")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($2)._with_("Workspace")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($2)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._openWorkspace();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["onClick:"]=2
//>>excludeEnd("ctx");
][0];
$3=$recv(html)._button();
$recv($3)._class_("button");
$recv($3)._with_("Test Runner");
$recv($3)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._openTestRunner();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,3)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderButtonsOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWelcomeWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09self\x0a\x09\x09renderHelpOn: html;\x0a\x09\x09renderButtonsOn: html",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["renderHelpOn:", "renderButtonsOn:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._renderHelpOn_(html);
$self._renderButtonsOn_(html);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWelcomeWidget);

$core.addMethod(
$core.method({
selector: "renderHelpOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderHelpOn: html\x0a\x09html h2 with: 'No tools are open'.\x0a\x09html ul with: [\x0a\x09\x09html li with: 'Perform actions with  ctrl + space'.\x0a\x09\x09html li with: 'Open one of the common tools:' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "h2", "ul", "li"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$recv($recv(html)._h2())._with_("No tools are open")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($recv(html)._ul())._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
[$recv([$recv(html)._li()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["li"]=1
//>>excludeEnd("ctx");
][0])._with_("Perform actions with  ctrl + space")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=3
//>>excludeEnd("ctx");
][0];
return $recv($recv(html)._li())._with_("Open one of the common tools:");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=2
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderHelpOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLWelcomeWidget);


});
