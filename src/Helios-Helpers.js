define(["amber/boot", "require", "amber/core/Kernel-Objects"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-Helpers");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLClassifier", $globals.Object, "Helios-Helpers");
$core.setSlots($globals.HLClassifier, ["next", "method"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLClassifier.comment="I am an abstract class implementing a link in a `chain of responsibility` pattern.\x0a\x0aSubclasses are in charge of classifying a method according to multiple strategies.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "classify",
protocol: "protocol",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "classify\x0a\x09self next ifNil: [ ^ false ].\x0a\x09\x0a\x09^ self doClassify\x0a\x09\x09ifTrue: [ true ]\x0a\x09\x09ifFalse: [ self next classify ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "next", "ifTrue:ifFalse:", "doClassify", "classify"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=[$self._next()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["next"]=1
//>>excludeEnd("ctx");
][0];
if($1 == null || $1.a$nil){
return false;
} else {
$1;
}
if($core.assert($self._doClassify())){
return true;
} else {
return $recv($self._next())._classify();
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"classify",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassifier);

$core.addMethod(
$core.method({
selector: "doClassify",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "doClassify\x0a\x09self subclassResponsibility",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["subclassResponsibility"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._subclassResponsibility();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"doClassify",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassifier);

$core.addMethod(
$core.method({
selector: "method",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "method\x0a\x09^ method",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.method;

}; }),
$globals.HLClassifier);

$core.addMethod(
$core.method({
selector: "method:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "method: anObject\x0a\x09method := anObject.\x0a\x09self next\x0a\x09\x09ifNotNil: [ :nextLink | nextLink method: anObject ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "next", "method:"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$self.method=anObject;
$1=$self._next();
if($1 == null || $1.a$nil){
$1;
} else {
var nextLink;
nextLink=$1;
$recv(nextLink)._method_(anObject);
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"method:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassifier);

$core.addMethod(
$core.method({
selector: "next",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "next\x0a\x09^ next",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.next;

}; }),
$globals.HLClassifier);

$core.addMethod(
$core.method({
selector: "next:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "next: anObject\x0a\x09next := anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
$self.next=anObject;
return self;

}; }),
$globals.HLClassifier);



$core.addClass("HLAccessorClassifier", $globals.HLClassifier, "Helios-Helpers");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLAccessorClassifier.comment="I am a classifier checking the method selector matches an instance variable name.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "doClassify",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "doClassify\x0a\x09| names selector |\x0a\x09\x0a\x09names := method origin allSlotNames.\x0a\x09selector := method selector.\x0a\x09\x0a\x09(selector last = ':')\x0a\x09\x09ifTrue: [ \x22selector might be a setter\x22\x0a\x09\x09\x09selector := selector allButLast ].\x0a\x09\x0a\x09(names includes: selector)\x0a\x09\x09ifFalse: [ ^ false ].\x0a\x09\x09\x0a\x09method protocol: 'accessing'.\x0a\x09^ true.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["allSlotNames", "origin", "selector", "ifTrue:", "=", "last", "allButLast", "ifFalse:", "includes:", "protocol:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var names,selector;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
names=$recv($recv($self.method)._origin())._allSlotNames();
selector=$recv($self.method)._selector();
if($core.assert($recv($recv(selector)._last()).__eq(":"))){
selector=$recv(selector)._allButLast();
selector;
}
if(!$core.assert($recv(names)._includes_(selector))){
return false;
}
$recv($self.method)._protocol_("accessing");
return true;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"doClassify",{names:names,selector:selector})});
//>>excludeEnd("ctx");
}; }),
$globals.HLAccessorClassifier);



$core.addClass("HLImplementorClassifier", $globals.HLClassifier, "Helios-Helpers");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLImplementorClassifier.comment="I am a classifier checking the other implementations of the same selector and choose the protocol the most populated.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "doClassify",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "doClassify\x0a\x09| currentClass |\x0a\x09currentClass := method origin.\x0a\x09\x0a\x09[ currentClass superclass isNil ] whileFalse: [\x0a\x09\x09currentClass := currentClass superclass.\x0a\x09\x09(currentClass includesSelector: method selector)\x0a\x09\x09\x09ifTrue: [ \x0a\x09\x09\x09\x09method protocol: (currentClass >> method selector) protocol.\x0a\x09\x09\x09\x09^ true ]].\x0a\x09\x0a\x09^ false.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["origin", "whileFalse:", "isNil", "superclass", "ifTrue:", "includesSelector:", "selector", "protocol:", "protocol", ">>"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var currentClass;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $early={};
try {
currentClass=$recv($self.method)._origin();
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv([$recv(currentClass)._superclass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["superclass"]=1
//>>excludeEnd("ctx");
][0])._isNil();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._whileFalse_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
currentClass=$recv(currentClass)._superclass();
if($core.assert($recv(currentClass)._includesSelector_([$recv($self.method)._selector()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["selector"]=1
//>>excludeEnd("ctx");
][0]))){
$recv($self.method)._protocol_($recv($recv(currentClass).__gt_gt($recv($self.method)._selector()))._protocol());
throw $early=[true];
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return false;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"doClassify",{currentClass:currentClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLImplementorClassifier);



$core.addClass("HLPrefixClassifier", $globals.HLClassifier, "Helios-Helpers");
$core.setSlots($globals.HLPrefixClassifier, ["prefixMapping"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLPrefixClassifier.comment="I am classifier checking the method selector to know if it begins with a known prefix.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "buildPrefixDictionary",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "buildPrefixDictionary\x0a\x09prefixMapping := Dictionary new.\x0a\x09prefixMapping \x0a\x09\x09at: 'test' put: 'tests';\x0a\x09 \x09at: 'bench' put: 'benchmarking';\x0a\x09 \x09at: 'copy' put: 'copying';\x0a\x09\x09at: 'initialize' put: 'initialization';\x0a\x09\x09at: 'accept' put: 'visitor';\x0a\x09\x09at: 'visit' put: 'visitor';\x0a\x09\x09at: 'signal' put: 'signalling';\x0a\x09\x09at: 'parse' put: 'parsing';\x0a\x09\x09at: 'add' put: 'adding';\x0a\x09\x09at: 'is' put: 'testing';\x0a\x09\x09at: 'as' put: 'converting';\x0a\x09\x09at: 'new' put: 'instance creation'.",
referencedClasses: ["Dictionary"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["new", "at:put:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$self.prefixMapping=$recv($globals.Dictionary)._new();
$1=$self.prefixMapping;
[$recv($1)._at_put_("test","tests")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._at_put_("bench","benchmarking")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($1)._at_put_("copy","copying")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=3
//>>excludeEnd("ctx");
][0];
[$recv($1)._at_put_("initialize","initialization")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=4
//>>excludeEnd("ctx");
][0];
[$recv($1)._at_put_("accept","visitor")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=5
//>>excludeEnd("ctx");
][0];
[$recv($1)._at_put_("visit","visitor")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=6
//>>excludeEnd("ctx");
][0];
[$recv($1)._at_put_("signal","signalling")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=7
//>>excludeEnd("ctx");
][0];
[$recv($1)._at_put_("parse","parsing")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=8
//>>excludeEnd("ctx");
][0];
[$recv($1)._at_put_("add","adding")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=9
//>>excludeEnd("ctx");
][0];
[$recv($1)._at_put_("is","testing")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=10
//>>excludeEnd("ctx");
][0];
[$recv($1)._at_put_("as","converting")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=11
//>>excludeEnd("ctx");
][0];
$recv($1)._at_put_("new","instance creation");
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"buildPrefixDictionary",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPrefixClassifier);

$core.addMethod(
$core.method({
selector: "doClassify",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "doClassify\x0a\x09prefixMapping keysAndValuesDo: [ :prefix :protocol |\x0a\x09\x09(method selector beginsWith: prefix)\x0a\x09\x09\x09ifTrue: [\x0a\x09\x09\x09\x09method protocol: protocol.\x0a\x09\x09\x09\x09^ true ]].\x0a\x09^ false.",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["keysAndValuesDo:", "ifTrue:", "beginsWith:", "selector", "protocol:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $early={};
try {
$recv($self.prefixMapping)._keysAndValuesDo_((function(prefix,protocol){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if($core.assert($recv($recv($self.method)._selector())._beginsWith_(prefix))){
$recv($self.method)._protocol_(protocol);
throw $early=[true];
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({prefix:prefix,protocol:protocol},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return false;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"doClassify",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPrefixClassifier);

$core.addMethod(
$core.method({
selector: "initialize",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initialize\x0a\x09super initialize.\x0a\x0a\x09self buildPrefixDictionary",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initialize", "buildPrefixDictionary"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._initialize.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$self._buildPrefixDictionary();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initialize",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPrefixClassifier);



$core.addClass("HLSuperclassClassifier", $globals.HLClassifier, "Helios-Helpers");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSuperclassClassifier.comment="I am a classifier checking the superclass chain to find a matching selector.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "doClassify",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "doClassify\x0a\x09| protocolBag methods protocolToUse counter |\x0a\x09\x0a\x09protocolBag := Dictionary new.\x0a\x09methods := HLReferencesModel new implementorsOf: method selector.\x0a\x09methods\x0a\x09\x09ifEmpty: [ ^ false ]\x0a\x09\x09ifNotEmpty: [\x0a\x09\x09\x09methods \x0a\x09\x09\x09\x09do: [ :aMethod || protocol |\x0a\x09\x09\x09\x09\x09protocol := aMethod method protocol.\x0a\x09\x09\x09\x09\x09(method origin = aMethod origin)\x0a\x09\x09\x09\x09\x09\x09ifFalse: [\x0a\x09\x09\x09\x09\x09\x09((protocol first = '*') or: [ protocol = method defaultProtocol ])\x0a\x09\x09\x09\x09\x09\x09\x09ifFalse: [ \x0a\x09\x09\x09\x09\x09\x09\x09\x09protocolBag \x0a\x09\x09\x09\x09\x09\x09\x09\x09\x09at: protocol \x0a\x09\x09\x09\x09\x09\x09\x09\x09\x09put: (protocolBag at: protocol ifAbsent: [ 0 ]) + 1 ] ] ] ].\x0a\x09\x09\x09\x0a\x09protocolBag ifEmpty: [ ^ false ].\x0a\x09protocolToUse := nil.\x0a\x09counter := 0.\x0a\x09protocolBag keysAndValuesDo: [ :key :value | value > counter \x0a\x09\x09ifTrue: [\x0a\x09\x09\x09counter := value.\x0a\x09\x09\x09protocolToUse := key ] ].\x0a\x09method protocol: protocolToUse.\x0a\x09^ true",
referencedClasses: ["Dictionary", "HLReferencesModel"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["new", "implementorsOf:", "selector", "ifEmpty:ifNotEmpty:", "do:", "protocol", "method", "ifFalse:", "=", "origin", "or:", "first", "defaultProtocol", "at:put:", "+", "at:ifAbsent:", "ifEmpty:", "keysAndValuesDo:", "ifTrue:", ">", "protocol:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var protocolBag,methods,protocolToUse,counter;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
var $early={};
try {
protocolBag=[$recv($globals.Dictionary)._new()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["new"]=1
//>>excludeEnd("ctx");
][0];
methods=$recv($recv($globals.HLReferencesModel)._new())._implementorsOf_($recv($self.method)._selector());
$recv(methods)._ifEmpty_ifNotEmpty_((function(){
throw $early=[false];

}),(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(methods)._do_((function(aMethod){
var protocol;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
protocol=$recv($recv(aMethod)._method())._protocol();
if(!$core.assert([$recv([$recv($self.method)._origin()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["origin"]=1
//>>excludeEnd("ctx");
][0]).__eq($recv(aMethod)._origin())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["="]=1
//>>excludeEnd("ctx");
][0])){
if($core.assert([$recv($recv(protocol)._first()).__eq("*")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["="]=2
//>>excludeEnd("ctx");
][0])){
$1=true;
} else {
$1=$recv(protocol).__eq($recv($self.method)._defaultProtocol());
}
if(!$core.assert($1)){
return $recv(protocolBag)._at_put_(protocol,$recv($recv(protocolBag)._at_ifAbsent_(protocol,(function(){
return (0);

}))).__plus((1)));
}
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({aMethod:aMethod,protocol:protocol},$ctx2,3)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
$recv(protocolBag)._ifEmpty_((function(){
throw $early=[false];

}));
protocolToUse=nil;
counter=(0);
$recv(protocolBag)._keysAndValuesDo_((function(key,value){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if($core.assert($recv(value).__gt(counter))){
counter=value;
protocolToUse=key;
return protocolToUse;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({key:key,value:value},$ctx1,9)});
//>>excludeEnd("ctx");
}));
$recv($self.method)._protocol_(protocolToUse);
return true;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"doClassify",{protocolBag:protocolBag,methods:methods,protocolToUse:protocolToUse,counter:counter})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSuperclassClassifier);



$core.addClass("HLGenerationOutput", $globals.Object, "Helios-Helpers");
$core.setSlots($globals.HLGenerationOutput, ["sourceCodes", "protocol", "targetClass"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLGenerationOutput.comment="I am a simple data object used to store the result of a generation process.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "addSourceCode:",
protocol: "protocol",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "addSourceCode: aString\x0a\x09sourceCodes add: aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["add:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.sourceCodes)._add_(aString);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"addSourceCode:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLGenerationOutput);

$core.addMethod(
$core.method({
selector: "compile",
protocol: "protocol",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "compile\x0a\x09sourceCodes do: [ :methodSourceCode |\x0a\x09\x09(targetClass includesSelector: methodSourceCode selector)\x0a\x09\x09\x09ifFalse: [ \x0a\x09\x09\x09\x09targetClass \x0a\x09\x09\x09\x09\x09compile: methodSourceCode sourceCode\x0a\x09\x09\x09\x09\x09protocol: protocol ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "ifFalse:", "includesSelector:", "selector", "compile:protocol:", "sourceCode"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.sourceCodes)._do_((function(methodSourceCode){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if(!$core.assert($recv($self.targetClass)._includesSelector_($recv(methodSourceCode)._selector()))){
return $recv($self.targetClass)._compile_protocol_($recv(methodSourceCode)._sourceCode(),$self.protocol);
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({methodSourceCode:methodSourceCode},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"compile",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLGenerationOutput);

$core.addMethod(
$core.method({
selector: "initialize",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initialize\x0a\x09super initialize.\x0a\x09\x0a\x09sourceCodes := OrderedCollection new",
referencedClasses: ["OrderedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initialize", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._initialize.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$self.sourceCodes=$recv($globals.OrderedCollection)._new();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initialize",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLGenerationOutput);

$core.addMethod(
$core.method({
selector: "protocol",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "protocol\x0a\x09^ protocol",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.protocol;

}; }),
$globals.HLGenerationOutput);

$core.addMethod(
$core.method({
selector: "protocol:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "protocol: aString\x0a\x09protocol := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.protocol=aString;
return self;

}; }),
$globals.HLGenerationOutput);

$core.addMethod(
$core.method({
selector: "sourceCodes",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "sourceCodes\x0a\x09^ sourceCodes",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.sourceCodes;

}; }),
$globals.HLGenerationOutput);

$core.addMethod(
$core.method({
selector: "sourceCodes:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection"],
source: "sourceCodes: aCollection\x0a\x09sourceCodes := aCollection",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aCollection){
var self=this,$self=this;
$self.sourceCodes=aCollection;
return self;

}; }),
$globals.HLGenerationOutput);

$core.addMethod(
$core.method({
selector: "targetClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "targetClass\x0a\x09^ targetClass",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.targetClass;

}; }),
$globals.HLGenerationOutput);

$core.addMethod(
$core.method({
selector: "targetClass:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass"],
source: "targetClass: aClass\x0a\x09targetClass := aClass",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aClass){
var self=this,$self=this;
$self.targetClass=aClass;
return self;

}; }),
$globals.HLGenerationOutput);



$core.addClass("HLMethodClassifier", $globals.Object, "Helios-Helpers");
$core.setSlots($globals.HLMethodClassifier, ["firstClassifier"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLMethodClassifier.comment="I am in charge of categorizing methods following this strategy:\x0a\x0a- is it an accessor?\x0a- is it overriding a superclass method?\x0a- is it starting with a know prefix?\x0a- how are categorized the other implementations?";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "addClassifier:",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClassifier"],
source: "addClassifier: aClassifier\x0a\x09aClassifier next: firstClassifier.\x0a\x09firstClassifier := aClassifier",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["next:"]
}, function ($methodClass){ return function (aClassifier){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(aClassifier)._next_($self.firstClassifier);
$self.firstClassifier=aClassifier;
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"addClassifier:",{aClassifier:aClassifier})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodClassifier);

$core.addMethod(
$core.method({
selector: "classify:",
protocol: "protocol",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aMethod"],
source: "classify: aMethod\x0a\x09firstClassifier\x0a\x09\x09method: aMethod;\x0a\x09\x09classify",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["method:", "classify"]
}, function ($methodClass){ return function (aMethod){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.firstClassifier;
$recv($1)._method_(aMethod);
$recv($1)._classify();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"classify:",{aMethod:aMethod})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodClassifier);

$core.addMethod(
$core.method({
selector: "classifyAll:",
protocol: "protocol",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollectionOfMethods"],
source: "classifyAll: aCollectionOfMethods\x0a\x09aCollectionOfMethods do: [ :method |\x0a\x09\x09self classify: method ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "classify:"]
}, function ($methodClass){ return function (aCollectionOfMethods){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(aCollectionOfMethods)._do_((function(method){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._classify_(method);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({method:method},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"classifyAll:",{aCollectionOfMethods:aCollectionOfMethods})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodClassifier);

$core.addMethod(
$core.method({
selector: "initialize",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initialize\x0a\x09super initialize.\x0a\x09\x0a\x09self setupClassifiers",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initialize", "setupClassifiers"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._initialize.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$self._setupClassifiers();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initialize",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodClassifier);

$core.addMethod(
$core.method({
selector: "setupClassifiers",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setupClassifiers\x0a\x09self addClassifier: HLImplementorClassifier new.\x0a\x09self addClassifier: HLPrefixClassifier new.\x0a\x09self addClassifier: HLSuperclassClassifier new.\x0a\x09self addClassifier: HLAccessorClassifier new",
referencedClasses: ["HLImplementorClassifier", "HLPrefixClassifier", "HLSuperclassClassifier", "HLAccessorClassifier"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["addClassifier:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$self._addClassifier_([$recv($globals.HLImplementorClassifier)._new()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["new"]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["addClassifier:"]=1
//>>excludeEnd("ctx");
][0];
[$self._addClassifier_([$recv($globals.HLPrefixClassifier)._new()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["new"]=2
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["addClassifier:"]=2
//>>excludeEnd("ctx");
][0];
[$self._addClassifier_([$recv($globals.HLSuperclassClassifier)._new()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["new"]=3
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["addClassifier:"]=3
//>>excludeEnd("ctx");
][0];
$self._addClassifier_($recv($globals.HLAccessorClassifier)._new());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setupClassifiers",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodClassifier);



$core.addClass("HLMethodGenerator", $globals.Object, "Helios-Helpers");
$core.setSlots($globals.HLMethodGenerator, ["output"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLMethodGenerator.comment="I am the abstract super class of the method generators.\x0a\x0aMy main method is `generate` which produces an `output` object accessed with `#output`.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "class:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass"],
source: "class: aClass\x0a\x09output targetClass: aClass",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["targetClass:"]
}, function ($methodClass){ return function (aClass){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.output)._targetClass_(aClass);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"class:",{aClass:aClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodGenerator);

$core.addMethod(
$core.method({
selector: "generate",
protocol: "protocol",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "generate\x0a\x09output targetClass ifNil: [ self error: 'class should not be nil'].",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "targetClass", "error:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self.output)._targetClass();
if($1 == null || $1.a$nil){
$self._error_("class should not be nil");
} else {
$1;
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"generate",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodGenerator);

$core.addMethod(
$core.method({
selector: "initialize",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initialize\x0a\x09super initialize.\x0a\x09\x0a\x09output := HLGenerationOutput new",
referencedClasses: ["HLGenerationOutput"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initialize", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._initialize.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$self.output=$recv($globals.HLGenerationOutput)._new();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initialize",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodGenerator);

$core.addMethod(
$core.method({
selector: "output",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "output\x0a\x09^ output",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.output;

}; }),
$globals.HLMethodGenerator);



$core.addClass("HLAccessorsGenerator", $globals.HLMethodGenerator, "Helios-Helpers");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLAccessorsGenerator.comment="I am a generator used to compile the getters/setters of a class.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "accessorProtocolForObject",
protocol: "double-dispatch",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "accessorProtocolForObject\x0a\x09output protocol: 'accessing'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["protocol:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.output)._protocol_("accessing");
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"accessorProtocolForObject",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLAccessorsGenerator);

$core.addMethod(
$core.method({
selector: "accessorsForObject",
protocol: "double-dispatch",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "accessorsForObject\x0a\x09| sources |\x0a\x09\x0a\x09sources := OrderedCollection new.\x0a\x09output targetClass slotNames sorted do: [ :each | \x0a\x09\x09sources \x0a\x09\x09\x09add: (self getterFor: each);\x0a\x09\x09\x09add: (self setterFor: each) ].\x0a\x09output sourceCodes: sources",
referencedClasses: ["OrderedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["new", "do:", "sorted", "slotNames", "targetClass", "add:", "getterFor:", "setterFor:", "sourceCodes:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var sources;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
sources=$recv($globals.OrderedCollection)._new();
$recv($recv($recv($recv($self.output)._targetClass())._slotNames())._sorted())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$1=sources;
[$recv($1)._add_($self._getterFor_(each))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["add:"]=1
//>>excludeEnd("ctx");
][0];
return $recv($1)._add_($self._setterFor_(each));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
$recv($self.output)._sourceCodes_(sources);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"accessorsForObject",{sources:sources})});
//>>excludeEnd("ctx");
}; }),
$globals.HLAccessorsGenerator);

$core.addMethod(
$core.method({
selector: "generate",
protocol: "protocol",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "generate\x0a\x09super generate.\x0a\x09\x0a\x09output targetClass \x0a\x09\x09accessorsSourceCodesWith: self;\x0a\x09\x09accessorProtocolWith: self",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["generate", "accessorsSourceCodesWith:", "targetClass", "accessorProtocolWith:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._generate.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$1=$recv($self.output)._targetClass();
$recv($1)._accessorsSourceCodesWith_(self);
$recv($1)._accessorProtocolWith_(self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"generate",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLAccessorsGenerator);

$core.addMethod(
$core.method({
selector: "getterFor:",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInstanceVariable"],
source: "getterFor: anInstanceVariable\x0a\x09^ HLMethodSourceCode new\x0a\x09\x09selector:anInstanceVariable;\x0a\x09\x09sourceCode: (String streamContents: [ :stream |\x0a\x09\x09stream << anInstanceVariable.\x0a\x09\x09stream cr tab.\x0a\x09\x09stream << '^ ' << anInstanceVariable ])",
referencedClasses: ["HLMethodSourceCode", "String"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selector:", "new", "sourceCode:", "streamContents:", "<<", "tab", "cr"]
}, function ($methodClass){ return function (anInstanceVariable){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLMethodSourceCode)._new();
$recv($1)._selector_(anInstanceVariable);
return $recv($1)._sourceCode_($recv($globals.String)._streamContents_((function(stream){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
[$recv(stream).__lt_lt(anInstanceVariable)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["<<"]=1
//>>excludeEnd("ctx");
][0];
$recv($recv(stream)._cr())._tab();
return [$recv($recv(stream).__lt_lt("^ ")).__lt_lt(anInstanceVariable)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["<<"]=2
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({stream:stream},$ctx1,1)});
//>>excludeEnd("ctx");
})));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"getterFor:",{anInstanceVariable:anInstanceVariable})});
//>>excludeEnd("ctx");
}; }),
$globals.HLAccessorsGenerator);

$core.addMethod(
$core.method({
selector: "setterFor:",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInstanceVariable"],
source: "setterFor: anInstanceVariable\x0a\x09^ HLMethodSourceCode new\x0a\x09\x09selector: anInstanceVariable, ':';\x0a\x09\x09sourceCode: (String streamContents: [ :stream |\x0a\x09\x09stream << anInstanceVariable << ': anObject'.\x0a\x09\x09stream cr tab.\x0a\x09\x09stream << anInstanceVariable << ' := anObject' ])",
referencedClasses: ["HLMethodSourceCode", "String"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selector:", "new", ",", "sourceCode:", "streamContents:", "<<", "tab", "cr"]
}, function ($methodClass){ return function (anInstanceVariable){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLMethodSourceCode)._new();
$recv($1)._selector_($recv(anInstanceVariable).__comma(":"));
return $recv($1)._sourceCode_($recv($globals.String)._streamContents_((function(stream){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
[$recv([$recv(stream).__lt_lt(anInstanceVariable)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["<<"]=2
//>>excludeEnd("ctx");
][0]).__lt_lt(": anObject")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["<<"]=1
//>>excludeEnd("ctx");
][0];
$recv($recv(stream)._cr())._tab();
return [$recv($recv(stream).__lt_lt(anInstanceVariable)).__lt_lt(" := anObject")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["<<"]=3
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({stream:stream},$ctx1,1)});
//>>excludeEnd("ctx");
})));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setterFor:",{anInstanceVariable:anInstanceVariable})});
//>>excludeEnd("ctx");
}; }),
$globals.HLAccessorsGenerator);



$core.addClass("HLInitializeGenerator", $globals.HLMethodGenerator, "Helios-Helpers");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLInitializeGenerator.comment="I am used to double-dispatch the `initialize` method(s) generation. I am a disposable object.\x0a\x0a## Usage\x0a\x0a    ^ HLInitializeGenerator new\x0a        class: aClass;\x0a        generate;\x0a        output";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "generate",
protocol: "protocol",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "generate\x0a\x09super generate.\x0a\x09\x0a\x09output targetClass \x0a\x09\x09initializeSourceCodesWith: self;\x0a\x09\x09initializeProtocolWith: self",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["generate", "initializeSourceCodesWith:", "targetClass", "initializeProtocolWith:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._generate.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$1=$recv($self.output)._targetClass();
$recv($1)._initializeSourceCodesWith_(self);
$recv($1)._initializeProtocolWith_(self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"generate",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInitializeGenerator);

$core.addMethod(
$core.method({
selector: "generateInitializeCodeForObject",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "generateInitializeCodeForObject\x09\x0a\x09^ String streamContents: [ :str || instVars size |\x0a\x09\x09instVars := output targetClass slotNames sorted.\x0a\x09\x09size := instVars size.\x0a\x09\x09str << 'initialize'.\x0a\x09\x09str cr tab << 'super initialize.';cr.\x0a\x09\x09str cr tab.\x0a\x09\x09instVars withIndexDo: [ :name :index |\x0a\x09\x09\x09index ~= 1 ifTrue: [ str cr tab ].\x0a\x09\x09\x09str << name << ' := nil'.\x0a\x09\x09\x09index ~= size ifTrue: [ str << '.' ] ] ].",
referencedClasses: ["String"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["streamContents:", "sorted", "slotNames", "targetClass", "size", "<<", "tab", "cr", "withIndexDo:", "ifTrue:", "~="]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
return $recv($globals.String)._streamContents_((function(str){
var instVars,size;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
instVars=$recv($recv($recv($self.output)._targetClass())._slotNames())._sorted();
size=$recv(instVars)._size();
[$recv(str).__lt_lt("initialize")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["<<"]=1
//>>excludeEnd("ctx");
][0];
$1=[$recv([$recv(str)._cr()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["cr"]=1
//>>excludeEnd("ctx");
][0])._tab()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["tab"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1).__lt_lt("super initialize.")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["<<"]=2
//>>excludeEnd("ctx");
][0];
[$recv($1)._cr()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["cr"]=2
//>>excludeEnd("ctx");
][0];
[$recv([$recv(str)._cr()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["cr"]=3
//>>excludeEnd("ctx");
][0])._tab()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["tab"]=2
//>>excludeEnd("ctx");
][0];
return $recv(instVars)._withIndexDo_((function(name,index){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
if($core.assert([$recv(index).__tild_eq((1))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["~="]=1
//>>excludeEnd("ctx");
][0])){
$recv($recv(str)._cr())._tab();
}
[$recv([$recv(str).__lt_lt(name)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["<<"]=4
//>>excludeEnd("ctx");
][0]).__lt_lt(" := nil")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["<<"]=3
//>>excludeEnd("ctx");
][0];
if($core.assert($recv(index).__tild_eq(size))){
return $recv(str).__lt_lt(".");
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({name:name,index:index},$ctx2,2)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({str:str,instVars:instVars,size:size},$ctx1,1)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"generateInitializeCodeForObject",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInitializeGenerator);

$core.addMethod(
$core.method({
selector: "initializeForObject",
protocol: "double-dispatch",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initializeForObject\x0a\x09output addSourceCode: self initializeMethodForObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["addSourceCode:", "initializeMethodForObject"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.output)._addSourceCode_($self._initializeMethodForObject());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initializeForObject",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInitializeGenerator);

$core.addMethod(
$core.method({
selector: "initializeMethodForObject",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initializeMethodForObject\x09\x0a\x09^ HLMethodSourceCode new\x0a\x09\x09selector: 'initialize';\x0a\x09\x09sourceCode: self generateInitializeCodeForObject;\x0a\x09\x09yourself",
referencedClasses: ["HLMethodSourceCode"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selector:", "new", "sourceCode:", "generateInitializeCodeForObject", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLMethodSourceCode)._new();
$recv($1)._selector_("initialize");
$recv($1)._sourceCode_($self._generateInitializeCodeForObject());
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initializeMethodForObject",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInitializeGenerator);

$core.addMethod(
$core.method({
selector: "initializeProtocolForObject",
protocol: "double-dispatch",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initializeProtocolForObject\x0a\x09output protocol: 'initialization'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["protocol:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.output)._protocol_("initialization");
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initializeProtocolForObject",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInitializeGenerator);



$core.addClass("HLMethodSourceCode", $globals.Object, "Helios-Helpers");
$core.setSlots($globals.HLMethodSourceCode, ["selector", "sourceCode"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLMethodSourceCode.comment="I am a simple data object keeping track of the information about a method that will be compiled at the end of the generation process.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "selector",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selector\x0a\x09^ selector",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.selector;

}; }),
$globals.HLMethodSourceCode);

$core.addMethod(
$core.method({
selector: "selector:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSelector"],
source: "selector: aSelector\x0a\x09selector := aSelector",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aSelector){
var self=this,$self=this;
$self.selector=aSelector;
return self;

}; }),
$globals.HLMethodSourceCode);

$core.addMethod(
$core.method({
selector: "sourceCode",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "sourceCode\x0a\x09^ sourceCode",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.sourceCode;

}; }),
$globals.HLMethodSourceCode);

$core.addMethod(
$core.method({
selector: "sourceCode:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "sourceCode: aString\x0a\x09sourceCode := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.sourceCode=aString;
return self;

}; }),
$globals.HLMethodSourceCode);



$core.addClass("HLPackageCommitErrorHelper", $globals.Object, "Helios-Helpers");
$core.setSlots($globals.HLPackageCommitErrorHelper, ["model"]);
$core.addMethod(
$core.method({
selector: "commitPackage",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "commitPackage\x0a\x09(HLCommitPackageCommand for: self model)\x0a\x09\x09execute",
referencedClasses: ["HLCommitPackageCommand"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["execute", "for:", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($globals.HLCommitPackageCommand)._for_($self._model()))._execute();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"commitPackage",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackageCommitErrorHelper);

$core.addMethod(
$core.method({
selector: "commitToPath:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "commitToPath: aString\x0a\x09\x22We only take AMD package transport into account for now\x22\x0a\x09\x0a\x09self package transport setPath: aString.\x0a\x09\x0a\x09self commitPackage",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["setPath:", "transport", "package", "commitPackage"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self._package())._transport())._setPath_(aString);
$self._commitPackage();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"commitToPath:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackageCommitErrorHelper);

$core.addMethod(
$core.method({
selector: "model",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "model\x0a\x09^ model",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.model;

}; }),
$globals.HLPackageCommitErrorHelper);

$core.addMethod(
$core.method({
selector: "model:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aToolModel"],
source: "model: aToolModel\x0a\x09model := aToolModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aToolModel){
var self=this,$self=this;
$self.model=aToolModel;
return self;

}; }),
$globals.HLPackageCommitErrorHelper);

$core.addMethod(
$core.method({
selector: "package",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "package\x0a\x09^ self model packageToCommit",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["packageToCommit", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._packageToCommit();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"package",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackageCommitErrorHelper);

$core.addMethod(
$core.method({
selector: "showHelp",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "showHelp\x0a\x09HLConfirmationWidget new\x0a\x09\x09confirmationString: 'Commit failed for namespace \x22', self package transport namespace, '\x22. Do you want to commit to another path?';\x0a\x09\x09actionBlock: [ self showNewCommitPath ];\x0a\x09\x09cancelButtonLabel: 'Abandon';\x0a\x09\x09confirmButtonLabel: 'Set path';\x0a\x09\x09show",
referencedClasses: ["HLConfirmationWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["confirmationString:", "new", ",", "namespace", "transport", "package", "actionBlock:", "showNewCommitPath", "cancelButtonLabel:", "confirmButtonLabel:", "show"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLConfirmationWidget)._new();
$recv($1)._confirmationString_([$recv("Commit failed for namespace \x22".__comma($recv($recv($self._package())._transport())._namespace())).__comma("\x22. Do you want to commit to another path?")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx[","]=1
//>>excludeEnd("ctx");
][0]);
$recv($1)._actionBlock_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._showNewCommitPath();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
$recv($1)._cancelButtonLabel_("Abandon");
$recv($1)._confirmButtonLabel_("Set path");
$recv($1)._show();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"showHelp",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackageCommitErrorHelper);

$core.addMethod(
$core.method({
selector: "showNewCommitPath",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "showNewCommitPath\x0a\x09HLRequestWidget new\x0a\x09\x09beSingleline;\x0a\x09\x09confirmationString: 'Set commit path';\x0a\x09\x09actionBlock: [ :url | self commitToPath: url ];\x0a\x09\x09confirmButtonLabel: 'Commit with new path';\x0a\x09\x09value: '/src';\x0a\x09\x09show",
referencedClasses: ["HLRequestWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["beSingleline", "new", "confirmationString:", "actionBlock:", "commitToPath:", "confirmButtonLabel:", "value:", "show"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLRequestWidget)._new();
$recv($1)._beSingleline();
$recv($1)._confirmationString_("Set commit path");
$recv($1)._actionBlock_((function(url){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._commitToPath_(url);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({url:url},$ctx1,1)});
//>>excludeEnd("ctx");
}));
$recv($1)._confirmButtonLabel_("Commit with new path");
$recv($1)._value_("/src");
$recv($1)._show();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"showNewCommitPath",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackageCommitErrorHelper);


$core.addMethod(
$core.method({
selector: "on:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aToolModel"],
source: "on: aToolModel\x0a\x09^ self new\x0a\x09\x09model: aToolModel;\x0a\x09\x09yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["model:", "new", "yourself"]
}, function ($methodClass){ return function (aToolModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._model_(aToolModel);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{aToolModel:aToolModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackageCommitErrorHelper.a$cls);

});
