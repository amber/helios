define(["amber/boot", "require", "helios/Helios-Commands-Core"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-Commands-Tools");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLToolCommand", $globals.HLModelCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "category",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "category\x0a\x09^ nil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return nil;

}; }),
$globals.HLToolCommand);


$core.addMethod(
$core.method({
selector: "for:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aToolModel"],
source: "for: aToolModel\x0a\x09^ self new\x0a    \x09model: aToolModel;\x0a        yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["model:", "new", "yourself"]
}, function ($methodClass){ return function (aToolModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._model_(aToolModel);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"for:",{aToolModel:aToolModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolCommand.a$cls);

$core.addMethod(
$core.method({
selector: "isValidFor:",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aModel"],
source: "isValidFor: aModel\x0a\x09^ aModel isToolModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["isToolModel"]
}, function ($methodClass){ return function (aModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv(aModel)._isToolModel();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isValidFor:",{aModel:aModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLToolCommand.a$cls);


$core.addClass("HLBrowseMethodCommand", $globals.HLToolCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "displayLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "displayLabel\x0a\x09^ 'browse method'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "browse method";

}; }),
$globals.HLBrowseMethodCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model openMethod",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["openMethod", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._openMethod();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowseMethodCommand);


$core.addMethod(
$core.method({
selector: "isValidFor:",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aModel"],
source: "isValidFor: aModel\x0a\x09^ aModel isReferencesModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["isReferencesModel"]
}, function ($methodClass){ return function (aModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv(aModel)._isReferencesModel();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isValidFor:",{aModel:aModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowseMethodCommand.a$cls);

$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'b'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "b";

}; }),
$globals.HLBrowseMethodCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'browse method'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "browse method";

}; }),
$globals.HLBrowseMethodCommand.a$cls);


$core.addClass("HLCommitPackageCommand", $globals.HLToolCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "category",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "category\x0a\x09^ 'Packages'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Packages";

}; }),
$globals.HLCommitPackageCommand);

$core.addMethod(
$core.method({
selector: "commitPackage",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "commitPackage\x0a\x09self model \x0a\x09\x09commitPackageOnSuccess: [ self informSuccess ]\x0a\x09\x09onError: [ :error | self onPackageCommitError: error ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["commitPackageOnSuccess:onError:", "model", "informSuccess", "onPackageCommitError:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._commitPackageOnSuccess_onError_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._informSuccess();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}),(function(error){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._onPackageCommitError_(error);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({error:error},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"commitPackage",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommitPackageCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self commitPackage",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["commitPackage"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._commitPackage();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommitPackageCommand);

$core.addMethod(
$core.method({
selector: "informSuccess",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "informSuccess\x0a\x09HLInformationWidget new\x0a\x09\x09informationString: 'Commit successful!';\x0a\x09\x09show",
referencedClasses: ["HLInformationWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["informationString:", "new", "show"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLInformationWidget)._new();
$recv($1)._informationString_("Commit successful!");
$recv($1)._show();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"informSuccess",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommitPackageCommand);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ true\x0a\x09\x22self model isPackageDirty\x22",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLCommitPackageCommand);

$core.addMethod(
$core.method({
selector: "onPackageCommitError:",
protocol: "error handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anError"],
source: "onPackageCommitError: anError\x0a\x09(HLPackageCommitErrorHelper on: self model)\x0a\x09\x09showHelp",
referencedClasses: ["HLPackageCommitErrorHelper"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["showHelp", "on:", "model"]
}, function ($methodClass){ return function (anError){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($globals.HLPackageCommitErrorHelper)._on_($self._model()))._showHelp();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onPackageCommitError:",{anError:anError})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommitPackageCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'k'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "k";

}; }),
$globals.HLCommitPackageCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Commit package'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Commit package";

}; }),
$globals.HLCommitPackageCommand.a$cls);


$core.addClass("HLCopyCommand", $globals.HLToolCommand, "Helios-Commands-Tools");

$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'c'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "c";

}; }),
$globals.HLCopyCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Copy'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Copy";

}; }),
$globals.HLCopyCommand.a$cls);


$core.addClass("HLCopyClassCommand", $globals.HLCopyCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "category",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "category\x0a\x09^ 'Classes'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Classes";

}; }),
$globals.HLCopyClassCommand);

$core.addMethod(
$core.method({
selector: "defaultInput",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "defaultInput\x0a\x09^ self model selectedClass theNonMetaClass name",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["name", "theNonMetaClass", "selectedClass", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($recv($self._model())._selectedClass())._theNonMetaClass())._name();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"defaultInput",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCopyClassCommand);

$core.addMethod(
$core.method({
selector: "displayLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "displayLabel\x0a\x09^ 'New class name:'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "New class name:";

}; }),
$globals.HLCopyClassCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model copyClassTo: self input",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["copyClassTo:", "model", "input"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._copyClassTo_($self._input());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCopyClassCommand);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ self model selectedClass notNil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["notNil", "selectedClass", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._model())._selectedClass())._notNil();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCopyClassCommand);

$core.addMethod(
$core.method({
selector: "isInputRequired",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isInputRequired\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLCopyClassCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'c'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "c";

}; }),
$globals.HLCopyClassCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Copy class'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Copy class";

}; }),
$globals.HLCopyClassCommand.a$cls);

$core.addMethod(
$core.method({
selector: "menuLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "menuLabel\x0a\x09^ 'Copy class...'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Copy class...";

}; }),
$globals.HLCopyClassCommand.a$cls);


$core.addClass("HLFindCommand", $globals.HLToolCommand, "Helios-Commands-Tools");

$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'f'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "f";

}; }),
$globals.HLFindCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Find'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Find";

}; }),
$globals.HLFindCommand.a$cls);


$core.addClass("HLFindClassCommand", $globals.HLFindCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "displayLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "displayLabel\x0a\x09^ 'select a class'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "select a class";

}; }),
$globals.HLFindClassCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model openClassNamed: self input",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["openClassNamed:", "model", "input"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._openClassNamed_($self._input());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLFindClassCommand);

$core.addMethod(
$core.method({
selector: "inputCompletion",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inputCompletion\x0a\x09^ self model availableClassNames",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["availableClassNames", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._availableClassNames();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inputCompletion",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLFindClassCommand);

$core.addMethod(
$core.method({
selector: "inputLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inputLabel\x0a\x09^ 'Find a class'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Find a class";

}; }),
$globals.HLFindClassCommand);

$core.addMethod(
$core.method({
selector: "isInputRequired",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isInputRequired\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLFindClassCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'c'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "c";

}; }),
$globals.HLFindClassCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Find class'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Find class";

}; }),
$globals.HLFindClassCommand.a$cls);


$core.addClass("HLFindClassReferencesCommand", $globals.HLFindCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "category",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "category\x0a\x09^ 'Classes'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Classes";

}; }),
$globals.HLFindClassReferencesCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x0a\x09HLReferences new \x0a\x09\x09openAsTab;\x0a\x09\x09search: self model selectedClass theNonMetaClass name",
referencedClasses: ["HLReferences"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["openAsTab", "new", "search:", "name", "theNonMetaClass", "selectedClass", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLReferences)._new();
$recv($1)._openAsTab();
$recv($1)._search_($recv($recv($recv($self._model())._selectedClass())._theNonMetaClass())._name());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLFindClassReferencesCommand);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ self model selectedClass notNil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["notNil", "selectedClass", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._model())._selectedClass())._notNil();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLFindClassReferencesCommand);

$core.addMethod(
$core.method({
selector: "isInputRequired",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isInputRequired\x0a\x09^ false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return false;

}; }),
$globals.HLFindClassReferencesCommand);


$core.addMethod(
$core.method({
selector: "menuLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "menuLabel\x0a\x09^ 'Find class references...'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Find class references...";

}; }),
$globals.HLFindClassReferencesCommand.a$cls);


$core.addClass("HLFindMethodReferencesCommand", $globals.HLFindCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "category",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "category\x0a\x09^ 'Methods'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Methods";

}; }),
$globals.HLFindMethodReferencesCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x0a\x09HLReferences new \x0a\x09\x09openAsTab;\x0a\x09\x09search: self model selectedMethod selector",
referencedClasses: ["HLReferences"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["openAsTab", "new", "search:", "selector", "selectedMethod", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLReferences)._new();
$recv($1)._openAsTab();
$recv($1)._search_($recv($recv($self._model())._selectedMethod())._selector());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLFindMethodReferencesCommand);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ self model selectedMethod notNil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["notNil", "selectedMethod", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._model())._selectedMethod())._notNil();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLFindMethodReferencesCommand);

$core.addMethod(
$core.method({
selector: "isInputRequired",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isInputRequired\x0a\x09^ false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return false;

}; }),
$globals.HLFindMethodReferencesCommand);


$core.addMethod(
$core.method({
selector: "menuLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "menuLabel\x0a\x09^ 'Find method references...'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Find method references...";

}; }),
$globals.HLFindMethodReferencesCommand.a$cls);


$core.addClass("HLFindReferencesCommand", $globals.HLFindCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "defaultInput",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "defaultInput\x0a\x09^ self model selectedMethod \x0a\x09\x09ifNil: [\x0a\x09\x09\x09self model selectedClass\x0a\x09\x09\x09\x09ifNil: [ '' ]\x0a\x09\x09\x09\x09ifNotNil: [ :class | class theNonMetaClass name ] ]\x0a\x09\x09ifNotNil: [ :method | method selector ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:ifNotNil:", "selectedMethod", "model", "selectedClass", "name", "theNonMetaClass", "selector"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._selectedMethod();
if($1 == null || $1.a$nil){
$2=$recv($self._model())._selectedClass();
if($2 == null || $2.a$nil){
return "";
} else {
var class_;
class_=$2;
return $recv($recv(class_)._theNonMetaClass())._name();
}
} else {
var method;
method=$1;
return $recv(method)._selector();
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"defaultInput",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLFindReferencesCommand);

$core.addMethod(
$core.method({
selector: "displayLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "displayLabel\x0a\x09^ 'find references'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "find references";

}; }),
$globals.HLFindReferencesCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09HLReferences new \x0a\x09\x09openAsTab;\x0a\x09\x09search: self input",
referencedClasses: ["HLReferences"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["openAsTab", "new", "search:", "input"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLReferences)._new();
$recv($1)._openAsTab();
$recv($1)._search_($self._input());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLFindReferencesCommand);

$core.addMethod(
$core.method({
selector: "inputCompletion",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inputCompletion\x0a\x09^ self model availableClassNames, self model allSelectors",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: [",", "availableClassNames", "model", "allSelectors"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._availableClassNames()).__comma($recv($self._model())._allSelectors());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inputCompletion",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLFindReferencesCommand);

$core.addMethod(
$core.method({
selector: "inputLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inputLabel\x0a\x09^ 'Find references of'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Find references of";

}; }),
$globals.HLFindReferencesCommand);

$core.addMethod(
$core.method({
selector: "isInputRequired",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isInputRequired\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLFindReferencesCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'r'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "r";

}; }),
$globals.HLFindReferencesCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Find references'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Find references";

}; }),
$globals.HLFindReferencesCommand.a$cls);


$core.addClass("HLMoveToCommand", $globals.HLToolCommand, "Helios-Commands-Tools");

$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'm'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "m";

}; }),
$globals.HLMoveToCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Move'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Move";

}; }),
$globals.HLMoveToCommand.a$cls);


$core.addClass("HLMoveClassToCommand", $globals.HLMoveToCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ self model selectedClass notNil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["notNil", "selectedClass", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._model())._selectedClass())._notNil();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMoveClassToCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'c'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "c";

}; }),
$globals.HLMoveClassToCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Move class'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Move class";

}; }),
$globals.HLMoveClassToCommand.a$cls);


$core.addClass("HLMoveClassToPackageCommand", $globals.HLMoveClassToCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "category",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "category\x0a\x09^ 'Classes'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Classes";

}; }),
$globals.HLMoveClassToPackageCommand);

$core.addMethod(
$core.method({
selector: "displayLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "displayLabel\x0a\x09^ 'select a package'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "select a package";

}; }),
$globals.HLMoveClassToPackageCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model moveClassToPackage: self input",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["moveClassToPackage:", "model", "input"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._moveClassToPackage_($self._input());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMoveClassToPackageCommand);

$core.addMethod(
$core.method({
selector: "inputCompletion",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inputCompletion\x0a\x09^ self model availablePackageNames",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["availablePackageNames", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._availablePackageNames();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inputCompletion",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMoveClassToPackageCommand);

$core.addMethod(
$core.method({
selector: "inputLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inputLabel\x0a\x09^ 'Move class to package:'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Move class to package:";

}; }),
$globals.HLMoveClassToPackageCommand);

$core.addMethod(
$core.method({
selector: "isInputRequired",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isInputRequired\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLMoveClassToPackageCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'p'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "p";

}; }),
$globals.HLMoveClassToPackageCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Move class to package'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Move class to package";

}; }),
$globals.HLMoveClassToPackageCommand.a$cls);

$core.addMethod(
$core.method({
selector: "menuLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "menuLabel\x09\x0a\x09^ 'Move to package...'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Move to package...";

}; }),
$globals.HLMoveClassToPackageCommand.a$cls);


$core.addClass("HLMoveMethodToCommand", $globals.HLMoveToCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "category",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "category\x0a\x09^ 'Methods'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Methods";

}; }),
$globals.HLMoveMethodToCommand);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ self model selectedMethod notNil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["notNil", "selectedMethod", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._model())._selectedMethod())._notNil();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMoveMethodToCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'm'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "m";

}; }),
$globals.HLMoveMethodToCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Move method'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Move method";

}; }),
$globals.HLMoveMethodToCommand.a$cls);


$core.addClass("HLMoveMethodToClassCommand", $globals.HLMoveMethodToCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "displayLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "displayLabel\x0a\x09^ 'select a class'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "select a class";

}; }),
$globals.HLMoveMethodToClassCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model moveMethodToClass: self input",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["moveMethodToClass:", "model", "input"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._moveMethodToClass_($self._input());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMoveMethodToClassCommand);

$core.addMethod(
$core.method({
selector: "inputCompletion",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inputCompletion\x0a\x09^ self model availableClassNames",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["availableClassNames", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._availableClassNames();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inputCompletion",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMoveMethodToClassCommand);

$core.addMethod(
$core.method({
selector: "inputLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inputLabel\x0a\x09^ 'Move method to class:'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Move method to class:";

}; }),
$globals.HLMoveMethodToClassCommand);

$core.addMethod(
$core.method({
selector: "isInputRequired",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isInputRequired\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLMoveMethodToClassCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'c'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "c";

}; }),
$globals.HLMoveMethodToClassCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x09\x0a\x09^ 'Move method to class'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Move method to class";

}; }),
$globals.HLMoveMethodToClassCommand.a$cls);

$core.addMethod(
$core.method({
selector: "menuLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "menuLabel\x09\x0a\x09^ 'Move to class...'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Move to class...";

}; }),
$globals.HLMoveMethodToClassCommand.a$cls);


$core.addClass("HLMoveMethodToProtocolCommand", $globals.HLMoveMethodToCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "displayLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "displayLabel\x0a\x09^ 'select a protocol'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "select a protocol";

}; }),
$globals.HLMoveMethodToProtocolCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model moveMethodToProtocol: self input",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["moveMethodToProtocol:", "model", "input"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._moveMethodToProtocol_($self._input());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMoveMethodToProtocolCommand);

$core.addMethod(
$core.method({
selector: "inputCompletion",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inputCompletion\x0a\x09^ self model availableProtocols",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["availableProtocols", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._availableProtocols();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inputCompletion",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMoveMethodToProtocolCommand);

$core.addMethod(
$core.method({
selector: "inputLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inputLabel\x0a\x09^ 'Move method to a protocol:'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Move method to a protocol:";

}; }),
$globals.HLMoveMethodToProtocolCommand);

$core.addMethod(
$core.method({
selector: "isInputRequired",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isInputRequired\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLMoveMethodToProtocolCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 't'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "t";

}; }),
$globals.HLMoveMethodToProtocolCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Move method to protocol'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Move method to protocol";

}; }),
$globals.HLMoveMethodToProtocolCommand.a$cls);

$core.addMethod(
$core.method({
selector: "menuLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "menuLabel\x0a\x09^ 'Move to protocol...'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Move to protocol...";

}; }),
$globals.HLMoveMethodToProtocolCommand.a$cls);


$core.addClass("HLRemoveCommand", $globals.HLToolCommand, "Helios-Commands-Tools");

$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'x'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "x";

}; }),
$globals.HLRemoveCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Remove'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Remove";

}; }),
$globals.HLRemoveCommand.a$cls);


$core.addClass("HLRemoveClassCommand", $globals.HLRemoveCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "category",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "category\x0a\x09^ 'Classes'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Classes";

}; }),
$globals.HLRemoveClassCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model removeClass",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["removeClass", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._removeClass();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRemoveClassCommand);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ self model selectedClass notNil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["notNil", "selectedClass", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._model())._selectedClass())._notNil();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRemoveClassCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'c'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "c";

}; }),
$globals.HLRemoveClassCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Remove class'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Remove class";

}; }),
$globals.HLRemoveClassCommand.a$cls);

$core.addMethod(
$core.method({
selector: "menuLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "menuLabel\x0a\x09^ 'Remove class'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Remove class";

}; }),
$globals.HLRemoveClassCommand.a$cls);


$core.addClass("HLRemoveMethodCommand", $globals.HLRemoveCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "category",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "category\x0a\x09^ 'Methods'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Methods";

}; }),
$globals.HLRemoveMethodCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model removeMethod",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["removeMethod", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._removeMethod();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRemoveMethodCommand);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ self model selectedMethod notNil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["notNil", "selectedMethod", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._model())._selectedMethod())._notNil();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRemoveMethodCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'm'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "m";

}; }),
$globals.HLRemoveMethodCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Remove method'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Remove method";

}; }),
$globals.HLRemoveMethodCommand.a$cls);

$core.addMethod(
$core.method({
selector: "menuLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "menuLabel\x0a\x09^ 'Remove method'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Remove method";

}; }),
$globals.HLRemoveMethodCommand.a$cls);


$core.addClass("HLRemoveProtocolCommand", $globals.HLRemoveCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "category",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "category\x0a\x09^ 'Protocols'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Protocols";

}; }),
$globals.HLRemoveProtocolCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model removeProtocol",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["removeProtocol", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._removeProtocol();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRemoveProtocolCommand);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ self model selectedProtocol notNil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["notNil", "selectedProtocol", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._model())._selectedProtocol())._notNil();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRemoveProtocolCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 't'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "t";

}; }),
$globals.HLRemoveProtocolCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Remove protocol'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Remove protocol";

}; }),
$globals.HLRemoveProtocolCommand.a$cls);

$core.addMethod(
$core.method({
selector: "menuLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "menuLabel\x0a\x09^ 'Remove protocol'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Remove protocol";

}; }),
$globals.HLRemoveProtocolCommand.a$cls);


$core.addClass("HLRenameCommand", $globals.HLToolCommand, "Helios-Commands-Tools");

$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'r'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "r";

}; }),
$globals.HLRenameCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Rename'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Rename";

}; }),
$globals.HLRenameCommand.a$cls);


$core.addClass("HLRenameClassCommand", $globals.HLRenameCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "category",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "category\x0a\x09^ 'Classes'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Classes";

}; }),
$globals.HLRenameClassCommand);

$core.addMethod(
$core.method({
selector: "defaultInput",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "defaultInput\x0a\x09^ self model selectedClass theNonMetaClass name",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["name", "theNonMetaClass", "selectedClass", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($recv($self._model())._selectedClass())._theNonMetaClass())._name();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"defaultInput",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRenameClassCommand);

$core.addMethod(
$core.method({
selector: "displayLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "displayLabel\x0a\x09^ 'Rename class to:'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Rename class to:";

}; }),
$globals.HLRenameClassCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model renameClassTo: self input",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["renameClassTo:", "model", "input"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._renameClassTo_($self._input());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRenameClassCommand);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ self model selectedClass notNil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["notNil", "selectedClass", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._model())._selectedClass())._notNil();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRenameClassCommand);

$core.addMethod(
$core.method({
selector: "isInputRequired",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isInputRequired\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLRenameClassCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'c'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "c";

}; }),
$globals.HLRenameClassCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Rename class'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Rename class";

}; }),
$globals.HLRenameClassCommand.a$cls);

$core.addMethod(
$core.method({
selector: "menuLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "menuLabel\x0a\x09^ 'Rename class...'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Rename class...";

}; }),
$globals.HLRenameClassCommand.a$cls);


$core.addClass("HLRenamePackageCommand", $globals.HLRenameCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "category",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "category\x0a\x09^ 'Packages'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Packages";

}; }),
$globals.HLRenamePackageCommand);

$core.addMethod(
$core.method({
selector: "defaultInput",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "defaultInput\x0a\x09^ self model selectedPackage name",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["name", "selectedPackage", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._model())._selectedPackage())._name();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"defaultInput",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRenamePackageCommand);

$core.addMethod(
$core.method({
selector: "displayLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "displayLabel\x0a\x09^ 'Rename package to:'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Rename package to:";

}; }),
$globals.HLRenamePackageCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model renamePackageTo: self input",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["renamePackageTo:", "model", "input"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._renamePackageTo_($self._input());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRenamePackageCommand);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ self model selectedPackage notNil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["notNil", "selectedPackage", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._model())._selectedPackage())._notNil();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRenamePackageCommand);

$core.addMethod(
$core.method({
selector: "isInputRequired",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isInputRequired\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLRenamePackageCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'p'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "p";

}; }),
$globals.HLRenamePackageCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Rename package'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Rename package";

}; }),
$globals.HLRenamePackageCommand.a$cls);

$core.addMethod(
$core.method({
selector: "menuLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "menuLabel\x0a\x09^ 'Rename package...'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Rename package...";

}; }),
$globals.HLRenamePackageCommand.a$cls);


$core.addClass("HLRenameProtocolCommand", $globals.HLRenameCommand, "Helios-Commands-Tools");
$core.addMethod(
$core.method({
selector: "category",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "category\x0a\x09^ 'Protocols'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Protocols";

}; }),
$globals.HLRenameProtocolCommand);

$core.addMethod(
$core.method({
selector: "defaultInput",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "defaultInput\x0a\x09^ self model selectedProtocol",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectedProtocol", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._selectedProtocol();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"defaultInput",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRenameProtocolCommand);

$core.addMethod(
$core.method({
selector: "displayLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "displayLabel\x0a\x09^ 'Rename protocol to:'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Rename protocol to:";

}; }),
$globals.HLRenameProtocolCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model renameProtocolTo: self input",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["renameProtocolTo:", "model", "input"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._renameProtocolTo_($self._input());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRenameProtocolCommand);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ self model selectedProtocol notNil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["notNil", "selectedProtocol", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._model())._selectedProtocol())._notNil();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLRenameProtocolCommand);

$core.addMethod(
$core.method({
selector: "isInputRequired",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isInputRequired\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLRenameProtocolCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 't'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "t";

}; }),
$globals.HLRenameProtocolCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Rename protocol'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Rename protocol";

}; }),
$globals.HLRenameProtocolCommand.a$cls);

$core.addMethod(
$core.method({
selector: "menuLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "menuLabel\x0a\x09^ 'Rename protocol...'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Rename protocol...";

}; }),
$globals.HLRenameProtocolCommand.a$cls);

});
