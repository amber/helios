define(["amber/boot", "require", "helios/Helios-Commands-Tools"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-Commands-SUnit");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLSUnitCommand", $globals.HLToolCommand, "Helios-Commands-SUnit");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnitCommand.comment="I group the commands pertaining to Helios-SUnit (`HLSUnitModel`)";
//>>excludeEnd("ide");

$core.addMethod(
$core.method({
selector: "isValidFor:",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aModel"],
source: "isValidFor: aModel\x0a\x09^ aModel isKindOf: HLSUnitModel",
referencedClasses: ["HLSUnitModel"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["isKindOf:"]
}, function ($methodClass){ return function (aModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv(aModel)._isKindOf_($globals.HLSUnitModel);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isValidFor:",{aModel:aModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitCommand.a$cls);


$core.addClass("HLSUnitInvertSelectedCommand", $globals.HLSUnitCommand, "Helios-Commands-SUnit");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnitInvertSelectedCommand.comment="I group the commands that invert selections";
//>>excludeEnd("ide");

$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'i'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "i";

}; }),
$globals.HLSUnitInvertSelectedCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^'Invert selection'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Invert selection";

}; }),
$globals.HLSUnitInvertSelectedCommand.a$cls);


$core.addClass("HLSUnitInvertSelectedClassesCommand", $globals.HLSUnitInvertSelectedCommand, "Helios-Commands-SUnit");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnitInvertSelectedClassesCommand.comment="Invert the currently selected classes on a `HLSUnitModel`";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "category",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "category\x0a\x09^'Classes'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Classes";

}; }),
$globals.HLSUnitInvertSelectedClassesCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model invertSelectedClasses",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["invertSelectedClasses", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._invertSelectedClasses();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitInvertSelectedClassesCommand);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^model selectedPackages notEmpty",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["notEmpty", "selectedPackages"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self.model)._selectedPackages())._notEmpty();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitInvertSelectedClassesCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'c'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "c";

}; }),
$globals.HLSUnitInvertSelectedClassesCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Invert selected classes'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Invert selected classes";

}; }),
$globals.HLSUnitInvertSelectedClassesCommand.a$cls);


$core.addClass("HLSUnitInvertSelectedPackagesCommand", $globals.HLSUnitInvertSelectedCommand, "Helios-Commands-SUnit");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnitInvertSelectedPackagesCommand.comment="Invert the currently selected packages on a `HLSUnitModel`";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "category",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "category\x0a\x09^'Packages'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Packages";

}; }),
$globals.HLSUnitInvertSelectedPackagesCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model invertSelectedPackages",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["invertSelectedPackages", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._invertSelectedPackages();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitInvertSelectedPackagesCommand);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLSUnitInvertSelectedPackagesCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'p'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "p";

}; }),
$globals.HLSUnitInvertSelectedPackagesCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Invert selected packages'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Invert selected packages";

}; }),
$globals.HLSUnitInvertSelectedPackagesCommand.a$cls);


$core.addClass("HLSUnitRunTests", $globals.HLSUnitCommand, "Helios-Commands-SUnit");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnitRunTests.comment="Run the test cases in the currently selected classes on a `HLSUnitModel`";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model runTests",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["runTests", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._runTests();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitRunTests);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLSUnitRunTests);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^'r'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "r";

}; }),
$globals.HLSUnitRunTests.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^'Run Tests'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Run Tests";

}; }),
$globals.HLSUnitRunTests.a$cls);


$core.addClass("HLSUnitSelectAllCommand", $globals.HLSUnitCommand, "Helios-Commands-SUnit");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnitSelectAllCommand.comment="I group the select all commands";
//>>excludeEnd("ide");

$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'a'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "a";

}; }),
$globals.HLSUnitSelectAllCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Select all'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Select all";

}; }),
$globals.HLSUnitSelectAllCommand.a$cls);


$core.addClass("HLSUnitSelectAllClassesCommand", $globals.HLSUnitSelectAllCommand, "Helios-Commands-SUnit");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnitSelectAllClassesCommand.comment="Select all available test classes based on what packages are selected on a `HLSUnitModel`";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "category",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "category\x0a\x09^'Classes'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Classes";

}; }),
$globals.HLSUnitSelectAllClassesCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model selectAllClasses",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectAllClasses", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._selectAllClasses();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitSelectAllClassesCommand);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^model selectedPackages notEmpty",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["notEmpty", "selectedPackages"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self.model)._selectedPackages())._notEmpty();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isActive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitSelectAllClassesCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'c'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "c";

}; }),
$globals.HLSUnitSelectAllClassesCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Select all classes'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Select all classes";

}; }),
$globals.HLSUnitSelectAllClassesCommand.a$cls);


$core.addClass("HLSUnitSelectAllPackagesCommand", $globals.HLSUnitSelectAllCommand, "Helios-Commands-SUnit");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLSUnitSelectAllPackagesCommand.comment="Select all packages with test cases on a `HLSUnitModel`";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "category",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "category\x0a\x09^'Packages'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Packages";

}; }),
$globals.HLSUnitSelectAllPackagesCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09self model selectAllPackages",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectAllPackages", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._selectAllPackages();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSUnitSelectAllPackagesCommand);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLSUnitSelectAllPackagesCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'p'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "p";

}; }),
$globals.HLSUnitSelectAllPackagesCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Select all packages'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Select all packages";

}; }),
$globals.HLSUnitSelectAllPackagesCommand.a$cls);

});
