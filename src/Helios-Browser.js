define(["amber/boot", "require", "amber/core/Kernel-Objects", "helios/Helios-Core"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-Browser");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLBrowser", $globals.HLWidget, "Helios-Browser");
$core.setSlots($globals.HLBrowser, ["model", "packagesListWidget", "classesListWidget", "protocolsListWidget", "methodsListWidget", "sourceWidget", "bottomDiv"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLBrowser.comment="I render a system browser with 4 panes (Packages, Classes, Protocols, Methods) and a source area.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "canHaveFocus",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "canHaveFocus\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLBrowser);

$core.addMethod(
$core.method({
selector: "classesListWidget",
protocol: "widgets",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "classesListWidget\x0a\x09^ classesListWidget ifNil: [\x0a      \x09classesListWidget := HLClassesListWidget on: self model.\x0a\x09\x09classesListWidget next: self protocolsListWidget ]",
referencedClasses: ["HLClassesListWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "on:", "model", "next:", "protocolsListWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.classesListWidget;
if($1 == null || $1.a$nil){
$self.classesListWidget=$recv($globals.HLClassesListWidget)._on_($self._model());
return $recv($self.classesListWidget)._next_($self._protocolsListWidget());
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"classesListWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser);

$core.addMethod(
$core.method({
selector: "environment",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "environment\x0a\x09^ self model environment",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["environment", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._environment();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"environment",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser);

$core.addMethod(
$core.method({
selector: "focus",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focus\x0a\x09^ self packagesListWidget focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focus", "packagesListWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._packagesListWidget())._focus();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focus",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser);

$core.addMethod(
$core.method({
selector: "methodsListWidget",
protocol: "widgets",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "methodsListWidget\x0a\x09^ methodsListWidget ifNil: [\x0a      \x09methodsListWidget := HLMethodsListWidget on: self model.\x0a\x09\x09methodsListWidget next: self sourceWidget ]",
referencedClasses: ["HLMethodsListWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "on:", "model", "next:", "sourceWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.methodsListWidget;
if($1 == null || $1.a$nil){
$self.methodsListWidget=$recv($globals.HLMethodsListWidget)._on_($self._model());
return $recv($self.methodsListWidget)._next_($self._sourceWidget());
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"methodsListWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser);

$core.addMethod(
$core.method({
selector: "model",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "model\x0a\x09^ model ifNil: [ self model: HLBrowserModel new. model ]",
referencedClasses: ["HLBrowserModel"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "model:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.model;
if($1 == null || $1.a$nil){
$self._model_($recv($globals.HLBrowserModel)._new());
return $self.model;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"model",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser);

$core.addMethod(
$core.method({
selector: "model:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aModel"],
source: "model: aModel\x0a\x09model := aModel.\x0a\x09self observeModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["observeModel"]
}, function ($methodClass){ return function (aModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.model=aModel;
$self._observeModel();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"model:",{aModel:aModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser);

$core.addMethod(
$core.method({
selector: "observeModel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeModel\x0a\x09self model announcer\x0a\x09\x09on: HLPackageSelected\x0a\x09\x09send: #onPackageSelected:\x0a\x09\x09to: self.\x0a\x09\x09\x0a\x09self model announcer\x0a\x09\x09on: HLClassSelected\x0a\x09\x09send: #onClassSelected:\x0a\x09\x09to: self",
referencedClasses: ["HLPackageSelected", "HLClassSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$recv([$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._announcer()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["announcer"]=1
//>>excludeEnd("ctx");
][0])._on_send_to_($globals.HLPackageSelected,"onPackageSelected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
$recv($recv($self._model())._announcer())._on_send_to_($globals.HLClassSelected,"onClassSelected:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser);

$core.addMethod(
$core.method({
selector: "onClassSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onClassSelected: anAnnouncement\x0a\x09anAnnouncement item \x0a\x09\x09ifNil: [ self setTabLabel: (self model selectedPackage \x0a\x09\x09\x09ifNil: [ self defaultTabLabel ]\x0a\x09\x09\x09ifNotNil: [ :package | package name ]) ] \x0a\x09\x09ifNotNil: [ :item | self setTabLabel: item name ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:ifNotNil:", "item", "setTabLabel:", "selectedPackage", "model", "defaultTabLabel", "name"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3;
$1=$recv(anAnnouncement)._item();
if($1 == null || $1.a$nil){
$2=$recv($self._model())._selectedPackage();
if($2 == null || $2.a$nil){
$3=$self._defaultTabLabel();
} else {
var package_;
package_=$2;
$3=[$recv(package_)._name()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["name"]=1
//>>excludeEnd("ctx");
][0];
}
[$self._setTabLabel_($3)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["setTabLabel:"]=1
//>>excludeEnd("ctx");
][0];
} else {
var item;
item=$1;
$self._setTabLabel_($recv(item)._name());
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassSelected:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser);

$core.addMethod(
$core.method({
selector: "onPackageSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onPackageSelected: anAnnouncement\x0a\x09anAnnouncement item ifNotNil: [ :item |\x0a\x09self setTabLabel: item name ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "item", "setTabLabel:", "name"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(anAnnouncement)._item();
if($1 == null || $1.a$nil){
$1;
} else {
var item;
item=$1;
$self._setTabLabel_($recv(item)._name());
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onPackageSelected:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser);

$core.addMethod(
$core.method({
selector: "openClassNamed:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "openClassNamed: aString\x0a\x09self model openClassNamed: aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["openClassNamed:", "model"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._openClassNamed_(aString);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"openClassNamed:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser);

$core.addMethod(
$core.method({
selector: "openMethod:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCompiledMethod"],
source: "openMethod: aCompiledMethod\x0a\x09self model\x0a\x09\x09\x22Workaround for the package selection announcement when the package list is focused\x22\x09\x0a\x09\x09focusOnSourceCode;\x0a\x09\x09selectedPackage: aCompiledMethod methodClass package;\x0a\x09\x09showInstance: aCompiledMethod methodClass isMetaclass not;\x0a\x09\x09selectedClass: aCompiledMethod methodClass;\x0a\x09\x09selectedProtocol: aCompiledMethod protocol;\x0a\x09\x09selectedMethod: aCompiledMethod;\x0a\x09\x09focusOnSourceCode",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focusOnSourceCode", "model", "selectedPackage:", "package", "methodClass", "showInstance:", "not", "isMetaclass", "selectedClass:", "selectedProtocol:", "protocol", "selectedMethod:"]
}, function ($methodClass){ return function (aCompiledMethod){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._model();
[$recv($1)._focusOnSourceCode()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["focusOnSourceCode"]=1
//>>excludeEnd("ctx");
][0];
$recv($1)._selectedPackage_($recv([$recv(aCompiledMethod)._methodClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["methodClass"]=1
//>>excludeEnd("ctx");
][0])._package());
$recv($1)._showInstance_($recv($recv([$recv(aCompiledMethod)._methodClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["methodClass"]=2
//>>excludeEnd("ctx");
][0])._isMetaclass())._not());
$recv($1)._selectedClass_($recv(aCompiledMethod)._methodClass());
$recv($1)._selectedProtocol_($recv(aCompiledMethod)._protocol());
$recv($1)._selectedMethod_(aCompiledMethod);
$recv($1)._focusOnSourceCode();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"openMethod:",{aCompiledMethod:aCompiledMethod})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser);

$core.addMethod(
$core.method({
selector: "packagesListWidget",
protocol: "widgets",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "packagesListWidget\x0a\x09^ packagesListWidget ifNil: [\x0a      \x09packagesListWidget := HLPackagesListWidget on: self model.\x0a\x09\x09packagesListWidget next: self classesListWidget ]",
referencedClasses: ["HLPackagesListWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "on:", "model", "next:", "classesListWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.packagesListWidget;
if($1 == null || $1.a$nil){
$self.packagesListWidget=$recv($globals.HLPackagesListWidget)._on_($self._model());
return $recv($self.packagesListWidget)._next_($self._classesListWidget());
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"packagesListWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser);

$core.addMethod(
$core.method({
selector: "protocolsListWidget",
protocol: "widgets",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "protocolsListWidget\x0a\x09^ protocolsListWidget ifNil: [\x0a      \x09protocolsListWidget := HLProtocolsListWidget on: self model.\x0a\x09\x09protocolsListWidget next: self methodsListWidget ]",
referencedClasses: ["HLProtocolsListWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "on:", "model", "next:", "methodsListWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.protocolsListWidget;
if($1 == null || $1.a$nil){
$self.protocolsListWidget=$recv($globals.HLProtocolsListWidget)._on_($self._model());
return $recv($self.protocolsListWidget)._next_($self._methodsListWidget());
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"protocolsListWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser);

$core.addMethod(
$core.method({
selector: "registerBindingsOn:",
protocol: "keybindings",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBindingGroup"],
source: "registerBindingsOn: aBindingGroup\x0a\x09HLToolCommand \x0a\x09\x09registerConcreteClassesOn: aBindingGroup \x0a\x09\x09for: self model",
referencedClasses: ["HLToolCommand"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["registerConcreteClassesOn:for:", "model"]
}, function ($methodClass){ return function (aBindingGroup){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($globals.HLToolCommand)._registerConcreteClassesOn_for_(aBindingGroup,$self._model());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"registerBindingsOn:",{aBindingGroup:aBindingGroup})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09html with: (HLContainer with: (HLHorizontalSplitter \x0a    \x09with: (HLVerticalSplitter\x0a        \x09with: (HLVerticalSplitter\x0a            \x09with: self packagesListWidget\x0a                with: self classesListWidget)\x0a            with: (HLVerticalSplitter\x0a            \x09with: self protocolsListWidget\x0a                with: self methodsListWidget)) \x0a        with: self sourceWidget)).\x0a\x09\x0a\x09self packagesListWidget focus",
referencedClasses: ["HLContainer", "HLHorizontalSplitter", "HLVerticalSplitter"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "with:with:", "packagesListWidget", "classesListWidget", "protocolsListWidget", "methodsListWidget", "sourceWidget", "focus"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$recv(html)._with_($recv($globals.HLContainer)._with_([$recv($globals.HLHorizontalSplitter)._with_with_([$recv($globals.HLVerticalSplitter)._with_with_([$recv($globals.HLVerticalSplitter)._with_with_([$self._packagesListWidget()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["packagesListWidget"]=1
//>>excludeEnd("ctx");
][0],$self._classesListWidget())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:with:"]=3
//>>excludeEnd("ctx");
][0],$recv($globals.HLVerticalSplitter)._with_with_($self._protocolsListWidget(),$self._methodsListWidget()))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:with:"]=2
//>>excludeEnd("ctx");
][0],$self._sourceWidget())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:with:"]=1
//>>excludeEnd("ctx");
][0]))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
$recv($self._packagesListWidget())._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser);

$core.addMethod(
$core.method({
selector: "sourceWidget",
protocol: "widgets",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "sourceWidget\x0a\x09^ sourceWidget ifNil: [\x0a      \x09sourceWidget := HLBrowserBottomWidget new\x0a\x09\x09\x09model: self model;\x0a\x09\x09\x09yourself ]",
referencedClasses: ["HLBrowserBottomWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "model:", "new", "model", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$self.sourceWidget;
if($1 == null || $1.a$nil){
$2=$recv($globals.HLBrowserBottomWidget)._new();
$recv($2)._model_($self._model());
$self.sourceWidget=$recv($2)._yourself();
return $self.sourceWidget;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"sourceWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser);

$core.addMethod(
$core.method({
selector: "unregister",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "unregister\x0a\x09super unregister.\x0a\x0a\x09{ \x0a\x09\x09self packagesListWidget.\x0a\x09\x09self classesListWidget.\x0a\x09\x09self protocolsListWidget.\x0a\x09\x09self methodsListWidget.\x0a\x09\x09self sourceWidget\x0a\x09} \x0a\x09\x09do: [ :each | each unregister ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unregister", "do:", "packagesListWidget", "classesListWidget", "protocolsListWidget", "methodsListWidget", "sourceWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._unregister.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["unregister"]=1,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv([$self._packagesListWidget(),$self._classesListWidget(),$self._protocolsListWidget(),$self._methodsListWidget(),$self._sourceWidget()])._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._unregister();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unregister",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser);


$core.setSlots($globals.HLBrowser.a$cls, ["nextId"]);
$core.addMethod(
$core.method({
selector: "canBeOpenAsTab",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "canBeOpenAsTab\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLBrowser.a$cls);

$core.addMethod(
$core.method({
selector: "nextId",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "nextId\x0a\x09nextId ifNil: [ nextId := 0 ].\x0a    ^ 'browser_', (nextId + 1) asString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", ",", "asString", "+"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.nextId;
if($1 == null || $1.a$nil){
$self.nextId=(0);
$self.nextId;
} else {
$1;
}
return "browser_".__comma($recv($recv($self.nextId).__plus((1)))._asString());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"nextId",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowser.a$cls);

$core.addMethod(
$core.method({
selector: "tabClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabClass\x0a\x09^ 'browser'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "browser";

}; }),
$globals.HLBrowser.a$cls);

$core.addMethod(
$core.method({
selector: "tabLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabLabel\x0a\x09^ 'Browser'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Browser";

}; }),
$globals.HLBrowser.a$cls);

$core.addMethod(
$core.method({
selector: "tabPriority",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabPriority\x0a\x09^ 0",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return (0);

}; }),
$globals.HLBrowser.a$cls);


$core.addClass("HLBrowserBottomWidget", $globals.HLWidget, "Helios-Browser");
$core.setSlots($globals.HLBrowserBottomWidget, ["model", "codeWidget", "documentationWidget"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLBrowserBottomWidget.comment="I render the code area of a browser and optionally the documentation for the selected class.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "canHaveFocus",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "canHaveFocus\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLBrowserBottomWidget);

$core.addMethod(
$core.method({
selector: "codeWidget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "codeWidget\x0a\x09^ codeWidget ifNil: [ codeWidget := HLBrowserCodeWidget new\x0a\x09\x09browserModel: self model;\x0a\x09\x09yourself ]",
referencedClasses: ["HLBrowserCodeWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "browserModel:", "new", "model", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$self.codeWidget;
if($1 == null || $1.a$nil){
$2=$recv($globals.HLBrowserCodeWidget)._new();
$recv($2)._browserModel_($self._model());
$self.codeWidget=$recv($2)._yourself();
return $self.codeWidget;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"codeWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserBottomWidget);

$core.addMethod(
$core.method({
selector: "documentationWidget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "documentationWidget\x0a\x09^ documentationWidget ifNil: [ documentationWidget := HLDocumentationWidget new\x0a\x09\x09model: self model;\x0a\x09\x09yourself ]",
referencedClasses: ["HLDocumentationWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "model:", "new", "model", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$self.documentationWidget;
if($1 == null || $1.a$nil){
$2=$recv($globals.HLDocumentationWidget)._new();
$recv($2)._model_($self._model());
$self.documentationWidget=$recv($2)._yourself();
return $self.documentationWidget;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"documentationWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserBottomWidget);

$core.addMethod(
$core.method({
selector: "focus",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focus\x0a\x09self codeWidget focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focus", "codeWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._codeWidget())._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focus",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserBottomWidget);

$core.addMethod(
$core.method({
selector: "model",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "model\x0a\x09^ model",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.model;

}; }),
$globals.HLBrowserBottomWidget);

$core.addMethod(
$core.method({
selector: "model:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aModel"],
source: "model: aModel\x0a\x09model := aModel.\x0a\x09self observeModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["observeModel"]
}, function ($methodClass){ return function (aModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.model=aModel;
$self._observeModel();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"model:",{aModel:aModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserBottomWidget);

$core.addMethod(
$core.method({
selector: "observeModel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeModel\x0a\x09self model announcer \x0a\x09\x09on: HLShowInstanceToggled\x0a\x09\x09send: #onShowInstanceToggled\x0a\x09\x09to: self;\x0a\x09\x09on: HLShowCommentToggled\x0a\x09\x09send: #onShowCommentToggled\x0a\x09\x09to: self",
referencedClasses: ["HLShowInstanceToggled", "HLShowCommentToggled"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._model())._announcer();
[$recv($1)._on_send_to_($globals.HLShowInstanceToggled,"onShowInstanceToggled",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
$recv($1)._on_send_to_($globals.HLShowCommentToggled,"onShowCommentToggled",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserBottomWidget);

$core.addMethod(
$core.method({
selector: "onShowCommentToggled",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onShowCommentToggled\x0a\x09self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["refresh"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onShowCommentToggled",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserBottomWidget);

$core.addMethod(
$core.method({
selector: "onShowInstanceToggled",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onShowInstanceToggled\x0a\x09self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["refresh"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onShowInstanceToggled",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserBottomWidget);

$core.addMethod(
$core.method({
selector: "previous",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "previous\x0a\x09\x22For navigation\x22",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return self;

}; }),
$globals.HLBrowserBottomWidget);

$core.addMethod(
$core.method({
selector: "previous:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aWidget"],
source: "previous: aWidget\x0a\x09\x22For navigation\x22",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aWidget){
var self=this,$self=this;
return self;

}; }),
$globals.HLBrowserBottomWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09self model showComment \x0a\x09\x09ifTrue: [ self renderPanesOn: html ]\x0a\x09\x09ifFalse: [ html with: self codeWidget ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:ifFalse:", "showComment", "model", "renderPanesOn:", "with:", "codeWidget"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv($self._model())._showComment())){
$self._renderPanesOn_(html);
} else {
$recv(html)._with_($self._codeWidget());
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserBottomWidget);

$core.addMethod(
$core.method({
selector: "renderPanesOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderPanesOn: html\x0a\x09html with: (HLVerticalSplitter\x0a\x09\x09with: self codeWidget\x0a\x09\x09with: self documentationWidget)",
referencedClasses: ["HLVerticalSplitter"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "with:with:", "codeWidget", "documentationWidget"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(html)._with_($recv($globals.HLVerticalSplitter)._with_with_($self._codeWidget(),$self._documentationWidget()));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderPanesOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserBottomWidget);



$core.addClass("HLBrowserModel", $globals.HLToolModel, "Helios-Browser");
$core.setSlots($globals.HLBrowserModel, ["showInstance", "showComment"]);
$core.addMethod(
$core.method({
selector: "editComment",
protocol: "commands actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "editComment\x0a\x09self announcer announce: HLEditComment new",
referencedClasses: ["HLEditComment"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._announcer())._announce_($recv($globals.HLEditComment)._new());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"editComment",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserModel);

$core.addMethod(
$core.method({
selector: "focusOnClasses",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focusOnClasses\x0a\x09self announcer announce: HLClassesFocusRequested new",
referencedClasses: ["HLClassesFocusRequested"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._announcer())._announce_($recv($globals.HLClassesFocusRequested)._new());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focusOnClasses",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserModel);

$core.addMethod(
$core.method({
selector: "focusOnDocumentation",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focusOnDocumentation\x0a\x09self announcer announce: HLDocumentationFocusRequested new",
referencedClasses: ["HLDocumentationFocusRequested"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._announcer())._announce_($recv($globals.HLDocumentationFocusRequested)._new());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focusOnDocumentation",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserModel);

$core.addMethod(
$core.method({
selector: "focusOnMethods",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focusOnMethods\x0a\x09self announcer announce: HLMethodsFocusRequested new",
referencedClasses: ["HLMethodsFocusRequested"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._announcer())._announce_($recv($globals.HLMethodsFocusRequested)._new());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focusOnMethods",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserModel);

$core.addMethod(
$core.method({
selector: "focusOnPackages",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focusOnPackages\x0a\x09self announcer announce: HLPackagesFocusRequested new",
referencedClasses: ["HLPackagesFocusRequested"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._announcer())._announce_($recv($globals.HLPackagesFocusRequested)._new());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focusOnPackages",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserModel);

$core.addMethod(
$core.method({
selector: "focusOnProtocols",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focusOnProtocols\x0a\x09self announcer announce: HLProtocolsFocusRequested new",
referencedClasses: ["HLProtocolsFocusRequested"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._announcer())._announce_($recv($globals.HLProtocolsFocusRequested)._new());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focusOnProtocols",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserModel);

$core.addMethod(
$core.method({
selector: "focusOnSourceCode",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focusOnSourceCode\x0a\x09self announcer announce: HLSourceCodeFocusRequested new",
referencedClasses: ["HLSourceCodeFocusRequested"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._announcer())._announce_($recv($globals.HLSourceCodeFocusRequested)._new());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focusOnSourceCode",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserModel);

$core.addMethod(
$core.method({
selector: "isBrowserModel",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isBrowserModel\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLBrowserModel);

$core.addMethod(
$core.method({
selector: "setClassComment:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "setClassComment: aString\x0a\x09self environment\x0a\x09\x09setClassCommentOf: self selectedClass theNonMetaClass\x0a\x09\x09to: aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["setClassCommentOf:to:", "environment", "theNonMetaClass", "selectedClass"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._environment())._setClassCommentOf_to_($recv($self._selectedClass())._theNonMetaClass(),aString);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setClassComment:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserModel);

$core.addMethod(
$core.method({
selector: "showClassTemplate",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "showClassTemplate\x0a\x09self selectedPackage ifNotNil: [ :package |\x0a\x09\x09self announcer announce: (HLShowTemplate new\x0a\x09\x09\x09template: package classTemplate;\x0a\x09\x09\x09yourself) ]",
referencedClasses: ["HLShowTemplate"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "selectedPackage", "announce:", "announcer", "template:", "new", "classTemplate", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3;
$1=$self._selectedPackage();
if($1 == null || $1.a$nil){
$1;
} else {
var package_;
package_=$1;
$2=$self._announcer();
$3=$recv($globals.HLShowTemplate)._new();
$recv($3)._template_($recv(package_)._classTemplate());
$recv($2)._announce_($recv($3)._yourself());
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"showClassTemplate",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserModel);

$core.addMethod(
$core.method({
selector: "showComment",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "showComment\x0a\x09^ showComment ifNil: [ \x0a\x09\x09showComment := 'helios.browser.showComment' settingValueIfAbsent: false ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "settingValueIfAbsent:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.showComment;
if($1 == null || $1.a$nil){
$self.showComment="helios.browser.showComment"._settingValueIfAbsent_(false);
return $self.showComment;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"showComment",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserModel);

$core.addMethod(
$core.method({
selector: "showComment:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBoolean"],
source: "showComment: aBoolean\x0a\x09self withChangesDo: [\x0a\x09\x09showComment := aBoolean.\x0a\x09\x09'helios.browser.showComment' settingValue: aBoolean.\x0a\x09\x09self announcer announce: HLShowCommentToggled new ]",
referencedClasses: ["HLShowCommentToggled"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "settingValue:", "announce:", "announcer", "new"]
}, function ($methodClass){ return function (aBoolean){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$self.showComment=aBoolean;
"helios.browser.showComment"._settingValue_(aBoolean);
return $recv($self._announcer())._announce_($recv($globals.HLShowCommentToggled)._new());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"showComment:",{aBoolean:aBoolean})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserModel);

$core.addMethod(
$core.method({
selector: "showInstance",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "showInstance\x0a\x09^ showInstance ifNil: [ true ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.showInstance;
if($1 == null || $1.a$nil){
return true;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"showInstance",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserModel);

$core.addMethod(
$core.method({
selector: "showInstance:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBoolean"],
source: "showInstance: aBoolean\x0a\x0a\x09self withChangesDo: [\x0a\x09\x09showInstance := aBoolean.\x0a\x0a    \x09self selectedClass ifNotNil: [\x0a    \x09\x09self selectedClass: (aBoolean\x0a    \x09\x09\x09ifTrue: [ self selectedClass theNonMetaClass ]\x0a\x09    \x09  \x09ifFalse: [ self selectedClass theMetaClass ]) ].\x0a    \x0a\x09\x09self announcer announce: HLShowInstanceToggled new ]",
referencedClasses: ["HLShowInstanceToggled"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["withChangesDo:", "ifNotNil:", "selectedClass", "selectedClass:", "ifTrue:ifFalse:", "theNonMetaClass", "theMetaClass", "announce:", "announcer", "new"]
}, function ($methodClass){ return function (aBoolean){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$self._withChangesDo_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$self.showInstance=aBoolean;
$1=[$self._selectedClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["selectedClass"]=1
//>>excludeEnd("ctx");
][0];
if($1 == null || $1.a$nil){
$1;
} else {
if($core.assert(aBoolean)){
$2=$recv([$self._selectedClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["selectedClass"]=2
//>>excludeEnd("ctx");
][0])._theNonMetaClass();
} else {
$2=$recv($self._selectedClass())._theMetaClass();
}
$self._selectedClass_($2);
}
return $recv($self._announcer())._announce_($recv($globals.HLShowInstanceToggled)._new());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"showInstance:",{aBoolean:aBoolean})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserModel);

$core.addMethod(
$core.method({
selector: "showMethodTemplate",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "showMethodTemplate\x0a\x09self selectedClass ifNotNil: [ :theClass |\x0a\x09\x09self announcer announce: (HLShowTemplate new\x0a\x09\x09\x09template: theClass methodTemplate;\x0a\x09\x09\x09yourself) ]",
referencedClasses: ["HLShowTemplate"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "selectedClass", "announce:", "announcer", "template:", "new", "methodTemplate", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3;
$1=$self._selectedClass();
if($1 == null || $1.a$nil){
$1;
} else {
var theClass;
theClass=$1;
$2=$self._announcer();
$3=$recv($globals.HLShowTemplate)._new();
$recv($3)._template_($recv(theClass)._methodTemplate());
$recv($2)._announce_($recv($3)._yourself());
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"showMethodTemplate",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserModel);


$core.addMethod(
$core.method({
selector: "on:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anEnvironment"],
source: "on: anEnvironment\x0a\x0a\x09^ self new\x0a    \x09environment: anEnvironment;\x0a        yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["environment:", "new", "yourself"]
}, function ($methodClass){ return function (anEnvironment){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._environment_(anEnvironment);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{anEnvironment:anEnvironment})});
//>>excludeEnd("ctx");
}; }),
$globals.HLBrowserModel.a$cls);


$core.addClass("HLClassCache", $globals.Object, "Helios-Browser");
$core.setSlots($globals.HLClassCache, ["class", "selectorsCache", "overrideCache", "overriddenCache"]);
$core.addMethod(
$core.method({
selector: "invalidateChildrenSelector:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSelector"],
source: "invalidateChildrenSelector: aSelector\x0a\x09self theClass subclasses do: [ :each |\x0a    \x09(self selectorsCache cacheFor: each)\x0a        \x09removeSelector: aSelector;\x0a        \x09invalidateChildrenSelector: aSelector ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "subclasses", "theClass", "removeSelector:", "cacheFor:", "selectorsCache", "invalidateChildrenSelector:"]
}, function ($methodClass){ return function (aSelector){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$recv($recv($self._theClass())._subclasses())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$1=$recv($self._selectorsCache())._cacheFor_(each);
$recv($1)._removeSelector_(aSelector);
return $recv($1)._invalidateChildrenSelector_(aSelector);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"invalidateChildrenSelector:",{aSelector:aSelector})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassCache);

$core.addMethod(
$core.method({
selector: "invalidateParentSelector:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSelector"],
source: "invalidateParentSelector: aSelector\x0a\x09self theClass superclass ifNotNil: [\x0a    \x09(self selectorsCache cacheFor: self theClass superclass)\x0a        \x09removeSelector: aSelector;\x0a\x09\x09\x09invalidateParentSelector: aSelector ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "superclass", "theClass", "removeSelector:", "cacheFor:", "selectorsCache", "invalidateParentSelector:"]
}, function ($methodClass){ return function (aSelector){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=[$recv([$self._theClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["theClass"]=1
//>>excludeEnd("ctx");
][0])._superclass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["superclass"]=1
//>>excludeEnd("ctx");
][0];
if($1 == null || $1.a$nil){
$1;
} else {
$2=$recv($self._selectorsCache())._cacheFor_($recv($self._theClass())._superclass());
$recv($2)._removeSelector_(aSelector);
$recv($2)._invalidateParentSelector_(aSelector);
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"invalidateParentSelector:",{aSelector:aSelector})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassCache);

$core.addMethod(
$core.method({
selector: "invalidateSelector:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSelector"],
source: "invalidateSelector: aSelector\x0a\x09self \x0a    \x09invalidateParentSelector: aSelector;\x0a        invalidateChildrenSelector: aSelector;\x0a        removeSelector: aSelector",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["invalidateParentSelector:", "invalidateChildrenSelector:", "removeSelector:"]
}, function ($methodClass){ return function (aSelector){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._invalidateParentSelector_(aSelector);
$self._invalidateChildrenSelector_(aSelector);
$self._removeSelector_(aSelector);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"invalidateSelector:",{aSelector:aSelector})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassCache);

$core.addMethod(
$core.method({
selector: "isOverridden:",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aMethod"],
source: "isOverridden: aMethod\x0a\x09^ self overriddenCache \x0a    \x09at: aMethod selector\x0a      \x09ifAbsentPut: [ aMethod isOverridden ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["at:ifAbsentPut:", "overriddenCache", "selector", "isOverridden"]
}, function ($methodClass){ return function (aMethod){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._overriddenCache())._at_ifAbsentPut_($recv(aMethod)._selector(),(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(aMethod)._isOverridden();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isOverridden:",{aMethod:aMethod})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassCache);

$core.addMethod(
$core.method({
selector: "isOverride:",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aMethod"],
source: "isOverride: aMethod\x0a\x09^ self overrideCache\x0a    \x09at: aMethod selector\x0a      \x09ifAbsentPut: [ aMethod isOverride ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["at:ifAbsentPut:", "overrideCache", "selector", "isOverride"]
}, function ($methodClass){ return function (aMethod){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._overrideCache())._at_ifAbsentPut_($recv(aMethod)._selector(),(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(aMethod)._isOverride();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isOverride:",{aMethod:aMethod})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassCache);

$core.addMethod(
$core.method({
selector: "overriddenCache",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "overriddenCache\x0a\x09^ overriddenCache ifNil: [ overriddenCache := HashedCollection new ]",
referencedClasses: ["HashedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.overriddenCache;
if($1 == null || $1.a$nil){
$self.overriddenCache=$recv($globals.HashedCollection)._new();
return $self.overriddenCache;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"overriddenCache",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassCache);

$core.addMethod(
$core.method({
selector: "overrideCache",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "overrideCache\x0a\x09^ overrideCache ifNil: [ overrideCache := HashedCollection new ]",
referencedClasses: ["HashedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.overrideCache;
if($1 == null || $1.a$nil){
$self.overrideCache=$recv($globals.HashedCollection)._new();
return $self.overrideCache;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"overrideCache",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassCache);

$core.addMethod(
$core.method({
selector: "removeSelector:",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSelector"],
source: "removeSelector: aSelector\x0a\x09self overriddenCache \x0a    \x09removeKey: aSelector\x0a        ifAbsent: [ ].\x0a    self overrideCache \x0a    \x09removeKey: aSelector\x0a        ifAbsent: [ ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["removeKey:ifAbsent:", "overriddenCache", "overrideCache"]
}, function ($methodClass){ return function (aSelector){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$recv($self._overriddenCache())._removeKey_ifAbsent_(aSelector,(function(){

}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["removeKey:ifAbsent:"]=1
//>>excludeEnd("ctx");
][0];
$recv($self._overrideCache())._removeKey_ifAbsent_(aSelector,(function(){

}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"removeSelector:",{aSelector:aSelector})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassCache);

$core.addMethod(
$core.method({
selector: "selectorsCache",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectorsCache\x0a\x09^ selectorsCache",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.selectorsCache;

}; }),
$globals.HLClassCache);

$core.addMethod(
$core.method({
selector: "selectorsCache:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCache"],
source: "selectorsCache: aCache\x0a\x09selectorsCache := aCache",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aCache){
var self=this,$self=this;
$self.selectorsCache=aCache;
return self;

}; }),
$globals.HLClassCache);

$core.addMethod(
$core.method({
selector: "theClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "theClass\x0a\x09^ class",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.class;

}; }),
$globals.HLClassCache);

$core.addMethod(
$core.method({
selector: "theClass:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass"],
source: "theClass: aClass\x0a\x09class := aClass",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aClass){
var self=this,$self=this;
$self.class=aClass;
return self;

}; }),
$globals.HLClassCache);


$core.addMethod(
$core.method({
selector: "on:selectorsCache:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass", "aSelectorsCache"],
source: "on: aClass selectorsCache: aSelectorsCache\x0a\x09^ self new\x0a    \x09theClass: aClass;\x0a        selectorsCache: aSelectorsCache;\x0a        yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["theClass:", "new", "selectorsCache:", "yourself"]
}, function ($methodClass){ return function (aClass,aSelectorsCache){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._theClass_(aClass);
$recv($1)._selectorsCache_(aSelectorsCache);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:selectorsCache:",{aClass:aClass,aSelectorsCache:aSelectorsCache})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassCache.a$cls);


$core.addClass("HLClassesListWidget", $globals.HLToolListWidget, "Helios-Browser");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLClassesListWidget.comment="I render a list of classes in the selected package.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "cssClassForItem:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass"],
source: "cssClassForItem: aClass\x0a\x09^ aClass theNonMetaClass classTag",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["classTag", "theNonMetaClass"]
}, function ($methodClass){ return function (aClass){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv(aClass)._theNonMetaClass())._classTag();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"cssClassForItem:",{aClass:aClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "focus",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focus\x0a\x09super focus.\x0a\x09\x0a\x09self selectedItem \x0a\x09\x09ifNil: [ self model showClassTemplate ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focus", "ifNil:", "selectedItem", "showClassTemplate", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._focus.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$1=$self._selectedItem();
if($1 == null || $1.a$nil){
$recv($self._model())._showClassTemplate();
} else {
$1;
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focus",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "focusMethodsListWidget",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focusMethodsListWidget\x0a\x09self model announcer announce: HLMethodsListFocus new",
referencedClasses: ["HLMethodsListFocus"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "model", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self._model())._announcer())._announce_($recv($globals.HLMethodsListFocus)._new());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focusMethodsListWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "focusProtocolsListWidget",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focusProtocolsListWidget\x0a\x09self model announcer announce: HLProtocolsListFocus new",
referencedClasses: ["HLProtocolsListFocus"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "model", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self._model())._announcer())._announce_($recv($globals.HLProtocolsListFocus)._new());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focusProtocolsListWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Classes'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Classes";

}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "observeModel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeModel\x0a\x09self model announcer \x0a    \x09on: HLPackageSelected\x0a\x09\x09send: #onPackageSelected:\x0a\x09\x09to: self;\x0a\x09\x09\x0a    \x09on: HLShowInstanceToggled \x0a\x09\x09send: #onShowInstanceToggled\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLShowCommentToggled\x0a\x09\x09send: #onShowCommentToggled\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLClassSelected\x0a\x09\x09send: #onClassSelected:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLClassesFocusRequested\x0a\x09\x09send: #onClassesFocusRequested\x0a\x09\x09to: self",
referencedClasses: ["HLPackageSelected", "HLShowInstanceToggled", "HLShowCommentToggled", "HLClassSelected", "HLClassesFocusRequested"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._model())._announcer();
[$recv($1)._on_send_to_($globals.HLPackageSelected,"onPackageSelected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLShowInstanceToggled,"onShowInstanceToggled",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLShowCommentToggled,"onShowCommentToggled",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=3
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLClassSelected,"onClassSelected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=4
//>>excludeEnd("ctx");
][0];
$recv($1)._on_send_to_($globals.HLClassesFocusRequested,"onClassesFocusRequested",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "observeSystem",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeSystem\x0a\x09self model systemAnnouncer\x0a    \x09on: ClassAdded\x0a\x09\x09send: #onClassAdded:\x0a\x09\x09to: self;\x0a\x09\x09\x0a        on: ClassRemoved\x0a        send: #onClassRemoved:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: ClassMoved\x0a\x09\x09send: #onClassMoved:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: ClassRenamed\x0a\x09\x09send: #onClassRenamed:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: ClassMigrated\x0a\x09\x09send: #onClassMigrated:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: ClassCommentChanged\x0a        send: #onClassCommentChanged:\x0a\x09\x09to: self",
referencedClasses: ["ClassAdded", "ClassRemoved", "ClassMoved", "ClassRenamed", "ClassMigrated", "ClassCommentChanged"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "systemAnnouncer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._model())._systemAnnouncer();
[$recv($1)._on_send_to_($globals.ClassAdded,"onClassAdded:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.ClassRemoved,"onClassRemoved:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.ClassMoved,"onClassMoved:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=3
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.ClassRenamed,"onClassRenamed:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=4
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.ClassMigrated,"onClassMigrated:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=5
//>>excludeEnd("ctx");
][0];
$recv($1)._on_send_to_($globals.ClassCommentChanged,"onClassCommentChanged:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeSystem",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "onClassAdded:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onClassAdded: anAnnouncement\x0a\x09| class |\x0a\x09\x0a\x09class := anAnnouncement theClass.\x0a\x09\x0a\x09(class package = self model selectedPackage or: [\x0a\x09\x09self items includes: class ]) ifFalse: [ ^ self ].\x0a    \x0a    self \x0a\x09\x09setItemsForSelectedPackage;\x0a\x09\x09refresh;\x0a\x09\x09focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["theClass", "ifFalse:", "or:", "=", "package", "selectedPackage", "model", "includes:", "items", "setItemsForSelectedPackage", "refresh", "focus"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var class_;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
class_=$recv(anAnnouncement)._theClass();
if($core.assert($recv($recv(class_)._package()).__eq($recv($self._model())._selectedPackage()))){
$1=true;
} else {
$1=$recv($self._items())._includes_(class_);
}
if(!$core.assert($1)){
return self;
}
$self._setItemsForSelectedPackage();
$self._refresh();
$self._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassAdded:",{anAnnouncement:anAnnouncement,class_:class_})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "onClassCommentChanged:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onClassCommentChanged: anAnnouncement\x0a\x09| class |\x0a\x09class := anAnnouncement theClass.\x0a\x0a\x09class package = self model selectedPackage ifFalse: [ ^ self ].\x0a    \x0a    self \x0a\x09\x09refresh;\x0a\x09\x09focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["theClass", "ifFalse:", "=", "package", "selectedPackage", "model", "refresh", "focus"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var class_;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
class_=$recv(anAnnouncement)._theClass();
if(!$core.assert($recv($recv(class_)._package()).__eq($recv($self._model())._selectedPackage()))){
return self;
}
$self._refresh();
$self._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassCommentChanged:",{anAnnouncement:anAnnouncement,class_:class_})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "onClassMigrated:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onClassMigrated: anAnnouncement\x0a\x09| class oldClass |\x0a\x09\x0a\x09class := anAnnouncement theClass.\x0a\x09oldClass := anAnnouncement oldClass.\x0a\x0a\x09(self items includes: oldClass) ifFalse: [ ^ self ].\x0a\x0a\x09self model selectedClass = oldClass ifTrue: [\x0a\x09\x09self model selectedClass: class ].\x0a    \x0a    self setItemsForSelectedPackage.\x0a    self \x0a\x09\x09refresh;\x0a\x09\x09focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["theClass", "oldClass", "ifFalse:", "includes:", "items", "ifTrue:", "=", "selectedClass", "model", "selectedClass:", "setItemsForSelectedPackage", "refresh", "focus"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var class_,oldClass;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
class_=$recv(anAnnouncement)._theClass();
oldClass=$recv(anAnnouncement)._oldClass();
if(!$core.assert($recv($self._items())._includes_(oldClass))){
return self;
}
if($core.assert($recv($recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._selectedClass()).__eq(oldClass))){
$recv($self._model())._selectedClass_(class_);
}
$self._setItemsForSelectedPackage();
$self._refresh();
$self._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassMigrated:",{anAnnouncement:anAnnouncement,class_:class_,oldClass:oldClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "onClassMoved:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onClassMoved: anAnnouncement\x0a\x09| class oldPackage |\x0a\x09\x0a\x09class := anAnnouncement theClass.\x0a\x09oldPackage := anAnnouncement oldPackage.\x0a\x09\x0a\x09(oldPackage = self model selectedPackage or: [\x0a\x09\x09class package = self model selectedPackage ])\x0a\x09\x09\x09ifFalse: [ ^ self ].\x0a\x09\x0a\x09oldPackage = self model selectedPackage ifTrue: [ \x0a\x09\x09self \x0a\x09\x09\x09selectedItem: nil;\x0a\x09\x09\x09selectItem: nil ].\x0a    \x0a    self setItemsForSelectedPackage.\x0a    self \x09\x0a\x09\x09refresh;\x0a\x09\x09focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["theClass", "oldPackage", "ifFalse:", "or:", "=", "selectedPackage", "model", "package", "ifTrue:", "selectedItem:", "selectItem:", "setItemsForSelectedPackage", "refresh", "focus"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var class_,oldPackage;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
class_=$recv(anAnnouncement)._theClass();
oldPackage=$recv(anAnnouncement)._oldPackage();
if($core.assert([$recv(oldPackage).__eq([$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._selectedPackage()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedPackage"]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["="]=1
//>>excludeEnd("ctx");
][0])){
$1=true;
} else {
$1=[$recv($recv(class_)._package()).__eq([$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=2
//>>excludeEnd("ctx");
][0])._selectedPackage()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedPackage"]=2
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["="]=2
//>>excludeEnd("ctx");
][0];
}
if(!$core.assert($1)){
return self;
}
if($core.assert($recv(oldPackage).__eq($recv($self._model())._selectedPackage()))){
$self._selectedItem_(nil);
$self._selectItem_(nil);
}
$self._setItemsForSelectedPackage();
$self._refresh();
$self._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassMoved:",{anAnnouncement:anAnnouncement,class_:class_,oldPackage:oldPackage})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "onClassRemoved:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onClassRemoved: anAnnouncement\x0a\x09| class |\x0a\x09class := anAnnouncement theClass.\x0a\x0a\x09class package = self model selectedPackage ifFalse: [ ^ self ].\x0a    \x0a\x09self \x0a\x09\x09selectItem: nil;\x0a\x09\x09selectedItem: nil.\x0a    self setItemsForSelectedPackage.\x0a    self \x0a\x09\x09refresh;\x0a\x09\x09focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["theClass", "ifFalse:", "=", "package", "selectedPackage", "model", "selectItem:", "selectedItem:", "setItemsForSelectedPackage", "refresh", "focus"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var class_;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
class_=$recv(anAnnouncement)._theClass();
if(!$core.assert($recv($recv(class_)._package()).__eq($recv($self._model())._selectedPackage()))){
return self;
}
$self._selectItem_(nil);
$self._selectedItem_(nil);
$self._setItemsForSelectedPackage();
$self._refresh();
$self._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassRemoved:",{anAnnouncement:anAnnouncement,class_:class_})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "onClassRenamed:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onClassRenamed: anAnnouncement\x0a\x09anAnnouncement theClass package = self model selectedPackage ifFalse: [ ^ self ].\x0a    \x0a    self setItemsForSelectedPackage.\x0a    self \x0a\x09\x09refresh;\x0a\x09\x09focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifFalse:", "=", "package", "theClass", "selectedPackage", "model", "setItemsForSelectedPackage", "refresh", "focus"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if(!$core.assert($recv($recv($recv(anAnnouncement)._theClass())._package()).__eq($recv($self._model())._selectedPackage()))){
return self;
}
$self._setItemsForSelectedPackage();
$self._refresh();
$self._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassRenamed:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "onClassSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onClassSelected: anAnnouncement\x0a\x09| selectedClass |\x0a\x09\x0a\x09anAnnouncement item ifNil: [ ^ self ].\x0a\x09\x0a\x09selectedClass := anAnnouncement item theNonMetaClass.\x0a\x09self selectedItem: selectedClass.\x0a\x0a\x09self hasFocus ifFalse: [\x0a\x09\x09self \x0a\x09\x09\x09activateItem: selectedClass;\x0a\x09\x09\x09focus ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "item", "theNonMetaClass", "selectedItem:", "ifFalse:", "hasFocus", "activateItem:", "focus"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var selectedClass;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=[$recv(anAnnouncement)._item()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["item"]=1
//>>excludeEnd("ctx");
][0];
if($1 == null || $1.a$nil){
return self;
} else {
$1;
}
selectedClass=$recv($recv(anAnnouncement)._item())._theNonMetaClass();
$self._selectedItem_(selectedClass);
if(!$core.assert($self._hasFocus())){
$self._activateItem_(selectedClass);
$self._focus();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassSelected:",{anAnnouncement:anAnnouncement,selectedClass:selectedClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "onClassesFocusRequested",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onClassesFocusRequested\x0a\x09self focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focus"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassesFocusRequested",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "onPackageSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onPackageSelected: anAnnouncement\x0a\x09anAnnouncement isSoft ifTrue: [ ^ self ].\x0a    self selectedItem: nil.\x0a    \x0a    self setItemsForSelectedPackage.\x0a    self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "isSoft", "selectedItem:", "setItemsForSelectedPackage", "refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv(anAnnouncement)._isSoft())){
return self;
}
$self._selectedItem_(nil);
$self._setItemsForSelectedPackage();
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onPackageSelected:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "onShowCommentToggled",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onShowCommentToggled\x0a\x09self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["refresh"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onShowCommentToggled",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "onShowInstanceToggled",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onShowInstanceToggled\x0a\x09self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["refresh"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onShowInstanceToggled",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "renderButtonsOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderButtonsOn: html\x0a\x09| checkbox |\x0a\x09\x0a\x09html div \x0a        class: 'btn-group';\x0a\x09\x09at: 'role' put: 'group';\x0a\x09\x09with: [\x0a           \x09html button \x0a                class: (String streamContents: [ :str |\x0a                \x09str nextPutAll: 'btn btn-default'.\x0a                    self showInstance ifTrue: [ \x0a                    \x09str nextPutAll: ' active' ] ]);\x0a  \x09\x09\x09\x09with: 'Instance';\x0a                onClick: [ self showInstance: true ].\x0a  \x09\x09\x09html button\x0a  \x09\x09\x09\x09class: (String streamContents: [ :str |\x0a                \x09str nextPutAll: 'btn btn-default'.\x0a                    self showClass ifTrue: [ \x0a                    \x09str nextPutAll: ' active' ] ]);\x0a  \x09\x09\x09\x09with: 'Class';\x0a\x09\x09\x09\x09onClick: [ self showInstance: false ] ].\x0a\x09\x09html label \x0a\x09\x09\x09class: 'checkbox';\x0a\x09\x09\x09with: [\x0a\x09\x09\x09\x09checkbox := html input\x0a\x09\x09\x09\x09\x09type: 'checkbox';\x0a\x09\x09\x09\x09\x09onClick: [ self toggleShowComment ].\x0a\x09\x09\x09\x09html with: 'Doc' ].\x0a\x09\x09\x09\x09\x0a\x09\x09self showComment ifTrue: [\x0a\x09\x09\x09checkbox at: 'checked' put: 'checked' ]",
referencedClasses: ["String"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "at:put:", "with:", "button", "streamContents:", "nextPutAll:", "ifTrue:", "showInstance", "onClick:", "showInstance:", "showClass", "label", "type:", "input", "toggleShowComment", "showComment"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
var checkbox;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3,$4,$5;
$1=$recv(html)._div();
[$recv($1)._class_("btn-group")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._at_put_("role","group")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["at:put:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$2=[$recv(html)._button()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["button"]=1
//>>excludeEnd("ctx");
][0];
[$recv($2)._class_([$recv($globals.String)._streamContents_((function(str){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
[$recv(str)._nextPutAll_("btn btn-default")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["nextPutAll:"]=1
//>>excludeEnd("ctx");
][0];
if($core.assert($self._showInstance())){
return [$recv(str)._nextPutAll_(" active")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["nextPutAll:"]=2
//>>excludeEnd("ctx");
][0];
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({str:str},$ctx2,2)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["streamContents:"]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["class:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($2)._with_("Instance")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($2)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return [$self._showInstance_(true)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["showInstance:"]=1
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,4)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["onClick:"]=1
//>>excludeEnd("ctx");
][0];
$3=$recv(html)._button();
[$recv($3)._class_($recv($globals.String)._streamContents_((function(str){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
[$recv(str)._nextPutAll_("btn btn-default")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["nextPutAll:"]=3
//>>excludeEnd("ctx");
][0];
if($core.assert($self._showClass())){
return $recv(str)._nextPutAll_(" active");
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({str:str},$ctx2,5)});
//>>excludeEnd("ctx");
})))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["class:"]=3
//>>excludeEnd("ctx");
][0];
[$recv($3)._with_("Class")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=3
//>>excludeEnd("ctx");
][0];
return [$recv($3)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._showInstance_(false);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,7)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["onClick:"]=2
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
$4=$recv(html)._label();
$recv($4)._class_("checkbox");
[$recv($4)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$5=$recv(html)._input();
$recv($5)._type_("checkbox");
checkbox=$recv($5)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._toggleShowComment();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,9)});
//>>excludeEnd("ctx");
}));
return $recv(html)._with_("Doc");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,8)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=4
//>>excludeEnd("ctx");
][0];
if($core.assert($self._showComment())){
$recv(checkbox)._at_put_("checked","checked");
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderButtonsOn:",{html:html,checkbox:checkbox})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "renderItemLabel:on:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClassAndLevel", "html"],
source: "renderItemLabel: aClassAndLevel on: html\x0a\x09| aClass level indent |\x0a\x09aClass := aClassAndLevel first.\x0a\x09level := aClassAndLevel second.\x0a\x09indent := String fromCharCode: 160.\x0a\x09indent := indent, indent, indent, indent.\x0a\x09html span with: (String streamContents: [ :str |\x0a\x09\x09level timesRepeat: [\x0a\x09\x09\x09str nextPutAll: indent ].\x0a\x09\x09\x09str nextPutAll: aClass name ])",
referencedClasses: ["String"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["first", "second", "fromCharCode:", ",", "with:", "span", "streamContents:", "timesRepeat:", "nextPutAll:", "name"]
}, function ($methodClass){ return function (aClassAndLevel,html){
var self=this,$self=this;
var aClass,level,indent;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
aClass=$recv(aClassAndLevel)._first();
level=$recv(aClassAndLevel)._second();
indent=$recv($globals.String)._fromCharCode_((160));
indent=[$recv([$recv($recv(indent).__comma(indent)).__comma(indent)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx[","]=2
//>>excludeEnd("ctx");
][0]).__comma(indent)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx[","]=1
//>>excludeEnd("ctx");
][0];
$recv($recv(html)._span())._with_($recv($globals.String)._streamContents_((function(str){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$recv(level)._timesRepeat_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return [$recv(str)._nextPutAll_(indent)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["nextPutAll:"]=1
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}));
return $recv(str)._nextPutAll_($recv(aClass)._name());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({str:str},$ctx1,1)});
//>>excludeEnd("ctx");
})));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderItemLabel:on:",{aClassAndLevel:aClassAndLevel,html:html,aClass:aClass,level:level,indent:indent})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "renderItems:level:on:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection", "anInteger", "html"],
source: "renderItems: aCollection level: anInteger on: html\x0a    aCollection do: [ :each |\x0a\x09\x09| aClass subclasses |\x0a\x09\x09aClass := each first.\x0a\x09\x09subclasses := each second.\x0a\x09\x0a\x09\x09self renderItemModel: aClass viewModel: {aClass. anInteger} on: html.\x0a\x09\x0a    \x09self renderItems: subclasses level: anInteger + 1 on: html ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["do:", "first", "second", "renderItemModel:viewModel:on:", "renderItems:level:on:", "+"]
}, function ($methodClass){ return function (aCollection,anInteger,html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(aCollection)._do_((function(each){
var aClass,subclasses;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
aClass=$recv(each)._first();
subclasses=$recv(each)._second();
$self._renderItemModel_viewModel_on_(aClass,[aClass,anInteger],html);
return $self._renderItems_level_on_(subclasses,$recv(anInteger).__plus((1)),html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each,aClass:aClass,subclasses:subclasses},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderItems:level:on:",{aCollection:aCollection,anInteger:anInteger,html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "renderListOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderListOn: html\x0a\x09self renderItems: (ClassBuilder sortClasses: self items) level: 0 on: html",
referencedClasses: ["ClassBuilder"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["renderItems:level:on:", "sortClasses:", "items"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._renderItems_level_on_($recv($globals.ClassBuilder)._sortClasses_($self._items()),(0),html);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderListOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "reselectItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anItem"],
source: "reselectItem: anItem\x0a\x09self model softSelectedClass: anItem",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["softSelectedClass:", "model"]
}, function ($methodClass){ return function (anItem){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._softSelectedClass_(anItem);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"reselectItem:",{anItem:anItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "selectItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass"],
source: "selectItem: aClass\x0a    self model selectedClass: aClass",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectedClass:", "model"]
}, function ($methodClass){ return function (aClass){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._selectedClass_(aClass);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectItem:",{aClass:aClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "setItemsForPackage:",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aPackage"],
source: "setItemsForPackage: aPackage\x0a\x09self items: (aPackage \x0a    \x09ifNil: [ #() ]\x0a  \x09\x09ifNotNil: [ (aPackage classes \x0a        \x09collect: [ :each | each theNonMetaClass ]) asArray \x0a            \x09sort: [ :a :b | a name < b name ] ]).",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["items:", "ifNil:ifNotNil:", "sort:", "asArray", "collect:", "classes", "theNonMetaClass", "<", "name"]
}, function ($methodClass){ return function (aPackage){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
if(aPackage == null || aPackage.a$nil){
$1=[];
} else {
$1=$recv($recv($recv($recv(aPackage)._classes())._collect_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._theNonMetaClass();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,3)});
//>>excludeEnd("ctx");
})))._asArray())._sort_((function(a,b){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv([$recv(a)._name()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["name"]=1
//>>excludeEnd("ctx");
][0]).__lt($recv(b)._name());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({a:a,b:b},$ctx1,4)});
//>>excludeEnd("ctx");
}));
}
$self._items_($1);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setItemsForPackage:",{aPackage:aPackage})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "setItemsForSelectedPackage",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setItemsForSelectedPackage\x0a\x09self setItemsForPackage: self model selectedPackage",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["setItemsForPackage:", "selectedPackage", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._setItemsForPackage_($recv($self._model())._selectedPackage());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setItemsForSelectedPackage",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "showClass",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "showClass\x0a\x09^ self model showInstance not",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["not", "showInstance", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._model())._showInstance())._not();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"showClass",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "showComment",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "showComment\x0a\x09^ self model showComment",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["showComment", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._showComment();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"showComment",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "showComment:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBoolean"],
source: "showComment: aBoolean\x0a\x09self model showComment: aBoolean",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["showComment:", "model"]
}, function ($methodClass){ return function (aBoolean){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._showComment_(aBoolean);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"showComment:",{aBoolean:aBoolean})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "showInstance",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "showInstance\x0a\x09^ self model showInstance",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["showInstance", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._showInstance();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"showInstance",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "showInstance:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBoolean"],
source: "showInstance: aBoolean\x0a\x09self model showInstance: aBoolean",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["showInstance:", "model"]
}, function ($methodClass){ return function (aBoolean){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._showInstance_(aBoolean);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"showInstance:",{aBoolean:aBoolean})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);

$core.addMethod(
$core.method({
selector: "toggleShowComment",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "toggleShowComment\x0a\x09self model showComment: self showComment not",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["showComment:", "model", "not", "showComment"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._showComment_($recv($self._showComment())._not());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"toggleShowComment",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLClassesListWidget);



$core.addClass("HLDocumentationWidget", $globals.HLFocusableWidget, "Helios-Browser");
$core.setSlots($globals.HLDocumentationWidget, ["model"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLDocumentationWidget.comment="I render the documentation for the selected class";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "defaultDocumentation",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "defaultDocumentation\x0a\x09^ 'No documentation is available for this class.'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "No documentation is available for this class.";

}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "defaultHead",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "defaultHead\x0a\x09^ 'No class selected'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "No class selected";

}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "documentation",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "documentation\x0a\x09^ self selectedItem \x0a\x09\x09ifNil: [ '' ]\x0a\x09\x09ifNotNil: [ :item | item comment ifEmpty: [ self defaultDocumentation ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:ifNotNil:", "selectedItem", "ifEmpty:", "comment", "defaultDocumentation"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._selectedItem();
if($1 == null || $1.a$nil){
return "";
} else {
var item;
item=$1;
return $recv($recv(item)._comment())._ifEmpty_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._defaultDocumentation();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,3)});
//>>excludeEnd("ctx");
}));
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"documentation",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "editDocumentation",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "editDocumentation\x0a\x09self model editComment",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["editComment", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._editComment();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"editDocumentation",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "head",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "head\x0a\x09^ self selectedItem \x0a\x09\x09ifNil: [ self defaultHead ]\x0a\x09\x09ifNotNil: [ :item | item name ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:ifNotNil:", "selectedItem", "defaultHead", "name"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._selectedItem();
if($1 == null || $1.a$nil){
return $self._defaultHead();
} else {
var item;
item=$1;
return $recv(item)._name();
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"head",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "model",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "model\x0a\x09^ model",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.model;

}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "model:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aModel"],
source: "model: aModel\x0a\x09model := aModel.\x0a\x09self \x0a\x09\x09observeSystem;\x0a\x09\x09observeModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["observeSystem", "observeModel"]
}, function ($methodClass){ return function (aModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.model=aModel;
$self._observeSystem();
$self._observeModel();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"model:",{aModel:aModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "observeModel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeModel\x0a\x09self model announcer \x0a\x09\x09on: HLClassSelected\x0a\x09\x09send: #onClassSelected:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLEditComment\x0a\x09\x09send: #onEditDocumentation\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLDocumentationFocusRequested\x0a\x09\x09send: #onDocumentationFocusRequested\x0a\x09\x09to: self",
referencedClasses: ["HLClassSelected", "HLEditComment", "HLDocumentationFocusRequested"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._model())._announcer();
[$recv($1)._on_send_to_($globals.HLClassSelected,"onClassSelected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLEditComment,"onEditDocumentation",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=2
//>>excludeEnd("ctx");
][0];
$recv($1)._on_send_to_($globals.HLDocumentationFocusRequested,"onDocumentationFocusRequested",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "observeSystem",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeSystem\x0a\x09self model systemAnnouncer \x0a\x09\x09on: ClassCommentChanged\x0a\x09\x09send: #onClassCommentChanged:\x0a\x09\x09to: self",
referencedClasses: ["ClassCommentChanged"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "systemAnnouncer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self._model())._systemAnnouncer())._on_send_to_($globals.ClassCommentChanged,"onClassCommentChanged:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeSystem",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "onClassCommentChanged:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onClassCommentChanged: anAnnouncement\x0a\x09self model selectedClass ifNil: [ ^ self ].\x0a\x09\x0a\x09anAnnouncement theClass = self model selectedClass theNonMetaClass\x0a\x09\x09ifTrue: [ self refresh ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "selectedClass", "model", "ifTrue:", "=", "theClass", "theNonMetaClass", "refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=[$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._selectedClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedClass"]=1
//>>excludeEnd("ctx");
][0];
if($1 == null || $1.a$nil){
return self;
} else {
$1;
}
if($core.assert($recv($recv(anAnnouncement)._theClass()).__eq($recv($recv($self._model())._selectedClass())._theNonMetaClass()))){
$self._refresh();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassCommentChanged:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "onClassSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onClassSelected: anAnnouncement\x0a\x09self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassSelected:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "onDocumentationFocusRequested",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onDocumentationFocusRequested\x0a\x09self focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focus"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onDocumentationFocusRequested",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "onEditDocumentation",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onEditDocumentation\x0a\x09self \x0a\x09\x09request: self model selectedClass theNonMetaClass name, ' comment'\x0a\x09\x09value: self model selectedClass theNonMetaClass comment\x0a\x09\x09do: [ :comment | self setClassComment: comment ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["request:value:do:", ",", "name", "theNonMetaClass", "selectedClass", "model", "comment", "setClassComment:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._request_value_do_($recv($recv([$recv([$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._selectedClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedClass"]=1
//>>excludeEnd("ctx");
][0])._theNonMetaClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["theNonMetaClass"]=1
//>>excludeEnd("ctx");
][0])._name()).__comma(" comment"),$recv($recv($recv($self._model())._selectedClass())._theNonMetaClass())._comment(),(function(comment){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._setClassComment_(comment);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({comment:comment},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onEditDocumentation",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "renderClassLinkTo:on:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClassOrTrait", "html"],
source: "renderClassLinkTo: aClassOrTrait on: html\x0a\x09html a \x0a\x09\x09with: aClassOrTrait name;\x0a\x09\x09onClick: [ self selectClass: aClassOrTrait ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "a", "name", "onClick:", "selectClass:"]
}, function ($methodClass){ return function (aClassOrTrait,html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._a();
$recv($1)._with_($recv(aClassOrTrait)._name());
$recv($1)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._selectClass_(aClassOrTrait);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderClassLinkTo:on:",{aClassOrTrait:aClassOrTrait,html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09html div \x0a\x09\x09class: 'doc';\x0a\x09\x09with: [\x0a\x09\x09\x09self \x0a\x09\x09\x09\x09renderHeadOn: html;\x0a\x09\x09\x09\x09renderDocOn: html ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "with:", "renderHeadOn:", "renderDocOn:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._div();
$recv($1)._class_("doc");
$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$self._renderHeadOn_(html);
return $self._renderDocOn_(html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "renderDocOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderDocOn: html\x0a\x09self selectedItem ifNotNil: [\x0a\x09\x09self renderInheritanceOn: html.\x0a\x09\x09html h1 \x0a\x09\x09\x09with: 'Overview';\x0a\x09\x09\x09with: [ \x0a\x09\x09\x09\x09html button\x0a\x09\x09\x09\x09\x09class: 'button default';\x0a\x09\x09\x09\x09\x09with: 'Edit';\x0a\x09\x09\x09\x09\x09onClick: [ self editDocumentation ] ].\x0a\x09\x09(html div \x0a\x09\x09\x09class: 'markdown';\x0a\x09\x09\x09asJQuery) html: ((Showdown at: 'converter') new makeHtml: self documentation) ]",
referencedClasses: ["Showdown"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "selectedItem", "renderInheritanceOn:", "with:", "h1", "class:", "button", "onClick:", "editDocumentation", "html:", "div", "asJQuery", "makeHtml:", "new", "at:", "documentation"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3,$4;
$1=$self._selectedItem();
if($1 == null || $1.a$nil){
$1;
} else {
$self._renderInheritanceOn_(html);
$2=$recv(html)._h1();
[$recv($2)._with_("Overview")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($2)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$3=$recv(html)._button();
[$recv($3)._class_("button default")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["class:"]=1
//>>excludeEnd("ctx");
][0];
$recv($3)._with_("Edit");
return $recv($3)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $self._editDocumentation();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,3)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=2
//>>excludeEnd("ctx");
][0];
$4=$recv(html)._div();
$recv($4)._class_("markdown");
$recv($recv($4)._asJQuery())._html_($recv($recv($recv($globals.Showdown)._at_("converter"))._new())._makeHtml_($self._documentation()));
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderDocOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "renderHeadOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderHeadOn: html\x0a\x09html div \x0a\x09\x09class: 'head';\x0a\x09\x09with: self head",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "with:", "head"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._div();
$recv($1)._class_("head");
$recv($1)._with_($self._head());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderHeadOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "renderInheritanceOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderInheritanceOn: html\x0a\x09html div \x09\x0a\x09\x09class: 'inheritance';\x0a\x09\x09with: [\x0a\x09\x09\x09self selectedItem isBehavior\x0a\x09\x09\x09\x09ifFalse: [ html with: 'Trait' ]\x0a\x09\x09\x09\x09ifTrue: [\x0a\x09\x09\x09\x09\x09html with: 'Subclass of '.\x0a\x09\x09\x09\x09\x09self selectedItem superclass \x0a\x09\x09\x09\x09\x09\x09ifNil: [ html em with: 'nil' ]\x0a\x09\x09\x09\x09\x09\x09ifNotNil: [ :sclass | self renderClassLinkTo: sclass on: html ] ] ];\x0a\x09\x09with: '. ';\x0a\x09\x09with: [ self selectedItem traitComposition ifNotEmpty: [ :composition |\x0a\x09\x09\x09html with: 'Uses '.\x0a\x09\x09\x09composition\x0a\x09\x09\x09\x09do: [ :each | self renderClassLinkTo: each trait on: html ]\x0a\x09\x09\x09\x09separatedBy: [ html with: ', ' ].\x0a\x09\x09\x09html with: '. ' ] ];\x0a\x09\x09with: [ self selectedItem isBehavior ifTrue: [\x0a\x09\x09\x09self selectedItem theMetaClass traitComposition ifNotEmpty: [ :composition |\x0a\x09\x09\x09\x09html with: 'Class-side uses '.\x0a\x09\x09\x09\x09composition\x0a\x09\x09\x09\x09\x09do: [ :each | self renderClassLinkTo: each trait on: html ]\x0a\x09\x09\x09\x09\x09separatedBy: [ html with: ', ' ].\x0a\x09\x09\x09\x09html with: '. ' ] ] ]\x0a\x09\x09",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "with:", "ifFalse:ifTrue:", "isBehavior", "selectedItem", "ifNil:ifNotNil:", "superclass", "em", "renderClassLinkTo:on:", "ifNotEmpty:", "traitComposition", "do:separatedBy:", "trait", "ifTrue:", "theMetaClass"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$recv(html)._div();
$recv($1)._class_("inheritance");
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if($core.assert([$recv([$self._selectedItem()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["selectedItem"]=1
//>>excludeEnd("ctx");
][0])._isBehavior()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["isBehavior"]=1
//>>excludeEnd("ctx");
][0])){
[$recv(html)._with_("Subclass of ")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=3
//>>excludeEnd("ctx");
][0];
$2=$recv([$self._selectedItem()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["selectedItem"]=2
//>>excludeEnd("ctx");
][0])._superclass();
if($2 == null || $2.a$nil){
return [$recv($recv(html)._em())._with_("nil")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=4
//>>excludeEnd("ctx");
][0];
} else {
var sclass;
sclass=$2;
return [$self._renderClassLinkTo_on_(sclass,html)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["renderClassLinkTo:on:"]=1
//>>excludeEnd("ctx");
][0];
}
} else {
return [$recv(html)._with_("Trait")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["with:"]=2
//>>excludeEnd("ctx");
][0];
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_(". ")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=5
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return [$recv([$recv([$self._selectedItem()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["selectedItem"]=3
//>>excludeEnd("ctx");
][0])._traitComposition()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["traitComposition"]=1
//>>excludeEnd("ctx");
][0])._ifNotEmpty_((function(composition){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
[$recv(html)._with_("Uses ")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["with:"]=7
//>>excludeEnd("ctx");
][0];
[$recv(composition)._do_separatedBy_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx4) {
//>>excludeEnd("ctx");
return [$self._renderClassLinkTo_on_([$recv(each)._trait()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx4.sendIdx["trait"]=1
//>>excludeEnd("ctx");
][0],html)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx4.sendIdx["renderClassLinkTo:on:"]=2
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx4) {$ctx4.fillBlock({each:each},$ctx3,8)});
//>>excludeEnd("ctx");
}),(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx4) {
//>>excludeEnd("ctx");
return [$recv(html)._with_(", ")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx4.sendIdx["with:"]=8
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx4) {$ctx4.fillBlock({},$ctx3,9)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["do:separatedBy:"]=1
//>>excludeEnd("ctx");
][0];
return [$recv(html)._with_(". ")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["with:"]=9
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({composition:composition},$ctx2,7)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["ifNotEmpty:"]=1
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,6)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=6
//>>excludeEnd("ctx");
][0];
[$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
if($core.assert($recv([$self._selectedItem()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["selectedItem"]=4
//>>excludeEnd("ctx");
][0])._isBehavior())){
return $recv($recv($recv($self._selectedItem())._theMetaClass())._traitComposition())._ifNotEmpty_((function(composition){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
[$recv(html)._with_("Class-side uses ")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx3.sendIdx["with:"]=11
//>>excludeEnd("ctx");
][0];
$recv(composition)._do_separatedBy_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx4) {
//>>excludeEnd("ctx");
return $self._renderClassLinkTo_on_($recv(each)._trait(),html);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx4) {$ctx4.fillBlock({each:each},$ctx3,13)});
//>>excludeEnd("ctx");
}),(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx4) {
//>>excludeEnd("ctx");
return [$recv(html)._with_(", ")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx4.sendIdx["with:"]=12
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx4) {$ctx4.fillBlock({},$ctx3,14)});
//>>excludeEnd("ctx");
}));
return $recv(html)._with_(". ");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({composition:composition},$ctx2,12)});
//>>excludeEnd("ctx");
}));
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,10)});
//>>excludeEnd("ctx");
}))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=10
//>>excludeEnd("ctx");
][0];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderInheritanceOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "selectClass:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass"],
source: "selectClass: aClass\x0a\x09self model selectedClass: aClass",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectedClass:", "model"]
}, function ($methodClass){ return function (aClass){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._selectedClass_(aClass);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectClass:",{aClass:aClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "selectedItem",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectedItem\x0a\x09^ self model selectedClass ifNotNil: [ :class | class theNonMetaClass ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNotNil:", "selectedClass", "model", "theNonMetaClass"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._model())._selectedClass();
if($1 == null || $1.a$nil){
return $1;
} else {
var class_;
class_=$1;
return $recv(class_)._theNonMetaClass();
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectedItem",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "setClassComment:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "setClassComment: aString\x0a\x09self model setClassComment: aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["setClassComment:", "model"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._setClassComment_(aString);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setClassComment:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);

$core.addMethod(
$core.method({
selector: "unregister",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "unregister\x0a\x09super unregister.\x0a\x09self model announcer unregister: self",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unregister", "unregister:", "announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._unregister.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv($recv($self._model())._announcer())._unregister_(self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unregister",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLDocumentationWidget);



$core.addClass("HLMethodsListWidget", $globals.HLToolListWidget, "Helios-Browser");
$core.setSlots($globals.HLMethodsListWidget, ["selectorsCache"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLMethodsListWidget.comment="I render a list of methods for the selected protocol.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "allProtocol",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "allProtocol\x0a\x09^ self model allProtocol",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["allProtocol", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._allProtocol();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"allProtocol",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "cssClassForItem:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSelector"],
source: "cssClassForItem: aSelector\x0a\x09| override overriden method |\x0a    \x0a    method := self methodForSelector: aSelector.\x0a    override := self isOverride: method.\x0a    overriden := self isOverridden: method.\x0a    \x0a\x09^ override\x0a    \x09ifTrue: [ overriden\x0a\x09\x09\x09ifTrue: [ 'override-overridden' ]\x0a\x09\x09\x09ifFalse: [ 'override' ] ]\x0a\x09\x09ifFalse: [\x0a\x09\x09\x09overriden\x0a\x09\x09\x09ifTrue: [ 'overridden' ]\x0a\x09\x09\x09ifFalse: [ '' ] ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["methodForSelector:", "isOverride:", "isOverridden:", "ifTrue:ifFalse:"]
}, function ($methodClass){ return function (aSelector){
var self=this,$self=this;
var override,overriden,method;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
method=$self._methodForSelector_(aSelector);
override=$self._isOverride_(method);
overriden=$self._isOverridden_(method);
if($core.assert(override)){
if($core.assert(overriden)){
return "override-overridden";
} else {
return "override";
}
} else {
if($core.assert(overriden)){
return "overridden";
} else {
return "";
}
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"cssClassForItem:",{aSelector:aSelector,override:override,overriden:overriden,method:method})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "focus",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focus\x0a\x09super focus.\x0a\x09\x0a\x09self selectedItem ifNil: [\x0a\x09\x09self model showMethodTemplate ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focus", "ifNil:", "selectedItem", "showMethodTemplate", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._focus.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$1=$self._selectedItem();
if($1 == null || $1.a$nil){
$recv($self._model())._showMethodTemplate();
} else {
$1;
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focus",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "isOverridden:",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aMethod"],
source: "isOverridden: aMethod\x0a   ^ self selectorsCache isOverridden: aMethod",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["isOverridden:", "selectorsCache"]
}, function ($methodClass){ return function (aMethod){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._selectorsCache())._isOverridden_(aMethod);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isOverridden:",{aMethod:aMethod})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "isOverride:",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aMethod"],
source: "isOverride: aMethod\x0a   ^ self selectorsCache isOverride: aMethod",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["isOverride:", "selectorsCache"]
}, function ($methodClass){ return function (aMethod){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._selectorsCache())._isOverride_(aMethod);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isOverride:",{aMethod:aMethod})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Methods'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Methods";

}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "methodForSelector:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSelector"],
source: "methodForSelector: aSelector\x0a\x09^ self model selectedClass\x0a    \x09methodDictionary at: aSelector",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["at:", "methodDictionary", "selectedClass", "model"]
}, function ($methodClass){ return function (aSelector){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($recv($self._model())._selectedClass())._methodDictionary())._at_(aSelector);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"methodForSelector:",{aSelector:aSelector})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "methodsInProtocol:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "methodsInProtocol: aString\x0a\x09self model selectedClass ifNil: [ ^ #() ].\x0a    \x0a\x09^ aString = self allProtocol\x0a    \x09ifTrue: [ self model selectedClass methods ]\x0a      \x09ifFalse: [ self model selectedClass methodsInProtocol: aString ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "selectedClass", "model", "ifTrue:ifFalse:", "=", "allProtocol", "methods", "methodsInProtocol:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=[$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._selectedClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedClass"]=1
//>>excludeEnd("ctx");
][0];
if($1 == null || $1.a$nil){
return [];
} else {
$1;
}
if($core.assert($recv(aString).__eq($self._allProtocol()))){
return $recv([$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=2
//>>excludeEnd("ctx");
][0])._selectedClass()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedClass"]=2
//>>excludeEnd("ctx");
][0])._methods();
} else {
return $recv($recv($self._model())._selectedClass())._methodsInProtocol_(aString);
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"methodsInProtocol:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "observeModel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeModel\x0a\x09self model announcer \x0a\x09\x09on: HLProtocolSelected \x0a\x09\x09send: #onProtocolSelected: \x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLShowInstanceToggled \x0a\x09\x09send: #onShowInstanceToggled\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLMethodSelected \x0a\x09\x09send: #onMethodSelected:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLMethodsFocusRequested \x0a\x09\x09send: #onMethodsFocusRequested\x0a\x09\x09to: self",
referencedClasses: ["HLProtocolSelected", "HLShowInstanceToggled", "HLMethodSelected", "HLMethodsFocusRequested"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._model())._announcer();
[$recv($1)._on_send_to_($globals.HLProtocolSelected,"onProtocolSelected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLShowInstanceToggled,"onShowInstanceToggled",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLMethodSelected,"onMethodSelected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=3
//>>excludeEnd("ctx");
][0];
$recv($1)._on_send_to_($globals.HLMethodsFocusRequested,"onMethodsFocusRequested",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "observeSystem",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeSystem\x0a\x09self model systemAnnouncer \x0a    \x09on: ProtocolAdded\x0a        send: #onProtocolAdded:\x0a\x09\x09to: self;\x0a    \x09\x0a\x09\x09on: ProtocolRemoved\x0a        send: #onProtocolRemoved:\x0a\x09\x09to: self;\x0a\x09\x09\x0a    \x09on: MethodAdded \x0a        send: #onMethodAdded:\x0a\x09\x09to: self;\x0a\x09\x09\x0a        on: MethodRemoved \x0a        send: #onMethodRemoved:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: MethodMoved \x0a        send: #onMethodMoved:\x0a\x09\x09to: self",
referencedClasses: ["ProtocolAdded", "ProtocolRemoved", "MethodAdded", "MethodRemoved", "MethodMoved"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "systemAnnouncer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._model())._systemAnnouncer();
[$recv($1)._on_send_to_($globals.ProtocolAdded,"onProtocolAdded:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.ProtocolRemoved,"onProtocolRemoved:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.MethodAdded,"onMethodAdded:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=3
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.MethodRemoved,"onMethodRemoved:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=4
//>>excludeEnd("ctx");
][0];
$recv($1)._on_send_to_($globals.MethodMoved,"onMethodMoved:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeSystem",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "onMethodAdded:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onMethodAdded: anAnnouncement\x0a\x09self model selectedClass = anAnnouncement method methodClass ifFalse: [ ^ self ].\x0a    \x0a    self setItemsForSelectedProtocol.\x0a    self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifFalse:", "=", "selectedClass", "model", "methodClass", "method", "setItemsForSelectedProtocol", "refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if(!$core.assert($recv($recv($self._model())._selectedClass()).__eq($recv($recv(anAnnouncement)._method())._methodClass()))){
return self;
}
$self._setItemsForSelectedProtocol();
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onMethodAdded:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "onMethodMoved:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onMethodMoved: anAnnouncement\x0a\x09self model selectedMethod = anAnnouncement method ifFalse: [ ^ self ].\x0a    \x0a\x09self model selectedProtocol = self model allProtocol ifFalse: [\x0a\x09\x09self \x0a\x09\x09\x09selectedItem: nil; \x0a\x09\x09\x09selectItem: nil;\x0a\x09\x09\x09setItemsForSelectedProtocol;\x0a    \x09\x09refresh ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifFalse:", "=", "selectedMethod", "model", "method", "selectedProtocol", "allProtocol", "selectedItem:", "selectItem:", "setItemsForSelectedProtocol", "refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if(!$core.assert([$recv($recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._selectedMethod()).__eq($recv(anAnnouncement)._method())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["="]=1
//>>excludeEnd("ctx");
][0])){
return self;
}
if(!$core.assert($recv($recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=2
//>>excludeEnd("ctx");
][0])._selectedProtocol()).__eq($recv($self._model())._allProtocol()))){
$self._selectedItem_(nil);
$self._selectItem_(nil);
$self._setItemsForSelectedProtocol();
$self._refresh();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onMethodMoved:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "onMethodRemoved:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onMethodRemoved: anAnnouncement\x0a\x09| method |\x0a\x09\x0a\x09method := anAnnouncement method.\x0a\x09\x0a\x09self items detect: [ :each | each = method selector ] ifNone: [ ^ self ].\x0a\x0a    self selectedItem ifNotNil: [\x0a      \x09(method methodClass = self model selectedClass and: [ method selector = self selectedItem ])\x0a  \x09\x09\x09ifTrue: [ \x0a\x09\x09\x09\x09self selectedItem: nil; \x0a\x09\x09\x09\x09selectItem: nil ] ].\x0a\x0a    self setItemsForSelectedProtocol.\x0a\x09self \x0a\x09\x09refresh;\x0a\x09\x09focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["method", "detect:ifNone:", "items", "=", "selector", "ifNotNil:", "selectedItem", "ifTrue:", "and:", "methodClass", "selectedClass", "model", "selectedItem:", "selectItem:", "setItemsForSelectedProtocol", "refresh", "focus"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var method;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
var $early={};
try {
method=$recv(anAnnouncement)._method();
$recv($self._items())._detect_ifNone_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return [$recv(each).__eq([$recv(method)._selector()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["selector"]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["="]=1
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
}),(function(){
throw $early=[self];

}));
$1=[$self._selectedItem()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedItem"]=1
//>>excludeEnd("ctx");
][0];
if($1 == null || $1.a$nil){
$1;
} else {
if($core.assert([$recv($recv(method)._methodClass()).__eq($recv($self._model())._selectedClass())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["="]=2
//>>excludeEnd("ctx");
][0])){
$2=$recv($recv(method)._selector()).__eq($self._selectedItem());
} else {
$2=false;
}
if($core.assert($2)){
$self._selectedItem_(nil);
$self._selectItem_(nil);
}
}
$self._setItemsForSelectedProtocol();
$self._refresh();
$self._focus();
return self;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onMethodRemoved:",{anAnnouncement:anAnnouncement,method:method})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "onMethodSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onMethodSelected: anAnnouncement\x0a\x09| selector method |\x0a\x09\x0a\x09method := anAnnouncement item.\x0a\x09\x0a\x09selector := method isCompiledMethod \x0a\x09\x09ifTrue: [ method selector ]\x0a\x09\x09ifFalse: [ nil ].\x0a\x09\x09\x0a\x09self \x0a\x09\x09selectedItem: selector;\x0a\x09\x09activateItem: selector",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["item", "ifTrue:ifFalse:", "isCompiledMethod", "selector", "selectedItem:", "activateItem:"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var selector,method;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
method=$recv(anAnnouncement)._item();
if($core.assert($recv(method)._isCompiledMethod())){
selector=$recv(method)._selector();
} else {
selector=nil;
}
$self._selectedItem_(selector);
$self._activateItem_(selector);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onMethodSelected:",{anAnnouncement:anAnnouncement,selector:selector,method:method})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "onMethodsFocusRequested",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onMethodsFocusRequested\x0a\x09self focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focus"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onMethodsFocusRequested",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "onProtocolAdded:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onProtocolAdded: anAnnouncement\x0a\x09self model selectedClass = anAnnouncement theClass ifFalse: [ ^ self ].\x0a\x09\x0a\x09self setItemsForSelectedProtocol.\x0a    self refresh.\x0a\x09self focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifFalse:", "=", "selectedClass", "model", "theClass", "setItemsForSelectedProtocol", "refresh", "focus"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if(!$core.assert($recv($recv($self._model())._selectedClass()).__eq($recv(anAnnouncement)._theClass()))){
return self;
}
$self._setItemsForSelectedProtocol();
$self._refresh();
$self._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onProtocolAdded:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "onProtocolRemoved:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onProtocolRemoved: anAnnouncement\x0a\x09self model selectedClass = anAnnouncement theClass ifFalse: [ ^ self ].\x0a\x09\x0a\x09self setItemsForSelectedProtocol.\x0a    self refresh.\x0a\x09self focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifFalse:", "=", "selectedClass", "model", "theClass", "setItemsForSelectedProtocol", "refresh", "focus"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if(!$core.assert($recv($recv($self._model())._selectedClass()).__eq($recv(anAnnouncement)._theClass()))){
return self;
}
$self._setItemsForSelectedProtocol();
$self._refresh();
$self._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onProtocolRemoved:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "onProtocolSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onProtocolSelected: anAnnouncement\x0a\x09anAnnouncement isSoft ifTrue: [ ^ self ].\x0a    self selectedItem: nil.\x0a    \x0a\x09self setItemsForSelectedProtocol.\x0a    self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "isSoft", "selectedItem:", "setItemsForSelectedProtocol", "refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv(anAnnouncement)._isSoft())){
return self;
}
$self._selectedItem_(nil);
$self._setItemsForSelectedProtocol();
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onProtocolSelected:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "onShowInstanceToggled",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onShowInstanceToggled\x0a\x09self onProtocolSelected: (HLProtocolSelected on: nil)",
referencedClasses: ["HLProtocolSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["onProtocolSelected:", "on:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._onProtocolSelected_($recv($globals.HLProtocolSelected)._on_(nil));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onShowInstanceToggled",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "overrideSelectors",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "overrideSelectors\x0a\x09^ self selectorsCache \x0a    \x09at: 'override'\x0a        ifAbsentPut: [ \x0a        \x09self model selectedClass allSuperclasses\x0a\x09\x09\x09\x09inject: Set new into: [ :acc :each | acc addAll: each selectors; yourself ] ]",
referencedClasses: ["Set"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["at:ifAbsentPut:", "selectorsCache", "inject:into:", "allSuperclasses", "selectedClass", "model", "new", "addAll:", "selectors", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._selectorsCache())._at_ifAbsentPut_("override",(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv($recv($self._model())._selectedClass())._allSuperclasses())._inject_into_($recv($globals.Set)._new(),(function(acc,each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
$recv(acc)._addAll_($recv(each)._selectors());
return $recv(acc)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({acc:acc,each:each},$ctx2,2)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"overrideSelectors",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "overridenSelectors",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "overridenSelectors\x0a\x09^ self selectorsCache \x0a    \x09at: 'overriden'\x0a        ifAbsentPut: [ \x0a        \x09self model selectedClass allSubclasses\x0a\x09\x09\x09\x09inject: Set new into: [ :acc :each | acc addAll: each selectors; yourself ] ]",
referencedClasses: ["Set"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["at:ifAbsentPut:", "selectorsCache", "inject:into:", "allSubclasses", "selectedClass", "model", "new", "addAll:", "selectors", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._selectorsCache())._at_ifAbsentPut_("overriden",(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv($recv($self._model())._selectedClass())._allSubclasses())._inject_into_($recv($globals.Set)._new(),(function(acc,each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
$recv(acc)._addAll_($recv(each)._selectors());
return $recv(acc)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({acc:acc,each:each},$ctx2,2)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"overridenSelectors",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09self model showInstance\x0a    \x09ifFalse: [ html div \x0a        \x09class: 'class_side'; \x0a            with: [ super renderContentOn: html ] ]\x0a      \x09ifTrue: [ super renderContentOn: html ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifFalse:ifTrue:", "showInstance", "model", "class:", "div", "with:", "renderContentOn:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
if($core.assert($recv($self._model())._showInstance())){
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._renderContentOn_.call($self,html))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
} else {
$1=$recv(html)._div();
$recv($1)._class_("class_side");
$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return [(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._renderContentOn_.call($self,html))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["renderContentOn:"]=1,$ctx2.supercall = false
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "renderItemLabel:on:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSelector", "html"],
source: "renderItemLabel: aSelector on: html\x0a\x09| origin |\x0a    \x0a    origin := (self methodForSelector: aSelector) origin.\x0a\x09self model selectedClass = origin\x0a\x09\x09ifTrue: [ html with: aSelector ]\x0a\x09\x09ifFalse: [ html with: aSelector; with: ' '; with: '(', origin name, ')' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["origin", "methodForSelector:", "ifTrue:ifFalse:", "=", "selectedClass", "model", "with:", ",", "name"]
}, function ($methodClass){ return function (aSelector,html){
var self=this,$self=this;
var origin;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
origin=$recv($self._methodForSelector_(aSelector))._origin();
if($core.assert($recv($recv($self._model())._selectedClass()).__eq(origin))){
[$recv(html)._with_(aSelector)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
} else {
[$recv(html)._with_(aSelector)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=2
//>>excludeEnd("ctx");
][0];
[$recv(html)._with_(" ")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=3
//>>excludeEnd("ctx");
][0];
$recv(html)._with_([$recv("(".__comma($recv(origin)._name())).__comma(")")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx[","]=1
//>>excludeEnd("ctx");
][0]);
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderItemLabel:on:",{aSelector:aSelector,html:html,origin:origin})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "reselectItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSelector"],
source: "reselectItem: aSelector\x0a\x09self model softSelectedMethod: (self methodForSelector: aSelector)",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["softSelectedMethod:", "model", "methodForSelector:"]
}, function ($methodClass){ return function (aSelector){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._softSelectedMethod_($self._methodForSelector_(aSelector));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"reselectItem:",{aSelector:aSelector})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "selectItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSelector"],
source: "selectItem: aSelector\x0a\x09aSelector ifNil: [ ^ self model selectedMethod: nil ].\x0a\x0a   \x09self model selectedMethod: (self methodForSelector: aSelector)",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "selectedMethod:", "model", "methodForSelector:"]
}, function ($methodClass){ return function (aSelector){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if(aSelector == null || aSelector.a$nil){
return [$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._selectedMethod_(nil)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedMethod:"]=1
//>>excludeEnd("ctx");
][0];
} else {
aSelector;
}
$recv($self._model())._selectedMethod_($self._methodForSelector_(aSelector));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectItem:",{aSelector:aSelector})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "selectorsCache",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectorsCache\x0a\x09^ self class selectorsCache",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectorsCache", "class"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._class())._selectorsCache();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectorsCache",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "selectorsInProtocol:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "selectorsInProtocol: aString\x0a\x09^ ((self methodsInProtocol: aString)\x0a    \x09collect: [ :each | each selector ]) sorted",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["sorted", "collect:", "methodsInProtocol:", "selector"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._methodsInProtocol_(aString))._collect_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._selector();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,1)});
//>>excludeEnd("ctx");
})))._sorted();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectorsInProtocol:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "setItemsForProtocol:",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "setItemsForProtocol: aString\x0a\x09^ self items: (aString\x0a    \x09ifNil: [ #() ]\x0a      \x09ifNotNil: [ self selectorsInProtocol: aString ])",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["items:", "ifNil:ifNotNil:", "selectorsInProtocol:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
if(aString == null || aString.a$nil){
$1=[];
} else {
$1=$self._selectorsInProtocol_(aString);
}
return $self._items_($1);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setItemsForProtocol:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);

$core.addMethod(
$core.method({
selector: "setItemsForSelectedProtocol",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setItemsForSelectedProtocol\x0a\x09self setItemsForProtocol: self model selectedProtocol",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["setItemsForProtocol:", "selectedProtocol", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._setItemsForProtocol_($recv($self._model())._selectedProtocol());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setItemsForSelectedProtocol",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget);


$core.setSlots($globals.HLMethodsListWidget.a$cls, ["selectorsCache"]);
$core.addMethod(
$core.method({
selector: "selectorsCache",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectorsCache\x0a\x09^ HLSelectorsCache current",
referencedClasses: ["HLSelectorsCache"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["current"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.HLSelectorsCache)._current();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectorsCache",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLMethodsListWidget.a$cls);


$core.addClass("HLPackagesListWidget", $globals.HLToolListWidget, "Helios-Browser");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLPackagesListWidget.comment="I render a list of the system packages.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "cssClassForItem:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anItem"],
source: "cssClassForItem: anItem\x09\x0a\x09^ anItem isDirty \x0a\x09\x09ifTrue: [ 'package_dirty' ]\x0a\x09\x09ifFalse: [ 'package' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:ifFalse:", "isDirty"]
}, function ($methodClass){ return function (anItem){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv(anItem)._isDirty())){
return "package_dirty";
} else {
return "package";
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"cssClassForItem:",{anItem:anItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackagesListWidget);

$core.addMethod(
$core.method({
selector: "focusClassesListWidget",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "focusClassesListWidget\x0a\x09self model announcer announce: HLClassesListFocus new",
referencedClasses: ["HLClassesListFocus"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "model", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self._model())._announcer())._announce_($recv($globals.HLClassesListFocus)._new());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"focusClassesListWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackagesListWidget);

$core.addMethod(
$core.method({
selector: "initializeItems",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initializeItems\x0a\x09^ items := self model packages \x0a\x09\x09sort: [ :a :b | a name < b name ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["sort:", "packages", "model", "<", "name"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.items=$recv($recv($self._model())._packages())._sort_((function(a,b){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv([$recv(a)._name()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["name"]=1
//>>excludeEnd("ctx");
][0]).__lt($recv(b)._name());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({a:a,b:b},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return $self.items;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initializeItems",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackagesListWidget);

$core.addMethod(
$core.method({
selector: "items",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "items\x0a\x09^ items ifNil: [ self initializeItems ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "initializeItems"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.items;
if($1 == null || $1.a$nil){
return $self._initializeItems();
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"items",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackagesListWidget);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Packages'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Packages";

}; }),
$globals.HLPackagesListWidget);

$core.addMethod(
$core.method({
selector: "observeModel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeModel\x0a    self model announcer \x0a\x09\x09on: HLPackageSelected\x0a\x09\x09send: #onPackageSelected:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLPackagesFocusRequested \x0a\x09\x09send: #onPackagesFocusRequested\x0a\x09\x09to: self",
referencedClasses: ["HLPackageSelected", "HLPackagesFocusRequested"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._model())._announcer();
[$recv($1)._on_send_to_($globals.HLPackageSelected,"onPackageSelected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
$recv($1)._on_send_to_($globals.HLPackagesFocusRequested,"onPackagesFocusRequested",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackagesListWidget);

$core.addMethod(
$core.method({
selector: "observeSystem",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeSystem\x0a    self model systemAnnouncer \x0a\x09\x09on: ClassAdded \x0a\x09\x09send: #onClassAdded:\x0a\x09\x09to: self.\x0a\x09\x09\x0a\x09self model systemAnnouncer\x0a\x09\x09on: PackageAdded\x0a\x09\x09send: #onPackageAdded:\x0a\x09\x09to: self.\x0a\x09\x09\x0a\x09self model systemAnnouncer\x0a\x09\x09on: PackageRemoved\x0a\x09\x09send: #onPackageRemoved:\x0a\x09\x09to: self.\x0a\x09\x09\x0a\x09self model systemAnnouncer\x0a\x09\x09on: PackageClean\x0a\x09\x09send: #onPackageStateChanged\x0a\x09\x09to: self.\x0a\x09\x09\x0a\x09self model systemAnnouncer\x0a\x09\x09on: PackageDirty\x0a\x09\x09send: #onPackageStateChanged\x0a\x09\x09to: self.",
referencedClasses: ["ClassAdded", "PackageAdded", "PackageRemoved", "PackageClean", "PackageDirty"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "systemAnnouncer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$recv([$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._systemAnnouncer()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["systemAnnouncer"]=1
//>>excludeEnd("ctx");
][0])._on_send_to_($globals.ClassAdded,"onClassAdded:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
[$recv([$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=2
//>>excludeEnd("ctx");
][0])._systemAnnouncer()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["systemAnnouncer"]=2
//>>excludeEnd("ctx");
][0])._on_send_to_($globals.PackageAdded,"onPackageAdded:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=2
//>>excludeEnd("ctx");
][0];
[$recv([$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=3
//>>excludeEnd("ctx");
][0])._systemAnnouncer()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["systemAnnouncer"]=3
//>>excludeEnd("ctx");
][0])._on_send_to_($globals.PackageRemoved,"onPackageRemoved:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=3
//>>excludeEnd("ctx");
][0];
[$recv([$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=4
//>>excludeEnd("ctx");
][0])._systemAnnouncer()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["systemAnnouncer"]=4
//>>excludeEnd("ctx");
][0])._on_send_to_($globals.PackageClean,"onPackageStateChanged",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=4
//>>excludeEnd("ctx");
][0];
$recv($recv($self._model())._systemAnnouncer())._on_send_to_($globals.PackageDirty,"onPackageStateChanged",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeSystem",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackagesListWidget);

$core.addMethod(
$core.method({
selector: "onClassAdded:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onClassAdded: anAnnouncement\x0a\x09\x22Amber doesn't have yet a global organizer for packages\x22\x0a\x09\x0a\x09(self items includes: anAnnouncement theClass package) ifFalse: [ \x0a\x09\x09self \x0a\x09\x09\x09initializeItems;\x0a\x09\x09\x09refresh ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifFalse:", "includes:", "items", "package", "theClass", "initializeItems", "refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if(!$core.assert($recv($self._items())._includes_($recv($recv(anAnnouncement)._theClass())._package()))){
$self._initializeItems();
$self._refresh();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassAdded:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackagesListWidget);

$core.addMethod(
$core.method({
selector: "onPackageAdded:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onPackageAdded: anAnnouncement\x0a\x09self \x0a\x09\x09initializeItems;\x0a\x09\x09refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initializeItems", "refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._initializeItems();
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onPackageAdded:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackagesListWidget);

$core.addMethod(
$core.method({
selector: "onPackageRemoved:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onPackageRemoved: anAnnouncement\x0a\x09self \x0a\x09\x09initializeItems;\x0a\x09\x09refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initializeItems", "refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._initializeItems();
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onPackageRemoved:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackagesListWidget);

$core.addMethod(
$core.method({
selector: "onPackageSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onPackageSelected: anAnnouncement\x0a\x09| package |\x0a\x09\x0a\x09package := anAnnouncement item.\x0a\x09\x0a\x09self selectedItem: package.\x0a\x09self hasFocus ifFalse: [\x0a\x09\x09self\x0a\x09\x09\x09activateItem: package;\x0a\x09\x09\x09focus ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["item", "selectedItem:", "ifFalse:", "hasFocus", "activateItem:", "focus"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var package_;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
package_=$recv(anAnnouncement)._item();
$self._selectedItem_(package_);
if(!$core.assert($self._hasFocus())){
$self._activateItem_(package_);
$self._focus();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onPackageSelected:",{anAnnouncement:anAnnouncement,package_:package_})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackagesListWidget);

$core.addMethod(
$core.method({
selector: "onPackageStateChanged",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onPackageStateChanged\x0a\x09self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["refresh"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onPackageStateChanged",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackagesListWidget);

$core.addMethod(
$core.method({
selector: "onPackagesFocusRequested",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onPackagesFocusRequested\x0a\x09self focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focus"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onPackagesFocusRequested",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackagesListWidget);

$core.addMethod(
$core.method({
selector: "renderItemLabel:on:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aPackage", "html"],
source: "renderItemLabel: aPackage on: html\x0a\x09html with: aPackage name",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "name"]
}, function ($methodClass){ return function (aPackage,html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(html)._with_($recv(aPackage)._name());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderItemLabel:on:",{aPackage:aPackage,html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackagesListWidget);

$core.addMethod(
$core.method({
selector: "reselectItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anItem"],
source: "reselectItem: anItem\x0a\x09self model softSelectedPackage: anItem",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["softSelectedPackage:", "model"]
}, function ($methodClass){ return function (anItem){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._softSelectedPackage_(anItem);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"reselectItem:",{anItem:anItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackagesListWidget);

$core.addMethod(
$core.method({
selector: "selectItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aPackage"],
source: "selectItem: aPackage\x0a\x09super selectItem: aPackage.\x0a\x09self model selectedPackage: aPackage",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectItem:", "selectedPackage:", "model"]
}, function ($methodClass){ return function (aPackage){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._selectItem_.call($self,aPackage))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv($self._model())._selectedPackage_(aPackage);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectItem:",{aPackage:aPackage})});
//>>excludeEnd("ctx");
}; }),
$globals.HLPackagesListWidget);



$core.addClass("HLProtocolsListWidget", $globals.HLToolListWidget, "Helios-Browser");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLProtocolsListWidget.comment="I render a list of protocols for the selected class.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "allProtocol",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "allProtocol\x0a\x09^ self model allProtocol",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["allProtocol", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._allProtocol();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"allProtocol",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProtocolsListWidget);

$core.addMethod(
$core.method({
selector: "cssClassForItem:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anItem"],
source: "cssClassForItem: anItem\x0a\x09anItem = self allProtocol ifTrue: [ ^ '' ].\x0a\x09anItem = 'private' ifTrue: [ ^ 'private' ].\x0a\x09anItem = 'initialization' ifTrue: [ ^ 'initialization' ].\x0a\x09(anItem match: '^\x5c*') ifTrue: [ ^ 'extension' ].\x0a\x09^ ''",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "=", "allProtocol", "match:"]
}, function ($methodClass){ return function (anItem){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert([$recv(anItem).__eq($self._allProtocol())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["="]=1
//>>excludeEnd("ctx");
][0])){
return "";
}
if($core.assert([$recv(anItem).__eq("private")
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["="]=2
//>>excludeEnd("ctx");
][0])){
return "private";
}
if($core.assert($recv(anItem).__eq("initialization"))){
return "initialization";
}
if($core.assert($recv(anItem)._match_("^\x5c*"))){
return "extension";
}
return "";
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"cssClassForItem:",{anItem:anItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProtocolsListWidget);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Protocols'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Protocols";

}; }),
$globals.HLProtocolsListWidget);

$core.addMethod(
$core.method({
selector: "observeModel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeModel\x0a    self model announcer \x0a\x09\x09on: HLClassSelected\x0a\x09\x09send: #onClassSelected:\x0a\x09\x09to: self;\x0a\x09\x09\x0a    \x09on: HLShowInstanceToggled \x0a\x09\x09send: #onShowInstanceToggled:\x0a\x09\x09to: self;\x0a\x09\x09\x0a    \x09on: HLProtocolSelected\x0a\x09\x09send: #onProtocolSelected:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: HLProtocolsFocusRequested \x0a\x09\x09send: #onProtocolsFocusRequested\x0a\x09\x09to: self",
referencedClasses: ["HLClassSelected", "HLShowInstanceToggled", "HLProtocolSelected", "HLProtocolsFocusRequested"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._model())._announcer();
[$recv($1)._on_send_to_($globals.HLClassSelected,"onClassSelected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLShowInstanceToggled,"onShowInstanceToggled:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=2
//>>excludeEnd("ctx");
][0];
[$recv($1)._on_send_to_($globals.HLProtocolSelected,"onProtocolSelected:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=3
//>>excludeEnd("ctx");
][0];
$recv($1)._on_send_to_($globals.HLProtocolsFocusRequested,"onProtocolsFocusRequested",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProtocolsListWidget);

$core.addMethod(
$core.method({
selector: "observeSystem",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeSystem\x0a\x09self model systemAnnouncer\x0a\x09\x09on: ProtocolAdded \x0a\x09\x09send: #onProtocolAdded:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: ProtocolRemoved\x0a\x09\x09send: #onProtocolRemoved:\x0a\x09\x09to: self",
referencedClasses: ["ProtocolAdded", "ProtocolRemoved"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "systemAnnouncer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($self._model())._systemAnnouncer();
[$recv($1)._on_send_to_($globals.ProtocolAdded,"onProtocolAdded:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
$recv($1)._on_send_to_($globals.ProtocolRemoved,"onProtocolRemoved:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeSystem",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProtocolsListWidget);

$core.addMethod(
$core.method({
selector: "onClassSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onClassSelected: anAnnouncement\x0a\x09anAnnouncement isSoft ifTrue: [ ^ self ].\x0a    self selectedItem: nil.\x0a    \x0a    self setItemsForSelectedClass.\x0a    self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:", "isSoft", "selectedItem:", "setItemsForSelectedClass", "refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($recv(anAnnouncement)._isSoft())){
return self;
}
$self._selectedItem_(nil);
$self._setItemsForSelectedClass();
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onClassSelected:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProtocolsListWidget);

$core.addMethod(
$core.method({
selector: "onProtocolAdded:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onProtocolAdded: anAnnouncement\x0a\x09| class |\x0a\x09\x0a\x09class := anAnnouncement theClass.\x0a\x09\x0a\x09class = self model selectedClass ifFalse: [ ^ self ].\x0a    \x0a    self setItemsForSelectedClass.\x0a    self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["theClass", "ifFalse:", "=", "selectedClass", "model", "setItemsForSelectedClass", "refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var class_;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
class_=$recv(anAnnouncement)._theClass();
if(!$core.assert($recv(class_).__eq($recv($self._model())._selectedClass()))){
return self;
}
$self._setItemsForSelectedClass();
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onProtocolAdded:",{anAnnouncement:anAnnouncement,class_:class_})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProtocolsListWidget);

$core.addMethod(
$core.method({
selector: "onProtocolRemoved:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onProtocolRemoved: anAnnouncement\x0a\x09| class protocol |\x0a\x09\x0a\x09class := anAnnouncement theClass.\x0a\x09protocol := anAnnouncement protocol.\x0a\x09\x0a\x09class = self model selectedClass ifFalse: [ ^ self ].\x0a    \x0a    self model selectedProtocol = protocol \x0a    \x09ifTrue: [ \x0a\x09\x09\x09self \x0a\x09\x09\x09\x09selectedItem: nil;\x0a\x09\x09\x09\x09selectItem: nil ].\x0a        \x0a    self setItemsForSelectedClass.\x0a    self refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["theClass", "protocol", "ifFalse:", "=", "selectedClass", "model", "ifTrue:", "selectedProtocol", "selectedItem:", "selectItem:", "setItemsForSelectedClass", "refresh"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var class_,protocol;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
class_=$recv(anAnnouncement)._theClass();
protocol=$recv(anAnnouncement)._protocol();
if(!$core.assert([$recv(class_).__eq($recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._selectedClass())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["="]=1
//>>excludeEnd("ctx");
][0])){
return self;
}
if($core.assert($recv($recv($self._model())._selectedProtocol()).__eq(protocol))){
$self._selectedItem_(nil);
$self._selectItem_(nil);
}
$self._setItemsForSelectedClass();
$self._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onProtocolRemoved:",{anAnnouncement:anAnnouncement,class_:class_,protocol:protocol})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProtocolsListWidget);

$core.addMethod(
$core.method({
selector: "onProtocolSelected:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onProtocolSelected: anAnnouncement\x0a\x09| protocol |\x0a\x09\x0a\x09protocol := anAnnouncement item.\x0a\x09\x0a\x09self selectedItem: protocol.\x0a\x09protocol ifNil: [ ^ self ].\x0a    \x0a\x09self hasFocus ifFalse: [\x0a\x09\x09self \x0a\x09\x09\x09activateItem: protocol;\x0a\x09\x09\x09focus ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["item", "selectedItem:", "ifNil:", "ifFalse:", "hasFocus", "activateItem:", "focus"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
var protocol;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
protocol=$recv(anAnnouncement)._item();
$self._selectedItem_(protocol);
$1=protocol;
if($1 == null || $1.a$nil){
return self;
} else {
$1;
}
if(!$core.assert($self._hasFocus())){
$self._activateItem_(protocol);
$self._focus();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onProtocolSelected:",{anAnnouncement:anAnnouncement,protocol:protocol})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProtocolsListWidget);

$core.addMethod(
$core.method({
selector: "onProtocolsFocusRequested",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onProtocolsFocusRequested\x0a\x09self focus",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["focus"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onProtocolsFocusRequested",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProtocolsListWidget);

$core.addMethod(
$core.method({
selector: "onShowInstanceToggled:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onShowInstanceToggled: anAnnouncement\x0a\x09self onClassSelected: (HLClassSelected on: nil)",
referencedClasses: ["HLClassSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["onClassSelected:", "on:"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._onClassSelected_($recv($globals.HLClassSelected)._on_(nil));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onShowInstanceToggled:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProtocolsListWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09self model showInstance\x0a    \x09ifFalse: [ html div \x0a        \x09class: 'class_side'; \x0a            with: [ super renderContentOn: html ] ]\x0a      \x09ifTrue: [ super renderContentOn: html ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifFalse:ifTrue:", "showInstance", "model", "class:", "div", "with:", "renderContentOn:"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
if($core.assert($recv($self._model())._showInstance())){
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._renderContentOn_.call($self,html))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
} else {
$1=$recv(html)._div();
$recv($1)._class_("class_side");
$recv($1)._with_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return [(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._renderContentOn_.call($self,html))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["renderContentOn:"]=1,$ctx2.supercall = false
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProtocolsListWidget);

$core.addMethod(
$core.method({
selector: "reselectItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anItem"],
source: "reselectItem: anItem\x0a\x09self model softSelectedProtocol: anItem",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["softSelectedProtocol:", "model"]
}, function ($methodClass){ return function (anItem){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._softSelectedProtocol_(anItem);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"reselectItem:",{anItem:anItem})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProtocolsListWidget);

$core.addMethod(
$core.method({
selector: "selectItem:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "selectItem: aString\x0a    self model selectedProtocol: aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectedProtocol:", "model"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._selectedProtocol_(aString);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectItem:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProtocolsListWidget);

$core.addMethod(
$core.method({
selector: "selectedItem",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectedItem\x0a\x09^ super selectedItem\x22 ifNil: [ self allProtocol ]\x22",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectedItem"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return [(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._selectedItem.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectedItem",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProtocolsListWidget);

$core.addMethod(
$core.method({
selector: "setItemsForClass:",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass"],
source: "setItemsForClass: aClass\x0a\x09self items: (aClass\x0a    \x09ifNil: [ Array with: self allProtocol ]\x0a      \x09ifNotNil: [ \x0a        \x09(Array with: self allProtocol) \x0a            \x09addAll: aClass protocols; \x0a                yourself ])",
referencedClasses: ["Array"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["items:", "ifNil:ifNotNil:", "with:", "allProtocol", "addAll:", "protocols", "yourself"]
}, function ($methodClass){ return function (aClass){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
if(aClass == null || aClass.a$nil){
$2=[$recv($globals.Array)._with_([$self._allProtocol()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["allProtocol"]=1
//>>excludeEnd("ctx");
][0])
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
} else {
$1=$recv($globals.Array)._with_($self._allProtocol());
$recv($1)._addAll_($recv(aClass)._protocols());
$2=$recv($1)._yourself();
}
$self._items_($2);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setItemsForClass:",{aClass:aClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProtocolsListWidget);

$core.addMethod(
$core.method({
selector: "setItemsForSelectedClass",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "setItemsForSelectedClass\x0a\x09self setItemsForClass: self model selectedClass",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["setItemsForClass:", "selectedClass", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._setItemsForClass_($recv($self._model())._selectedClass());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setItemsForSelectedClass",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLProtocolsListWidget);



$core.addClass("HLSelectorsCache", $globals.Object, "Helios-Browser");
$core.setSlots($globals.HLSelectorsCache, ["classesCache"]);
$core.addMethod(
$core.method({
selector: "cacheFor:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass"],
source: "cacheFor: aClass\x0a\x09aClass ifNil: [ ^ nil ].\x0a    \x0a\x09^ self classesCache\x0a    \x09at: aClass name\x0a        ifAbsentPut: [ self newCacheFor: aClass ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "at:ifAbsentPut:", "classesCache", "name", "newCacheFor:"]
}, function ($methodClass){ return function (aClass){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if(aClass == null || aClass.a$nil){
return nil;
} else {
aClass;
}
return $recv($self._classesCache())._at_ifAbsentPut_($recv(aClass)._name(),(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._newCacheFor_(aClass);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"cacheFor:",{aClass:aClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSelectorsCache);

$core.addMethod(
$core.method({
selector: "classesCache",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "classesCache\x0a\x09^ classesCache ifNil: [ classesCache := HashedCollection new ]",
referencedClasses: ["HashedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.classesCache;
if($1 == null || $1.a$nil){
$self.classesCache=$recv($globals.HashedCollection)._new();
return $self.classesCache;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"classesCache",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSelectorsCache);

$core.addMethod(
$core.method({
selector: "initialize",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initialize\x0a\x09super initialize.\x0a    self observeSystem",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initialize", "observeSystem"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._initialize.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$self._observeSystem();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initialize",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSelectorsCache);

$core.addMethod(
$core.method({
selector: "invalidateCacheFor:",
protocol: "private",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aMethod"],
source: "invalidateCacheFor: aMethod\x0a\x09(self cacheFor: aMethod methodClass)\x0a    \x09invalidateSelector: aMethod selector",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["invalidateSelector:", "cacheFor:", "methodClass", "selector"]
}, function ($methodClass){ return function (aMethod){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._cacheFor_($recv(aMethod)._methodClass()))._invalidateSelector_($recv(aMethod)._selector());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"invalidateCacheFor:",{aMethod:aMethod})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSelectorsCache);

$core.addMethod(
$core.method({
selector: "isOverridden:",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aMethod"],
source: "isOverridden: aMethod\x0a\x09^ (self cacheFor: aMethod methodClass)\x0a    \x09isOverridden: aMethod",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["isOverridden:", "cacheFor:", "methodClass"]
}, function ($methodClass){ return function (aMethod){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._cacheFor_($recv(aMethod)._methodClass()))._isOverridden_(aMethod);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isOverridden:",{aMethod:aMethod})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSelectorsCache);

$core.addMethod(
$core.method({
selector: "isOverride:",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aMethod"],
source: "isOverride: aMethod\x0a\x09^ (self cacheFor: aMethod methodClass)\x0a    \x09isOverride: aMethod",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["isOverride:", "cacheFor:", "methodClass"]
}, function ($methodClass){ return function (aMethod){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._cacheFor_($recv(aMethod)._methodClass()))._isOverride_(aMethod);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isOverride:",{aMethod:aMethod})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSelectorsCache);

$core.addMethod(
$core.method({
selector: "newCacheFor:",
protocol: "factory",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aClass"],
source: "newCacheFor: aClass\x0a\x09^ HLClassCache \x0a    \x09on: aClass\x0a        selectorsCache: self",
referencedClasses: ["HLClassCache"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:selectorsCache:"]
}, function ($methodClass){ return function (aClass){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.HLClassCache)._on_selectorsCache_(aClass,self);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"newCacheFor:",{aClass:aClass})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSelectorsCache);

$core.addMethod(
$core.method({
selector: "observeSystem",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeSystem\x0a\x09SystemAnnouncer current\x0a\x09\x09on: MethodAdded\x0a\x09\x09send: #onMethodAdded:\x0a\x09\x09to: self;\x0a\x09\x09\x0a\x09\x09on: MethodRemoved\x0a        send: #onMethodRemoved:\x0a\x09\x09to: self",
referencedClasses: ["SystemAnnouncer", "MethodAdded", "MethodRemoved"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "current"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.SystemAnnouncer)._current();
[$recv($1)._on_send_to_($globals.MethodAdded,"onMethodAdded:",self)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["on:send:to:"]=1
//>>excludeEnd("ctx");
][0];
$recv($1)._on_send_to_($globals.MethodRemoved,"onMethodRemoved:",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeSystem",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSelectorsCache);

$core.addMethod(
$core.method({
selector: "onMethodAdded:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onMethodAdded: anAnnouncement\x0a\x09self invalidateCacheFor: anAnnouncement method",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["invalidateCacheFor:", "method"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._invalidateCacheFor_($recv(anAnnouncement)._method());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onMethodAdded:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSelectorsCache);

$core.addMethod(
$core.method({
selector: "onMethodRemoved:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAnnouncement"],
source: "onMethodRemoved: anAnnouncement\x0a\x09self invalidateCacheFor: anAnnouncement method",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["invalidateCacheFor:", "method"]
}, function ($methodClass){ return function (anAnnouncement){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._invalidateCacheFor_($recv(anAnnouncement)._method());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onMethodRemoved:",{anAnnouncement:anAnnouncement})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSelectorsCache);


$core.setSlots($globals.HLSelectorsCache.a$cls, ["current"]);
$core.addMethod(
$core.method({
selector: "current",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "current\x0a\x09^ current ifNil: [ current := super new ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.current;
if($1 == null || $1.a$nil){
$self.current=[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._new.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
return $self.current;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"current",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSelectorsCache.a$cls);

$core.addMethod(
$core.method({
selector: "flush",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "flush\x0a\x09current := nil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
$self.current=nil;
return self;

}; }),
$globals.HLSelectorsCache.a$cls);

$core.addMethod(
$core.method({
selector: "new",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "new\x0a\x09self shouldNotImplement",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["shouldNotImplement"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._shouldNotImplement();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"new",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSelectorsCache.a$cls);

});
