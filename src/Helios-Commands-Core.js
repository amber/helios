define(["amber/boot", "require", "amber/core/Kernel-Objects"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-Commands-Core");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLCommand", $globals.Object, "Helios-Commands-Core");
$core.setSlots($globals.HLCommand, ["input"]);
$core.addMethod(
$core.method({
selector: "asActionBinding",
protocol: "converting",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "asActionBinding\x0a\x09^ (HLBindingAction on: self keyCode labelled: self label)\x0a    \x09command: self;\x0a\x09\x09yourself",
referencedClasses: ["HLBindingAction"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["command:", "on:labelled:", "keyCode", "label", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLBindingAction)._on_labelled_($self._keyCode(),$self._label());
$recv($1)._command_(self);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"asActionBinding",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "asBinding",
protocol: "converting",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "asBinding\x0a\x09^ self isBindingGroup\x0a\x09\x09ifTrue: [ self asGroupBinding ]\x0a\x09\x09ifFalse: [ self asActionBinding ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:ifFalse:", "isBindingGroup", "asGroupBinding", "asActionBinding"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($self._isBindingGroup())){
return $self._asGroupBinding();
} else {
return $self._asActionBinding();
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"asBinding",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "asGroupBinding",
protocol: "converting",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "asGroupBinding\x0a\x09^ HLBindingGroup \x0a\x09\x09on: self keyCode\x0a\x09\x09labelled: self label",
referencedClasses: ["HLBindingGroup"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:labelled:", "keyCode", "label"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.HLBindingGroup)._on_labelled_($self._keyCode(),$self._label());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"asGroupBinding",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "commandError:",
protocol: "error handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "commandError: aString\x0a\x09self error: aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["error:"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._error_(aString);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"commandError:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "defaultInput",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "defaultInput\x0a\x09^ ''",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "";

}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "documentation",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "documentation\x0a\x09^ self class documentation",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["documentation", "class"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._class())._documentation();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"documentation",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return self;

}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "input",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "input\x0a\x09^ input",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.input;

}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "input:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "input: aString\x0a\x09^ input := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.input=aString;
return $self.input;

}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "inputCompletion",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inputCompletion\x0a\x09^ #()",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return [];

}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "inputLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inputLabel\x0a\x09^ self label",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["label"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $self._label();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inputLabel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "isAction",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isAction\x0a\x09^ self isBindingGroup not",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["not", "isBindingGroup"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._isBindingGroup())._not();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isAction",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "isActive",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isActive\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return true;

}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "isBindingGroup",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isBindingGroup\x0a\x09^ (self class methodDictionary includesKey: 'execute') not",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["not", "includesKey:", "methodDictionary", "class"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($recv($self._class())._methodDictionary())._includesKey_("execute"))._not();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isBindingGroup",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "isInputRequired",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isInputRequired\x0a\x09^ false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return false;

}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ self class key",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["key", "class"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._class())._key();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"key",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "keyCode",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "keyCode\x0a\x09^ self key asUppercase charCodeAt: 1",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["charCodeAt:", "asUppercase", "key"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._key())._asUppercase())._charCodeAt_((1));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"keyCode",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ self class label",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["label", "class"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._class())._label();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"label",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "menuLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "menuLabel\x0a\x09^ self class menuLabel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["menuLabel", "class"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._class())._menuLabel();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"menuLabel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand);

$core.addMethod(
$core.method({
selector: "registerOn:",
protocol: "registration",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBinding"],
source: "registerOn: aBinding\x0a\x09^ aBinding add: self asBinding",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["add:", "asBinding"]
}, function ($methodClass){ return function (aBinding){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv(aBinding)._add_($self._asBinding());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"registerOn:",{aBinding:aBinding})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand);


$core.addMethod(
$core.method({
selector: "concreteClasses",
protocol: "registration",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "concreteClasses\x0a\x09| classes |\x0a\x09\x0a\x09classes := OrderedCollection new.\x0a\x09\x0a\x09self isConcrete\x0a\x09\x09ifTrue: [ classes add: self ].\x0a\x09\x09\x0a\x09self subclasses do: [ :each | \x0a\x09\x09classes addAll: each concreteClasses ].\x0a\x09\x09\x0a\x09^ classes",
referencedClasses: ["OrderedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["new", "ifTrue:", "isConcrete", "add:", "do:", "subclasses", "addAll:", "concreteClasses"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var classes;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
classes=$recv($globals.OrderedCollection)._new();
if($core.assert($self._isConcrete())){
$recv(classes)._add_(self);
}
$recv($self._subclasses())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(classes)._addAll_($recv(each)._concreteClasses());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return classes;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"concreteClasses",{classes:classes})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand.a$cls);

$core.addMethod(
$core.method({
selector: "documentation",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "documentation\x0a\x09^ ''",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "";

}; }),
$globals.HLCommand.a$cls);

$core.addMethod(
$core.method({
selector: "isConcrete",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "isConcrete\x0a\x09^ self key notNil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["notNil", "key"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._key())._notNil();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"isConcrete",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand.a$cls);

$core.addMethod(
$core.method({
selector: "isValidFor:",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aModel"],
source: "isValidFor: aModel\x0a\x09^ true",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aModel){
var self=this,$self=this;
return true;

}; }),
$globals.HLCommand.a$cls);

$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09\x22Answer a single character string or nil if no key\x22\x0a\x09\x0a\x09^ nil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return nil;

}; }),
$globals.HLCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ ''",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "";

}; }),
$globals.HLCommand.a$cls);

$core.addMethod(
$core.method({
selector: "menuLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "menuLabel\x0a\x09^ self label",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["label"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $self._label();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"menuLabel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand.a$cls);

$core.addMethod(
$core.method({
selector: "registerConcreteClassesOn:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBinding"],
source: "registerConcreteClassesOn: aBinding\x0a\x09| newBinding |\x0a\x09\x0a\x09self isConcrete\x0a\x09\x09ifTrue: [ newBinding := self registerOn: aBinding ]\x0a\x09\x09ifFalse: [ newBinding := aBinding ].\x0a\x09\x09\x0a\x09self subclasses do: [ :each | each registerConcreteClassesOn: newBinding ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:ifFalse:", "isConcrete", "registerOn:", "do:", "subclasses", "registerConcreteClassesOn:"]
}, function ($methodClass){ return function (aBinding){
var self=this,$self=this;
var newBinding;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if($core.assert($self._isConcrete())){
newBinding=$self._registerOn_(aBinding);
newBinding;
} else {
newBinding=aBinding;
newBinding;
}
$recv($self._subclasses())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._registerConcreteClassesOn_(newBinding);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,3)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"registerConcreteClassesOn:",{aBinding:aBinding,newBinding:newBinding})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand.a$cls);

$core.addMethod(
$core.method({
selector: "registerOn:",
protocol: "registration",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBinding"],
source: "registerOn: aBinding\x0a\x09^ self new registerOn: aBinding",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["registerOn:", "new"]
}, function ($methodClass){ return function (aBinding){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._new())._registerOn_(aBinding);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"registerOn:",{aBinding:aBinding})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCommand.a$cls);


$core.addClass("HLCloseTabCommand", $globals.HLCommand, "Helios-Commands-Core");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09HLManager current removeActiveTab",
referencedClasses: ["HLManager"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["removeActiveTab", "current"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($globals.HLManager)._current())._removeActiveTab();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLCloseTabCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'w'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "w";

}; }),
$globals.HLCloseTabCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Close tab'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Close tab";

}; }),
$globals.HLCloseTabCommand.a$cls);


$core.addClass("HLModelCommand", $globals.HLCommand, "Helios-Commands-Core");
$core.setSlots($globals.HLModelCommand, ["model"]);
$core.addMethod(
$core.method({
selector: "model",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "model\x0a\x09^ model",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.model;

}; }),
$globals.HLModelCommand);

$core.addMethod(
$core.method({
selector: "model:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aModel"],
source: "model: aModel\x0a\x09model := aModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aModel){
var self=this,$self=this;
$self.model=aModel;
return self;

}; }),
$globals.HLModelCommand);


$core.addMethod(
$core.method({
selector: "for:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aModel"],
source: "for: aModel\x0a\x09^ self new",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["new"]
}, function ($methodClass){ return function (aModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $self._new();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"for:",{aModel:aModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLModelCommand.a$cls);

$core.addMethod(
$core.method({
selector: "registerConcreteClassesOn:for:",
protocol: "registration",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBinding", "aModel"],
source: "registerConcreteClassesOn: aBinding for: aModel\x0a\x09| newBinding |\x0a\x09\x0a\x09(self isConcrete and: [ self isValidFor: aModel ])\x0a\x09\x09ifTrue: [ newBinding := self registerOn: aBinding for: aModel ]\x0a\x09\x09ifFalse: [ newBinding := aBinding ].\x0a\x09\x09\x0a\x09self subclasses do: [ :each |\x0a\x09\x09each registerConcreteClassesOn: newBinding for: aModel ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifTrue:ifFalse:", "and:", "isConcrete", "isValidFor:", "registerOn:for:", "do:", "subclasses", "registerConcreteClassesOn:for:"]
}, function ($methodClass){ return function (aBinding,aModel){
var self=this,$self=this;
var newBinding;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
if($core.assert($self._isConcrete())){
$1=$self._isValidFor_(aModel);
} else {
$1=false;
}
if($core.assert($1)){
newBinding=$self._registerOn_for_(aBinding,aModel);
newBinding;
} else {
newBinding=aBinding;
newBinding;
}
$recv($self._subclasses())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(each)._registerConcreteClassesOn_for_(newBinding,aModel);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each},$ctx1,4)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"registerConcreteClassesOn:for:",{aBinding:aBinding,aModel:aModel,newBinding:newBinding})});
//>>excludeEnd("ctx");
}; }),
$globals.HLModelCommand.a$cls);

$core.addMethod(
$core.method({
selector: "registerOn:for:",
protocol: "registration",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aBinding", "aModel"],
source: "registerOn: aBinding for: aModel\x0a\x09^ (self for: aModel) registerOn: aBinding",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["registerOn:", "for:"]
}, function ($methodClass){ return function (aBinding,aModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._for_(aModel))._registerOn_(aBinding);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"registerOn:for:",{aBinding:aBinding,aModel:aModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLModelCommand.a$cls);


$core.addClass("HLOpenCommand", $globals.HLCommand, "Helios-Commands-Core");

$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'o'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "o";

}; }),
$globals.HLOpenCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Open'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Open";

}; }),
$globals.HLOpenCommand.a$cls);


$core.addClass("HLOpenBrowserCommand", $globals.HLOpenCommand, "Helios-Commands-Core");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09^ HLBrowser openAsTab",
referencedClasses: ["HLBrowser"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["openAsTab"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.HLBrowser)._openAsTab();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLOpenBrowserCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'b'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "b";

}; }),
$globals.HLOpenBrowserCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Browser'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Browser";

}; }),
$globals.HLOpenBrowserCommand.a$cls);


$core.addClass("HLOpenSUnitCommand", $globals.HLOpenCommand, "Helios-Commands-Core");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09^ HLSUnit openAsTab",
referencedClasses: ["HLSUnit"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["openAsTab"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.HLSUnit)._openAsTab();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLOpenSUnitCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 's'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "s";

}; }),
$globals.HLOpenSUnitCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'SUnit'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "SUnit";

}; }),
$globals.HLOpenSUnitCommand.a$cls);


$core.addClass("HLOpenWorkspaceCommand", $globals.HLOpenCommand, "Helios-Commands-Core");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09^ HLWorkspace openAsTab",
referencedClasses: ["HLWorkspace"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["openAsTab"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.HLWorkspace)._openAsTab();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLOpenWorkspaceCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 'w'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "w";

}; }),
$globals.HLOpenWorkspaceCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Workspace'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Workspace";

}; }),
$globals.HLOpenWorkspaceCommand.a$cls);


$core.addClass("HLSwitchTabCommand", $globals.HLCommand, "Helios-Commands-Core");
$core.addMethod(
$core.method({
selector: "execute",
protocol: "executing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "execute\x0a\x09| activeTab |\x0a\x09\x0a\x09activeTab := self selectedTab.\x0a\x09\x0a\x09^ HLTabSelectionWidget new\x0a\x09\x09tabs: self tabs;\x0a\x09\x09selectedTab: self selectedTab;\x0a\x09\x09selectCallback: [ :tab | tab activate ];\x0a\x09\x09confirmCallback: [ :tab | tab focus ];\x0a\x09\x09cancelCallback: [ activeTab activate ];\x0a\x09\x09show",
referencedClasses: ["HLTabSelectionWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectedTab", "tabs:", "new", "tabs", "selectedTab:", "selectCallback:", "activate", "confirmCallback:", "focus", "cancelCallback:", "show"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var activeTab;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
activeTab=[$self._selectedTab()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["selectedTab"]=1
//>>excludeEnd("ctx");
][0];
$1=$recv($globals.HLTabSelectionWidget)._new();
$recv($1)._tabs_($self._tabs());
$recv($1)._selectedTab_($self._selectedTab());
$recv($1)._selectCallback_((function(tab){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return [$recv(tab)._activate()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx2.sendIdx["activate"]=1
//>>excludeEnd("ctx");
][0];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({tab:tab},$ctx1,1)});
//>>excludeEnd("ctx");
}));
$recv($1)._confirmCallback_((function(tab){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(tab)._focus();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({tab:tab},$ctx1,2)});
//>>excludeEnd("ctx");
}));
$recv($1)._cancelCallback_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(activeTab)._activate();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,3)});
//>>excludeEnd("ctx");
}));
return $recv($1)._show();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"execute",{activeTab:activeTab})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSwitchTabCommand);

$core.addMethod(
$core.method({
selector: "selectedTab",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectedTab\x0a\x09^ HLManager current activeTab",
referencedClasses: ["HLManager"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["activeTab", "current"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($globals.HLManager)._current())._activeTab();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectedTab",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSwitchTabCommand);

$core.addMethod(
$core.method({
selector: "tabs",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabs\x0a\x09^ HLManager current tabs",
referencedClasses: ["HLManager"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["tabs", "current"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($globals.HLManager)._current())._tabs();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"tabs",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLSwitchTabCommand);


$core.addMethod(
$core.method({
selector: "key",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "key\x0a\x09^ 's'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "s";

}; }),
$globals.HLSwitchTabCommand.a$cls);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'Switch tab'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Switch tab";

}; }),
$globals.HLSwitchTabCommand.a$cls);


$core.addClass("HLViewCommand", $globals.HLCommand, "Helios-Commands-Core");

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ 'View'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "View";

}; }),
$globals.HLViewCommand.a$cls);

});
