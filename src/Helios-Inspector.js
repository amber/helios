define(["amber/boot", "require", "helios/Helios-Core"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Helios-Inspector");
$pkg.transport = {"type":"amd","amdNamespace":"helios"};

$core.addClass("HLInspectorDisplayWidget", $globals.HLNavigationListWidget, "Helios-Inspector");
$core.setSlots($globals.HLInspectorDisplayWidget, ["inspector"]);
$core.addMethod(
$core.method({
selector: "inspector",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inspector\x0a\x09^ inspector",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.inspector;

}; }),
$globals.HLInspectorDisplayWidget);

$core.addMethod(
$core.method({
selector: "inspector:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInspector"],
source: "inspector: anInspector\x0a\x09inspector := anInspector",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anInspector){
var self=this,$self=this;
$self.inspector=anInspector;
return self;

}; }),
$globals.HLInspectorDisplayWidget);

$core.addMethod(
$core.method({
selector: "model",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "model\x0a\x0a\x09^ self inspector model",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["model", "inspector"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._inspector())._model();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"model",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorDisplayWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09\x0a    html div with: self selectionDisplayString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "div", "selectionDisplayString"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv(html)._div())._with_($self._selectionDisplayString());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorDisplayWidget);

$core.addMethod(
$core.method({
selector: "selectionDisplayString",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectionDisplayString\x0a\x09|selection|\x0a\x09selection := self model selection.\x0a    ^ (self model instVarObjectAt: selection ifAbsent: [ ^ '' ]) printString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selection", "model", "printString", "instVarObjectAt:ifAbsent:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var selection;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $early={};
try {
selection=$recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._selection();
return $recv($recv($self._model())._instVarObjectAt_ifAbsent_(selection,(function(){
throw $early=[""];

})))._printString();
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectionDisplayString",{selection:selection})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorDisplayWidget);



$core.addClass("HLInspectorModel", $globals.HLModel, "Helios-Inspector");
$core.setSlots($globals.HLInspectorModel, ["inspectee", "code", "variables", "label", "selection"]);
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.HLInspectorModel.comment="I am the model of the Helios inspector `HLInspectorWidget`.\x0a\x0a## API\x0a\x0aUse the method `inspect:on:` to inspect an object on an inspector.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "code",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "code\x0a\x09\x22Answers the code model working for this workspace model\x22\x0a\x09^ code ifNil:[ code := HLCodeModel on: self environment ]",
referencedClasses: ["HLCodeModel"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "on:", "environment"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.code;
if($1 == null || $1.a$nil){
$self.code=$recv($globals.HLCodeModel)._on_($self._environment());
return $self.code;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"code",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorModel);

$core.addMethod(
$core.method({
selector: "inspect:on:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "anInspector"],
source: "inspect: anObject on: anInspector\x0a\x09inspectee := anObject.\x0a\x09variables := #().\x0a\x09inspectee inspectOn: anInspector",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["inspectOn:"]
}, function ($methodClass){ return function (anObject,anInspector){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.inspectee=anObject;
$self.variables=[];
$recv($self.inspectee)._inspectOn_(anInspector);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inspect:on:",{anObject:anObject,anInspector:anInspector})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorModel);

$core.addMethod(
$core.method({
selector: "inspectee",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inspectee \x0a\x09^ inspectee",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.inspectee;

}; }),
$globals.HLInspectorModel);

$core.addMethod(
$core.method({
selector: "inspectee:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "inspectee: anObject \x0a\x09inspectee := anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
$self.inspectee=anObject;
return self;

}; }),
$globals.HLInspectorModel);

$core.addMethod(
$core.method({
selector: "instVarObjectAt:ifAbsent:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInstVarName", "aBlock"],
source: "instVarObjectAt: anInstVarName ifAbsent: aBlock\x0a\x09^ (self variables detect: [ :assoc | assoc key = anInstVarName ] ifNone: aBlock) value",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["value", "detect:ifNone:", "variables", "=", "key"]
}, function ($methodClass){ return function (anInstVarName,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._variables())._detect_ifNone_((function(assoc){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv(assoc)._key()).__eq(anInstVarName);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({assoc:assoc},$ctx1,1)});
//>>excludeEnd("ctx");
}),aBlock))._value();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"instVarObjectAt:ifAbsent:",{anInstVarName:anInstVarName,aBlock:aBlock})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorModel);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a    ^ label ifNil: [ self inspectee printString ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "printString", "inspectee"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.label;
if($1 == null || $1.a$nil){
return $recv($self._inspectee())._printString();
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"label",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorModel);

$core.addMethod(
$core.method({
selector: "label:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "label: aString\x0a    label := aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
$self.label=aString;
return self;

}; }),
$globals.HLInspectorModel);

$core.addMethod(
$core.method({
selector: "selectedInstVar:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInstVarName"],
source: "selectedInstVar: anInstVarName\x0a    self selection: anInstVarName",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selection:"]
}, function ($methodClass){ return function (anInstVarName){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._selection_(anInstVarName);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectedInstVar:",{anInstVarName:anInstVarName})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorModel);

$core.addMethod(
$core.method({
selector: "selectedInstVarObject",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selectedInstVarObject\x0a\x09^ self instVarObjectAt: self selection ifAbsent: [ self error: 'Unable to get variable value.' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["instVarObjectAt:ifAbsent:", "selection", "error:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $self._instVarObjectAt_ifAbsent_($self._selection(),(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._error_("Unable to get variable value.");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectedInstVarObject",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorModel);

$core.addMethod(
$core.method({
selector: "selection",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selection\x0a\x09^ selection ifNil:[ '' ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.selection;
if($1 == null || $1.a$nil){
return "";
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selection",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorModel);

$core.addMethod(
$core.method({
selector: "selection:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "selection: anObject\x0a\x09selection := anObject.\x0a\x0a\x09self announcer announce: (HLInstanceVariableSelected on: selection)",
referencedClasses: ["HLInstanceVariableSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "on:"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.selection=anObject;
$recv($self._announcer())._announce_($recv($globals.HLInstanceVariableSelected)._on_($self.selection));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selection:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorModel);

$core.addMethod(
$core.method({
selector: "subscribe:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aWidget"],
source: "subscribe: aWidget\x0a\x09aWidget subscribeTo: self announcer",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["subscribeTo:", "announcer"]
}, function ($methodClass){ return function (aWidget){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(aWidget)._subscribeTo_($self._announcer());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"subscribe:",{aWidget:aWidget})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorModel);

$core.addMethod(
$core.method({
selector: "variables",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "variables\x0a\x09^ variables ifNil: [ #() ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.variables;
if($1 == null || $1.a$nil){
return [];
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"variables",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorModel);

$core.addMethod(
$core.method({
selector: "variables:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection"],
source: "variables: aCollection\x0a\x09variables := aCollection",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aCollection){
var self=this,$self=this;
$self.variables=aCollection;
return self;

}; }),
$globals.HLInspectorModel);


$core.addMethod(
$core.method({
selector: "on:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anEnvironment"],
source: "on: anEnvironment\x0a\x0a\x09^ self new\x0a    \x09environment: anEnvironment;\x0a        yourself",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["environment:", "new", "yourself"]
}, function ($methodClass){ return function (anEnvironment){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._environment_(anEnvironment);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{anEnvironment:anEnvironment})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorModel.a$cls);


$core.addClass("HLInspectorVariablesWidget", $globals.HLNavigationListWidget, "Helios-Inspector");
$core.setSlots($globals.HLInspectorVariablesWidget, ["announcer", "inspector", "list", "diveButton"]);
$core.addMethod(
$core.method({
selector: "announcer",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "announcer\x0a\x09^ announcer ifNil:[ announcer := Announcer new ]",
referencedClasses: ["Announcer"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.announcer;
if($1 == null || $1.a$nil){
$self.announcer=$recv($globals.Announcer)._new();
return $self.announcer;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"announcer",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorVariablesWidget);

$core.addMethod(
$core.method({
selector: "defaultItems",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "defaultItems\x0a\x09^ self variables",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["variables"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $self._variables();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"defaultItems",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorVariablesWidget);

$core.addMethod(
$core.method({
selector: "dive",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "dive\x0a\x09self announcer announce: HLDiveRequested new",
referencedClasses: ["HLDiveRequested"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["announce:", "announcer", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._announcer())._announce_($recv($globals.HLDiveRequested)._new());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"dive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorVariablesWidget);

$core.addMethod(
$core.method({
selector: "inspector",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inspector\x0a\x09^ inspector",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return $self.inspector;

}; }),
$globals.HLInspectorVariablesWidget);

$core.addMethod(
$core.method({
selector: "inspector:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInspector"],
source: "inspector: anInspector\x0a\x09inspector := anInspector",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (anInspector){
var self=this,$self=this;
$self.inspector=anInspector;
return self;

}; }),
$globals.HLInspectorVariablesWidget);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a\x09^ self model label",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["label", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._label();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"label",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorVariablesWidget);

$core.addMethod(
$core.method({
selector: "model",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "model\x0a    ^ self inspector model",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["model", "inspector"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._inspector())._model();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"model",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorVariablesWidget);

$core.addMethod(
$core.method({
selector: "refresh",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "refresh\x0a\x09self variables = self items ifFalse: [\x0a\x09\x09self resetItems.\x0a    \x09super refresh ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifFalse:", "=", "variables", "items", "resetItems", "refresh"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
if(!$core.assert($recv($self._variables()).__eq($self._items()))){
$self._resetItems();
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._refresh.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"refresh",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorVariablesWidget);

$core.addMethod(
$core.method({
selector: "renderButtonsOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderButtonsOn: html\x0a\x09diveButton := html button \x0a\x09\x09class: 'btn btn-default';\x0a\x09\x09with: 'Dive'; \x0a\x09\x09onClick: [ self dive ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "button", "with:", "onClick:", "dive"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._button();
$recv($1)._class_("btn btn-default");
$recv($1)._with_("Dive");
$self.diveButton=$recv($1)._onClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._dive();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderButtonsOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorVariablesWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a\x09self renderHeadOn: html.\x0a\x09super renderContentOn: html.\x0a\x09self wrapper onDblClick: [ self dive ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["renderHeadOn:", "renderContentOn:", "onDblClick:", "wrapper", "dive"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._renderHeadOn_(html);
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._renderContentOn_.call($self,html))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv($self._wrapper())._onDblClick_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._dive();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorVariablesWidget);

$core.addMethod(
$core.method({
selector: "renderHeadOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderHeadOn: html\x0a\x09html div \x0a\x09\x09class: 'list-label';\x0a\x09\x09with: self label",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["class:", "div", "with:", "label"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(html)._div();
$recv($1)._class_("list-label");
$recv($1)._with_($self._label());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderHeadOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorVariablesWidget);

$core.addMethod(
$core.method({
selector: "resetItems",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "resetItems\x0a\x09items := nil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
$self.items=nil;
return self;

}; }),
$globals.HLInspectorVariablesWidget);

$core.addMethod(
$core.method({
selector: "selectItem:",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "selectItem: anObject\x0a\x09super selectItem: anObject.\x0a    self model selectedInstVar: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selectItem:", "selectedInstVar:", "model"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._selectItem_.call($self,anObject))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv($self._model())._selectedInstVar_(anObject);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selectItem:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorVariablesWidget);

$core.addMethod(
$core.method({
selector: "selection",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "selection\x0a\x09^ self model selection",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selection", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._selection();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"selection",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorVariablesWidget);

$core.addMethod(
$core.method({
selector: "variables",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "variables\x0a\x09^ self model variables collect: #key",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["collect:", "variables", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($recv($self._model())._variables())._collect_("key");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"variables",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorVariablesWidget);



$core.addClass("HLInspectorWidget", $globals.HLWidget, "Helios-Inspector");
$core.setSlots($globals.HLInspectorWidget, ["model", "variablesWidget", "displayWidget", "codeWidget"]);
$core.addMethod(
$core.method({
selector: "codeWidget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "codeWidget\x0a\x09^ codeWidget ifNil: [\x0a\x09\x09codeWidget := self defaultCodeWidget ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "defaultCodeWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.codeWidget;
if($1 == null || $1.a$nil){
$self.codeWidget=$self._defaultCodeWidget();
return $self.codeWidget;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"codeWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "codeWidget:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aWidget"],
source: "codeWidget: aWidget\x0a\x09codeWidget := aWidget",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aWidget){
var self=this,$self=this;
$self.codeWidget=aWidget;
return self;

}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "defaultCodeWidget",
protocol: "defaults",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "defaultCodeWidget\x0a\x09^ HLCodeWidget new\x0a    \x09model: self model code;\x0a       \x09receiver: self model inspectee;\x0a       \x09yourself",
referencedClasses: ["HLCodeWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["model:", "new", "code", "model", "receiver:", "inspectee", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLCodeWidget)._new();
$recv($1)._model_($recv([$self._model()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["model"]=1
//>>excludeEnd("ctx");
][0])._code());
$recv($1)._receiver_($recv($self._model())._inspectee());
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"defaultCodeWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "displayWidget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "displayWidget\x0a\x09^ displayWidget ifNil: [\x0a\x09\x09displayWidget := HLInspectorDisplayWidget new\x0a    \x09\x09inspector: self;\x0a        \x09yourself ]",
referencedClasses: ["HLInspectorDisplayWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "inspector:", "new", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$self.displayWidget;
if($1 == null || $1.a$nil){
$2=$recv($globals.HLInspectorDisplayWidget)._new();
$recv($2)._inspector_(self);
$self.displayWidget=$recv($2)._yourself();
return $self.displayWidget;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"displayWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "initialize",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initialize\x0a\x09super initialize.\x0a\x09self register",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initialize", "register"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._initialize.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$self._register();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initialize",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "inspect:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "inspect: anObject\x0a\x09self model inspect: anObject on: self.\x0a\x09self codeWidget receiver: anObject.\x0a    \x0a\x09self \x0a    \x09refreshVariablesWidget;\x0a\x09\x09refreshDisplayWidget",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["inspect:on:", "model", "receiver:", "codeWidget", "refreshVariablesWidget", "refreshDisplayWidget"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._inspect_on_(anObject,self);
$recv($self._codeWidget())._receiver_(anObject);
$self._refreshVariablesWidget();
$self._refreshDisplayWidget();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inspect:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "inspectee",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inspectee\x0a\x09^ self model inspectee",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["inspectee", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._inspectee();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inspectee",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "inspectee:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "inspectee: anObject\x0a\x09self model inspectee: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["inspectee:", "model"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._inspectee_(anObject);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inspectee:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "label",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "label\x0a    ^ self model label",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["label", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._model())._label();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"label",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "model",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "model\x0a\x09^ model ifNil: [ \x0a    \x09self model: HLInspectorModel new.\x0a\x09\x09model ]",
referencedClasses: ["HLInspectorModel"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "model:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.model;
if($1 == null || $1.a$nil){
$self._model_($recv($globals.HLInspectorModel)._new());
return $self.model;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"model",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "model:",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aModel"],
source: "model: aModel\x0a\x09model := aModel. \x0a    self codeWidget model: aModel code.\x0a    \x0a    self \x0a        observeCodeWidget;\x0a    \x09observeVariablesWidget;\x0a        observeModel",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["model:", "codeWidget", "code", "observeCodeWidget", "observeVariablesWidget", "observeModel"]
}, function ($methodClass){ return function (aModel){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self.model=aModel;
$recv($self._codeWidget())._model_($recv(aModel)._code());
$self._observeCodeWidget();
$self._observeVariablesWidget();
$self._observeModel();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"model:",{aModel:aModel})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "observeCodeWidget",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeCodeWidget\x0a\x09self codeWidget announcer \x0a    \x09on: HLDoItExecuted \x0a        do: [ self onDoneIt ]",
referencedClasses: ["HLDoItExecuted"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:do:", "announcer", "codeWidget", "onDoneIt"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self._codeWidget())._announcer())._on_do_($globals.HLDoItExecuted,(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._onDoneIt();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeCodeWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "observeModel",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeModel\x0a\x09self model announcer\x0a        on: HLInstanceVariableSelected\x0a\x09\x09send: #onInstanceVariableSelected\x0a\x09\x09to: self",
referencedClasses: ["HLInstanceVariableSelected"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "announcer", "model"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self._model())._announcer())._on_send_to_($globals.HLInstanceVariableSelected,"onInstanceVariableSelected",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeModel",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "observeVariablesWidget",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "observeVariablesWidget\x0a\x09self variablesWidget announcer \x0a        on: HLDiveRequested \x0a\x09\x09send: #onDive\x0a\x09\x09to: self",
referencedClasses: ["HLDiveRequested"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["on:send:to:", "announcer", "variablesWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($recv($self._variablesWidget())._announcer())._on_send_to_($globals.HLDiveRequested,"onDive",self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"observeVariablesWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "onDive",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onDive\x0a\x09HLInspector new \x0a\x09\x09inspect: self model selectedInstVarObject;\x0a\x09\x09openAsTab",
referencedClasses: ["HLInspector"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["inspect:", "new", "selectedInstVarObject", "model", "openAsTab"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.HLInspector)._new();
$recv($1)._inspect_($recv($self._model())._selectedInstVarObject());
$recv($1)._openAsTab();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onDive",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "onDoneIt",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onDoneIt\x0a\x09| refreshInterval |\x0a\x09refreshInterval := 'helios.inspector.refreshMillis' settingValueIfAbsent: 500.\x0a\x09refreshInterval < 0 ifFalse: [ self refresh ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["settingValueIfAbsent:", "ifFalse:", "<", "refresh"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var refreshInterval;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
refreshInterval="helios.inspector.refreshMillis"._settingValueIfAbsent_((500));
if(!$core.assert($recv(refreshInterval).__lt((0)))){
$self._refresh();
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onDoneIt",{refreshInterval:refreshInterval})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "onInspectIt",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onInspectIt",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return self;

}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "onInstanceVariableSelected",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onInstanceVariableSelected\x0a\x09self refreshDisplayWidget",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["refreshDisplayWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._refreshDisplayWidget();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"onInstanceVariableSelected",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "onPrintIt",
protocol: "reactions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "onPrintIt",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return self;

}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "refresh",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "refresh\x0a\x09self inspect: self inspectee",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["inspect:", "inspectee"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._inspect_($self._inspectee());
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"refresh",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "refreshDisplayWidget",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "refreshDisplayWidget\x0a\x09self displayWidget refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["refresh", "displayWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._displayWidget())._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"refreshDisplayWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "refreshVariablesWidget",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "refreshVariablesWidget\x0a\x09self variablesWidget refresh",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["refresh", "variablesWidget"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._variablesWidget())._refresh();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"refreshVariablesWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "register",
protocol: "registration",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "register\x0a\x09HLInspector register: self",
referencedClasses: ["HLInspector"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["register:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($globals.HLInspector)._register_(self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"register",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a   \x09html with: (HLHorizontalSplitter\x0a    \x09with: (HLVerticalSplitter \x0a            with: self variablesWidget\x0a            with: self displayWidget)\x0a        with: self codeWidget)",
referencedClasses: ["HLHorizontalSplitter", "HLVerticalSplitter"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "with:with:", "variablesWidget", "displayWidget", "codeWidget"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(html)._with_([$recv($globals.HLHorizontalSplitter)._with_with_($recv($globals.HLVerticalSplitter)._with_with_($self._variablesWidget(),$self._displayWidget()),$self._codeWidget())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:with:"]=1
//>>excludeEnd("ctx");
][0]);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "setLabel:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString"],
source: "setLabel: aString\x0a\x09self model label: aString",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["label:", "model"]
}, function ($methodClass){ return function (aString){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._model())._label_(aString);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setLabel:",{aString:aString})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "setVariables:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection"],
source: "setVariables: aCollection\x0a\x09self model variables: (\x0a\x09\x09(aCollection respondsTo: #associations)\x0a\x09\x09\x09ifTrue: [ aCollection associations ]\x0a\x09\x09\x09ifFalse: [ aCollection ])",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["variables:", "model", "ifTrue:ifFalse:", "respondsTo:", "associations"]
}, function ($methodClass){ return function (aCollection){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$self._model();
if($core.assert($recv(aCollection)._respondsTo_("associations"))){
$2=$recv(aCollection)._associations();
} else {
$2=aCollection;
}
$recv($1)._variables_($2);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"setVariables:",{aCollection:aCollection})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "tabLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabLabel\x0a    ^ 'Inspector'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Inspector";

}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "unregister",
protocol: "registration",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "unregister\x0a\x09super unregister.\x0a\x09HLInspector unregister: self",
referencedClasses: ["HLInspector"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["unregister", "unregister:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._unregister.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv($globals.HLInspector)._unregister_(self);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unregister",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);

$core.addMethod(
$core.method({
selector: "variablesWidget",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "variablesWidget\x0a\x09^ variablesWidget ifNil: [\x0a\x09\x09variablesWidget := HLInspectorVariablesWidget new\x0a    \x09\x09inspector: self;\x0a        \x09yourself ]",
referencedClasses: ["HLInspectorVariablesWidget"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "inspector:", "new", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2;
$1=$self.variablesWidget;
if($1 == null || $1.a$nil){
$2=$recv($globals.HLInspectorVariablesWidget)._new();
$recv($2)._inspector_(self);
$self.variablesWidget=$recv($2)._yourself();
return $self.variablesWidget;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"variablesWidget",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspectorWidget);



$core.addClass("HLInspector", $globals.HLInspectorWidget, "Helios-Inspector");
$core.addMethod(
$core.method({
selector: "inspect:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "inspect: anObject\x0a\x09super inspect: anObject.\x0a\x09[ self setTabLabel: self label ] fork",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["inspect:", "fork", "setTabLabel:", "label"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._inspect_.call($self,anObject))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $self._setTabLabel_($self._label());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._fork();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inspect:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspector);

$core.addMethod(
$core.method({
selector: "renderContentOn:",
protocol: "rendering",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["html"],
source: "renderContentOn: html\x0a   \x09html with: (HLContainer with: (HLHorizontalSplitter\x0a    \x09with: (HLVerticalSplitter \x0a            with: self variablesWidget\x0a            with: self displayWidget)\x0a        with: self codeWidget)).\x0a\x09\x0a\x09self variablesWidget focus",
referencedClasses: ["HLContainer", "HLHorizontalSplitter", "HLVerticalSplitter"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["with:", "with:with:", "variablesWidget", "displayWidget", "codeWidget", "focus"]
}, function ($methodClass){ return function (html){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[$recv(html)._with_($recv($globals.HLContainer)._with_([$recv($globals.HLHorizontalSplitter)._with_with_($recv($globals.HLVerticalSplitter)._with_with_([$self._variablesWidget()
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["variablesWidget"]=1
//>>excludeEnd("ctx");
][0],$self._displayWidget()),$self._codeWidget())
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:with:"]=1
//>>excludeEnd("ctx");
][0]))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["with:"]=1
//>>excludeEnd("ctx");
][0];
$recv($self._variablesWidget())._focus();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderContentOn:",{html:html})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspector);


$core.setSlots($globals.HLInspector.a$cls, ["inspectors"]);
$core.addMethod(
$core.method({
selector: "canBeOpenAsTab",
protocol: "testing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "canBeOpenAsTab\x0a\x09^ false",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return false;

}; }),
$globals.HLInspector.a$cls);

$core.addMethod(
$core.method({
selector: "initialize",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initialize\x0a\x09super initialize.\x0a\x09self watchChanges",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["initialize", "watchChanges"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._initialize.call($self))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
$self._watchChanges();
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initialize",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspector.a$cls);

$core.addMethod(
$core.method({
selector: "inspect:",
protocol: "actions",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "inspect: anObject\x0a\x09self new\x0a\x09\x09openAsTab;\x0a\x09\x09inspect: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["openAsTab", "new", "inspect:"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._new();
$recv($1)._openAsTab();
$recv($1)._inspect_(anObject);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inspect:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspector.a$cls);

$core.addMethod(
$core.method({
selector: "inspectors",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inspectors\x0a\x09^ inspectors ifNil: [ inspectors := OrderedCollection new ]",
referencedClasses: ["OrderedCollection"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "new"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self.inspectors;
if($1 == null || $1.a$nil){
$self.inspectors=$recv($globals.OrderedCollection)._new();
return $self.inspectors;
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inspectors",{})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspector.a$cls);

$core.addMethod(
$core.method({
selector: "register:",
protocol: "registration",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInspector"],
source: "register: anInspector\x0a\x09self inspectors add: anInspector",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["add:", "inspectors"]
}, function ($methodClass){ return function (anInspector){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._inspectors())._add_(anInspector);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"register:",{anInspector:anInspector})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspector.a$cls);

$core.addMethod(
$core.method({
selector: "tabClass",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabClass\x0a\x09^ 'inspector'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "inspector";

}; }),
$globals.HLInspector.a$cls);

$core.addMethod(
$core.method({
selector: "tabLabel",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabLabel\x0a\x09^ 'Inspector'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "Inspector";

}; }),
$globals.HLInspector.a$cls);

$core.addMethod(
$core.method({
selector: "tabPriority",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "tabPriority\x0a\x09^ 10",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return (10);

}; }),
$globals.HLInspector.a$cls);

$core.addMethod(
$core.method({
selector: "unregister:",
protocol: "registration",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anInspector"],
source: "unregister: anInspector\x0a\x09self inspectors remove: anInspector",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["remove:", "inspectors"]
}, function ($methodClass){ return function (anInspector){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self._inspectors())._remove_(anInspector);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"unregister:",{anInspector:anInspector})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspector.a$cls);

$core.addMethod(
$core.method({
selector: "watchChanges",
protocol: "initialization",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "watchChanges\x0a\x09| refreshInterval |\x0a\x09refreshInterval := 'helios.inspector.refreshMillis' settingValueIfAbsent: 500.\x0a\x09refreshInterval > 0 ifTrue: [\x0a\x09\x09[ self inspectors do: [ :each | each refresh ] ]\x0a\x09\x09\x09valueWithInterval: refreshInterval ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["settingValueIfAbsent:", "ifTrue:", ">", "valueWithInterval:", "do:", "inspectors", "refresh"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
var refreshInterval;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
refreshInterval="helios.inspector.refreshMillis"._settingValueIfAbsent_((500));
if($core.assert($recv(refreshInterval).__gt((0)))){
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($self._inspectors())._do_((function(each){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv(each)._refresh();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({each:each},$ctx2,3)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,2)});
//>>excludeEnd("ctx");
}))._valueWithInterval_(refreshInterval);
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"watchChanges",{refreshInterval:refreshInterval})});
//>>excludeEnd("ctx");
}; }),
$globals.HLInspector.a$cls);

});
